export default function addPartialRecordItemToArray<Item, Key extends string>(key: Key, item: Item, record: Partial<Record<Key, Array<Item>>>) {
	const items = record[key]
	if (items) {
		if (!items.includes(item))
			items.push(item)
	} else
		record[key] = [item]
}