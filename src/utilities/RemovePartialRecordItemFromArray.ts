export default function removePartialRecordItemFromArray<Item, Key extends string>(key: Key, item: Item, record: Partial<Record<Key, Array<Item>>>) {
	const items = record[key]
	if (items) {
		const remaining = items.filter(x => item !== x)
		if (remaining.length === 0)
			delete record[key]
		else
			record[key] = remaining
	} else
		throw Error(`'${key}' was not found while attempting to remove '${item}' from it`)
}