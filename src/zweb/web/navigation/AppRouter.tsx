import React from "react"
import Home from "zweb/web/home/Home"
import AncestriesScreen from "zweb/web/catalogs/ancestries/AncestriesScreen"
import GeneralistMagicScreen from "zweb/web/catalogs/magic/GeneralistMagicScreen"
import MagicSourceScreen, {MagicSourceProps} from "zweb/web/catalogs/magic/MagicSourceScreen"
import BestiaryScreen from "zweb/web/catalogs/bestiary/BestiaryScreen"
import CreatureScreen from "zweb/web/catalogs/bestiary/CreatureScreen"
import TalentsScreen from "zweb/web/catalogs/talents/TalentsScreen"
import CharacterScreen from "zweb/web/character_sheet/CharacterScreen"
import CharactersScreen from "zweb/web/character_sheet/CharactersScreen"
import ProfessionsScreen from "zweb/web/catalogs/professions/ProfessionsScreen"
import NotFoundScreen from "zweb/web/app/NotFoundScreen"
import logo from "zweb/web/navigation/ZweihanderLogo.png"
import {Main} from "misevi"
import {Provider} from "zweb/core/Provider"
import * as H from "history"
import talent from "zweb/web/navigation/menu/talent.png"
import dwarf from "zweb/web/navigation/menu/dwarf.png"
import child from "zweb/web/navigation/menu/child.png"
import magic_hat from "zweb/web/navigation/menu/magic-hat.png"
import monster from "zweb/web/navigation/menu/monster.png"
import wand from "zweb/web/navigation/menu/wand.png"
import wicca from "zweb/web/navigation/menu/wicca.png"
import businessman from "zweb/web/navigation/menu/businessman.png"
import spell_book from "zweb/web/navigation/menu/book.png"
import prayer from "zweb/web/navigation/menu/prayer.png"
import {ScreenItem} from "misevi/dist/components/Main"

type Props = { provider: Provider, history: H.History }

export default function AppRouter({provider, history}: Props) {
	return (
		<Main
			history={history}
			provider={provider}
			screens={screens}
			logo={logo}
		/>
	)
}

const screens: Array<ScreenItem> = [
	{path: "", component: Home},
	{path: "characters", component: CharactersScreen, name: "Characters", icon: child},
	{path: "ancestries/:ancestry?/:trait?", component: AncestriesScreen, name: "Ancestries", icon: dwarf},
	{path: "professions/:profession?", component: ProfessionsScreen, name: "Professions", icon: businessman},
	{path: "talents", component: TalentsScreen, name: "Talents", icon: talent}, {
		name: "Magic", icon: wand, items: [
			{
				name: "Generalists",
				icon: magic_hat,
				path: "magic/generalists/:spell?",
				component: GeneralistMagicScreen
			},
			{
				name: "Arcana",
				icon: spell_book,
				path: "magic/arcanas/:school?/:spell?",
				component: (props: MagicSourceProps) => <MagicSourceScreen {...props} source="arcanas"/>
			},
			{
				name: "Prayers",
				icon: prayer,
				path: "magic/prayers/:school?/:spell?",
				component: (props: MagicSourceProps) => <MagicSourceScreen {...props} source="prayers"/>
			},
			{
				name: "Covenants",
				icon: wicca,
				path: "magic/covenants/:school?/:spell?",
				component: (props: MagicSourceProps) => <MagicSourceScreen {...props} source="covenants"/>
			}
		]
	},
	{path: "creatures", component: BestiaryScreen, name: "Creatures", icon: monster},
	{path: "characters/:id", component: CharacterScreen},
	{path: "magic/generalists/:spell?", component: GeneralistMagicScreen},
	{path: "creatures/:type", component: CreatureScreen},
	{path: "professions/:profession?", component: ProfessionsScreen},
	{path: "*", component: NotFoundScreen}
]