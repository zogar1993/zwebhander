import React from "react"
import party from "zweb/web/home/Party.jpg"
import {Flex, Picture} from "misevi"

export default function Home() {
	return (
		<Flex vertical x-align="center">
			<Picture src={party} alt="party"/>
		</Flex>
	)
}
