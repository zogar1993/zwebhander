import {useCallback} from "react"

export default function useCallbackAsync(doAction: () => Promise<void>, dependencies: any[]) {
	return useCallback(() => {
		doAction()
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, dependencies)
}

