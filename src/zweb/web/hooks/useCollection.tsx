import {useEffect, useState} from "react"

export default function useCollection<T>(getCollection: () => Promise<Array<T>>) {
	const [collection, setCollection] = useState<Array<T> | undefined>(undefined)
	useEffect(() => {
		(async () => {
			const collection = await getCollection()
			setCollection(collection)
		})()
	}, [getCollection])
	return collection
}

