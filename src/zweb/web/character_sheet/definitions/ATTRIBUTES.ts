import {AttributeCode} from "zweb/core/domains/character/AttributeCode"

export const ATTRIBUTES: Array<AttributeDefinition> = [
	{
		"name": "Combat",
		"code": "combat"
	},
	{
		"name": "Brawn",
		"code": "brawn"
	},
	{
		"name": "Agility",
		"code": "agility"
	},
	{
		"name": "Perception",
		"code": "perception"
	},
	{
		"name": "Intelligence",
		"code": "intelligence"
	},
	{
		"name": "Willpower",
		"code": "willpower"
	},
	{
		"name": "Fellowship",
		"code": "fellowship"
	}
]

export type AttributeDefinition = {
	name: string, code: AttributeCode
}