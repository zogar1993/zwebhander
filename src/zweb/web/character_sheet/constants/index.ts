import {SEPARATION} from "web_components/constants"

export const BLOCK_WIDTH = "255px"
export const HEADER_HEIGHT = "192px"
export const ATTRIBUTES_HEIGHT = "405px"
export const CONDITION_TRACK_HEIGHT = "139px"
export const DERIVATES_HEIGHT = `calc(${ATTRIBUTES_HEIGHT} - (${CONDITION_TRACK_HEIGHT} * 2) - (${SEPARATION} * 2))`
export const DESKTOP_MAX_WIDTH = `calc((${BLOCK_WIDTH} * 4) + (${SEPARATION} * 3))`
export const DESKTOP_CHARACTER_SHEET_HEIGHT = `calc(${HEADER_HEIGHT} + ${SEPARATION} + ${ATTRIBUTES_HEIGHT})`