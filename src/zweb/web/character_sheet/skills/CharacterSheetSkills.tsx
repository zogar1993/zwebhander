import React from "react"
import CharacterSheetSkill from "zweb/web/character_sheet/skills/CharacterSheetSkill"
import {SkillViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Box, Flex} from "misevi"
import {ATTRIBUTES_HEIGHT, BLOCK_WIDTH} from "zweb/web/character_sheet/constants"
import {SEPARATION} from "web_components/constants"

export default function CharacterSheetSkills({skills}: Props) {
	const [firstHalf, secondHalf] = splitHalves(skills)
	return (
		<Flex area="skills">
		<Box
			width={`calc((${BLOCK_WIDTH} * 2) + ${SEPARATION})`}
			max-width="100%"
			height={ATTRIBUTES_HEIGHT}
			mobile-height={`calc((${ATTRIBUTES_HEIGHT} * 2) + ${SEPARATION})`}
			area="skills"
		>
			<Flex wrap x-align="space-between" height="100%" width="100%">
				<Flex vertical y-align="space-around" height="100%" mobile-height="50%" mobile-width="100%">
					{firstHalf.map(skill => <CharacterSheetSkill key={skill.code} skill={skill}/>)}
				</Flex>
				<Flex vertical y-align="space-around" height="100%" mobile-height="50%" mobile-width="100%">
					{secondHalf.map(skill => <CharacterSheetSkill key={skill.code} skill={skill}/>)}
				</Flex>
			</Flex>
		</Box>
		</Flex>
	)
}

function splitHalves<T>(array: Array<T>) {
	const half = Math.ceil(array.length / 2)
	return [array.slice(0, half), array.slice(half, array.length)]
}

interface Props {
	skills: Array<SkillViewModel>
}
