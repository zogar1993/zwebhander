import React from "react"
import {SkillViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Dots, Flex, Picture, Text} from "misevi"
import emptyStar from "zweb/web/character_sheet/skills/star empty.png"
import filledStar from "zweb/web/character_sheet/skills/star filled.png"
import {Flip} from "zweb/core/actions/character_sheet/GetCharacterSheetOfID"

export default function CharacterSheetSkill({skill}: Props) {
	const specialMark = skill.special ? "*" : ""
	return (
		<Flex
			x-align="space-between"
			y-align="center"
			width="220px"
			mobile-width="100%"
		>
			<Text width="125px" tooltip={skill.special ? "Trained" : undefined}>{skill.name + specialMark}</Text>
			<Dots
				total={3}
				value={skill.ranks}
				onChange={skill.setRanks}
				coloring={({value, number}) => {
					if (skill.profession_ranks >= number && number > value) return "palegreen"
				}}
			/>
			<Text width="14px" handwritten mode={skill.flip === Flip.ToFail ? "negative" : "default"}>{skill.chance}</Text>
			<Picture src={skill.has_focuses ? filledStar : emptyStar} width="16px"/>
		</Flex>
	)
}

interface Props {
	skill: SkillViewModel
}
