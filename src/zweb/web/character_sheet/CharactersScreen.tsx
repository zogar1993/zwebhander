import React, {useEffect, useState} from "react"
import {useHistory} from "react-router-dom"
import {CharacterSheetTag} from "zweb/core/domains/character/CharacterSheetTag"
import {Provider} from "zweb/core/Provider"
import {Button, Flex, FlexItem, Picture, Text} from "misevi"
import {avatarOrDefault} from "zweb/web/character_sheet/helpers"
import {SEPARATION} from "web_components/constants"
import styled from "styled-components"
import bin from "zweb/web/character_sheet/bin.svg"
import pencil from "zweb/web/character_sheet/pencil.svg"

export default function CharactersScreen({provider}: Props) {
	const history = useHistory()
	const [characters, setCharacters] = useState<Array<CharacterSheetTag>>()
	const deleteCharacter = async (id: string) => {
		await provider.deleteCharacterSheet(id)
		const characters = await provider.getCharacterSheetTags()
		setCharacters(characters)
	}
	useEffect(() => {
		(async () => {
			const characters = await provider.getCharacterSheetTags()
			setCharacters(characters)
		})()
	}, [provider])

	const navigateToCharacter = (id: string) => history.push(`/characters/${id}`)
	const createNewCharacter = () => {
		(async () => {
			const id = await provider.createCharacterSheet()
			navigateToCharacter(id)
		})()
	}


	return (
		<Flex vertical gap>
			<Flex width="100%" x-align="right">
				<Button size="large" onClick={createNewCharacter}>Create New Character</Button>
			</Flex>
			<Flex wrap width="100%" gap>
				{characters === undefined ? Array.from({length: 20}, (_, i) => i).map(i =>
						<Card delimited key={i} skeleton/>) :
					characters.map(character =>
						<Card delimited key={character.id}>
							<Flex vertical width="100%" height="100%" gap>
								<Text
									x-align="center"
									width="100%"
									font="Almendra"
									bold
									size="large"
								>
									{character.name || "unnamed"}
								</Text>
								<Flex width="100%">
									<Picture
										src={avatarOrDefault(character.avatar)}
										alt="Avatar"
										width="70px"
										height="70px"
										rounded-borders
									/>
									<FlexItem grow={1}>
										<Flex vertical height="70px" margin-left="8px" y-align="space-evenly">
											<Text>{character.ancestry}</Text>
											<Text>{character.profession1}</Text>
										</Flex>
									</FlexItem>
									<Flex vertical height="70px" margin-left={SEPARATION} y-align="space-evenly">
										<CardImageButton
											src={pencil}
											onClick={() => navigateToCharacter(character.id)}
										/>
										<CardImageButton
											src={bin}
											onClick={() => deleteCharacter(character.id)}
										/>
									</Flex>
								</Flex>
							</Flex>
						</Card>
					)}
			</Flex>
		</Flex>
	)
}

interface Props {
	provider: Provider
}

const Card = styled(Flex)`
  width: calc(100% / 5 - ${SEPARATION});
  height: 120px;
  padding: 10px;
`

const CardImageButton = styled(Picture)`
  width: 24px;
  height: 24px;
`