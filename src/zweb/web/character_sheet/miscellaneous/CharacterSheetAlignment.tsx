import React from "react"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Provider} from "zweb/core/Provider"
import {CircularNumberInput, Dots, Flex, FlexItem} from "misevi"
import Label from "web_components/text/Label"

export default function CharacterSheetAlignment({character}: Props) {
	return (
		<Flex gap x-align="space-between" y-align="stretch" width="100%">
			<Flex vertical>
				<Label small>Order Ranks</Label>
				<Dots value={character.order_ranks} total={9} onChange={(value) => character.setOrderRanks({value})}/>
				<Label small>Chaos Ranks</Label>
				<Dots value={character.chaos_ranks} total={9} onChange={(value) => character.setChaosRanks({value})}/>
			</Flex>
			<FlexItem grow={1}>
				<Flex vertical x-align="center" y-align="center" height="100%">
					<Label>Corruption</Label>
					<CircularNumberInput
						min={0}
						max={9}
						value={character.corruption}
						onBlur={(value) => character.setCorruption({value})}
					/>
				</Flex>
			</FlexItem>
		</Flex>
	)
}

type Props = {
	character: CharacterSheetViewModel
	provider: Provider
}