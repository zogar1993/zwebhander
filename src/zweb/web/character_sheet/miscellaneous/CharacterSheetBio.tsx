import React, {useState} from "react"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Provider} from "zweb/core/Provider"
import {Field, Flex} from "misevi"
import {d10} from "zweb/web/character_sheet/components/d10"
import {upbringings} from "zweb/web/character_sheet/helpers"
import useCollection from "zweb/web/hooks/useCollection"
import {Profession} from "zweb/core/domains/profession/Profession"
import useStateEffect from "zweb/web/hooks/useStateEffect"
import useEffectAsync from "zweb/web/hooks/useEffectAsync"

export default function CharacterSheetBio({character, provider}: Props) {
	const orderAlignments = useCollection(provider.getOrderAlignments)
	const chaosAlignments = useCollection(provider.getChaosAlignments)

	return (
		<Flex area="professions" vertical gap x-align="stretch">
			<Field
				type="combobox"
				label="Sex"
				options={sexes}
				value={character.sex}
				onChange={(value) => character.setSex({value})}
			/>
			<Field
				type="number"
				label="Age"
				value={character.age}
				onBlur={(value) => character.setAge({value})}
				min={0}
			/>
			<CharacterSheetAncestry character={character} provider={provider}/>
			<Field
				type="combobox"
				label="Social Class"
				options={socialClasses}
				value={character.social_class}
				onChange={(value) => character.setSocialClass({value})}
			/>
			<Field
				type="combobox"
				label="Upbringing"
				options={upbringings}
				value={character.upbringing}
				onChange={(value) => character.setUpbringing({value})}
			/>
			<CharacterSheetProfessions character={character} provider={provider}/>
			<Field
				type="combobox"
				label="Order Alignment"
				options={orderAlignments}
				value={character.order_alignment}
				onChange={(value) => character.setOrderAlignment({value})}
			/>
			<Field
				type="combobox"
				label="Chaos Alignment"
				options={chaosAlignments}
				value={character.chaos_alignment}
				onChange={(value) => character.setChaosAlignment({value})}
			/>
		</Flex>
	)
}

type Props = {
	character: CharacterSheetViewModel
	provider: Provider
}

function CharacterSheetAncestry({character, provider}: { character: CharacterSheetViewModel, provider: Provider }) {
	const {ancestry, setAncestry} = character
	const {ancestry_trait, setAncestryTrait} = character
	const ancestries = useCollection(provider.getAllAncestries)
	const ancestryTraits = useStateEffect(async () => {
		if (ancestry === null) return []
		if (ancestry === undefined) return
		return (await provider.getAncestry(ancestry)).traits
	}, [ancestry, provider])

	return (
		<>
			<Field
				type="combobox"
				label="Ancestry"
				options={ancestries}
				value={ancestry}
				onChange={(value) => setAncestry({value})}
			/>
			{ancestry &&
      <Field
          type="combobox"
          label="Ancestry Trait"
          options={ancestryTraits}
          value={ancestry_trait}
          onChange={(value) => setAncestryTrait({value})}
      />
			}
		</>
	)
}

function CharacterSheetProfessions({character, provider}: { character: CharacterSheetViewModel, provider: Provider }) {
	const {archetype, setArchetype} = character
	const {profession1, setProfession1} = character
	const {profession2, setProfession2} = character
	const {profession3, setProfession3} = character
	const professions = useCollection(provider.getAllProfessions)
	const archetypes = useCollection(provider.getAllArchetypes)
	
	const [firstProfessions, setFirstProfessions] = useState<Array<Profession>>([])
	useEffectAsync(async () => {
		if (archetype === undefined) return
		const basics = await provider.getBasicProfessions(archetype)
		setFirstProfessions(basics)
	}, [archetype, provider])

	return (
		<>
			<Field
				type="combobox"
				label="Archetype"
				options={archetypes}
				value={archetype}
				onChange={(value) => setArchetype({value})}
				buttons={[d10]}
				disabled={profession1 !== null}
			/>
			<Field
				type="combobox"
				label="Profession 1"
				options={firstProfessions}
				value={profession1}
				onChange={(value) => setProfession1({value})}
				buttons={archetype ? [d10] : undefined}
				disabled={profession2 !== null}
			/>
			{profession1 &&
				<Field
						type="combobox"
						label="Profession 2"
						options={professions}
						value={profession2}
						onChange={(value) => setProfession2({value})}
						disabled={profession3 !== null}
				/>
			}
			{profession2 &&
				<Field
						type="combobox"
						label="Profession 3"
						options={professions}
						value={profession3}
						onChange={(value) => setProfession3({value})}
				/>
			}
		</>
	)
}

const sexes = [
	{name: "Male", code: "male"},
	{name: "Female", code: "female"},
	{name: "Other", code: "other"}
]
const socialClasses = [
	{name: "Lowborn", code: "lowborn"},
	{name: "Bourgeois", code: "bourgeois"},
	{name: "Aristocrat", code: "aristocrat"}
]