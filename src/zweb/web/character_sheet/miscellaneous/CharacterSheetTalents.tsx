import React from "react"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import Talent from "../../../core/domains/talent/Talent"
import {Field, Flex} from "misevi"
import useCollection from "../../hooks/useCollection"
import {Provider} from "../../../core/Provider"

export default function CharacterSheetTalents({character, provider}: Props) {
	const talents = useCollection(provider.getAllTalents)
	return (
		<Flex gap vertical x-align="stretch" mobile-height="auto">
			{
				Array.from({length: 9}, (_, i) => i).map(i =>
					<TalentComboBox talents={talents} character={character} index={i} key={i.toString()}/>
				)
			}
		</Flex>
	)
}

type Props = {
	character: CharacterSheetViewModel
	provider: Provider
}

function TalentComboBox({
													talents,
													character,
													index
												}: { talents: Array<Talent> | undefined, character: CharacterSheetViewModel, index: number }) {
	return (
		<Field
			type="combobox"
			label={"Talent " + (index + 1)}
			options={talents}
			value={character.talents && character.talents[index]}
			onChange={talent => character.setTalent({talent, index})}
		/>
	)
}
