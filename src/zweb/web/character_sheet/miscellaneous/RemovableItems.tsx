import React from "react"
import {Flex, Tag, Text} from "misevi"


type RemovableItemsProps<Key extends string, Item> = {
	items: Partial<Record<Key, Array<Item>>>
	definitions: Array<{ name: string, code: Key }>
	removeItem: (params: {item: Item, key: Key}) => void
}

export default function RemovableItems <Key extends string, Item>({items, definitions, removeItem}: RemovableItemsProps<Key, Item>) {
	const keys = Object.keys(items) as Array<Key>
	return (
		<>
			{keys.map(code => {
				const skill = definitions.find(x => x.code === code)!
				return (
					<React.Fragment key={code}>
						<Text size="large">{skill.name}</Text>
						<Flex gap padded wrap>
							{(items[code] as Array<Item>).map(focus =>
								<Tag onClick={() => removeItem({item: focus, key: code})}>{focus}</Tag>
							)}
						</Flex>
					</React.Fragment>
				)
			})}
		</>
	)
}