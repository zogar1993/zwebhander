import React from "react"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import BoxedNumber from "../components/BoxedNumber"
import Grid from "web_components/Grid"

export default function CharacterSheetBio({character}: Props) {
	return (
		<Grid columns={3} width="100%">
			<BoxedNumber name="Enc. Limit" value={character.encumbrance_limit}/>
			<BoxedNumber name="Initiative" value={character.initiative}/>
			<BoxedNumber name="Movement" value={character.movement}/>
			<BoxedNumber name="Experience" value={character.spent_experience}/>
			<BoxedNumber name="Max. Focuses" value={character.maximum_focuses}/>
			<BoxedNumber name="Max. Langs." value={character.maximum_languages}/>
		</Grid>
	)
}

type Props = {
	character: CharacterSheetViewModel
}