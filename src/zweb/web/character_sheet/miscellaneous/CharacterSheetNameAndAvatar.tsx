import React from "react"
import {Provider} from "zweb/core/Provider"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Avatar, Field, Flex} from "misevi"

export default function CharacterSheetNameAndAvatar({character}: Props) {
	const {name, setName} = character
	const {avatar, setAvatar} = character

	return (
		<Flex vertical gap area="bio" x-align="center">
			<Field
			  type="text"
				label="Name"
				value={name}
				onBlur={(value) => setName({value})}
				area="name"
				width="100%"
			/>
			<Avatar
				src={avatar}
				alt="Avatar"
				onChange={(base64avatar, base64thumbnail) => setAvatar({base64avatar, base64thumbnail})}
				width={AVATAR_LENGTH}
				height={AVATAR_LENGTH}
				area="avatar"
			/>
		</Flex>
	)
}

type Props = {
	character: CharacterSheetViewModel
	provider: Provider
}

export const AVATAR_LENGTH = `143px`
