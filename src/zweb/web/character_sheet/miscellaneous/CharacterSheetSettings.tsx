import {Field} from "misevi"
import React from "react"
import {SkillOrder} from "zweb/core/domains/character/CharacterSheetData"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"

export default function CharacterSheetExtras({character}: { character: CharacterSheetViewModel }) {
	return (
		<Field
			label="Skill Order"
			type="combobox"
			value={character.settings.skill_order}
			options={[
				{name: "Alphabetic", code: "alphabetic"},
				{name: "By Attribute", code: "by_attribute"},
			] as Array<{name: string, code: SkillOrder}>}
			onChange={(value: SkillOrder | null) => character.setSkillOrder({ value: value as SkillOrder }) }
			unclearable
		/>
	)
}

// TODO fix type on unclearable combobox
