import React, {useState} from "react"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Button, Field, Flex} from "misevi"
import useCollection from "zweb/web/hooks/useCollection"
import {Provider} from "zweb/core/Provider"
import {SchoolCode, SpellCode} from "zweb/core/domains/character/CharacterSpells"
import {MagicSchool} from "zweb/core/domains/magic/MagicSchool"
import RemovableItems from "zweb/web/character_sheet/miscellaneous/RemovableItems"

export default function CharacterSheetSpells({character, provider}: { character: CharacterSheetViewModel, provider: Provider }) {
	const schools = useCollection(provider.getAllMagicSchools)
	const [school, setSchool] = useState<MagicSchool | null>(null)
	const [spell, setSpell] = useState<SpellCode | null>(null)

	return (
		<Flex vertical x-align="stretch" gap>
			<Field
				label="School"
				type="combobox"
				options={schools}
				onChange={(value: SchoolCode | null) => {
					setSpell(null)
					if(value === null) setSchool(null)
					else setSchool(schools!.find(x => x.code === value)!)
				}}
				value={school && school.code}
				unclearable
			/>
			<Field
				label="Spell"
				type="combobox"
				options={school ? school.spells : []}
				onChange={(value: SpellCode | null) => { setSpell(value) }}
				value={spell}
				unclearable
			/>
			<Button
				disabled={spell === null || school === null}
				onClick={() => character.addSpell({spell: spell!, school: school!.code})}
			>
				Add
			</Button>
			{schools &&
				<RemovableItems
					items={character.spells}
					definitions={schools}
					removeItem={({item, key}) => character.removeSpell({spell: item, school: key})}
				/>}
		</Flex>
	)
}

