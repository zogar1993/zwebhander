import React, {useEffect, useState} from "react"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import styled from "styled-components"
import {BORDER_RADIUS} from "web_components/constants"

export default function CharacterSheetExtras({character}: { character: CharacterSheetViewModel }) {
	return (
		<TextArea
			value={character.journal}
			onBlur={(value) => character.setJournal({value})}
		/>
	)
}

function TextArea({value, onBlur}: {value: string, onBlur: (value: string) => void}) {
	const [text, setText] = useState(value)
	useEffect(() => {
		setText(value)
	}, [value])
	return (
		<TextAreaElement
			value={text}
			onChange={(e: any) => { setText(e.target.value) }}
			onBlur={() => onBlur(text)}
		/>
	)
}

const TextAreaElement = styled.textarea<any>`
  box-sizing: border-box;
  border: 1px solid lightgray;
  border-radius: ${BORDER_RADIUS};
  padding: 5px 15px 5px 15px;
	width: 100%;
`
