import {Box, Flex} from "misevi"
import Paragraph from "web_components/text/Paragraph"
import Property from "web_components/text/Property"
import Title from "web_components/text/Title"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"

type Props = {
	character: CharacterSheetViewModel
}

export default function CharacterSheetSpecialRules({character}: Props) {
	return (
		<Flex vertical gap>
			{character.special_rules.map(x =>
				<Box>
					<Title>{x.name}</Title>
					<Paragraph>{x.description}</Paragraph>
					<Property name="Effect">{x.effect}</Property>
				</Box>
			)}
		</Flex>
	)
}
