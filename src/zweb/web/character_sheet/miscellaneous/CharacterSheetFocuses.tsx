import React, {useState} from "react"
import {CharacterSheetViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Button, Field, Flex} from "misevi"
import {SKILLS} from "zweb/web/character_sheet/definitions/SKILLS"
import {SkillCode} from "zweb/core/domains/character/SkillCode"
import RemovableItems from "zweb/web/character_sheet/miscellaneous/RemovableItems"

export default function CharacterSheetFocuses({character}: { character: CharacterSheetViewModel }) {
	const [skill, setSkill] = useState<SkillCode | null>(null)
	const [focus, setFocus] = useState<string>("")

	return (
		<Flex vertical x-align="stretch" gap>
			<Field
				label="Skill"
				type="combobox"
				options={SKILLS}
				onChange={(value: SkillCode | null) => { setSkill(value) }}
				value={skill}
				unclearable
			/>
			<Field
				label="Focus"
				value={focus}
				onBlur={setFocus}
			/>
			<Button
				disabled={focus === "" || skill === null}
				onClick={() => character.addFocus({focus, skill: skill!})}
			>
				Add
			</Button>
			<RemovableItems
				items={character.focuses}
				definitions={SKILLS}
				removeItem={({item, key}) => character.removeFocus({focus: item, skill: key})}
			/>
		</Flex>
	)
}

