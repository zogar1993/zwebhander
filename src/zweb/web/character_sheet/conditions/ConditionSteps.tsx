import ConditionStep from "zweb/web/character_sheet/conditions/ConditionStep"
import React from "react"
import {ConditionTrackViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Flex} from "misevi"

export default function ConditionSteps({conditions, condition}: Props) {
	return (
		<Flex vertical gap>
			{
				conditions.map(x => {
					return (
						<ConditionStep
							value={x.code}
							selected={condition?.value === x.code}
							onChange={condition?.setValue}
							text={x.name}
							key={x.code}
						/>
					)
				})
			}
		</Flex>
	)
}

type Props = {
	conditions: Array<{ name: string, code: number }>
	condition?: ConditionTrackViewModel
}