import React from "react"
import {Flex, Text} from "misevi"
import styled from "styled-components"
import {BORDER_RADIUS} from "web_components/constants"

export default function Threshold({value}: any) {
	return (
		<Flex vertical x-align="center" y-align="center" height="100%" gap>
			<Text size="small">Threshold</Text>
			<Box>
				<Text
					width="30px"
					height="30px"
					x-align="center"
					y-align="center"
					size="large"
					handwritten
					bold
				>
					{value}
				</Text>
			</Box>
		</Flex>
	)
}
const Box = styled(Flex)`
  border: black 1px solid;
  border-radius: ${BORDER_RADIUS};
`
