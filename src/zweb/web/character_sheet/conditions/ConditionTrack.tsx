import {ConditionTrackViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Flex, FlexItem, Text} from "misevi"
import Threshold from "zweb/web/character_sheet/conditions/Threshold"
import ConditionSteps from "zweb/web/character_sheet/conditions/ConditionSteps"
import React from "react"

export default function ConditionTrack({conditions, condition, type}: Props) {
	return (
		<Flex vertical delimited padded gap y-align="space-evenly" width="100%" height="100%">
			<Text size="large" bold>{type} Condition Track</Text>
			<Flex width="100%" padded>
				<ConditionSteps conditions={conditions} condition={condition}/>
				<FlexItem grow={1} height="100%">
					<Threshold value={condition?.threshold}/>
				</FlexItem>
			</Flex>
		</Flex>
	)
}

type Props = {
	conditions: Array<{ name: string, code: number }>
	condition: ConditionTrackViewModel
	type: string
}
