import React from "react"
import {Flex, RadioButton, Text} from "misevi"

export default function ConditionStep({text, onChange, selected, value}: PropsConditionStep) {
	return (
		<Flex width="100%" gap>
			<RadioButton value={value} checked={selected} onChange={onChange && (() => onChange(value))}/>
			<Text size="small">{text}</Text>
		</Flex>
	)
}

interface PropsConditionStep {
	value: number
	text: string
	onChange?: (value: number) => void
	selected: boolean
}