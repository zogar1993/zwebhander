import React, {useEffect, useState} from "react"
import CharacterSheetNameAndAvatar from "zweb/web/character_sheet/miscellaneous/CharacterSheetNameAndAvatar"
import {Provider} from "zweb/core/Provider"
import CharacterSheet from "zweb/core/actions/character_sheet/transfer/CharacterSheet"
import CharacterSheetBio from "zweb/web/character_sheet/miscellaneous/CharacterSheetBio"
import CharacterSheetAlignment from "zweb/web/character_sheet/miscellaneous/CharacterSheetAlignment"
import {DESKTOP_MAX_WIDTH} from "zweb/web/character_sheet/constants"
import {
	CharacterSheetViewModel,
	createCharacterViewModel, simplifyCharacterProvider
} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {SKILLS} from "zweb/web/character_sheet/definitions/SKILLS"
import {ATTRIBUTES} from "zweb/web/character_sheet/definitions/ATTRIBUTES"
import styled from "styled-components"
import {SEPARATION} from "web_components/constants"
import CharacterSheetAttributes from "zweb/web/character_sheet/attributes/CharacterSheetAttributes"
import CharacterSheetSkills from "zweb/web/character_sheet/skills/CharacterSheetSkills"
import ConditionTrack from "zweb/web/character_sheet/conditions/ConditionTrack"
import {damageConditions, perilConditions} from "zweb/web/character_sheet/helpers"
import CharacterSheetExtras from "zweb/web/character_sheet/miscellaneous/CharacterSheetExtras"
import useCallbackAsync from "zweb/web/hooks/useCallbackAsync"
import CharacterSheetFocuses from "zweb/web/character_sheet/miscellaneous/CharacterSheetFocuses"
import CharacterSheetSpells from "zweb/web/character_sheet/miscellaneous/CharacterSheetSpells"
import CharacterSheetSpecialRules from "zweb/web/character_sheet/miscellaneous/CharacterSheetSpecialRules"
import useStateEffect from "../hooks/useStateEffect"
import CharacterSheetStats from "./miscellaneous/CharacterSheetStats"
import CharacterSheetTalents from "./miscellaneous/CharacterSheetTalents"
import Accordion from "web_components/Accordion"
import CharacterSheetSettings from "./miscellaneous/CharacterSheetSettings"

export default function CharacterScreen({id, provider}: Props) {
	const [characterSheet, setCharacter] = useState<CharacterSheet>()
	const refresh = useCallbackAsync(async () => {
		const character = await provider.getCharacterSheetOfID(id)
		setCharacter(character)
	}, [provider, id])

	useEffect(() => {
		refresh()
	}, [refresh])

	const characterProvider = useStateEffect(
		() => simplifyCharacterProvider(provider.character, characterSheet?.id || "", refresh)
		, [provider, characterSheet?.id, refresh])

	const character = characterSheet && characterProvider ?
		createCharacterViewModel(characterSheet, characterProvider) : PLACEHOLDER_CHARACTER_SHEET

	return (
		<Layout>
			<CharacterSheetNameAndAvatar character={character} provider={provider}/>
			<ConditionTrack type="Peril" condition={character.peril} conditions={perilConditions}/>
			<ConditionTrack type="Damage" condition={character.damage} conditions={damageConditions}/>
			<CharacterSheetAttributes attributes={character.attributes}/>
			<CharacterSheetSkills skills={character.skills}/>
			<Accordion items={[
				{
					name: "Bio",
					content: <CharacterSheetBio character={character} provider={provider}/>
				},
				{
					name: "Corruption",
					content: <CharacterSheetAlignment character={character} provider={provider}/>
				},
				{
					name: "Stats",
					content: <CharacterSheetStats character={character}/>
				},
				{
					name: "Talents",
					content: <CharacterSheetTalents character={character} provider={provider}/>
				},
				{
					name: "Focuses",
					content: <CharacterSheetFocuses character={character}/>
				},
				{
					name: "Spells",
					content: <CharacterSheetSpells character={character} provider={provider}/>
				},
				{
					name: "Special Rules",
					content: <CharacterSheetSpecialRules character={character}/>
				},
				{
					name: "Journal",
					content: <CharacterSheetExtras character={character}/>
				},
				{
					name: "Settings",
					content: <CharacterSheetSettings character={character}/>
				}
			]}/>
		</Layout>
	)
}

type Props = {
	id: string
	provider: Provider
}

const PLACEHOLDER_CHARACTER_SHEET = {
	skills: SKILLS as any,
	attributes: ATTRIBUTES as any,
	focuses: {},
	spells: {},
	special_rules: [] as any,
	settings: {},
} as CharacterSheetViewModel

const Layout = styled.div`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  gap: ${SEPARATION};
  width: ${DESKTOP_MAX_WIDTH};
  grid-template-areas: 
    "bio peril-tracker damage-tracker misc"
    "attributes skills skills misc";

  @media (max-width: 768px) {
    grid-template-columns: minmax(0, 1fr);
    width: 100%;
    grid-template-areas: 
    "bio"
		"peril-tracker"
		"damage-tracker"
    "attributes" 
		"skills"
		"misc";
  }
`

//TODO implement reputation and fate points
//TODO allow skills to not be alphabetical

