import React from "react"
import {Flex, Text} from "misevi"

const BoxedNumber = ({name, value}: { name: string, value: number }) => {
	return (
		<Flex vertical x-align="center" y-align="center">
			<Text size="smallest">{name}</Text>
			<Text
				width="40px"
				height="24px"
				x-align="center"
				size="large"
				handwritten
				bold
			>
				{value}
			</Text>
		</Flex>
	)
}
export default BoxedNumber