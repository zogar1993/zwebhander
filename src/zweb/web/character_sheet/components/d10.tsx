import img from "zweb/web/images/d10.png"
import {ComboBoxProps} from "misevi/dist/components/inner_components/ComboBox"

export const d10 = {
	name: "randomize",
	src: img,
	onClick: ({options, onChange}: ComboBoxProps) => {
		const value = Math.ceil(Math.random() * 100)
		// @ts-ignore
		const option = options.find(x => x.from <= value && value <= x.to)!
		onChange!(option.code)
	}
}