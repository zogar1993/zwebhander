import CharacterSheet from "zweb/core/actions/character_sheet/transfer/CharacterSheet"
import Skill from "zweb/core/actions/character_sheet/transfer/Skill"
import Attribute from "zweb/core/actions/character_sheet/transfer/Attribute"
import ConditionTrack from "zweb/core/actions/character_sheet/transfer/ConditionTrack"
import {Provider} from "zweb/core/Provider"

export type CharacterSheetViewModel = {
	attributes: Array<AttributeViewModel>
	skills: Array<SkillViewModel>
	peril: ConditionTrackViewModel
	damage: ConditionTrackViewModel
} & SimplifiedCharacter & CharacterSheet

export interface AttributeViewModel extends Attribute {
	setBase: (value: number) => void
	setAdvances: (value: number) => void
	applyMercy: () => void
	removeMercy: () => void
	randomize: () => void
}

export interface SkillViewModel extends Skill {
	setRanks: (value: number) => void
}

export interface ConditionTrackViewModel extends ConditionTrack {
	setValue: (value: number) => void
}

type CharacterParam = {id: string}
type CharacterFn<P extends CharacterParam> = (p: P) => Promise<void>
type SimplifiedCharacterFn<P extends CharacterParam> = (p: Omit<P, "id">) => Promise<void>
type ProviderCharacter = Provider["character"]
type SimplifiedCharacter = {[X in keyof ProviderCharacter]: SimplifiedCharacterFn<Parameters<ProviderCharacter[X]>[0]>}

function simplify<P extends CharacterParam>(fn: CharacterFn<P>, id: string, onFinish: () => void): SimplifiedCharacterFn<P> {
	return (p: Omit<P, "id">) => fn({...p, id: id} as P).then(onFinish)
}

export function simplifyCharacterProvider(character: Provider["character"], id: string, onFinish: () => void): SimplifiedCharacter {
	const keys = Object.keys(character)
	const result = {}
	keys.forEach(key => {
		//@ts-ignore
		result[key] = simplify(character[key], id, onFinish)
	})
	return result as SimplifiedCharacter
}

export function
createCharacterViewModel(character: CharacterSheet, provider: SimplifiedCharacter) {
	return {
		...character,
		...provider,
		attributes: character.attributes.map(x => ({
			...x,
			setBase: (value: number) => provider.setAttributeBase({attributeCode: x.code, value}),
			setAdvances: (value: number) => provider.setAttributeAdvances({attributeCode: x.code, value}),
			randomize: () => provider.randomizeAttribute({attributeCode: x.code}),
			applyMercy: () => provider.applyMercyToAttribute({attributeCode: x.code}),
			removeMercy: () => provider.removeMercy({})
		})),
		skills: character.skills.map(x => ({
			...x,
			setRanks: (value: number) => provider.setSkillRanks({skillCode: x.code, value})
		})),
		damage: {
			...character.damage,
			setValue: (value: number) => provider.setDamageCondition({value})
		},
		peril: {
			...character.peril,
			setValue: (value: number) => provider.setPerilCondition({value})
		}
	}
}