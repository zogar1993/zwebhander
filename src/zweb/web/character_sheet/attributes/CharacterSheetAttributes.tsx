import CharacterSheetAttribute from "zweb/web/character_sheet/attributes/CharacterSheetAttribute"
import React from "react"
import {AttributeViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {ATTRIBUTES_HEIGHT, BLOCK_WIDTH} from "zweb/web/character_sheet/constants"
import {Box, Flex} from "misevi"

export default function CharacterSheetAttributes({attributes}: Props) {
	return (
		<Flex area="attributes">
			<Box width={BLOCK_WIDTH} height={ATTRIBUTES_HEIGHT} mobile-width="100%" >
				<Flex vertical y-align="space-between" height="100%">
					{attributes.map(x => <CharacterSheetAttribute attribute={x} key={x.code}/>)}
				</Flex>
			</Box>
		</Flex>
	)
}

interface Props {
	attributes: Array<AttributeViewModel>
}