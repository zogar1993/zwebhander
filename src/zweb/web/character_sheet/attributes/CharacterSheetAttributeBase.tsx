import {CircularNumberInput, Flex, Picture} from "misevi"
import React, {useState} from "react"
import styled from "styled-components"
import {AttributeViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import dove from "zweb/web/images/dove.png"
import d10 from "zweb/web/images/d10.png"

export default function CharacterSheetAttributeBase({attribute}: Props) {
	const [seed, setSeed] = useState(0)

	const {base, mercy, mercy_possible} = attribute
	const {setBase, randomize, removeMercy, applyMercy} = attribute

	const loading = base === undefined
	const randomize_enabled = !mercy && !loading
	const mercy_enabled = mercy_possible && !loading

	return (
		<Flex x-align="space-evenly" width="100%">
			<AttributeImageButton
				src={d10}
				onClick={randomize_enabled ? () => {
					randomize()
					setSeed(seed + 1)
				} : null}
				disabled={!randomize_enabled}
			/>
			<CircularNumberInput
				value={base}
				onBlur={setBase}
				min={28}
				max={55}
				disabled={mercy}
				animation-seed={seed}
			/>
			<AttributeImageButton
				src={dove}
				onClick={mercy ? removeMercy : mercy_enabled ? applyMercy : null}
				highlight={mercy}
				disabled={!mercy_enabled}
			/>
		</Flex>
	)
}

interface Props {
	attribute: AttributeViewModel
}

//TODO could be an image button
const AttributeImageButton = styled<any>(Picture)`
  ${({highlight}) => highlight ?
          "filter: drop-shadow(1px 1px 1px dodgerblue) drop-shadow(-1px -1px 1px dodgerblue)" : ""
  };
  width: 28px;
  height: 28px;
`