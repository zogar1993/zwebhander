import React from "react"
import {AttributeViewModel} from "zweb/web/character_sheet/models/CharacterSheetViewModel"
import {Dots, Flex, Text} from "misevi"
import CharacterSheetAttributeBase from "zweb/web/character_sheet/attributes/CharacterSheetAttributeBase"

export default function CharacterSheetAttribute({attribute}: Props) {
	const {name, advances, bonus, ancestry_bonus} = attribute
	const {setAdvances} = attribute
	return (
		<Flex x-align="space-between" y-align="bottom" width="100%">
			<Flex vertical x-align="center" width="106px">
				<Text size="large">{name}</Text>
				<CharacterSheetAttributeBase attribute={attribute}/>
			</Flex>
			<Dots
				total={6}
				value={advances}
				onChange={setAdvances}
				rows={3}
				coloring={({value, number}) => {
					if (attribute.profession_advances >= number && number > value) return "palegreen"
				}}
			/>
			<Flex vertical y-align="center" x-align="center" width="34px" height="auto">
				<Text>bonus</Text>
				<Text
					width="100%"
					height="24px"
					x-align="center"
					mode={mode(ancestry_bonus)}
					handwritten
					size="large"
					bold
				>
					{bonus}
				</Text>
			</Flex>
		</Flex>
	)
}

function mode(value: number) {
	if (value > 0) return "positive"
	if (value < 0) return "negative"
	return "default"
}

interface Props {
	attribute: AttributeViewModel
}
