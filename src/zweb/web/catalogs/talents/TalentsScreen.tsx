import React from "react"
import Paragraph from "web_components/text/Paragraph"
import {Trait} from "zweb/core/domains/Trait"
import {Provider} from "zweb/core/Provider"
import {DataTable} from "misevi"
import useCollection from "zweb/web/hooks/useCollection"

export default function TalentsScreen({provider}: Props) {
	const talents = useCollection(provider.getAllTalents)

	return (
		<DataTable
			data={talents || []}
			identifier={(trait: Trait) => trait.name}
			columns={[
				{
					header: "Name",
					sort: {value: "name", default: "asc"},
					format: (trait: Trait) => <span>{trait.name}</span>
				},
				{
					header: "Description",
					format: (trait: Trait) => <Paragraph italic>{trait.description}</Paragraph>
				},
				{
					header: "Effect",
					format: (trait: Trait) => <Paragraph>{trait.effect}</Paragraph>
				}
			]}
		/>
	)
}

interface Props {
	provider: Provider
}