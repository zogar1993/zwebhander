import React from "react"
import SpellCards from "zweb/web/catalogs/magic/components/SpellCards"
import {Provider} from "zweb/core/Provider"
import Title from "web_components/text/Title"
import {Flex} from "misevi"
import useCollection from "zweb/web/hooks/useCollection"

export default function GeneralistMagicScreen({provider, spell}: Props) {
	const spells = useCollection(provider.getGeneralistSpells)
	return (
		<Flex vertical x-align="center" gap>
			<Title font="Almendra" font-size="34px">Generalist Magic</Title>
			<SpellCards
				spells={spells}
				parentUrl="/magic/generalists"
				selected={spell}
			/>
		</Flex>
	)
}

type Props = {
	provider: Provider
	spell?: string
}