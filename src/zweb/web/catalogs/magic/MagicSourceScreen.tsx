import React from "react"
import {ButtonsGroup, Flex} from "misevi"
import MagicSchoolSpells from "zweb/web/catalogs/magic/components/MagicSchoolSpells"
import {Provider} from "zweb/core/Provider"
import {useHistory} from "react-router-dom"
import Title from "web_components/text/Title"
import useStateEffect from "zweb/web/hooks/useStateEffect"

export default function MagicSourceScreen({provider, source, school, spell}: MagicSourceProps) {
	const history = useHistory()
	const state = useStateEffect(async () => {
			const schools = await provider.getMagicSchoolsOfSource(source)
			return {source, schools}
		}, [provider, source]
	)

	if (state === undefined) return null
	if (state.source !== source) return null
	const {schools} = state

	if (school === undefined) {
		history.push(`/magic/${source}/${schools[0].code}`)
		return null
	}

	return (
		<Flex vertical x-align="center" gap>
			<Title font="Almendra" font-size="34px">{source}</Title>
			<ButtonsGroup
				items={schools}
				selected={school}
				onChange={(code: string) => history.push(`/magic/${source}/${code}`)}
				x-align="center"
				size="large"
				bold
			/>
			<MagicSchoolSpells
				school={school}
				spell={spell}
				parentUrl={`/magic/${source}`}
				provider={provider}
			/>
		</Flex>
	)
}

export type MagicSourceProps = {
	source: string
	school?: string
	spell?: string
	provider: Provider
}

