import React from "react"
import {Spell} from "zweb/core/domains/magic/Spell"
import Paragraph from "web_components/text/Paragraph"
import Title from "web_components/text/Title"
import Section from "web_components/outline/Section"
import {Box, Flex, Tag} from "misevi"

export default function SpellCard({spell, show}: Props) {
	return (
		<Section onClick={() => show(spell)} selectable>
			<Box min-width="200px" height="100%">
				<Title font="Almendra SC">{spell.name}</Title>
				<Flex gap>
					<Tag>{spell.school}</Tag>
					<Tag>{spell.principle}</Tag>
					<Tag>{spell.distance_tag}</Tag>
				</Flex>
				<Paragraph font="Almendra">{spell.description}</Paragraph>
			</Box>
		</Section>
	)
}

type Props = {
	spell: Spell,
	show: (spell: Spell) => void
}