import React, {ReactNode} from "react"
import {Spell} from "zweb/core/domains/magic/Spell"
import SpellCard from "zweb/web/catalogs/magic/components/SpellCard"
import Grid from "web_components/Grid"
import {useHistory} from "react-router-dom"
import {Flex, ItemsModal} from "misevi"
import Paragraph from "web_components/text/Paragraph"
import Property from "web_components/text/Property"

export default function SpellCards({spells, selected, parentUrl}: Props) {
	const history = useHistory()
	const navigate = (path: string = "") => { history.push(`${parentUrl}${path}`) }

	const spell = spells?.find(({code}) => code === selected) || null

	return (
		<ItemsModal
			item={spell}
			items={spells || []}
			navigate={navigate}
			render={SpellModalContent}
		>
			{(show: (spell: Spell) => void) =>
				<Grid columns={3} mobile-columns={1} width="100%">
					{spells ?
						spells.map(spell => <SpellCard spell={spell} show={show} key={spell.code}/>) :
						Array.from({length: 9}, (_, i) => i).map(i => <Flex skeleton key={i} min-height="150px"/>)
					}
				</Grid>
			}
		</ItemsModal>
	)
}

type Props = {
	spells: Array<Spell> | undefined
	selected?: string,
	parentUrl: string
}

function SpellModalContent(item: Spell): ReactNode {
	return (
		<>
			<Paragraph handwritten italic>{item.description}</Paragraph>
			<Property font="Almendra" name="Principle">{item.principle}</Property>
			<Property font="Almendra" name="Distance">{item.distance}</Property>
			<Property font="Almendra" name="Reagents">{item.reagents}</Property>
			<Property font="Almendra" name="Duration">{item.duration}</Property>
			<Property font="Almendra" name="Effect">{item.effect}</Property>
			<Property font="Almendra" name="Critical Success">{item.critical_success}</Property>
			<Property font="Almendra" name="Critical Failure">{item.critical_failure}</Property>
		</>
	)
}