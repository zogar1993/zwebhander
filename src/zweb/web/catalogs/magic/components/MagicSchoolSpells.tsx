import React, {useEffect, useState} from "react"
import SpellCards from "zweb/web/catalogs/magic/components/SpellCards"
import {Spell} from "zweb/core/domains/magic/Spell"
import {Provider} from "zweb/core/Provider"

export default function MagicSchoolSpells({school, spell, parentUrl, provider}: Props) {
	const [spells, setSpells] = useState<Array<Spell>>()
	useEffect(() => {
		(async () => {
			const spells = await provider.getSpellsOfSchool(school)
			const petty = spells.filter(x => x.principle === "Petty")
			const lesser = spells.filter(x => x.principle === "Lesser")
			const greater = spells.filter(x => x.principle === "Greater")
			const sorted = petty.concat(lesser).concat(greater)
			setSpells(sorted)
		})()
	}, [provider, school])

	return (
		<SpellCards
			spells={spells}
			selected={spell}
			parentUrl={`${parentUrl}/${school}`}
		/>
	)
}

type Props = {
	school: string
	spell?: string
	parentUrl: string
	provider: Provider
}