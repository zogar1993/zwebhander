import React from "react"
import Title from "web_components/text/Title"
import Section from "web_components/outline/Section"
import {AncestryTrait} from "zweb/core/domains/ancestry/AncestryTrait"
import {Box} from "misevi"
import styled from "styled-components"

export default function AncestryTraitCard(props: Props) {
	const trait = props.trait
	return (
		<Section aria-label={trait.name}>
			<Box min-width="200px" height="100%" width="100%">
				{/*<span>{`${trait.from}-${trait.to}`}</span>*/}
				<Title font="Almendra" bold>{trait.name}</Title>
				<Description>{trait.description}</Description>
				<PropertyList>
					<Property name="Effect">{trait.effect}</Property>
				</PropertyList>
			</Box>
		</Section>
	)
}

type Props = {
	trait: AncestryTrait
}


function Property({name, children}: PropertyProps) {
	return (
		<>
			<Term>{name}</Term>
			<Definition>{children}</Definition>
		</>
	)
}

type PropertyProps = {
	name: string
	children: string
}

const PropertyList = styled.dl`
  font-family: Arial, Times, serif;
  font-size: 16px;
  color: black;
`

const Term = styled.dt`
  display: inline-block;
  cursor: text;
  font-weight: bold;
  font-style: italic;
  
  :after {
  	content: ": ";
    white-space: pre;
  }
`

const Definition = styled.dd`
  display: inline;
  cursor: text;

  :after{
    display: block;
    content: '';
  }
`

const Description = styled.p`
    font-style: italic;
    color: black;
`