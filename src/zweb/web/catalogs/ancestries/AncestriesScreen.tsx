import React, {useEffect, useRef, useState} from "react"
import {Provider} from "zweb/core/Provider"
import {ButtonsGroup, Flex, Picture, Tabs} from "misevi"
import Paragraph from "web_components/text/Paragraph"
import {useHistory} from "react-router-dom"
import AncestryTraitCard from "zweb/web/catalogs/ancestries/components/AncestryTraitCard"
import useCollection from "zweb/web/hooks/useCollection"
import useStateEffect from "zweb/web/hooks/useStateEffect"
import styled from "styled-components"
import useEffectAsync from "zweb/web/hooks/useEffectAsync"
import {Ancestry} from "zweb/core/domains/ancestry/Ancestry"

export default function AncestriesScreen({provider, ancestry: ancestryCode, trait: traitCode}: Props) {
	const history = useHistory()
	const ancestries = useCollection(provider.getAllAncestries)

	const ancestry = useStateEffect(() => {
		if(ancestryCode === undefined) return
		return provider.getAncestry(ancestryCode)
	}, [ancestryCode, provider])

	const trait = useStateEffect(() => {
		if (ancestry === undefined) return
		const trait = ancestry.traits.find(x => x.code === traitCode)
		if (trait === undefined) return
		return trait
	}, [traitCode, ancestry])

	useEffect(() => {
		if (ancestries === undefined) return
		if (ancestryCode === undefined) history.push(`/ancestries/human`)
		else {
			if (ancestry === undefined) return
			if (traitCode === undefined && ancestry.code === ancestryCode)
				history.push(`/ancestries/${ancestryCode}/${ancestry.traits[0].code}`)
		}
	}, [ancestries, ancestryCode, ancestry, traitCode, history])

	//TODO Add skelletons
	if (ancestries === undefined) return null
	if (ancestry === undefined) return null
	if (ancestryCode === undefined) return null
//TODO make tabs content semantic
	return (
		<Flex vertical x-align="center" gap>
			<Tabs
				items={ancestries}
				selected={ancestry.code}
				onChange={(code: string) => history.push(`/ancestries/${code}`)}
			/>
			<Block>
				<AncestryPicture {...{ancestryCode, ancestry, provider}}/>
				<Paragraph>{ancestry.description}</Paragraph>
			</Block>
			{
				trait &&
        <>
            <ButtonsGroup
                columns={3}
                items={ancestry.traits}
                selected={trait.code}
                onChange={(code: string) => history.push(`/ancestries/${ancestryCode}/${code}`)}
            />
            <AncestryTraitCard trait={trait}/>
        </>
			}
		</Flex>
	)
}

type Props = {
	provider: Provider
	ancestry?: string
	trait?: string
}

const Block = styled.div`
  display: block;
`

function AncestryPicture(
		{ancestryCode, ancestry, provider}:
		{ancestryCode: string, ancestry: Ancestry, provider: Provider}
	) {
	const [image, setImage] = useState<{ imageOf?: string, image?: string }>({})
	// TODO make a cancel instead of all this
	//This is used to fix racing condition when you do the following:
	//1) Are standing on ancestry A
	//2) Click on ancestry B, which has not loaded its image yet
	//3) Click on ancestry A before image for ancestry B finished loading
	//4) Wait for Image of Ancestry B to show, it removes Ancestry A image
	const latestAncestryCodeRef = useRef(ancestryCode)
	latestAncestryCodeRef.current = ancestryCode

	useEffectAsync(async () => {
		const image = await provider.getAncestryImage(ancestryCode)
		if(ancestryCode === latestAncestryCodeRef.current)
			setImage({imageOf: ancestryCode, image: image})
	}, [ancestryCode, provider])

	return (
		<Picture
			float="right"
			bordered
			src={ancestryCode === image.imageOf ? image.image : undefined}
			width="420px"
			height="273px"
			alt={ancestry.name}
		/>
	)
}
