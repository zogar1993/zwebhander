import React from "react"
import {Attributes} from "zweb/core/domains/creature/Creature"
import StrippedTable from "web_components/stripped_table/StrippedTable"
import CreatureAttribute from "zweb/web/catalogs/bestiary/components/CreatureAttribute"

const CreatureAttributes = (props: Props) => {
	const attributes = props.attributes
	return (
		<StrippedTable>
			<CreatureAttribute attribute={{...attributes.combat, name: "Combat"}}/>
			<CreatureAttribute attribute={{...attributes.brawn, name: "Brawn"}}/>
			<CreatureAttribute attribute={{...attributes.agility, name: "Agility"}}/>
			<CreatureAttribute attribute={{...attributes.perception, name: "Perception"}}/>
			<CreatureAttribute attribute={{...attributes.intelligence, name: "Intelligence"}}/>
			<CreatureAttribute attribute={{...attributes.willpower, name: "Willpower"}}/>
			<CreatureAttribute attribute={{...attributes.fellowship, name: "Fellowship"}}/>
		</StrippedTable>
	)
}
export default CreatureAttributes

interface Props {
	attributes: Attributes
}