import React from "react"
import {Attribute} from "zweb/core/domains/creature/Creature"
import StrippedTableRow from "web_components/stripped_table/StrippedTableRow"
import StrippedTableData from "web_components/stripped_table/StrippedTableData"

const CreatureAttribute = ({attribute}: Props) => {
	return (
		<StrippedTableRow header={attribute.name}>
			<StrippedTableData>{attribute.base}%</StrippedTableData>
			<StrippedTableData><strong>{attribute.bonus}</strong></StrippedTableData>
		</StrippedTableRow>
	)
}
export default CreatureAttribute

interface Props {
	attribute: Attribute & {name: string}
}