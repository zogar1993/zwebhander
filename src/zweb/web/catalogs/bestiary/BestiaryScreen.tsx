import React from "react"
import {bestiary} from "zweb/data/Bestiary"
import {Creature} from "zweb/core/domains/creature/Creature"
import {Link} from "react-router-dom"
import {DataTable} from "misevi"

export default function BestiaryScreen() {
//TODO move beasts to mongo
	return (
		<DataTable
			data={bestiary}
			identifier={(creature: Creature) => creature.type}
			columns={[
				{
					header: "Type",
					sort: {value: "name"},
					format: ({type}: Creature) => <Link
						to={`/Creatures/${type.replace(/ /g, "_")}`}>{type}</Link>
				},
				{
					header: "Family",
					sort: {value: "family", default: "asc"},
					format: ({family}: Creature) => <span>{family}</span>
				},
				{
					header: "Notch",
					sort: {value: "notch"},
					format: ({notch}: Creature) => <span>{notch}</span>
				},
				{
					header: "Risk Factor",
					sort: {value: "risk_factor"},
					format: ({risk_factor}: Creature) => <span>{risk_factor}</span>
				}
			]}
		/>
	)
}