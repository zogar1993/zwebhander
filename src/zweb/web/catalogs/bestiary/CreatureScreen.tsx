import React from "react"
import {bestiary} from "zweb/data/Bestiary"
import Title from "web_components/text/Title"
import Paragraphs from "web_components/text/Paragraphs"
import {Creature} from "zweb/core/domains/creature/Creature"
import CreatureAttributes from "zweb/web/catalogs/bestiary/components/CreatureAttributes"
import Property from "web_components/text/Property"
import Paragraph from "web_components/text/Paragraph"
import {Box, Flex, Picture} from "misevi"
import styled from "styled-components"

export default function CreatureScreen(props: Props) {
	const type = props.type.replace("_", " ")
	const creature: Creature | null = bestiary.find(x => x.type === type) || null

	if (creature === null) return null
	const image = require(`zweb/data/bestiary/${creature.type}.jpg`).default
	return (
		<Flex x-align="center" gap vertical>
			<Title font="Mano Negra" bold>{creature.type}</Title>
			<Flex gap>
				<Flex vertical gap>
					<Property name="Notch">{creature.notch}</Property>
					<Property name="Risk Factor">{creature.risk_factor}</Property>
					<Property name="Size">{creature.size.join(" or ")}</Property>
					<CreatureAttributes attributes={creature.attributes}/>
					<Property name="Initiative">{creature.initiative}</Property>
					{
						creature.movement.map(x =>
							<Property name="Movement">{x.value + " (" + x.type + ")"}</Property>)
					}
					<Title>TRAPPINGS</Title>
					{creature.trappings.map(x => <Paragraph>{x}</Paragraph>)}
					{
						creature.attack_profile.map(x =>
							<Box key={x.name}>
								<Property name="Name">{x.name}</Property>
								<Property name="Chance">{x.chance}</Property>
								<Property name="Distance">{x.distance}</Property>
								{x.load ? <Property name="Load">{x.load}</Property> : null}
								<Property name="Damage">{x.damage}</Property>
								{x.qualities.length > 0 ? <Paragraph>{x.qualities.join(" - ")}</Paragraph> : null}
							</Box>
						)
					}
				</Flex>
				<Block>
					<Picture bordered float="right" src={image} alt={creature.type} width="255px" height="255px"/>
					<Paragraphs values={creature.description}/>
				</Block>
				<Flex vertical width="400px" gap>
					{
						creature.traits.map(x =>
							<Box key={x.name}>
								<Title>{x.name}</Title>
								<Paragraph>{x.description}</Paragraph>
							</Box>
						)
					}
				</Flex>
			</Flex>
		</Flex>
	)
}

type Props = {
	type: string
}

const Block = styled.div`
  display: block;
`
