import Paragraph from "web_components/text/Paragraph"
import React from "react"
import {Profession, ProfessionAdvances} from "zweb/core/domains/profession/Profession"
import Property from "web_components/text/Property"
import Section from "web_components/outline/Section"
import {Trait} from "zweb/core/domains/Trait"
import {BLOCK_WIDTH} from "zweb/web/character_sheet/constants"
import {Box, Flex, Text} from "misevi"
import {ATTRIBUTES} from "zweb/web/character_sheet/definitions/ATTRIBUTES"
import {SKILLS} from "zweb/web/character_sheet/definitions/SKILLS"
import Title from "web_components/text/Title"

export default function ProfessionModal(item: Profession) {
	return (
		<Flex gap max-height="80vh">
			<Flex vertical gap>
				{
					!item.prerequisite ? null :
						<Box><Paragraph>{item.prerequisite}</Paragraph></Box>
				}
				<Property font="Almendra" name="From">{item.book}</Property>
				<Paragraph small>{item.description}</Paragraph>
			</Flex>
			<Flex vertical gap>
				{item.traits.map(x => <TraitCard key={x.code} trait={x}/>)}
			</Flex>
			<ProfessionAdvancesGrid {...(item.advances)}/>
		</Flex>
	)
}

function TraitCard({trait}: { trait: Trait }) {
	return (
		<Section max-width={BLOCK_WIDTH}>
			<Box min-width="200px" height="100%" width={BLOCK_WIDTH}>
				<Text size="large" font="Almendra" bold>{trait.name}</Text>
				<Paragraph small italic>{trait.description}</Paragraph>
				<Property small name="Effect">{trait.effect}</Property>
			</Box>
		</Section>
	)
}

function ProfessionAdvancesGrid({bonus_advances, skill_ranks, talents}: ProfessionAdvances) {
	return (
		<Box>
			<Title>Advances</Title>
			<Flex vertical gap>
				<Flex vertical>
					<Text size="large" bold>Attributes</Text>
					{ATTRIBUTES.map(({code, name}) => {
						// @ts-ignore
						const value = bonus_advances.hasOwnProperty(code) ? bonus_advances[code] : 0
						return <Property name={name}>{value}</Property>
					})}
				</Flex>
				<Flex vertical>
					<Text size="large" bold>Skills</Text>
					{skill_ranks.map(code => {
						const skill = SKILLS.find(x => x.code === code)
						if (!skill) throw Error(`Skill '${code}' not found`)
						return <Text>{skill.name}</Text>
					})}
				</Flex>
				<Flex vertical>
					<Text size="large" bold>Talents</Text>
					{talents.map(rank => <Text>{rank}</Text>)}
				</Flex>
			</Flex>
		</Box>
	)
}

