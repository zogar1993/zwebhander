import React from "react"
import {Provider} from "zweb/core/Provider"
import {Profession} from "zweb/core/domains/profession/Profession"
import {Link, useHistory} from "react-router-dom"
import ProfessionModal from "zweb/web/catalogs/professions/components/ProfessionModal"
import {DataTable, ItemsModal, Text} from "misevi"
import useCollection from "zweb/web/hooks/useCollection"

export default function ProfessionsScreen({provider, profession}: Props) {
	const history = useHistory()
	const professions = useCollection(provider.getAllProfessions)

	if (professions === undefined) return null

	const selected = professions.find(x => x.code === profession) || null

	return (
		<ItemsModal
			item={selected}
			items={professions}
			navigate={(path: string = "") => history.push(`/professions${path}`)}
			render={ProfessionModal}
		>
			{() =>
				<DataTable
					data={professions}
					identifier={({code}: Profession) => code}
					columns={[
						{
							header: "Name",
							sort: {value: "name"},
							format: ({code, name}: Profession) => <Link to={`/professions/${code}`}>{name}</Link>
						},
						{
							header: "Type",
							sort: {value: "type"},
							format: ({type}: Profession) => <Text>{type}</Text>
						},
						{
							header: "Book",
							sort: {value: "book"},
							format: ({book}: Profession) => <Text>{book}</Text>
						}
					]}
				/>
			}
		</ItemsModal>
	)
}

type Props = {
	provider: Provider
	profession?: string
}