import React from "react"
import {Flex, Text as Span} from "misevi"
import styled from "styled-components"

export default function NotFoundScreen() {
	return (
		<Background>
			<Flex x-align="center" y-align="center" height="100%">
				<Circle>
					<StatusCode font="Mano Negra" large>404</StatusCode>
					<Text font="Mano Negra" large>Page Not Found</Text>
				</Circle>
			</Flex>
		</Background>
	)
}

const StatusCode = styled<any>(Span)`
    font-size: 600%;  
`

const Text = styled<any>(Span)`
    font-size: 300%;  
`

const Circle = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    align-content: center;
    justify-content: center;
    
    border-radius: 50%;
    background-color: lightgrey;
    
    width: 400px;
    height: 400px;
`

const Background = styled.div`
    background-color: lightgrey;    
    width: 100%;
    height: 100%;
`


