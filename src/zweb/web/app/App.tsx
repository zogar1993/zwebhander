import React, {useEffect, useState} from "react"
import AppRouter from "zweb/web/navigation/AppRouter"
import {newProvider, Provider} from "zweb/core/Provider"
import generateMongoDBConnection from "zweb/core/infrastructure/initialization/initializeMongoDatabase"
import getAncestriesFromMongo from "zweb/core/infrastructure/initialization/getAncestriesFromMongo"
import getTalentsFromMongo from "zweb/core/infrastructure/initialization/getTalentsFromMongo"
import styled from "styled-components"
import getProfessionsFromMongo from "zweb/core/infrastructure/initialization/getProfessionsFromMongoDB"
import CharactersMongoDB from "zweb/core/infrastructure/CharactersMongoDB"
import {createBrowserHistory} from "history"
import getAlignmentsFromMongo from "zweb/core/infrastructure/initialization/getAlignmentsFromMongo"
import getMagicSchoolsFromMongo from "zweb/core/infrastructure/initialization/getMagicSchoolsFromMongo"
import loading from "zweb/web/app/loading.gif"
import ImagesMongoDB from "zweb/core/infrastructure/ImagesMongoDB"

const history = createBrowserHistory()
export default function App() {
	const [provider, setProvider] = useState<Provider | null>(null)

	useEffect(() => {
		(async () => {
			const db = await generateMongoDBConnection()
			const [
				ancestries,
				talents,
				professions,
				alignments,
				schools
			] = await Promise.all([
					getAncestriesFromMongo(db),
					getTalentsFromMongo(db),
					getProfessionsFromMongo(db),
					getAlignmentsFromMongo(db),
					getMagicSchoolsFromMongo(db)
				]
			)
			const characters = new CharactersMongoDB(db)
			const images = new ImagesMongoDB(db)
			const provider = newProvider(
				ancestries,
				talents,
				professions,
				alignments,
				characters,
				schools,
				images
			)
			setProvider(provider)
		})()
	}, [setProvider])
	if (provider === null) return <Loading src={loading}/>
	return <AppRouter provider={provider} history={history}/>
}

const Loading = styled.img`
  position: fixed;
  width: 100vw;
  height: 100vh;
  left: 0;
  top: 0;
`


