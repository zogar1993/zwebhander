import Ancestries from "zweb/core/domains/ancestry/Ancestries"

export default function GetAllAncestries(ancestries: Ancestries) {
	return async (code: string) => {
		return ancestries.getByCode(code)
	}
}