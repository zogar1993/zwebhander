import Ancestries from "zweb/core/domains/ancestry/Ancestries"

export default function GetAllAncestries(ancestries: Ancestries) {
	return async () => {
		return ancestries.getAll()
	}
}