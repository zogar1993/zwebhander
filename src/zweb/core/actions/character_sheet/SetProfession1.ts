import {Characters} from "zweb/core/domains/character/Characters"
import Professions from "zweb/core/domains/profession/Professions"

export default function SetProfession1({characters, professions}: {characters: Characters, professions: Professions}) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.profession1 = value
		if (value && character.archetype === null)
			character.archetype = professions.getArchetypeForProfession(value).code

		characters.patch(character.id, {
			profession1: character.profession1,
			archetype: character.archetype,
		})
		//TODO return promises to be handled in browser
	}
}