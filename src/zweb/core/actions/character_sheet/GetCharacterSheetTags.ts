import {Characters} from "zweb/core/domains/character/Characters"
import {CharacterSheetTag} from "zweb/core/domains/character/CharacterSheetTag"

export default function GetCharacterSheetTags(
	characters: Characters
): () => Promise<Array<CharacterSheetTag>> {
	return async () => {
		return await characters.findAll()
	}
}