import {Characters} from "zweb/core/domains/character/Characters"

export default function SetAncestry(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		if (value === character.ancestry) return
		character.ancestry = value
		character.ancestry_trait = null

		characters.patch(character.id, {ancestry: character.ancestry, ancestry_trait: character.ancestry_trait})
	}
}