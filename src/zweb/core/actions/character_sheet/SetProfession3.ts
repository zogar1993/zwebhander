import {Characters} from "zweb/core/domains/character/Characters"

export default function SetProfession3(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.profession3 = value

		characters.patch(character.id, {profession3: character.profession3})
	}
}