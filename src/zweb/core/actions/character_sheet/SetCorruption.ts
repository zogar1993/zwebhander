import {Characters} from "zweb/core/domains/character/Characters"

export default function SetCorruption(characters: Characters) {
	return async ({id, value}: {id: string, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.corruption = value

		characters.patch(character.id, {corruption: character.corruption})
	}
}