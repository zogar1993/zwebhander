import {Characters} from "zweb/core/domains/character/Characters"

export default function SetAge(characters: Characters) {
	return async ({id, value}: {id: string, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.age = value

		characters.patch(character.id, {age: character.age})
	}
}