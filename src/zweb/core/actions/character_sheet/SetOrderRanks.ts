import {Characters} from "zweb/core/domains/character/Characters"

export default function SetOrderRanks(characters: Characters) {
	return async ({id, value}: {id: string, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.order_ranks = value

		characters.patch(character.id, {order_ranks: character.order_ranks})
	}
}