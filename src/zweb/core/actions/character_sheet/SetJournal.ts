import {Characters} from "zweb/core/domains/character/Characters"

export default function SetJournal(characters: Characters) {
	return async ({id, value}: {id: string, value: string}) => {
		const character = await characters.retrieveByID(id)

		character.journal = value

		characters.patch(character.id, {journal: character.journal})
	}
}