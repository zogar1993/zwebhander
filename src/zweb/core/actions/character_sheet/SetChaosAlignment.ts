import {Characters} from "zweb/core/domains/character/Characters"

export default function SetChaosAlignment(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.chaos_alignment = value

		characters.patch(character.id, {chaos_alignment: character.chaos_alignment})
	}
}