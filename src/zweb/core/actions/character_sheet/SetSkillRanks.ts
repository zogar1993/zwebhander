import {Characters} from "zweb/core/domains/character/Characters"
import {SkillCode} from "zweb/core/domains/character/SkillCode"

export default function SetSkillRanks(characters: Characters) {
	return async ({id, skillCode, value}: {id: string, skillCode: SkillCode, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.skills[skillCode].ranks = value

		characters.patch(character.id, {[`skills.${skillCode}.ranks`]: value})
	}
}