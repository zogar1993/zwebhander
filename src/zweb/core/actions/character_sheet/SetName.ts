import {Characters} from "zweb/core/domains/character/Characters"

export default function SetName(characters: Characters) {
	return async ({id, value}: {id: string, value: string}) => {
		const character = await characters.retrieveByID(id)

		character.name = value

		characters.patch(character.id, {name: character.name})
	}
}