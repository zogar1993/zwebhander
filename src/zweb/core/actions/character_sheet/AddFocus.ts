import {Characters} from "zweb/core/domains/character/Characters"
import {SkillCode} from "zweb/core/domains/character/SkillCode"
import addPartialRecordItemToArray from "utilities/AddPartialRecordItemToArray"

export default function AddFocus(characters: Characters) {
	return async ({id, focus, skill}: {id: string, focus: string, skill: SkillCode}) => {
		const character = await characters.retrieveByID(id)

		addPartialRecordItemToArray(skill, focus, character.focuses)

		characters.patch(character.id, {focuses: character.focuses})
	}
}
