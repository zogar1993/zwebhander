import {Characters} from "zweb/core/domains/character/Characters"

export default function SetArchetype(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		if (value === character.archetype) return
		character.profession1 = null
		character.archetype = value

		characters.patch(character.id, {archetype: value, profession1: null})
	}
}