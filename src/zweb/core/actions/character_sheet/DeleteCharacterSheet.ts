import {Characters} from "zweb/core/domains/character/Characters"

export default function DeleteCharacterSheet(
	characters: Characters
): (id: string) => Promise<void> {
	return async (id: string) => {
		await characters.delete(id)
	}
}