import {Characters} from "zweb/core/domains/character/Characters"

export default function SetPerilCondition(characters: Characters) {
	return async ({id, value}: {id: string, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.peril = value

		characters.patch(character.id, {peril: character.peril})
	}
}