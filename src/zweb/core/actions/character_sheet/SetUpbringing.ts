import {Characters} from "zweb/core/domains/character/Characters"

export default function SetUpbringing(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.upbringing = value

		characters.patch(character.id, {upbringing: character.upbringing})
	}
}