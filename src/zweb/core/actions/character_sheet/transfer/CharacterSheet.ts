import CharacterSheetAttribute from "zweb/core/actions/character_sheet/transfer/Attribute"
import CharacterSheetSkill from "zweb/core/actions/character_sheet/transfer/Skill"
import ConditionTrack from "zweb/core/actions/character_sheet/transfer/ConditionTrack"
import {CharacterSheetSettings} from "zweb/core/domains/character/CharacterSheetData"
import {Focuses} from "zweb/core/domains/character/Focuses"
import {CharacterSpells} from "zweb/core/domains/character/CharacterSpells"
import {SpecialRule} from "zweb/core/domains/SpecialRule"

export default interface CharacterSheet {
	id: string,
	name: string
	age: number
	sex: string | null

	archetype: string | null
	social_class: string | null
	upbringing: string | null
	avatar: string | null
	order_alignment: string | null
	chaos_alignment: string | null
	order_ranks: number
	chaos_ranks: number
	corruption: number

	ancestry: string | null
	profession1: string | null
	profession2: string | null
	profession3: string | null

	encumbrance_limit: number
	initiative: number
	movement: number
	maximum_focuses: number
	maximum_languages: number

	damage: ConditionTrack
	peril: ConditionTrack
	attributes: Array<CharacterSheetAttribute>
	skills: Array<CharacterSheetSkill>
	talents: Array<string | null>
	ancestry_trait: string | null

	journal: string
	spent_experience: number
	focuses: Focuses
	spells: CharacterSpells

	special_rules: Array<SpecialRule>

	settings: CharacterSheetSettings
}

//TODO add new behaviour for mercy