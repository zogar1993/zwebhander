export default interface ConditionTrack {
	value: number,
	threshold: number
}