import {Flip} from "zweb/core/actions/character_sheet/GetCharacterSheetOfID"
import {SkillCode} from "zweb/core/domains/character/SkillCode"
import {AttributeCode} from "../../../domains/character/AttributeCode"

export default interface Skill {
	name: string
	code: SkillCode
	special: boolean
	ranks: number
	profession_ranks: number
	chance: number
	flip: Flip
	has_focuses: boolean
	attribute: AttributeCode
}