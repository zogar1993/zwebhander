import {AttributeCode} from "zweb/core/domains/character/AttributeCode"

export default interface Attribute {
	name: string
	code: AttributeCode
	base: number
	advances: number
	profession_advances: number
	bonus: number
	mercy: boolean
	ancestry_bonus: number

	mercy_possible: boolean
}