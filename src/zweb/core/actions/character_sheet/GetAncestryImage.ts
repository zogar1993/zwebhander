import {Images} from "zweb/core/domains/Images"

export default function GetAncestryImage(images: Images) {
	return async (id: string) => {
		return await images.findAncestry(id)
	}
}