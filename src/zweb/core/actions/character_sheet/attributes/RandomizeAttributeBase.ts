import {Characters} from "zweb/core/domains/character/Characters"
import {AttributeCode} from "zweb/core/domains/character/AttributeCode"

export default function RandomizeAttributeBase(characters: Characters) {
	return async ({id, attributeCode}: {id: string, attributeCode: AttributeCode}) => {
		const character = await characters.retrieveByID(id)

		const r1 = Math.ceil(Math.random() * 10)
		const r2 = Math.ceil(Math.random() * 10)
		const r3 = Math.ceil(Math.random() * 10)
		const base = 25 + r1 + r2 + r3

		character.attributes[attributeCode].base = base

		characters.patch(character.id, {[`attributes.${attributeCode}.base`]: base})
	}
}