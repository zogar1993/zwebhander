import {Characters} from "zweb/core/domains/character/Characters"
import {AttributeCode} from "zweb/core/domains/character/AttributeCode"

export default function SetAttributeBase(characters: Characters) {
	return async ({id, attributeCode, value}: {id: string, attributeCode: AttributeCode, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.attributes[attributeCode].base = value

		characters.patch(character.id, {[`attributes.${attributeCode}.base`]: value})
	}
}