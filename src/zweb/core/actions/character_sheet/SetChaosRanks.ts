import {Characters} from "zweb/core/domains/character/Characters"

export default function SetChaosRanks(characters: Characters) {
	return async ({id, value}: {id: string, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.chaos_ranks = value

		characters.patch(character.id, {chaos_ranks: character.chaos_ranks})
	}
}