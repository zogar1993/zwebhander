import {Characters} from "zweb/core/domains/character/Characters"

export default function SetSocialClass(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.social_class = value

		characters.patch(character.id, {social_class: character.social_class})
	}
}