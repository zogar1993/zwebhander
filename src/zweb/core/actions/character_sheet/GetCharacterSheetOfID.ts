import CharacterSheet from "zweb/core/actions/character_sheet/transfer/CharacterSheet"
import {Characters} from "zweb/core/domains/character/Characters"
import {CharacterSheetData, SkillOrder} from "zweb/core/domains/character/CharacterSheetData"
import Ancestries from "zweb/core/domains/ancestry/Ancestries"
import {Ancestry} from "zweb/core/domains/ancestry/Ancestry"
import Professions from "zweb/core/domains/profession/Professions"
import {Profession} from "zweb/core/domains/profession/Profession"
import Attribute from "zweb/core/actions/character_sheet/transfer/Attribute"
import {AttributeCode} from "zweb/core/domains/character/AttributeCode"
import Talents from "zweb/core/domains/talent/Talents"
import {ATTRIBUTES} from "zweb/web/character_sheet/definitions/ATTRIBUTES"
import {upbringings} from "zweb/web/character_sheet/helpers"
import calculateExperience from "zweb/core/domains/character/CalculateExperience"
import MagicSchools from "zweb/core/domains/magic/MagicSchools"
import Skill from "zweb/core/actions/character_sheet/transfer/Skill"
import {SkillCode} from "zweb/core/domains/character/SkillCode"

type Props = {
	characters: Characters
	ancestries: Ancestries
	professions: Professions
	schools: MagicSchools
	talents: Talents
}

export enum Flip {
	None,
	ToFail,
	ToSucceed
}

export enum Peril {
	Unhindered = 0,
	Imperiled = 1,
	Ignore1SkillRank = 2,
	Ignore2SkillRank = 3,
	Ignore3SkillRank = 4,
	Incapacitated = 5
}

export default function GetCharacterSheetOfID(
	{characters, ancestries, professions, schools, talents}: Props
): (id: string) => Promise<CharacterSheet> {
	return async (id: string) => {
		const character = await characters.retrieveByID(id)
		const ancestry = character.ancestry ? ancestries.getByCode(character.ancestry) : null
		const profs = getProfessions({character, professions})
		const attributes = getAttributes({character, ancestry, professions: profs})
		const getAttribute = (code: AttributeCode) => attributes.find(x => x.code === code)!

		const skills = getSkills({character, getAttribute, professions: profs})

		const special_rules = getSpecialRules({character, talents, professions: profs, ancestry})

		return {
			...character,
			attributes,
			skills: orderSkills(skills, character.settings.skill_order),
			encumbrance_limit: 3 + getAttribute("brawn").bonus,
			initiative: 3 + getAttribute("perception").bonus,
			movement: 3 + getAttribute("agility").bonus,
			spent_experience: spentExperience({character, skills, attributes, schools}),
			damage: {value: character.damage, threshold: getAttribute("brawn").bonus},
			peril: {value: character.peril, threshold: getAttribute("willpower").bonus + 3},
			maximum_focuses: getAttribute("intelligence").bonus,
			maximum_languages: getAttribute("fellowship").bonus,
			special_rules: special_rules
		}
	}
}

type GetAttributeProps = {
	character: CharacterSheetData,
	ancestry: Ancestry | null
	professions: Array<Profession>
}

function getAttributes({character, ancestry, professions}: GetAttributeProps): Array<Attribute> {
	const codes = Object.keys(character.attributes) as Array<AttributeCode>
	return codes.map(code => {
		const {base: raw_base, advances, ...definition} = character.attributes[code]
		const ancestry_bonus = ancestry?.attribute_bonuses[code] || 0
		const mercy = character.mercy === code
		const base = mercy ? 42 : raw_base
		const bonus = Math.floor(base / 10) + advances + ancestry_bonus
		const profession_advances = professions
			.map(x => x.advances.bonus_advances[code] || 0)
			.reduce((x, y) => x + y, 0)

		return {
			...definition,
			code: code,
			base,
			advances,
			bonus,
			ancestry_bonus,
			profession_advances,
			mercy_possible: raw_base < 42,
			mercy: mercy
		}
	})
}

type GetSpecialRulesProps = {
	character: CharacterSheetData
	talents: Talents
	professions: Array<Profession>
	ancestry: Ancestry | null
}

function getSpecialRules({character, talents, professions, ancestry}: GetSpecialRulesProps) {
	const ancestry_trait = ancestry?.traits.find(x => x.code === character.ancestry_trait)
	return [
		...(ancestry_trait ? [ancestry_trait] : []),
		...character.talents.filter(x => x).map(x => talents.getByCode(x!)),
		...professions.flatMap(x => x.traits)
	]
}

type GetProfessionsProps = {
	character: CharacterSheetData,
	professions: Professions
}

function getProfessions({character, professions}: GetProfessionsProps) {
	const profs = []
	if (character.profession1) profs.push(professions.getByCode(character.profession1))
	if (character.profession2) profs.push(professions.getByCode(character.profession2))
	if (character.profession3) profs.push(professions.getByCode(character.profession3))
	return profs
}

type GetSkillProps = {
	character: CharacterSheetData,
	professions: Array<Profession>
	getAttribute: (code: AttributeCode) => Attribute
}

function getSkills({character, professions, getAttribute}: GetSkillProps): Array<Skill> {
	const codes = Object.keys(character.skills) as Array<SkillCode>
	return codes.map(code => {
		const {ranks, ...definition} = character.skills[code]
		const attribute = getAttribute(definition.attribute)
		const is_incapacitated = () => character.peril === Peril.Incapacitated
		const relevant_ranks = () => character.peril >= Peril.Ignore1SkillRank ?
			Math.max(0, ranks - (character.peril - 1)) : ranks
		const profession_ranks = professions
			.map(profession => profession.advances.skill_ranks.includes(code) ? 1 : 0 as number)
			.reduce((x, y) => x + y, 0)

		return {
			...definition,
			code: code,
			ranks,
			chance: is_incapacitated() ? 0 : attribute.base + relevant_ranks() * 10,
			profession_ranks,
			has_focuses: !!character.focuses[code],
			flip: definition.special && ranks === 0 ? Flip.ToFail : Flip.None
		}
	})
}

type SpentExperienceProps = {
	character: CharacterSheetData,
	schools: MagicSchools,
	attributes: Array<Attribute>,
	skills: Array<Skill>
}

function spentExperience({character, schools, attributes, skills}: SpentExperienceProps): number {
	let profession1_talents_amount = 0
	profession1_talents_amount += character.talents[0] === null ? 0 : 1
	profession1_talents_amount += character.talents[1] === null ? 0 : 1
	profession1_talents_amount += character.talents[2] === null ? 0 : 1
	let profession2_talents_amount = 0
	profession2_talents_amount += character.talents[3] === null ? 0 : 1
	profession2_talents_amount += character.talents[4] === null ? 0 : 1
	profession2_talents_amount += character.talents[5] === null ? 0 : 1
	let profession3_talents_amount = 0
	profession3_talents_amount += character.talents[6] === null ? 0 : 1
	profession3_talents_amount += character.talents[7] === null ? 0 : 1
	profession3_talents_amount += character.talents[8] === null ? 0 : 1

	const favored_attribute = upbringings.find(x => x.code === character.upbringing)?.attribute
	const favored_skills = skills.filter(x => x.attribute === favored_attribute).map(x => x.code)

	const spells = {Petty: 0, Lesser: 0, Greater: 0}
	const keys = Object.keys(character.spells)
	for (const schoolCode of keys) {
		const schoolSpells = schools.findSpellsBySchool(schoolCode)
		const spellCodes = character.spells[schoolCode]!
		for (const spellCode of spellCodes) {
			const principle = schoolSpells.find(x => x.code === spellCode)!.principle
			spells[principle]++
		}
	}

	return calculateExperience({
		profession1: character.profession1,
		profession2: character.profession2,
		profession3: character.profession3,
		skills: skills,
		attributes: attributes,
		focuses: character.focuses,
		favored_skills: favored_skills,
		profession1_talents_amount: profession1_talents_amount,
		profession2_talents_amount: profession2_talents_amount,
		profession3_talents_amount: profession3_talents_amount,
		spells
	})
}

function orderSkills(skills: Array<Skill>, order: SkillOrder) {
	if (order === "alphabetic") return skills
	return ATTRIBUTES
		.map(attr => skills.filter(skill => skill.attribute === attr.code))
		.flatMap(x => x)
}