import {Characters} from "zweb/core/domains/character/Characters"
import {SchoolCode, SpellCode} from "zweb/core/domains/character/CharacterSpells"
import addPartialRecordItemToArray from "utilities/AddPartialRecordItemToArray"

export default function AddSpell(characters: Characters) {
	return async ({id, spell, school}: {id: string, spell: SpellCode, school: SchoolCode}) => {
		const character = await characters.retrieveByID(id)

		addPartialRecordItemToArray(school, spell, character.spells)

		characters.patch(character.id, {spells: character.spells})
	}
}