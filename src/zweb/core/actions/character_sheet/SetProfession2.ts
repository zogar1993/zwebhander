import {Characters} from "zweb/core/domains/character/Characters"

export default function SetProfession2(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.profession2 = value

		characters.patch(character.id, {profession2: character.profession2})
	}
}