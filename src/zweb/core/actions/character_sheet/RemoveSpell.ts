import {Characters} from "zweb/core/domains/character/Characters"
import {SchoolCode, SpellCode} from "zweb/core/domains/character/CharacterSpells"
import removePartialRecordItemFromArray from "utilities/RemovePartialRecordItemFromArray"

export default function RemoveSpell(characters: Characters) {
	return async ({id, spell, school}: {id: string, spell: SpellCode, school: SchoolCode}) => {
		const character = await characters.retrieveByID(id)

		removePartialRecordItemFromArray(school, spell, character.spells)

		characters.patch(character.id, {spells: character.spells})
	}
}