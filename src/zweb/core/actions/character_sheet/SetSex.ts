import {Characters} from "zweb/core/domains/character/Characters"

export default function SetSex(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.sex = value

		characters.patch(character.id, {sex: character.sex})
	}
}