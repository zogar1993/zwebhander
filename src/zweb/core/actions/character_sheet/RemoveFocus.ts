import {Characters} from "zweb/core/domains/character/Characters"
import {SkillCode} from "zweb/core/domains/character/SkillCode"
import removePartialRecordItemFromArray from "utilities/RemovePartialRecordItemFromArray"

export default function RemoveFocus(characters: Characters) {
	return async ({id, focus, skill}: {id: string, focus: string, skill: SkillCode}) => {
		const character = await characters.retrieveByID(id)

		removePartialRecordItemFromArray(skill, focus, character.focuses)

		characters.patch(character.id, {focuses: character.focuses})
	}
}