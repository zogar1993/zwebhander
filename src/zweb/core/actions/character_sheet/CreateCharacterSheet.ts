import {Characters} from "zweb/core/domains/character/Characters"

export default function CreateCharacterSheet(characters: Characters) {
	return async () => {
		const character = {name: "", age: 30, ancestry: "human"}
		return await characters.create(character)
	}
}