import {Characters} from "zweb/core/domains/character/Characters"

export default function SetAvatar(characters: Characters) {
	return async ({id, base64avatar, base64thumbnail}: {id: string, base64avatar: string, base64thumbnail: string}) => {
		const character = await characters.retrieveByID(id)

		character.avatar = base64avatar;

		await characters.patch(character.id, {
			avatar: character.avatar,
			thumbnail: base64thumbnail
		})
	}
}