import {Characters} from "zweb/core/domains/character/Characters"
import {SkillOrder} from "zweb/core/domains/character/CharacterSheetData"

export default function SetCharacterSkillOrder(characters: Characters) {
	return async ({id, value}: {id: string, value: SkillOrder}) => {
		const character = await characters.retrieveByID(id)

		character.settings.skill_order = value

		characters.patch(character.id, {'settings.skill_order': character.settings.skill_order})
	}
}