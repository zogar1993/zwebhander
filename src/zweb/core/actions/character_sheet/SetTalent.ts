import {Characters} from "zweb/core/domains/character/Characters"

export default function SetTalent(characters: Characters) {
	return async ({id, talent, index}: {id: string, talent: string | null, index: number}) => {
		const character = await characters.retrieveByID(id)

		character.talents[index] = talent

		characters.patch(character.id, {talents: character.talents})
	}
}