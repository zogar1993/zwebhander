import {Characters} from "zweb/core/domains/character/Characters"

export default function RemoveMercy(characters: Characters) {
	return async ({id}: {id: string}) => {
		const character = await characters.retrieveByID(id)

		character.mercy = null

		characters.patch(character.id, {mercy: null})
	}
}