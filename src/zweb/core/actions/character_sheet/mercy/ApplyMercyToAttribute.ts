import {Characters} from "zweb/core/domains/character/Characters"
import {AttributeCode} from "zweb/core/domains/character/AttributeCode"

export default function ApplyMercyToAttribute(characters: Characters) {
	return async ({id, attributeCode}: {id: string, attributeCode: AttributeCode}) => {
		const character = await characters.retrieveByID(id)

		character.mercy = attributeCode

		characters.patch(character.id, {mercy: attributeCode})
	}
}