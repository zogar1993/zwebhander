import {Characters} from "zweb/core/domains/character/Characters"

export default function SetOrderAlignment(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.order_alignment = value

		characters.patch(character.id, {order_alignment: character.order_alignment})
	}
}