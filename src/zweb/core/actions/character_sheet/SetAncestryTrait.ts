import {Characters} from "zweb/core/domains/character/Characters"

export default function SetAncestryTrait(characters: Characters) {
	return async ({id, value}: {id: string, value: string | null}) => {
		const character = await characters.retrieveByID(id)

		character.ancestry_trait = value

		characters.patch(character.id, {ancestry_trait: character.ancestry_trait})
	}
}