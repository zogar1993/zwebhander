import {Characters} from "zweb/core/domains/character/Characters"

export default function SetDamageCondition(characters: Characters) {
	return async ({id, value}: {id: string, value: number}) => {
		const character = await characters.retrieveByID(id)

		character.damage = value

		characters.patch(character.id, {damage: character.damage})
	}
}