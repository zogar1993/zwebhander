import Alignments from "zweb/core/domains/alignment/Alignments"

export default function GetChaosAlignments(alignments: Alignments) {
	return async () => {
		return alignments.getChaos()
	}
}