import Alignments from "zweb/core/domains/alignment/Alignments"

export default function GetOrderAlignments(alignments: Alignments) {
	return async () => {
		return alignments.getOrder()
	}
}