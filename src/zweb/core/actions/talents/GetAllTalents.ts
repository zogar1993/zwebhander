import Talents from "zweb/core/domains/talent/Talents"

export default function GetAllTalents(talents: Talents) {
	return async () => talents.getAll()
}