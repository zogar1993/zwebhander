import MagicSchools from "zweb/core/domains/magic/MagicSchools"

export default function GetGeneralistSpells(schools: MagicSchools) {
	return async () => {
		return schools.findSpellsBySchool("generalist")
	}
}
