import MagicSchools from "zweb/core/domains/magic/MagicSchools"

export default function GetAllMagicSchools(schools: MagicSchools) {
	return async () => {
		return schools.findAll()
	}
}