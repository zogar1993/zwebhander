import MagicSchools from "zweb/core/domains/magic/MagicSchools"

export default function GetSpellsOfSchool(schools: MagicSchools) {
	return async (school: string) => {
		return schools.findSpellsBySchool(school)
	}
}
