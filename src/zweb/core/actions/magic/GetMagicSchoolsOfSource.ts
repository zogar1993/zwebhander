import MagicSchools from "zweb/core/domains/magic/MagicSchools"

export default function GetMagicSchoolsOfSource(schools: MagicSchools) {
	return async (source: string) => {
		if (source === "arcanas") return schools.findBySource("arcana")
		if (source === "prayers") return schools.findBySource("prayers")
		if (source === "covenants") return schools.findBySource("covenant")
		throw Error(`No source found with type '${source}'`)
	}
}