import Professions from "zweb/core/domains/profession/Professions"

export default function GetAllArchetypes(professions: Professions) {
	return async () => {
		return professions.getArchetypes()
	}
}