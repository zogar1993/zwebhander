import Professions from "zweb/core/domains/profession/Professions"

export default function GetAllProfessions(professions: Professions) {
	return async () => {
		return professions.getAll()
	}
}