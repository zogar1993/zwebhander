import Professions from "zweb/core/domains/profession/Professions"

export default function GetBasicProfessions(professions: Professions) {
	return async (archetype: string | null) => {
		if (archetype) return professions.getBasicsOfArchetype(archetype, "Main Gauche")
		//TODO implement book
		return professions.getBasics("Main Gauche")
	}
}