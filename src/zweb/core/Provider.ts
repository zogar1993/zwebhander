import GetAncestry from "zweb/core/actions/ancestries/GetAncestry"
import GetAllAncestries from "zweb/core/actions/ancestries/GetAllAncestries"
import SetCharacterSkillOrder from "zweb/core/actions/character_sheet/settings/SetCharacterSkillOrder"
import GetMagicSchoolsOfSource from "zweb/core/actions/magic/GetMagicSchoolsOfSource"
import GetSpellsOfSchool from "zweb/core/actions/magic/GetSpellsOfSchool"
import SetName from "zweb/core/actions/character_sheet/SetName"
import SetAge from "zweb/core/actions/character_sheet/SetAge"
import SetSocialClass from "zweb/core/actions/character_sheet/SetSocialClass"
import SetSex from "zweb/core/actions/character_sheet/SetSex"
import SetAncestry from "zweb/core/actions/character_sheet/SetAncestry"
import SetAttributeBase from "zweb/core/actions/character_sheet/attributes/SetAttributeBase"
import SetAttributeAdvances from "zweb/core/actions/character_sheet/attributes/SetAttributeAdvances"
import SetSkillRanks from "zweb/core/actions/character_sheet/SetSkillRanks"
import GetCharacterSheetOfID from "zweb/core/actions/character_sheet/GetCharacterSheetOfID"
import GetCharacterSheetTags from "zweb/core/actions/character_sheet/GetCharacterSheetTags"
import CreateCharacterSheet from "zweb/core/actions/character_sheet/CreateCharacterSheet"
import DeleteCharacterSheet from "zweb/core/actions/character_sheet/DeleteCharacterSheet"
import GetAllTalents from "zweb/core/actions/talents/GetAllTalents"
import RandomizeAttributeBase from "zweb/core/actions/character_sheet/attributes/RandomizeAttributeBase"
import {Characters} from "zweb/core/domains/character/Characters"
import ApplyMercyToAttribute from "zweb/core/actions/character_sheet/mercy/ApplyMercyToAttribute"
import SetArchetype from "zweb/core/actions/character_sheet/SetArchetype"
import Ancestries from "zweb/core/domains/ancestry/Ancestries"
import Talents from "zweb/core/domains/talent/Talents"
import SetDamageCondition from "zweb/core/actions/character_sheet/SetDamageCondition"
import SetPerilCondition from "zweb/core/actions/character_sheet/SetPerilCondition"
import Professions from "zweb/core/domains/profession/Professions"
import GetAllProfessions from "zweb/core/actions/professions/GetAllProfessions"
import SetProfession1 from "zweb/core/actions/character_sheet/SetProfession1"
import SetUpbringing from "zweb/core/actions/character_sheet/SetUpbringing"
import SetAvatar from "zweb/core/actions/character_sheet/SetAvatar"
import Alignments from "zweb/core/domains/alignment/Alignments"
import GetOrderAlignments from "zweb/core/actions/alignments/GetOrderAlignments"
import GetChaosAlignments from "zweb/core/actions/alignments/GetChaosAlignments"
import SetOrderAlignment from "zweb/core/actions/character_sheet/SetOrderAlignment"
import SetChaosAlignment from "zweb/core/actions/character_sheet/SetChaosAlignment"
import SetOrderRanks from "zweb/core/actions/character_sheet/SetOrderRanks"
import SetChaosRanks from "zweb/core/actions/character_sheet/SetChaosRanks"
import SetCorruption from "zweb/core/actions/character_sheet/SetCorruption"
import SetJournal from "zweb/core/actions/character_sheet/SetJournal"
import GetGeneralistSpells from "zweb/core/actions/magic/GetGeneralistSpells"
import MagicSchools from "zweb/core/domains/magic/MagicSchools"
import RemoveMercy from "zweb/core/actions/character_sheet/mercy/RemoveMercy"
import SetProfession2 from "zweb/core/actions/character_sheet/SetProfession2"
import SetProfession3 from "zweb/core/actions/character_sheet/SetProfession3"
import SetTalent from "zweb/core/actions/character_sheet/SetTalent"
import GetAncestryImage from "zweb/core/actions/character_sheet/GetAncestryImage"
import SetAncestryTrait from "zweb/core/actions/character_sheet/SetAncestryTrait"
import AddFocus from "zweb/core/actions/character_sheet/AddFocus"
import RemoveFocus from "zweb/core/actions/character_sheet/RemoveFocus"
import GetAllArchetypes from "zweb/core/actions/professions/GetAllArchetypes"
import GetBasicProfessions from "zweb/core/actions/professions/GetBasicProfessions"
import GetAllMagicSchools from "zweb/core/actions/magic/GellAllMagicSchools"
import AddSpell from "zweb/core/actions/character_sheet/AddSpell"
import RemoveSpell from "zweb/core/actions/character_sheet/RemoveSpell"
import {Images} from "zweb/core/domains/Images"

export const newProvider = (
	ancestries: Ancestries,
	talents: Talents,
	professions: Professions,
	alignments: Alignments,
	characters: Characters,
	schools: MagicSchools,
	images: Images
) => ({
	getAncestry: GetAncestry(ancestries),
	getAllAncestries: GetAllAncestries(ancestries),

	getAllProfessions: GetAllProfessions(professions),
	getBasicProfessions: GetBasicProfessions(professions),
	getAllArchetypes: GetAllArchetypes(professions),

	getAllMagicSchools: GetAllMagicSchools(schools),
	getMagicSchoolsOfSource: GetMagicSchoolsOfSource(schools),
	getSpellsOfSchool: GetSpellsOfSchool(schools),
	getGeneralistSpells: GetGeneralistSpells(schools),

	getAllTalents: GetAllTalents(talents),

	getCharacterSheetOfID: GetCharacterSheetOfID({characters, schools, professions, ancestries, talents}),
	getCharacterSheetTags: GetCharacterSheetTags(characters),
	createCharacterSheet: CreateCharacterSheet(characters),
	deleteCharacterSheet: DeleteCharacterSheet(characters),

	getAncestryImage: GetAncestryImage(images),

	getOrderAlignments: GetOrderAlignments(alignments),
	getChaosAlignments: GetChaosAlignments(alignments),

	character: {
		setTalent: SetTalent(characters),
		addFocus: AddFocus(characters),
		removeFocus: RemoveFocus(characters),
		addSpell: AddSpell(characters),
		removeSpell: RemoveSpell(characters),

		setOrderAlignment: SetOrderAlignment(characters),
		setChaosAlignment: SetChaosAlignment(characters),
		setOrderRanks: SetOrderRanks(characters),
		setChaosRanks: SetChaosRanks(characters),
		setCorruption: SetCorruption(characters),

		setName: SetName(characters),
		setAge: SetAge(characters),
		setSex: SetSex(characters),
		setArchetype: SetArchetype(characters),
		setSocialClass: SetSocialClass(characters),
		setUpbringing: SetUpbringing(characters),
		setAvatar: SetAvatar(characters),

		setAncestry: SetAncestry(characters),
		setAncestryTrait: SetAncestryTrait(characters),
		setProfession1: SetProfession1({characters, professions}),
		setProfession2: SetProfession2(characters),
		setProfession3: SetProfession3(characters),

		setJournal: SetJournal(characters),

		setAttributeBase: SetAttributeBase(characters),
		setAttributeAdvances: SetAttributeAdvances(characters),
		randomizeAttribute: RandomizeAttributeBase(characters),
		applyMercyToAttribute: ApplyMercyToAttribute(characters),
		removeMercy: RemoveMercy(characters),

		setPerilCondition: SetPerilCondition(characters),
		setDamageCondition: SetDamageCondition(characters),

		setSkillRanks: SetSkillRanks(characters),

		setSkillOrder: SetCharacterSkillOrder(characters),
	},
})

export type Provider = ReturnType<typeof newProvider>
