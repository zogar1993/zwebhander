import {AnonymousCredential, RemoteMongoClient, Stitch} from "mongodb-stitch-browser-sdk"
import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"

export default async function initializeMongoDatabase() {
    const client = Stitch.initializeDefaultAppClient("zweihander-wjpnq")
    await client.auth.loginWithCredential(new AnonymousCredential())
    const db = await client.getServiceClient(RemoteMongoClient.factory, "mongodb-atlas").db("ZWEIHANDER")
    return new MongoDBConnection(db)
}