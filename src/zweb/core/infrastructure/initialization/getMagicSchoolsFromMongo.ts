import MagicSchools from "zweb/core/domains/magic/MagicSchools"
import {MagicSchool} from "zweb/core/domains/magic/MagicSchool"
import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"

export default async function getMagicSchoolsFromMongo(db: MongoDBConnection): Promise<MagicSchools> {
    const schools = await db.collection("MAGIC_SCHOOLS").find({}).asArray() as Array<MagicSchool>
    return new MagicSchools(schools)
}