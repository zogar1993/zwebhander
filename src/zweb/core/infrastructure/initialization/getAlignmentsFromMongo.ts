import Alignments from "zweb/core/domains/alignment/Alignments"
import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"
import {Alignment} from "zweb/core/domains/alignment/Alignment"

export default async function getAlignmentsFromMongo(db: MongoDBConnection): Promise<Alignments> {
    const alignments = await db.collection("ALIGNMENTS").find({}).asArray() as Array<Alignment>
    return new Alignments(alignments)
}