import Professions from "zweb/core/domains/profession/Professions"
import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"
import {Profession} from "zweb/core/domains/profession/Profession"
import {Archetype} from "zweb/core/domains/profession/Archetype"

export default async function getProfessionsFromMongoDB(db: MongoDBConnection): Promise<Professions> {
    const [professions, archetypes] = await Promise.all([
        db.collection("PROFESSIONS").find({}).asArray() as Array<Profession>,
        db.collection("ARCHETYPES").find({}).asArray() as Array<Archetype>
    ])
    return new Professions(professions, archetypes)
}