import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"
import Talent from "zweb/core/domains/talent/Talent"
import Talents from "zweb/core/domains/talent/Talents"


export default async function getTalentsFromMongo(db: MongoDBConnection): Promise<Talents> {
    const talents = await db.collection("TALENTS").find({}).asArray() as Array<Talent>
    return new Talents(talents)
}