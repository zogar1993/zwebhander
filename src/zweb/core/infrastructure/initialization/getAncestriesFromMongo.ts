import {Ancestry} from "zweb/core/domains/ancestry/Ancestry"
import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"
import Ancestries from "zweb/core/domains/ancestry/Ancestries"

export default async function getAncestriesFromMongo(db: MongoDBConnection): Promise<Ancestries> {
	const ancestries = await db.collection("ANCESTRIES").find({}, {projection: {image: 0}}).asArray()
	return new Ancestries(ancestries as Array<Ancestry>)
}