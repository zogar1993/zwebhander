
//const authenticated =  client.auth.loginWithCredential(new AnonymousCredential())

export default class MongoDBConnection {
    constructor(private db: any) {}

    collection(name: string) {
        if(this.db === null) throw Error("DB not initialized")
        return this.db.collection(name)
    }
}