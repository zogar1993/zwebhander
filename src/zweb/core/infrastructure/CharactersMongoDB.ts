import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"
import {CharacterSheetTag} from "zweb/core/domains/character/CharacterSheetTag"
import {Characters} from "zweb/core/domains/character/Characters"
import {CharacterSheetData} from "zweb/core/domains/character/CharacterSheetData"
import sanitizeCharacterSheetData from "zweb/core/domains/character/SanitizeCharacterSheetData"

export default class CharactersMongoDB implements Characters {
	constructor(private db: MongoDBConnection) {}

	private cache = new Map<string, CharacterSheetData>()

	async retrieveByID(id: string): Promise<CharacterSheetData> {
		const catched = this.cache.get(id)
		if (catched) return catched

		const characters = await this.db.collection("CHARACTERS")
			.find({_id: {$oid: id}}).asArray()
		if (characters.length === 0) throw Error(`No character of id '${id}' found`)

		const character = sanitizeCharacterSheetData(characters[0]._id.toString(), characters[0])
		this.cache.set(id, character)
		return character
	}

	async findAll(): Promise<Array<CharacterSheetTag>> {
		const characters = await this.db.collection("CHARACTERS")
			.find({}, {projection: {name: 1, ancestry: 1, profession1: 1, thumbnail: 1}})
			.asArray() as Array<any>
		return characters.map(x => ({
			id: x._id,
			name: x.name,
			avatar: x.thumbnail || null,
			ancestry: x.ancestry,
			profession1: (x.profession1 || "").split("_").join(" ")
		}))
	}

	async patch(id: string, part: any) {
		await this.db.collection("CHARACTERS").updateOne({_id: {$oid: id}}, {$set: part})
	}

	async create(character: any): Promise<string> {
		const result = await this.db.collection("CHARACTERS").insertOne(character)
		return result.insertedId
	}

	async delete(id: string): Promise<void> {
		await this.db.collection("CHARACTERS").deleteOne({_id: id})
	}
}
