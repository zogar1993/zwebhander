import MongoDBConnection from "zweb/core/infrastructure/MongoDBConnection"
import {Images} from "zweb/core/domains/Images"

export default class ImagesMongoDB implements Images {
	constructor(
		private db: MongoDBConnection
	) {}

	cache: Map<string, string> = new Map<string, string>()

	async findAncestry(code: string): Promise<string> {

		const catched = this.cache.get(code)
		if (catched) return catched

		const images = await this.db.collection("ANCESTRIES")
			.find({code: code}, {projection: {image: 1}})
			.asArray()

		if (images.length === 0) throw Error("No image found")

		const image = images[0].image
		this.cache.set(code, image)
		return image
	}
}