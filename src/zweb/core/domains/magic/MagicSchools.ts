import {MagicSchool} from "zweb/core/domains/magic/MagicSchool"
import {Spell} from "zweb/core/domains/magic/Spell"

export default class MagicSchools {
	private readonly schools: Array<MagicSchool>

	constructor(schools: Array<MagicSchool>) {
		this.schools = [...schools]
	}

	findBySource(source: string): Array<MagicSchool> {
		return this.schools.filter(x => x.source === source)
	}

	findSpellsBySchool(school: string): Array<Spell> {
		return this.schools.find(x => x.code === school)!.spells
	}

	findAll(): Array<MagicSchool> {
		return this.schools
	}
}