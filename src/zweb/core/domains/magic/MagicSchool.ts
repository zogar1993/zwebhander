import {Spell} from "zweb/core/domains/magic/Spell"

export type MagicSchool = {
	name: string
	code: string
	source: string
	spells: Array<Spell>
}