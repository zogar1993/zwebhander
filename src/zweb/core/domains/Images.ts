export type Images = {
	findAncestry(code: string): Promise<string>
}