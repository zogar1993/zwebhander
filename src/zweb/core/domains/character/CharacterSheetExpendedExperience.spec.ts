import calculateExperience, {CalculateExperienceProps} from "zweb/core/domains/character/CalculateExperience"

describe("Character Sheet expended experience", () => {
	it("0 is the default experience", () => {
		const experience = calculate({})

		expect(experience).toBe(0)
	})

	it("first profession costs 100", () => {
		const experience = calculate({profession1: A_PROFESSION})

		expect(experience).toBe(100)
	})

	it("second profession costs 200", () => {
		const experience = calculate({profession2: A_PROFESSION})

		expect(experience).toBe(200)
	})

	it("third profession costs 300", () => {
		const experience = calculate({profession3: A_PROFESSION})

		expect(experience).toBe(300)
	})

	it("first skill rank costs 100 when having 1 profession rank", () => {
		const experience = calculate({skills: [{ranks: 1, profession_ranks: 1}]})

		expect(experience).toBe(100)
	})

	it("first skill rank costs 100 when having more than 1 profession ranks", () => {
		const experience = calculate({skills: [{ranks: 1, profession_ranks: 2}]})

		expect(experience).toBe(100)
	})

	it("second skill rank costs 200 when having 2 profession rank", () => {
		const experience = calculate({skills: [{ranks: 2, profession_ranks: 2}]})

		expect(experience - COST_OF_1_SKILL_DOT).toBe(200)
	})

	it("second skill rank costs 200 when having more than 2 profession ranks", () => {
		const experience = calculate({skills: [{ranks: 2, profession_ranks: 3}]})

		expect(experience - COST_OF_1_SKILL_DOT).toBe(200)
	})

	it("third skill rank costs 300 when having 3 profession ranks", () => {
		const experience = calculate({skills: [{ranks: 3, profession_ranks: 3}]})

		expect(experience - COST_OF_2_SKILL_DOTS).toBe(300)
	})

	it("first skill rank costs 200 when having less than 1 profession ranks", () => {
		const experience = calculate({skills: [{ranks: 1, profession_ranks: 0}]})

		expect(experience).toBe(200)
	})

	it("second skill rank costs 400 when having less than 2 profession ranks", () => {
		const experience = calculate({skills: [{ranks: 2, profession_ranks: 1}]})

		expect(experience - COST_OF_1_SKILL_DOT).toBe(400)
	})

	it("third skill rank costs 600 when having less than 3 profession ranks", () => {
		const experience = calculate({skills: [{ranks: 3, profession_ranks: 2}]})

		expect(experience - COST_OF_2_SKILL_DOTS).toBe(600)
	})

	it("first attribute advance costs 100 when having 1 profession advance", () => {
		const experience = calculate({attributes: [{advances: 1, profession_advances: 1}]})

		expect(experience).toBe(100)
	})

	it("first attribute advance costs 100 when having more than 1 profession advances", () => {
		const experience = calculate({attributes: [{advances: 1, profession_advances: 2}]})

		expect(experience).toBe(100)
	})

	it("first attribute advance costs 200 when having less than 1 profession advances", () => {
		const experience = calculate({attributes: [{advances: 1, profession_advances: 0}]})

		expect(experience).toBe(200)
	})

	it("second attribute advance costs 100 when having 2 profession advances", () => {
		const experience = calculate({attributes: [{advances: 2, profession_advances: 2}]})

		expect(experience - COST_OF_1_ATTRIBUTE_DOT).toBe(100)
	})

	it("second attribute advance costs 100 when having more than 2 profession advances", () => {
		const experience = calculate({attributes: [{advances: 2, profession_advances: 3}]})

		expect(experience - COST_OF_1_ATTRIBUTE_DOT).toBe(100)
	})

	it("second attribute advance costs 200 when having less than 2 profession advances", () => {
		const experience = calculate({attributes: [{advances: 2, profession_advances: 1}]})

		expect(experience - COST_OF_1_ATTRIBUTE_DOT).toBe(200)
	})

	it("third attribute advance costs 200 when having 3 profession advances", () => {
		const experience = calculate({attributes: [{advances: 3, profession_advances: 3}]})

		expect(experience - COST_OF_2_ATTRIBUTE_DOTS).toBe(200)
	})

	it("third attribute advance costs 200 when having more than 3 profession advances", () => {
		const experience = calculate({attributes: [{advances: 3, profession_advances: 4}]})

		expect(experience - COST_OF_2_ATTRIBUTE_DOTS).toBe(200)
	})

	it("third attribute advance costs 400 when having less than 3 profession advances", () => {
		const experience = calculate({attributes: [{advances: 3, profession_advances: 2}]})

		expect(experience - COST_OF_2_ATTRIBUTE_DOTS).toBe(400)
	})

	it("fourth attribute advance costs 200 when having 4 profession advances", () => {
		const experience = calculate({attributes: [{advances: 4, profession_advances: 4}]})

		expect(experience - COST_OF_3_ATTRIBUTE_DOTS).toBe(200)
	})

	it("fourth attribute advance costs 200 when having more than 4 profession advances", () => {
		const experience = calculate({attributes: [{advances: 4, profession_advances: 5}]})

		expect(experience - COST_OF_3_ATTRIBUTE_DOTS).toBe(200)
	})

	it("fourth attribute advance costs 400 when having less than 4 profession advances", () => {
		const experience = calculate({attributes: [{advances: 4, profession_advances: 3}]})

		expect(experience - COST_OF_3_ATTRIBUTE_DOTS).toBe(400)
	})

	it("fifth attribute advance costs 300 when having 5 profession advances", () => {
		const experience = calculate({attributes: [{advances: 5, profession_advances: 5}]})

		expect(experience - COST_OF_4_ATTRIBUTE_DOTS).toBe(300)
	})

	it("fifth attribute advance costs 300 when having more than 5 profession advances", () => {
		const experience = calculate({attributes: [{advances: 5, profession_advances: 6}]})

		expect(experience - COST_OF_4_ATTRIBUTE_DOTS).toBe(300)
	})

	it("fifth attribute advance costs 600 when having less than 5 profession advances", () => {
		const experience = calculate({attributes: [{advances: 5, profession_advances: 4}]})

		expect(experience - COST_OF_4_ATTRIBUTE_DOTS).toBe(600)
	})

	it("sixth attribute advance costs 300 when having 6 profession advances", () => {
		const experience = calculate({attributes: [{advances: 6, profession_advances: 6}]})

		expect(experience - COST_OF_5_ATTRIBUTE_DOTS).toBe(300)
	})

	it("sixth attribute advance costs 600 when having less than 6 profession advances", () => {
		const experience = calculate({attributes: [{advances: 6, profession_advances: 5}]})

		expect(experience - COST_OF_5_ATTRIBUTE_DOTS).toBe(600)
	})

	it("one talent from profession 1 costs 100", () => {
		const experience = calculate({profession1_talents_amount: 1})

		expect(experience).toBe(100)
	})

	it("one talent from profession 2 costs 200", () => {
		const experience = calculate({profession2_talents_amount: 1})

		expect(experience).toBe(200)
	})

	it("one talent from profession 3 costs 300", () => {
		const experience = calculate({profession3_talents_amount: 1})

		expect(experience).toBe(300)
	})

	it("one focus from a skill costs 100", () => {
		const experience = calculate({focuses: {simple_melee: [A_FOCUS]}})

		expect(experience).toBe(100)
	})

	it("two focuses from a skill cost 200", () => {
		const experience = calculate({focuses: {simple_melee: [A_FOCUS, ANOTHER_FOCUS]}})

		expect(experience).toBe(200)
	})

	it("two focuses from two different skills cost 200", () => {
		const experience = calculate({focuses: {simple_melee: [A_FOCUS], alchemy: [ANOTHER_FOCUS]}})

		expect(experience).toBe(200)
	})

	it("one focus from a favored skill costs 50", () => {
		const experience = calculate({focuses: {simple_melee: [A_FOCUS]}, favored_skills: ["simple_melee"]})

		expect(experience).toBe(50)
	})

	it("one petty spell is free", () => {
		const experience = calculate({spells: {Petty: 1}})

		expect(experience).toBe(0)
	})

	it("two petty spells are free", () => {
		const experience = calculate({spells: {Petty: 2}})

		expect(experience).toBe(0)
	})

	it("three petty spells are free", () => {
		const experience = calculate({spells: {Petty: 3}})

		expect(experience).toBe(0)
	})

	it("the fourth petty spell costs 100", () => {
		const experience = calculate({spells: {Petty: 4}})

		expect(experience).toBe(100)
	})

	it("the two petty spells cost 200 (first 3 are free)", () => {
		const experience = calculate({spells: {Petty: 5}})

		expect(experience).toBe(200)
	})

	it("one lesser spell costs 200", () => {
		const experience = calculate({spells: {Lesser: 1}})

		expect(experience).toBe(200)
	})

	it("one greater spell costs 300", () => {
		const experience = calculate({spells: {Greater: 1}})

		expect(experience).toBe(300)
	})

	it("different principle spells add up", () => {
		const experience = calculate({spells: {Lesser: 1, Greater: 1}})

		expect(experience).toBe(500)
	})
})

function calculate({
										 profession1 = null,
										 profession2 = null,
										 profession3 = null,
										 skills = [],
										 attributes = [],
										 profession1_talents_amount = 0,
										 profession2_talents_amount = 0,
										 profession3_talents_amount = 0,
										 focuses = {},
										 spells = {},
										 favored_skills = []
									 }: Partial<CalculateExperienceProps>) {
	return calculateExperience({
		profession1,
		profession2,
		profession3,
		skills,
		attributes,
		profession1_talents_amount,
		profession2_talents_amount,
		profession3_talents_amount,
		focuses,
		favored_skills,
		spells
	})
}

const COST_OF_1_SKILL_DOT = 100
const COST_OF_2_SKILL_DOTS = COST_OF_1_SKILL_DOT + 200
const COST_OF_1_ATTRIBUTE_DOT = 100
const COST_OF_2_ATTRIBUTE_DOTS = COST_OF_1_ATTRIBUTE_DOT + 100
const COST_OF_3_ATTRIBUTE_DOTS = COST_OF_2_ATTRIBUTE_DOTS + 200
const COST_OF_4_ATTRIBUTE_DOTS = COST_OF_3_ATTRIBUTE_DOTS + 200
const COST_OF_5_ATTRIBUTE_DOTS = COST_OF_4_ATTRIBUTE_DOTS + 300
const A_PROFESSION = "a_profession"
const A_FOCUS = "a_focus"
const ANOTHER_FOCUS = "another_focus"