import {instance, mock, when} from "ts-mockito"
import {Characters} from "zweb/core/domains/character/Characters"
import GetCharacterSheetOfID from "zweb/core/actions/character_sheet/GetCharacterSheetOfID"
import sanitizeCharacterSheetData from "zweb/core/domains/character/SanitizeCharacterSheetData"
import {ATTRIBUTES} from "zweb/web/character_sheet/definitions/ATTRIBUTES"
import {UnsanitizedCharacterSheetData} from "./UnsanitizedCharacterSheetData"
import Ancestries from "../ancestry/Ancestries"
import MagicSchools from "../magic/MagicSchools"
import Professions from "../profession/Professions"

const CHARACTER_ID = "character_id"
describe("GetCharacterOfId", () => {
	const charactersMock = mock<Characters>()
	const characters = instance(charactersMock)
	const getCharacter = GetCharacterSheetOfID({
		characters,
		ancestries: new Ancestries([]),
    schools: new MagicSchools([]),
		professions: new Professions([], []),
	})

	describe("static properties", () => {
		it("name", async () => {
			const character =	await get_character({name: "linuar"})
			expect(character.name).toBe("linuar")
		})

		it("age", async () => {
			const character =	await get_character({age: 36})
			expect(character.age).toBe(36)
		})
	})

	describe("attribute", () => {
		const ATTRIBUTE = ATTRIBUTES[0]

		it("base attributes", async () => {
			const character =	await get_character({attributes: {[ATTRIBUTE.code]: { base: 47, advances: 3 }}})

			const attribute = character.attributes.find(x => x.code === ATTRIBUTE.code)!
			expect(attribute.name).toBe(ATTRIBUTE.name)
			expect(attribute.advances).toBe(3)
			expect(attribute.base).toBe(47)
		})

		it("bonus without advances", async () => {
			const character =	await get_character({attributes: {[ATTRIBUTE.code]: { base: 40, advances: 0 }}})
			const attribute = character.attributes.find(x => x.code === ATTRIBUTE.code)!
			expect(attribute.bonus).toBe(4)
		})

		it("bonus with an advance", async () => {
			const character =	await get_character({attributes: {[ATTRIBUTE.code]: { base: 40, advances: 1 }}})
			const attribute = character.attributes.find(x => x.code === ATTRIBUTE.code)!
			expect(attribute.bonus).toBe(5)
		})

		it("bonus from attribute with mercy", async () => {
			const character =	await get_character({attributes: {[ATTRIBUTE.code]: { base: 28 }}, mercy: ATTRIBUTE.code})
			const attribute = character.attributes.find(x => x.code === ATTRIBUTE.code)!
			expect(attribute.bonus).toBe(4)
		})
	})

	describe("attribute derivatives", () => {
		it("peril threshold is willpower bonus + 3", async () => {
			const character =	await get_character({attributes: {willpower: { base: 40, advances: 1 }}})

			expect(character.peril.threshold).toBe(8)
		})

		it("damage threshold is brawn bonus", async () => {
			const character =	await get_character({attributes: {brawn: { base: 40, advances: 1 }}})

			expect(character.damage.threshold).toBe(5)
		})

		it("encumbrance limit is brawn bonus + 3", async () => {
			const character =	await get_character({attributes: {brawn: { base: 40, advances: 1 }}})

			expect(character.encumbrance_limit).toBe(8)
		})

		it("initiative is perception bonus + 3", async () => {
			const character =	await get_character({attributes: {perception: { base: 40, advances: 1 }}})

			expect(character.initiative).toBe(8)
		})

		it("speed is movement bonus + 3", async () => {
			const character =	await get_character({attributes: {agility: { base: 40, advances: 1 }}})

			expect(character.movement).toBe(8)
		})

		it("maximum focuses is intelligence bonus", async () => {
			const character =	await get_character({attributes: {intelligence: { base: 40, advances: 1 }}})

			expect(character.maximum_focuses).toBe(5)
		})

		it("maximum languages is fellowship bonus", async () => {
			const character =	await get_character({attributes: {fellowship: { base: 40, advances: 1 }}})

			expect(character.maximum_languages).toBe(5)
		})
	})

	async function get_character(data: UnsanitizedCharacterSheetData) {
		when(charactersMock.retrieveByID(CHARACTER_ID)).
			thenResolve(sanitizeCharacterSheetData(CHARACTER_ID, data))
		return await getCharacter(CHARACTER_ID)
	}
})

