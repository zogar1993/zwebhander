import {SkillCode} from "zweb/core/domains/character/SkillCode"
import {Principle} from "zweb/core/domains/magic/Spell"

export default function calculateExperience({
																							profession1,
																							profession2,
																							profession3,
																							skills,
																							attributes,
																							profession1_talents_amount,
																							profession2_talents_amount,
																							profession3_talents_amount,
																							focuses,
																							spells,
																							favored_skills
																						}: CalculateExperienceProps): number {
	let experience = 0

	if (profession1) experience += 100
	if (profession2) experience += 200
	if (profession3) experience += 300

	skills.forEach(skill => {
		if (skill.ranks >= 1) experience += skill.profession_ranks >= 1 ? 100 : 200
		if (skill.ranks >= 2) experience += skill.profession_ranks >= 2 ? 200 : 400
		if (skill.ranks >= 3) experience += skill.profession_ranks >= 3 ? 300 : 600
	})

	attributes.forEach(attribute => {
		if (attribute.advances >= 1) experience += attribute.profession_advances >= 1 ? 100 : 200
		if (attribute.advances >= 2) experience += attribute.profession_advances >= 2 ? 100 : 200
		if (attribute.advances >= 3) experience += attribute.profession_advances >= 3 ? 200 : 400
		if (attribute.advances >= 4) experience += attribute.profession_advances >= 4 ? 200 : 400
		if (attribute.advances >= 5) experience += attribute.profession_advances >= 5 ? 300 : 600
		if (attribute.advances >= 6) experience += attribute.profession_advances >= 6 ? 300 : 600
	})

	if(spells.Petty && spells.Petty > 3) experience += (spells.Petty - 3) * 100
	if(spells.Lesser) experience += spells.Lesser * 200
	if(spells.Greater) experience += spells.Greater * 300

	experience += 100 * profession1_talents_amount
	experience += 200 * profession2_talents_amount
	experience += 300 * profession3_talents_amount

	forEachEntryInRecord(focuses, ([skill, focuses]) => {
		const favored = favored_skills.includes(skill)
		experience += (favored ? 50 : 100) * focuses.length
	})

	return experience
}

export type CalculateExperienceProps = {
	profession1: string | null,
	profession2: string | null,
	profession3: string | null,
	skills: Array<{ ranks: number, profession_ranks: number }>
	attributes: Array<{ advances: number, profession_advances: number }>
	profession1_talents_amount: number,
	profession2_talents_amount: number,
	profession3_talents_amount: number,
	focuses: Partial<Record<SkillCode, Array<string>>>,
	favored_skills: Array<SkillCode>
	spells: Partial<Record<Principle, number>>
}

function partialRecordKeys<Key extends string, Value>(record: Partial<Record<Key, Value>>) {
	return Object.entries(record) as Array<[Key, Value]>
}

function forEachEntryInRecord<Key extends string, Value>(
	record: Partial<Record<Key, Value>>,
	func: (pair: [key: Key, value: Value]) => void
) {
	const pairs = partialRecordKeys(record)
	pairs.forEach(func)
}