import {SkillCode} from "zweb/core/domains/character/SkillCode"
import {AttributeCode} from "zweb/core/domains/character/AttributeCode"
import {CharacterSheetAttributeData, CharacterSheetData, CharacterSheetSkillData} from "./CharacterSheetData"

export type UnsanitizedCharacterSheetData =
	Partial<Omit<CharacterSheetData, "skills" | "attributes">> &
	{skills?: UnsanitizedSkills} & {attributes?: UnsanitizedAttributes}

export type UnsanitizedSkills = Partial<Record<SkillCode, CharacterSheetSkillData>>
export type UnsanitizedAttributes = Partial<Record<AttributeCode, CharacterSheetAttributeData>>