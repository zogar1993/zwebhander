import {CharacterSpells} from "zweb/core/domains/character/CharacterSpells"
import {Focuses} from "zweb/core/domains/character/Focuses"
import {SkillCode} from "zweb/core/domains/character/SkillCode"
import {AttributeCode} from "zweb/core/domains/character/AttributeCode"
import {SkillDefinition} from "../../../web/character_sheet/definitions/SKILLS"
import {AttributeDefinition} from "../../../web/character_sheet/definitions/ATTRIBUTES"

export type CharacterSheetData = {
	id: string
	name: string,
	age: number
	sex: string | null
	social_class: string | null
	upbringing: string | null
	damage: number
	peril: number
	avatar: string | null
	order_alignment: string | null
	chaos_alignment: string | null
	order_ranks: number
	chaos_ranks: number
	corruption: number
	journal: string
	talents: Array<string|null>
	ancestry_trait: string | null
	focuses: Focuses
	spells: CharacterSpells
	ancestry: string | null
	archetype: string | null
	profession1: string | null
	profession2: string | null
	profession3: string | null
	mercy: string | null
	skills: CharacterSheetSkills
	attributes: CharacterSheetAttributes
	settings: CharacterSheetSettings
}

export type CharacterSheetSkills = Record<SkillCode, CharacterSheetSkillData & Omit<SkillDefinition, "code">>
export type CharacterSheetAttributes = Record<AttributeCode, CharacterSheetAttributeData & Omit<AttributeDefinition, "code">>
export type CharacterSheetSkillData = { ranks: number }
export type CharacterSheetAttributeData = { base: number, advances: number }

export type SkillOrder = "alphabetic" | "by_attribute"
export type CharacterSheetSettings = {
	skill_order: SkillOrder
}