export interface CharacterSheetTag {
    id: string
    name: string
    avatar: string | null
    ancestry: string
    profession1: string
}
