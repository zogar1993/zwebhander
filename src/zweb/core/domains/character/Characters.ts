import {CharacterSheetTag} from "zweb/core/domains/character/CharacterSheetTag"
import {CharacterSheetData} from "zweb/core/domains/character/CharacterSheetData"

export interface Characters {
    retrieveByID(id: string): Promise<CharacterSheetData>

    findAll(): Promise<Array<CharacterSheetTag>>

    patch(id: string, part: any): Promise<void>

    create(character: any): Promise<string>

    delete(id: string): Promise<void>
}
