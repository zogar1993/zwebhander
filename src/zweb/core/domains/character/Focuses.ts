import {SkillCode} from "zweb/core/domains/character/SkillCode"

export type Focuses = Partial<Record<SkillCode, Array<string>>>