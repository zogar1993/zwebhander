export type SpecialRule = {
	name: string,
	description: string,
	effect: string,
}