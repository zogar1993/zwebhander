import {Book} from "zweb/core/domains/Book"

export type Archetype = {
	name: string
	code: string
	from: number
	to: number
} & Record<Book, Array<ArchetypeProfession>>

export type ArchetypeProfession = {
	profession: string,
	from: number,
	to: number
}