import {Profession} from "zweb/core/domains/profession/Profession"
import {Book} from "zweb/core/domains/Book"
import {BasicProfession} from "zweb/core/domains/profession/BasicProfession"
import {Archetype, ArchetypeProfession} from "zweb/core/domains/profession/Archetype"

export default class Professions {
	professions: Array<Profession>
	archetypes: Array<Archetype>

	constructor(professions: Array<Profession>, archetypes: Array<Archetype>) {
		this.professions = [...professions]
		this.archetypes = [...archetypes]
	}

	getAll(): Array<Profession> {
		return this.professions
	}

	getByCode(code: string): Profession {
		const profession = this.professions.find(x => x.code === code)
		if (profession === undefined) throw Error(`Profession of code '${code}' not found`)
		return profession
	}

	getBasicsOfArchetype(archetypeCode: string, book: Book): Array<BasicProfession> {
		const basics = this.getProfessionsByArchetypeCode(archetypeCode, book)
		return basics.map(x => ({...x, ...this.getByCode(x.profession)}))
	}

	getArchetypeForProfession(profession: string) {
		const archetype = this.archetypes.find(
			archetype => archetype["Main Gauche"].some(prof => prof.profession === profession)
		)
		if (archetype === undefined) throw Error(`Archetype for profession '${profession}' not found`)
		return archetype
	}

	getBasics(book: Book) {
		return this.professions.filter(profession =>
			this.archetypes.some(archetype =>
				archetype[book].some(prof => prof.profession === profession.code)
			)
		)
	}

	getArchetypes(): Array<Archetype> {
		return this.archetypes
	}

	private getProfessionsByArchetypeCode(code: string, book: Book): Array<ArchetypeProfession> {
		const archetype = this.archetypes.find(x => x.code === code)
		if (archetype === undefined) throw Error(`Archetype of code '${code}' not found`)
		return archetype[book]
	}
}
