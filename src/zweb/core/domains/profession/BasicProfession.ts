import {Profession} from "zweb/core/domains/profession/Profession"
import {ArchetypeProfession} from "zweb/core/domains/profession/Archetype"

export type BasicProfession =
	Omit<Profession, "prerequisite">
	& Omit<ArchetypeProfession, "profession">