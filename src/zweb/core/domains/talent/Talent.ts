export default interface Talent {
	name: string,
	code: string,
	description: string,
	effect: string,
}