import Talent from "zweb/core/domains/talent/Talent"

export default class Talents {
	talents: Array<Talent>

	constructor(talents: Array<Talent>) {
		this.talents = [...talents]
	}

	getAll(): Array<Talent> {
		return this.talents
	}

	getByCode(code: string): Talent {
		const talent = this.talents.find(x => x.code === code)
		if (talent === undefined) throw Error(`Talent of code '${code}' not found`)
		return talent
	}
}