import {Alignment} from "zweb/core/domains/alignment/Alignment"

export default class Alignments {
    alignments: Array<Alignment>

    constructor(alignments: Array<Alignment>) {
        this.alignments = [...alignments]
    }

    getChaos(): Array<Alignment> {
        return this.alignments.filter(x => x.type === "chaos")
    }

    getOrder(): Array<Alignment> {
        return this.alignments.filter(x => x.type === "order")
    }
}
