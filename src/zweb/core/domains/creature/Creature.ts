export type Creature = {
    type: string
    family: string
    description: Array<string>
    size: Array<string>
    attributes: Attributes
    initiative: number
    movement: Array<Movement>
    notch: string
    risk_factor: string
    traits: Array<Trait>
    attack_profile: Array<Attack>
    trappings: Array<string>
}

export type Attributes = {
    combat: Attribute
    brawn: Attribute
    agility: Attribute
    perception: Attribute
    intelligence: Attribute
    willpower: Attribute
    fellowship: Attribute
}

export type Attribute = {
    base: number,
    bonus: number
}

export type Movement = {
    value: number,
    type: string
}

export type Trait = {
    name: string,
    description: string
}

export type Attack = {
    name: string,
    chance: number,
    distance: string,
    load?: string,
    damage: string,
    qualities: Array<string>
}