import {Ancestry} from "zweb/core/domains/ancestry/Ancestry"
import {AncestryTag} from "zweb/core/domains/ancestry/AncestryTag"

export default class Ancestries {
	ancestries: Array<Ancestry>

	constructor(ancestries: Array<Ancestry>) {
		this.ancestries = [...ancestries]
	}

	getAll(): Array<AncestryTag> {
		return this.ancestries
	}

	getByCode(code: string): Ancestry {
		const ancestry = this.ancestries.find(x => x.code === code)
		if (ancestry === undefined) throw Error(`Ancestry of code '${code}' not found`)
		return ancestry
	}
}