import {AncestryTrait} from "zweb/core/domains/ancestry/AncestryTrait"
import {AttributeCode} from "zweb/core/domains/character/AttributeCode"

export type Ancestry = {
	name: string
	code: string
	description: string
	type: string
	family: string
	traits: Array<AncestryTrait>
	attribute_bonuses: AttributeBonuses
}

export type AttributeBonuses = Partial<Record<AttributeCode, number>>
