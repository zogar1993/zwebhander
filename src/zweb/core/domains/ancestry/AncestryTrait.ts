export type AncestryTrait = {
	name: string,
	code: string,
	description: string,
	effect: string,
	from: number,
	to: number
}