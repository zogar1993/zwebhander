export interface AncestryTag {
	name: string,
	code: string,
}
