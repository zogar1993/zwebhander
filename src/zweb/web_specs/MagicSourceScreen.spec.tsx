import React from "react"
import {fireEvent, render} from "@testing-library/react"
import "@testing-library/jest-dom/extend-expect"
import "jest-extended"
import {Spell} from "zweb/core/domains/magic/Spell"
import AppRouter from "zweb/web/navigation/AppRouter"
import {createMemoryHistory} from "history"
import {MemoryHistory} from "history/createMemoryHistory"
import {magic_schools} from "zweb/data/magic_schools"
import newTestProvider from "zweb/web_specs/TestProvider"

const schools = [
	{
		"name": "Animism",
		"type": "arcana",
		"code": "animism"
	},
	{
		"name": "Astromancy",
		"type": "arcana",
		"code": "astromancy"
	}
]
const spells = magic_schools.find(x => x.code === "animism" || x.code === "astromancy")!.spells

xdescribe("Magic Schools Screen", () => {
	const ANIMISM = "animism"
	const ASTROMANCY = "astromancy"
	let history: MemoryHistory
	let screen: Element

	it("when first loaded should show all arcana magic schools with animism selected", async () => {
		given_the_magic_source_screen_is_shown_for("arcanas")
		await then_all_arcana_magic_schools_are_shown()
		await then_all_spells_are_shown_for_school(ANIMISM)
	})

	it("should show selected school when clicked", async () => {
		given_the_arcanas_screen_is_shown()
		await when_school_tab_is_clicked(ASTROMANCY)
		await then_all_spells_are_shown_for_school(ASTROMANCY)
	})

	function given_the_magic_source_screen_is_shown_for(source: string) {
		history = createMemoryHistory()
		history.push(`/magic/${source}`)
		screen = render(<AppRouter provider={provider} history={history}/>).baseElement
	}

	function given_the_arcanas_screen_is_shown() {
		history = createMemoryHistory()
		history.push("/magic/arcanas")
		screen = render(<AppRouter provider={provider} history={history}/>).baseElement
	}

	async function when_school_tab_is_clicked(name: string) {
		//@ts-ignore
		const school = await findChildOf(screen, {tag: "button", text: name})
		fireEvent.click(school)
	}

	async function then_all_arcana_magic_schools_are_shown() {
		//@ts-ignore
		const article = await findChildOf(screen, {tag: "article", heading: "arcanas"})
		//@ts-ignore
		const tabs = await findChildrenOf(article, {tag: "button"})

		//@ts-ignore
		expect(tabs.map(school => school.textContent))
			.toIncludeSameMembers(schools.map(school => school.name))
	}

	async function then_all_spells_are_shown_for_school(name: string) {
		for (const spell of spellsOfSchool(name)) {
			//@ts-ignore
			const card = await findChildOf(screen, {tag: "section", heading: spell.name})
			//@ts-ignore
			getChildOf(card, {tag: "span", text: spell.distance_tag})
			//@ts-ignore
			getChildOf(card, {tag: "span", text: spell.school})
			//@ts-ignore
			getChildOf(card, {tag: "span", text: spell.principle})
			//@ts-ignore
			getChildOf(card, {tag: "p", text: spell.description})
		}
	}

	function spellsOfSchool(school: string): Array<Spell> {
		//@ts-ignore
		return spells.filter(spell => spell.school === school)
	}
})

const provider = newTestProvider()