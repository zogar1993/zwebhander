import newTestProvider from "zweb/web_specs/TestProvider"

describe("Character Screen", () => {
	let screen: HTMLElement
	const provider = newTestProvider()

	xit("Second profession field should be disabled if first profession is not set", async () => {
		const cosa = screen || provider
		expect(cosa || {}).not.toBeNull()
	})
})