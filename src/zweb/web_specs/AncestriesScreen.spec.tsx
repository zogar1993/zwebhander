import React from "react"
import {act, fireEvent, render, within} from "@testing-library/react"
import "@testing-library/jest-dom/extend-expect"
import "jest-extended"
import {Ancestry} from "zweb/core/domains/ancestry/Ancestry"
import newTestProvider from "zweb/web_specs/TestProvider"
import {createMemoryHistory} from "history"
import AppRouter from "zweb/web/navigation/AppRouter"
import {MemoryHistory} from "history/createMemoryHistory"
import {BoundFunctions} from "@testing-library/dom/types/get-queries-for-element"
import * as Queries from "@testing-library/dom/types/queries"
import {AncestryTrait} from "../core/domains/ancestry/AncestryTrait"

xdescribe("Ancestries Screen", () => {
	let history: MemoryHistory
	let screen: BoundFunctions<typeof Queries>
	const provider = newTestProvider()
	provider.getAncestryImage = () => Promise.resolve("")
	provider.getAllAncestries = () => Promise.resolve(ancestries)
	provider.getAncestry = (code: string) => Promise.resolve(ancestries.find(x => x.code === code)!!)

	it("when first loaded should show all ancestries tabs with human selected", async () => {
		await given_the_ancestries_screen_is_shown()
		await then_all_ancestries_tabs_are_shown()
		await then_details_are_shown_for(HUMAN_CODE)
		await then_first_ancestry_trait_is_shown_for(HUMAN_CODE)
	})

	it("should show selected ancestry when clicked", async () => {
		await given_the_ancestries_screen_is_shown()
		await when_ancestry_tab_is_clicked(DWARF_TEXT)
		await then_details_are_shown_for(DWARF_CODE)
		await then_first_ancestry_trait_is_shown_for(DWARF_CODE)
	})

	it("should show selected ancestry when clicked", async () => {
		await given_the_ancestries_screen_is_shown_with_human_selected()
		await when_trait_is_clicked(human.traits[1])
		await then_ancestry_trait_is_shown(human.traits[1])
	})

	async function given_the_ancestries_screen_is_shown() {
		history = createMemoryHistory()
		history.push("/ancestries")
		await act(async () => {
			const page = render(<AppRouter provider={provider} history={history}/>)
			screen = within(await page.findByRole("article"))
		})
	}

	async function given_the_ancestries_screen_is_shown_with_human_selected() {
		await given_the_ancestries_screen_is_shown()
	}

	async function when_ancestry_tab_is_clicked(name: string) {
		await act(async () => {
			const ancestry = await screen.findByRole("tab", {name: name})
			fireEvent.click(ancestry)
		})
	}

	async function when_trait_is_clicked(trait: AncestryTrait) {
		await act(async () => {
			const ancestry = await screen.findByRole("button", {name: trait.name})
			fireEvent.click(ancestry)
		})
	}

	async function then_all_ancestries_tabs_are_shown() {
		const tabs = await screen.findAllByRole("tab")

		expect(tabs.map(ancestry => ancestry.textContent))
			.toIncludeSameMembers(ancestries.map(ancestry => ancestry.name))
	}

	async function then_details_are_shown_for(name: string) {
		const ancestry = ancestryOfName(name)
		await screen.findByText(ancestry.description)
	}

	async function then_first_ancestry_trait_is_shown_for(name: string) {
		const ancestry = ancestryOfName(name)
		await then_ancestry_trait_is_shown(ancestry.traits[0])
	}

	async function then_ancestry_trait_is_shown(trait: AncestryTrait) {
		const card = getRegion(trait.name, screen)
		within(card).getByText(trait.description)
		const effect = getByTerm("Effect", within(card))
		expect(effect.textContent).toBe(trait.effect)
	}

	function getByTerm(term: string, component: BoundFunctions<typeof Queries>) {
		const terms = component.getAllByRole("term")
		//TODO when issue is fixed, remove workaround from package.json
		//ISSUE: https://github.com/testing-library/dom-testing-library/issues/703#issuecomment-678267112
		const termComponent = terms.find(x => x.textContent === term)
		if(termComponent === undefined) throw Error(`term '${term}' not found`)
		const details = getDetailsOfTerm(termComponent)
		if(details.length === 0) throw Error(`No detail found for term '${term}'`)
		if(details.length > 1) throw Error(`Multiple details found for term '${term}'`)
		return details[0]
	}

	function getDetailsOfTerm(term: HTMLElement) {
		let detail = term.nextSibling as HTMLElement | null
		while (detail?.tagName === "DT") detail = detail.nextSibling as HTMLElement | null
		const details: Array<HTMLElement> = []
		while (detail?.tagName === "DD") {
			details.push(detail)
			detail = detail.nextSibling as HTMLElement | null
		}
		return details
	}

	function ancestryOfName(code: string): Ancestry {
		const ancestry = ancestries.find(ancestry => ancestry.code === code)
		if (ancestry === undefined) throw Error(`Ancestry of code '${code}' not found.`)
		else return ancestry
	}
})

const HUMAN_CODE = "human"
const HUMAN_TEXT = "Human"
const DWARF_CODE = "dwarf"
const DWARF_TEXT = "Dwarf"

const human: Ancestry = {
	name: HUMAN_TEXT,
	code: HUMAN_CODE,
	type: "playable",
	family: "humanoid",
	description: "human description",
	traits: [
		{
			name: "human trait name A",
			code: "human_trait_name_A",
			description: "human trait description A",
			effect: "human trait effect A",
			from: 1,
			to: 50
		},
		{
			name: "human trait name B",
			code: "human_trait_name_B",
			description: "human trait description B",
			effect: "human trait effect B",
			from: 51,
			to: 100
		}
	],
	attribute_bonuses: {}
}

const dwarf: Ancestry = {
	name: DWARF_TEXT,
	code: DWARF_CODE,
	type: "playable",
	family: "humanoid",
	description: "dwarf description",
	traits: [
		{
			name: "dwarf trait name A",
			code: "dwarf_trait_name_A",
			description: "dwarf trait description A",
			effect: "dwarf trait effect A",
			from: 1,
			to: 50
		},
		{
			name: "dwarf trait name B",
			code: "dwarf_trait_name_B",
			description: "dwarf trait description B",
			effect: "dwarf trait effect B",
			from: 51,
			to: 100
		}
	],
	attribute_bonuses: {}
}

const ancestries: Array<Ancestry> = [human, dwarf]

function hasHeading(heading: string, element: HTMLElement): boolean {
	const headings = within(element).queryAllByRole('heading')
	if (headings.length === 0) return false
	return headings[0].textContent === heading
}

function getRegion(heading: string, screen: BoundFunctions<typeof Queries>) {
	const regions = screen.getAllByRole('region')
	const region = regions.find(region => hasHeading(heading, region))
	if (!region) throw Error(`heading '${heading}' not found`)
	return region
}