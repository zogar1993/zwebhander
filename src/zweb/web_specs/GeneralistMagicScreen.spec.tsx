import React from "react"
import {fireEvent, render} from "@testing-library/react"
import "@testing-library/jest-dom/extend-expect"
import "jest-extended"
import {Spell} from "zweb/core/domains/magic/Spell"
import {createMemoryHistory} from "history"
import AppRouter from "zweb/web/navigation/AppRouter"
import {MemoryHistory} from "history/createMemoryHistory"
import {magic_schools} from "zweb/data/magic_schools"
import newTestProvider from "zweb/web_specs/TestProvider"

const spells = magic_schools.find(x => x.code === "generalist")!.spells

xdescribe("Generalist Magic Spells Screen", () => {
	let history: MemoryHistory
	let screen: Element

	it("should show all generalist magic spell summary cards", async () => {
		given_the_generalist_magic_spells_screen_is_shown()
		await then_the_article_has_the_heading("generalist magic")
		await then_all_spell_summary_cards_are_shown()
	})

	it("should show details of selected spell", async () => {
		given_the_generalist_magic_spells_screen_is_shown()
		await when_clicking_the_spell_card_of_name(spells[0].name)
		await then_details_are_shown_for_spell_of_name(spells[0].name)
	})

	function given_the_generalist_magic_spells_screen_is_shown() {
		history = createMemoryHistory()
		history.push("/magic/generalists")
		screen = render(<AppRouter provider={provider} history={history}/>).baseElement
	}

	async function when_clicking_the_spell_card_of_name(name: string) {
		const card = await findChildOf(screen, {tag: "section", heading: name})
		fireEvent.click(card)
	}

	async function then_details_are_shown_for_spell_of_name(name: string) {
		const spell = spellOfName(name)
		const card = await findChildOf(screen, {tag: "dialog", heading: spell.name})
		getChildOf(card, {tag: "p", text: spell.description})
		getChildOf(card, {tag: "p", text: `Distance: ${spell.distance}`})
		getChildOf(card, {tag: "p", text: `Reagents: ${spell.reagents}`})
		getChildOf(card, {tag: "p", text: `Effect: ${spell.effect}`})
		getChildOf(card, {tag: "p", text: `Critical Success: ${spell.critical_success}`})
		getChildOf(card, {tag: "p", text: `Critical Failure: ${spell.critical_failure}`})
	}

	async function then_the_article_has_the_heading(heading: string) {
		await findChildOf(screen, {tag: "article", heading: heading})
	}

	async function then_all_spell_summary_cards_are_shown() {
		for (const spell of spells) {
			const card = await findChildOf(screen, {tag: "section", heading: spell.name})
			getChildOf(card, {tag: "span", text: spell.distance_tag})
			getChildOf(card, {tag: "span", text: spell.school})
			getChildOf(card, {tag: "span", text: spell.principle})
			getChildOf(card, {tag: "p", text: spell.description})
		}
	}

	function spellOfName(name: string | null): Spell {
		const spell = spells.find(spell => spell.name === name)
		if (spell === undefined) throw Error(`Spell of name '${name}' not found'.`)
		else return spell
	}
})

const provider = newTestProvider()
