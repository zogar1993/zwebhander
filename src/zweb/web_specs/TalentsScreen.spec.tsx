import React from "react"
import {render, screen} from "@testing-library/react"
import "@testing-library/jest-dom/extend-expect"
import "jest-extended"
import TalentsScreen from "zweb/web/catalogs/talents/TalentsScreen"
import Talent from "zweb/core/domains/talent/Talent"
import newTestProvider from "zweb/web_specs/TestProvider"

xdescribe("Talents Screen", () => {

	it("when loaded it should show all talents", async () => {
		await given_the_talents_screen_is_shown()
		await then_all_talents_are_shown()
	})

	async function given_the_talents_screen_is_shown() {
		render(<TalentsScreen provider={provider}/>)
	}

	async function then_all_talents_are_shown() {
		for (const talent of talents) {
			//@ts-ignore
			const nameCell = await findChildOf(screen, {tag: "span", text: talent.name})
			//@ts-ignore
			const row = getAncestorOf(nameCell, {tag: "tr"})
			//@ts-ignore
			const cells = getChildrenOf(row, {tag: "td"})

			//@ts-ignore
			getChildOf(cells[0], {tag: "span", text: talent.name})
			//@ts-ignore
			getChildOf(cells[1], {tag: "p", text: talent.description})
			//@ts-ignore
			getChildOf(cells[2], {tag: "p", text: talent.effect})
		}
	}
})

const talentA = {
	name: "name A",
	code: "talent_A",
	description: "description A",
	effect: "effect A"
}
const talentB = {
	name: "name B",
	code: "talent_B",
	description: "description B",
	effect: "effect B"
}

const talents: Array<Talent> = [talentA, talentB]

const provider = newTestProvider()