import {newProvider} from "zweb/core/Provider"
import Ancestries from "zweb/core/domains/ancestry/Ancestries"
import Talents from "zweb/core/domains/talent/Talents"
import Professions from "zweb/core/domains/profession/Professions"
import Alignments from "zweb/core/domains/alignment/Alignments"
import {mock} from "ts-mockito"
import MagicSchools from "zweb/core/domains/magic/MagicSchools"

const newTestProvider = () => {
    const provider = newProvider(
        new Ancestries([]),
        new Talents([]),
        new Professions([], []),
        new Alignments([]),
        mock(),
        new MagicSchools([]),
        mock()
    )

    Object.keys(provider).forEach(key =>{
        // @ts-ignore
        provider[key] = () => { throw Error(`'${key}' was not mocked`) }
    })

    return provider
}
export default newTestProvider