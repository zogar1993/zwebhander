export const bestiary = [
    {
        "type": "AIR SYLPH",
        "family": "ÆTHERIC SPIRIT",
        "description": [
            "There are no such things as fairies  though common in folktales and myths, there has never been any physical evidence these little moth-winged sprites ever existing. Perhaps they were made from the rabble-rousing of Redcaps or maybe ancient Elves, but these small fanged spirits of mischievousness are on all accounts just rumors. However, the Air Sylph may be an adaptation, or maybe even the origin, of those same fairy tales.",
            "By cooking a Gryphon's egg, an Air Sylph can be brought forth from the Æthereal Veil. With a violent squall and a clap of thunder, the Air Sylph materializes from the void from a miniature cyclone. The Air Sylph is often no bigger than a child, though their physical form is that of a comely  and half-nude  Elven woman. However, this is shadowed by seemingly insectile features  they have a set of large moth wings sprouting from their backs, furry antennae atop their heads and eyes both segmented and compounded. They kick up winds wherever they go, chattering incessantly in a halfwhispered, half-seductive tone. Air Sylphs are flighty and mercurial, causing instinctual chaos just by simply existing. They are also extremely quick on the wing and even able to blend into the very winds to remain unseen. They may not be intelligent, but they are likely the most cunning of all the Ætheric Spirits  though that honestly isn't saying much. Air Sylphs are slaves to the wind though  they cannot journey underground or over water, where their powers are slowly drained until they are tossed back to the Veil.",
            "Air Sylphs are the most common of the Ætheric Spirits, and arcanists believe that they are an Ætheric adaptation of Human myths, taking form to fill a sort of metaphysical void in the consciousness of mankind. Sylphs are spoken of very highly among far-flung cultures. Far to the east, they are often referred to as genies and stories tell of their ability to grant three wishes upon being released from their Ætheric prison. Perhaps this is related to the Air Sylphs' instincts to follow orders, but the myth has become so popular that many arcanists have died due to their overconfidence in summoning these vaporous beings, all in hopes of a simple wish."
        ],
        "size": [
            "Small",
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 10
            },
            "agility": {
                "base": 45,
                "bonus": 10
            },
            "perception": {
                "base": 35,
                "bonus": 4
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 16,
                "type": "normal"
            },
            {
                "value": 19,
                "type": "fly"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": null,
        "parry": null,
        "dodge": {
            "chance": 85,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "stealth": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Wind Kick",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "Special",
                "qualities": []
            },
            {
                "name": "Elemental Lash",
                "chance": 70,
                "distance": "ranged 7 yards",
                "load": "2 AP",
                "damage": "10",
                "qualities": [
                    "Entangling",
                    "Pummeling"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "ELEMENTAL WEAKNESS",
                "description": "Those creatures which are composed of elemental air cannot move underground or across water. Those which are composed of elemental earth instantly disintegrate and are Slain! if detached from the ground. Creatures composed of elemental fire are Slain! if they enter a body of water or are caught in a heavy rain shower. Finally, those composed of water instantly disintegrate, Slain! if they leave the water."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these creatures."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            },
            {
                "name": "WIND KICK",
                "description": "When creatures use this attack, it forces a foe to Resist with a Coordination Test or be thrown 1D10+ [BB] in yards. This attack can be used against three foes at once."
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "EARTHEN GNOME",
        "family": "ÆTHERIC SPIRIT",
        "description": [
            "What lurks below our feet, in the cold belly of the earth, has always been a worry for land dwellers. Though Dwarves may indeed know the depths of the stone, even they choose to avoid the most chthonic depths. Rumors persist of massive carrion worms the size of villages, who dig enormous tunnels while feeding on even fouler things. Though no real sign of these has ever been found, the Earthen Gnome follows these fears closely.",
            "A Basilisk's eyes bring forth an Earthen Gnome, as it is dissolved in acid and spread about the circle. With a terrible rumbling and noise of shattering stone, up from the ground burrows the imposing Earthen Gnome, covered in rot and dirt. Earthen Gnomes are squat and disgusting, possessing clearly worm-like attributes. They are putrid white-yellow and ridged, covered in a carapace of diamond and rock. Two groups of three small, beady eyes crown their head and their mouth opens four ways into a massive saw-toothed jaw designed for crushing stone and bone. Around their neck are multiple retracting tentacles they can use to manipulate objects into their jaws. They are eerily quiet, not even making a noise when burrowing or eating. Earthen Gnomes are always half-submerged in the earth. Whether they have an actual hind end is up for debate, as they are instantly killed if they are ripped from their burrow.",
            "Though Earthen Gnomes are threatening, they are relatively calm and immobile compared to the other Ætheric Spirits, making them only slightly less dangerous. One of the more interesting facets of the Earthen Gnome is their name, which is far too similar to the humanoid Gnomes  at least in their opinion. Myths exist that Earthen Gnomes once were actual Gnomes, who journeyed too deep into the earth and became trapped, devolving into these fat and fetid worm-like creatures. Earthen Gnomes are also used to scare children, taking on a similar role as the classic changeling of Human society. Luckily, due to the Earthen Gnomes literal lack of intelligence, this idea bears no merit outside of a deterrent to not steal from or strike other children."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 13
            },
            "agility": {
                "base": 45,
                "bonus": 7
            },
            "perception": {
                "base": 35,
                "bonus": 4
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 17,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "stealth": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Earthen Fisticuffs",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "13",
                "qualities": [
                    "Slow"
                ]
            },
            {
                "name": "Stone Blast",
                "chance": 70,
                "distance": "ranged 7 yards",
                "load": "2 AP",
                "damage": "Special",
                "qualities": []
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "ELEMENTAL WEAKNESS",
                "description": "Those creatures which are composed of elemental air cannot move underground or across water. Those which are composed of elemental earth instantly disintegrate and are Slain! if detached from the ground. Creatures composed of elemental fire are Slain! if they enter a body of water or caught in a heavy rain shower. Finally, those composed of water instantly disintegrate, Slain! if they leave the water."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "MONSTROUS BELLOW",
                "description": "When these creatures successfully use a Litany of Hatred, they inflict 2D10+[BB] physical Peril."
            },
            {
                "name": "NATURAL ARMOR (4)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these creatures."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition, they ignore the effects of Hard Terrain."
            },
            {
                "name": "STONE BLAST",
                "description": "When creatures use this attack, it forces a foe to Resist with an Athletics Test or suffer 1D10+ the creature's [BB] in Damage. This attack can be used against three foes at once and cannot be Dodged or Parried."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "FIRE SALAMANDER",
        "family": "ÆTHERIC SPIRIT",
        "description": [
            "Dragons are an ancient terror whispered of in the dark and rumors abound of half-breeds which can wreak as much havoc as their parents. One of these half-breeds was a mythical four-legged serpent, which unable to fly like its draconic parent, haunted loamy forests and deep caverns. Though they were long destroyed by Slayers, the Fire Salamander assumes the form of these aged creatures whence summoned.",
            "The heart of a Chimaera is used in the summoning ritual, as it is burned, inhaled and exhaled. A wave of heat and fetid brimstone heralds a Fire Salamander's arrival, as a magma rift opens that the beast crawls out of. The Fire Salamander takes the form of a squat lizard, roughly the size of a komodo dragon and similar in shape. Their scales are obsidian black, cut in between with molten rock separating the plates and their eyes burn with a yellow-red fire. Their hiss crackles with cinders and they are able to breath a searing gout of flame that melts both skin and metal. They are always filled with a fiery rage, one that leads them to scorch the land they trod upon. Water is anathema to them, thus their propensity to stay within caves deep within the heat of the earth.",
            "Fire Salamanders represent the fury and destructive force of flame and heat and have done so since the world was young. Since dragons are so rare, Fire Salamanders are often mistaken for them by those unfortunate to stumble upon ones that freely dwell in the Material Realm. And why not? They are lizards that breathe fire, have jagged teeth and exude Corruption, so they might as well be the feared wyrms. An easy defense for the Fire Salamander is as simple to carrying a bucket of water with you, but they are so powerful and terrible you are lucky if you do not lose your possessions or your skin when you bear witness to their burning countenance. Fire Salamanders function on pure Ætheric instinct, its predatory urges driving them to engulf and devour everything in Ætheric flames."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 9
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 7
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 6,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "stealth": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Gnashing Teeth & Claws",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Spit Fire",
                "chance": 75,
                "distance": "ranged 6 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "AUTOTOMY",
                "description": "When these creatures would be Slain!, they immediately break off their tail, remaining Grievously Wounded and can use any Movement Action for 0 APs. Should they suffer Damage again, they are permanently Slain!."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "DEATH ROLL",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a Chokehold."
            },
            {
                "name": "ELEMENTAL WEAKNESS",
                "description": "Those creatures which are composed of elemental air cannot move underground or across water. Those which are composed of elemental earth instantly disintegrate and are Slain! if detached from the ground. Creatures composed of elemental fire are Slain! if they enter a body of water or caught in a heavy rain shower. Finally, those composed of water instantly disintegrate, Slain! if they leave the water."
            },
            {
                "name": "FEEL THE HEAT",
                "description": "When foes are Engaged with this creature, they must Resist with a successful Coordination Test or be exposed to Mildly Dangerous flames."
            },
            {
                "name": "FIERY RETRIBUTION",
                "description": "These creatures can use Spit Fire as an Opportunity Attack without having to Load."
            },
            {
                "name": "FIREPROOF",
                "description": "These creatures and their possessions are entirely immune to Damage from fire."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage, they can force a foe to Resist a Takedown."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SPIT FIRE",
                "description": "These creatures can use their breath as a ranged weapon. This allows then to strike a single foe within 3+[PB], as the foe suffers 2D10+2 Damage from fire. A foe can attempt to Dodge Spit Fire or Parry it with a shield. Spit Fire can be used while Engaged with foes."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "WATER UNDINE",
        "family": "ÆTHERIC SPIRIT",
        "description": [
            "The medusae is a mysterious creature of the sea: it has no brain, no heart, no mouth, yet it moves and lives, undulating with its many tentacles and gelatinous body. They are found in the darkest recesses of the ocean and can grow to monstrous size. No wonder that the enigmatic Water Undine assumes this form when summoned to the Material Realm.",
            "Like all Ætheric Spirits, Water Undines are formless amoebas floating in the Æthereal Veil, representing the crushing gravity of the ocean depths. Water Undines are summoned through the use of a pulsating Shimmering Mimic's brain, pulverized and devoured within the arcane sigil. With a blast of mist and brine, the Water Undine emerges from the murky pool that forms within the circle. They are a shimmering blue, alternating between wine dark and translucent. They are roughly the size of a humanoid and have the shape of a jellyfish, their bell and tentacles pulsing. They have no intelligence, only a primal survival instinct and an almost devilish contempt for the world. They are only ever mobile within water and when they are, Water Undine are stealthy hunters who fill their prey with poison. Some believe that the pulsing colors of their dome-shaped body are a sort of primitive or chemical language, but their leviathan fury doesn't leave much time to contemplate their nature.",
            "Under the sway of the right summoner, Water Undines are intimidating foes, but useless without any sort of direction. Some do manage to escape the thrall of their masters and descend deep into the oceanic depths to grow massive and powerful. It is thought that krakens and giant squids are eaten by massive Water Undines, only further fueling the myths that Water Undines  when left to their own devices  could threaten coastal cities as they grow bigger. Luckily, Water Undines cannot leave the water. That said, using their massive tentacles, they drag prey into the dark depths of the sea, never to be seen again."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 35,
                "bonus": 3
            },
            "agility": {
                "base": 45,
                "bonus": 12
            },
            "perception": {
                "base": 45,
                "bonus": 9
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 12,
        "movement": [
            {
                "value": 15,
                "type": "normal"
            }
        ],
        "damage_threshold": 3,
        "peril_threshold": null,
        "parry": null,
        "dodge": {
            "chance": 75,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 10,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "simple_ranged": 30,
            "stealth": 30,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Stinging Tentacle",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Entangling",
                    "Fast",
                    "Pummeling"
                ]
            },
            {
                "name": "Elemental Lash",
                "chance": 70,
                "distance": "ranged 13 yards",
                "load": "2 AP",
                "damage": "12",
                "qualities": [
                    "Entangling",
                    "Finesse",
                    "Pummeling"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "ELEMENTAL WEAKNESS",
                "description": "Those creatures which are composed of elemental air cannot move underground or across water. Those which are composed of elemental earth instantly disintegrate and are Slain! if detached from the ground. Creatures composed of elemental fire are Slain! if they enter a body of water or caught in a heavy rain shower. Finally, those composed of water instantly disintegrate, Slain! if they leave the water."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FWIP! FWIP!",
                "description": "These creatures may spend 3 APs to attack twice with ranged weapons without Loading."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these creatures."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "STINGING TENTACLE",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a Stunning Blow."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "GEKKOTA",
        "family": "AZTLAN",
        "description": [
            "Aztlan cities can be massive, some ranging into the tens of thousands of residents. Millions of tons of stone are used to build the massive stepped pyramids and housing of the city and thousands upon thousands of bushels of maize and head of cattle are needed to support the populace.",
            "The species who make up the very infrastructure of the temple-cities are the massive Gekkota (pronounced Gekko-tah). Muscular, with hard scales and roughly the size of an Ogre, the Gekkota are laborers and farmers. They inherited many qualities from their alligator-ancestors and other crocodilians, including the massive heads and fearsome tempers. Gekkota are noticeably silent, unable to speak beyond hissing or growling, but still maintaining the cunning intellect of all Aztlan. Gekkota are long lived and rarely breed, seeing their spawn as little more than an inconvenience to their work. They spend their long lives hauling massive stones to build the cities and harvesting the back-breaking step crops that each city's residents subsist on. That being said, the Gekkota are the most fearsome of foes  their massive jaws rend limb from limb and their scales are so thick that they seem impervious to missiles.",
            "Though they seem disenfranchised, Gekkota bear much civic pride in their drudging duty. To them, they are building the palaces of the gods and feeding their servants, an honor which only they have the ability to carry out. Aztlan society is organized by castes, but the only ones on a higher level are the Tlaloc  all the others carry an equal burden and responsibility. The highest honor that a Gekkota can carry is being promoted to one of the Silent Guardians, an elite group of Gekkota whose job involves being the personal bodyguard to Tlaloc god-kings. These few are the ones that actually perform the de-sanguination of captives into the bathing pools. That being said, the Gekkota have the dishonor of being the least evolved of Aztlan, chosen by the Ancient Ones for only their brute strength and nothing more. They can easily go feral, becoming too wild to reign in and being released into the fetid swamps surrounding the city. Some say that the oldest of these feral Gekkota are the mysterious Water Panthers, but little proof supports this."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 9
            },
            "brawn": {
                "base": 40,
                "bonus": 9
            },
            "agility": {
                "base": 50,
                "bonus": 5
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 7,
        "parry": {
            "chance": 85,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 80,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "intimidate": 30,
            "leadership": 10,
            "martial_melee": 30,
            "simple_melee": 30,
            "simple_ranged": 10,
            "stealth": 10,
            "toughness": 30,
            "warfare": 30
        },
        "attack_profile": [
            {
                "name": "Saw-toothed Sword",
                "chance": 75,
                "distance": "melee engaged or 1 yard",
                "damage": "9",
                "qualities": [
                    "Fast",
                    "Light",
                    "Vicious",
                    "Weak"
                ]
            },
            {
                "name": "Bronze War Hammer",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Powerful",
                    "Slow"
                ]
            },
            {
                "name": "Unhinged Jaw",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Pummeling",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "AUTOTOMY",
                "description": "When these creatures would be Slain!, they immediately break off their tail, remaining Grievously Wounded and can use any Movement Action for 0 APs. Should they suffer Damage again, they are permanently Slain!."
            },
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DEATH ROLL",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a Chokehold."
            },
            {
                "name": "NATURAL ARMOR (2)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "WANTON HUNGER",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1-5', their hunger has been sated. If the result is face '6', their hunger has not yet been sated and they are in a state of frenzy. When sated, they add +1 to both Damage and Peril Condition Tracks. When in a state of frenzy, they add an additional 1D6 Fury"
            }
        ],
        "trappings": [
            "Bronze hammer",
            "Gekkota hide",
            "Munitions plate armor",
            "Sawtoothed sword",
            "Wooden shield"
        ]
    },
    {
        "type": "IGUANIA",
        "family": "AZTLAN",
        "description": [
            "Swarming the Aztlan temple-cities are the seemingly endless masses of the Iguania (pronounced I'gwan-ya). By far the most populous of all the Ancestries evolved by the Ancient Ones, the Iguania serve as both the commoners and military backbone of the city-states.",
            "Iguania are the height of an adult Human and have the general features of an iguana or monitor-lizard, most likely from their great ancestors. Iguanias start their lives in the massive and sweltering birthing temples, where they were laid as eggs by their mothers, concentrated with all other eggs in the city. When they hatch after a few weeks, Iguanias already are birthed with knowledge of combat, how to fend for themselves and how to survive. They are then led into massive training grounds, where they undergo rigorous and regimented training in the spear, sword and bow, all becoming elite soldiers  each like the last. If any Iguanias show any sign of weakness or corruption while training, they are killed  thrown into the Tlaloc's bathing pools, providing them with a great service even in their inept deaths.",
            "Despite being stringent and exacting in their mannerisms, not all Iguania are the same. They come in a wide variety of colors and shapes, but all possess ridges along their spines that are popularized as status symbols. When not in training or worship of their Tlaloc, they compete in tests of strength and cunning, often through blood sports or a strange game involving a hard-rubber ball passed through hoops. But at any moment, Iguanias will pick up their ancestral weapons and wade into battle to eliminate threats to their way of life. Iguanias are trained to kill only when absolutely necessary  their favored tactic is through fear, disarmament and capturing prisoners of war. They then take their captives to their Tlaloc, sacrificing them and draining their blood to keep the lords nearly invulnerable and ageless. However, 'foul blooded' creatures like demons of the Abyss and the Skrzzak are to be killed without mercy, as their blood is too unclean to be bathed in."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 7,
        "parry": {
            "chance": 75,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Basic",
        "risk_factor": "Medium",
        "skills": {
            "awareness": 10,
            "coordination": 10,
            "ride": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "stealth": 10,
            "warfare": 10
        },
        "attack_profile": [
            {
                "name": "Pole Cleaver",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "4",
                "qualities": [
                    "Reach"
                ]
            },
            {
                "name": "Javelin",
                "chance": 55,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "AUTOTOMY",
                "description": "When these creatures would be Slain!, they immediately break off their tail, remaining Grievously Wounded and can use any Movement Action for 0 APs. Should they suffer Damage again, they are permanently Slain!."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Blackroot",
            "Fire-hardened spear",
            "Folkbane",
            "Iguania hide",
            "Leather armor",
            "Javelin (3)",
            "Wooden shield"
        ]
    },
    {
        "type": "LACERTILIA",
        "family": "AZTLAN",
        "description": [
            "All Aztlan possess a cold, alien reptilian intellect, one that is so foreign to humanoids to be nearly impossible. This is shown even further in the Lacertilia (pronounced La-cer-tili-a), the second most common species of Aztlan.",
            "Lacertilia serve as artisans, priests and scholars, possessing an ancestral memory that reaches back hundreds of years. Lacertilia are small and slight, roughly the size of a stout Dwarf and possessing physical traits of chameleons, such as mimicry. Lacertilia are birthed and hatched only one at a time so as best to raise them to be intellectuals. Not only is their knowledge passed down through their bloodline, but as is their place in society  a Lacertilia born in a unit of masons will always be a mason, as it is the will of the Ancient Ones and the universe. There is only one way in which Lacertilia can be elevated above their station: the most intelligent are chosen to serve by the side of the Tlaloc, serving as their interpreters and caretakers. Just like their Iguania brethren, Lacertilia are trained in the military arts  though they serve as scouts and spies rather than front-line fighters.",
            "The Lacertilia and their Tlaloc lords share very similar qualities, as they too descend into long states of meditation to brood on their intellect, their skin mirroring the patterns and colors of the environment they sit within. Lacertilia are perhaps the most important cog in the machine of Aztlan society, as they provide all the services that involve at least a measure of skill. They are also the species who has by far the most contact with the outside world  Lacertilian emissaries will trade with humanoid states and even form alliances at times. They are said to speak with the voice of the gods and thus are treated as leaders outside of their own cities."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 7
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 9,
        "parry": null,
        "dodge": {
            "chance": 60,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "Medium",
        "skills": {
            "awareness": 10,
            "coordination": 10,
            "education": 10,
            "folklore": 20,
            "guile": 10,
            "handle_animal": 20,
            "resolve": 10,
            "ride": 20,
            "simple_melee": 10,
            "simple_ranged": 20,
            "stealth": 20,
            "warfare": 10
        },
        "attack_profile": [
            {
                "name": "Shiv",
                "chance": 50,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Weak"
                ]
            },
            {
                "name": "Xenostaff",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Adaptable",
                    "Finesse",
                    "Weak"
                ]
            },
            {
                "name": "Bolas",
                "chance": 60,
                "distance": "ranged 18 yards",
                "load": "2 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "AUTOTOMY",
                "description": "When these creatures would be Slain!, they immediately break off their tail, remaining Grievously Wounded and can use any Movement Action for 0 APs. Should they suffer Damage again, they are permanently Slain!."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FWIP! FWIP!",
                "description": "These creatures may spend 3 APs to attack twice with ranged weapons without Loading."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these creatures."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth"
            }
        ],
        "trappings": [
            "Blackroot",
            "Bolas (3)",
            "Brigandine armor",
            "Folkbane",
            "Lacertilia hide",
            "Xenostaff"
        ]
    },
    {
        "type": "TLALOC",
        "family": "AZTLAN",
        "description": [
            "The Tlaloc (pronounced Tlai-lohk) are strangers to this world, the original servants of the Ancient Ones brought along to aid their labors. Tlaloc assert that they are from beyond the heavens, brought here by some divine chariot that was annihilated over our earthly sphere.",
            "When they were abandoned by the Ancient Ones, they were invested with vast knowledge and Magickal potency in order to rule until the Ancient Ones returned. Though the Tlaloc have a long memory, few can remember with any detail what their old homestead was like and they often treat it more like a mystical locale of legend than their actual birthing place. Tlaloc are the de-facto rulers of the Aztlan, with one lording over each temple city. They are voluminous and adipose batrachian creatures, very rarely journeying far from their royal palaces, being borne on palanquin by Gekkota when they do. They are unimaginably ancient  to stay alive, they bathe in massive pools of their enemy's blood, absorbing it to prolong their lifespan. Some of the most ancient Tlaloc never leave their pools, as it would mean certain death for them. Tlaloc also think in terms of centuries, often meditating on problems for dozens of decades before uttering some cryptic missive for their Lacertilia to interpret and lead their people with.",
            "The Tlaloc are treated as gods among the Aztlan castes. And even to a layman outsider, Aztlan powers border on the god-like. They are formidable arcanists, using Magicks that some argue are not even native to this world. They are highly intelligent and their Ancestry shares a collaborative hive-mind that allows them to communicate instantly. Tlaloc also have hidden caches of their ancient technology, artifacts that could level forts and spread lakes of disease with just a push of a button. What is stopping the Tlaloc from leading their Aztlan to conquer the world is the inflexibility of their orders. Before the Ancient Ones left, the Tlaloc were told to remain static and unmoving  at least until the stars were right and the Ancient Ones returned to reclaim what was rightfully theirs. Not only do they wait as ordered, they know that to activate their celestial engines that would enable them to conquer this world would also unleash the Abyss and so destroy the world before the Aztlan could even complete their conquest. They believe mortals are under-evolved life forms worthy of little less than bathing pool fodder, but the Demons are threats to both the Tlaloc, their people and potentially the entire celestial sphere."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 45,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 7
            },
            "intelligence": {
                "base": 45,
                "bonus": 7
            },
            "willpower": {
                "base": 50,
                "bonus": 11
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 14,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 55,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "bargain": 30,
            "eavesdrop": 30,
            "education": 30,
            "folklore": 30,
            "incantation": 30,
            "intimidate": 30,
            "resolve": 30,
            "scrutinize": 30,
            "simple_ranged": 30,
            "warfare": 30
        },
        "attack_profile": [],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously. Assuming Æthereal Form and Material Form costs 2 APs."
            },
            {
                "name": "ÆTHERIC DOMINATION",
                "description": "When these creatures would potentially invoke a Chaos Manifestation, they must have two or more face '6' on Chaos Dice to invoke it."
            },
            {
                "name": "AVERSION TO LIGHT",
                "description": "When these creatures are exposed to any sort of light (as from a torch), they suffer a penalty of -3 to their Damage Threshold."
            },
            {
                "name": "AUTOTOMY",
                "description": "When these creatures would be Slain!, they immediately break off their tail, remaining Grievously Wounded and can use any Movement Action for 0 APs. Should they suffer Damage again, they are permanently Slain!."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "LUMBERING BRUTE",
                "description": "These creatures cannot Charge, Run or use Movement Actions that require 3 AP."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "THE CHANGER OF WAYS",
                "description": "When these creatures are made subject to Chaos Manifestations, they can roll twice and choose the most desirous result. Finally, they can Counterspell for 0 APs and can use Dispel Magick without having to make an Incantation Test."
            },
            {
                "name": "UNGAINLY",
                "description": "When these creatures are Slain! all those Engaged with it must succeed at a Coordination Test or be knocked Prone beneath of it, suffering 3D10+3 Damage"
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Reagents for all Magick spells (9)",
            "Tlaloc hide"
        ]
    },
    {
        "type": "BASILISK",
        "family": "BEAST",
        "description": [
            "A surprising and frightening number of creatures in our dark world possess the ability to paralyze their victims, leaving them alive and motionless, but able to feel every inch of pain inflicted. One of these beasts is the ancient Basilisk, creatures so secluded that they were once thought to be a tall tale.",
            "Basilisks are massive snake-like creatures, their squamous bodies extending roughly the length of a carriage and having the thickness of an Ogre's thigh. Their scales are mottled green and brown and the top of their serpentine heads are crowned with small horns. Their eyes are a sickly green color, almost that of bile and their gaze is what they use to paralyze their foes. Their meal only needs to meet their eyes, succumbing to rigor where they stand. Despite their size, Basilisks do not have very large teeth and in fact lack fangs, unlike most pythons. Instead, they secrete an acidic bile from ducts in their throat, a liquid they drip onto the still-living bodies of their food. They then tear away melted chunks of flesh, an excruciating experience that their victim cannot even scream from. A Basilisk's lair will sometimes be full of a dozen meals, all in various states of devoured and in various stages of life  the stench is terrible and the fear is palpable.",
            "Basilisks usually live alone and when they mate, the female will kill the male for sustenance during the long incubation period. It is believed there was once a campaign to eliminate these beasts, as they are both prone to Corruption and are able to produce fast-growing broods numbering in the dozens. Because of this, it was long thought that they were wiped out of existence. However, recent reports of standing, half-consumed torsos and trails of acidic spittle have provided terrible evidence to the contrary. The best defense when fighting a Basilisk is to destroy its eyes, leaving it all but defenseless. Their organs, especially their eyes, acid glands and hearts, fetch great prices among the mystical circles  few feel bad about hunting these beasts and some kingdoms offer incentives for killing them during state-sanctioned 'hunting seasons'."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 8
            },
            "agility": {
                "base": 45,
                "bonus": 5
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 65,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "simple_melee": 10,
            "stealth": 20,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Acidic Spittle",
                "chance": 55,
                "distance": "ranged 5 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            },
            {
                "name": "Vile Claws",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACIDIC SPITTLE",
                "description": "These creatures can use their saliva as a ranged weapon. This allows them to strike a single foe within 1+[PB], causing the foe to suffer 1D10+1 Damage from acid. However, Acidic Spittle ignores a foe's Damage Threshold Modifier from armor. A foe can attempt to Dodge Acidic Spittle or Parry it with a shield. Acidic Spittle can be used while Engaged with foes."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "EYES WIDE SHUT",
                "description": "When this creature's eyes are successfully struck with a Called Shot (otherwise treated as their head), they lose their ability to use Petrifying Gaze."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GASTRIC ACIDITY",
                "description": "When these creatures deal Damage, a foe must Resist with Coordination. If they fail, the foe's armor, weapon or shield is Ruined!."
            },
            {
                "name": "PETRIFYING GAZE",
                "description": "When foes are Engaged with this creature (and the creature can see them), they must Resist with a successful Awareness Test or be Petrified. While Petrified, they are left Helpless and unconscious. Petrification lasts until the heart of the creature is pulverized into a bloody concoction and poured over the victim with a successful Alchemy Test."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Basilisk eyes (2)",
            "Basilisk hide",
            "Basilisk heart"
        ]
    },
    {
        "type": "BOG BEHEMOTH",
        "family": "BEAST",
        "description": [
            "Terrible things lurk beneath the muddy waters and shallow bogs of the lands. Not only caimans, snakes and man-eating fish, but the oddly malevolent and hidden Bog Behemoth.",
            "The Bog Behemoth has the same general form as an octopus, but the similarities end there. They are massively grotesque, easily able to rip off a foe's head with their sharp beak. Their eight tentacles are as thick as logs and flail about like whips, as their ever-turning eyes twist in place. They are generally pinkish in color, but they possess color-changing skin that lets them blend into nearly any environment. They do not need to breathe water  as long as their skin is moist, they can survive. Bog Behemoths are stealthy hunters, hiding under mud beds or as floating debris waiting for prey to walk by  after which, they strike with blinding speed. Though the Bog Behemoth will eat fish, beavers, alligators and so on, they are a real threat to swamp-dwellers and river-going marines. Their tentacles are extremely strong and can crush ribs, able to toss its victims yards away with little effort. Luckily, Bog Behemoths are curious creatures and often get distracted easily and can be made to retreat if exposed to fire.",
            "Myths abound that Bog Behemoths are actually intelligent, nearly sentient. Although they are stealthy, Behemoths are able to coordinate with one another to set up archaic traps and knowingly torture their food for something amounting to entertainment. Luckily, they are few in number and seem to all have an unhealthy fear of fire  the flames easily catch their moss and lichen-ridden bodies alight, so they go up in flames quite nicely if they cannot immediately find a water source to dive into. That may not balance out their beastly strength and the tendency for their muck-covered limbs to cause terrible infections, but any concession you can take with these monsters is quite worth it."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 13
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 7
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 13,
        "peril_threshold": 10,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Advanced",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 20,
            "eavesdrop": 20,
            "intimidate": 20,
            "resolve": 20,
            "simple_melee": 30,
            "simple_ranged": 30,
            "stealth": 30,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Thrashing Tentacles",
                "chance": 80,
                "distance": "melee engaged or 9 yards",
                "damage": "13",
                "qualities": [
                    "Powerful",
                    "Reach"
                ]
            }
        ],
        "traits": [
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Bloody Flux)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FLAILING TENTACLES",
                "description": "These creatures can attack up to 9 foes in an Engagement with one Attack Action. In addition, their tentacles have the Reach Quality, able to strike foes up to 3 yards away."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "HERP DERP",
                "description": "These creatures are easily distracted. When their Turn starts, they must succeed a Resolve Test or else lose 1 AP."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Remnant trappings of undigested victims (3)"
        ]
    },
    {
        "type": "CHIMAERA",
        "family": "BEAST",
        "description": [
            "The Chimaera is a legendary beast, best known for its distinctively monstrous and confusing form. However, the Chimaera is not a singular type of creature, but rather a whole designation of bestial creatures which display incongruent traits to those found in nature.",
            "Chimaeras are thought to be the blessed avengers of the Demiurge's wrath. The most common Chimaera has a leonine body with goatish hind-legs, bristling with muscle, fur and sores. They have three heads on their broad shoulders  one of which is an angry dragon, one of a roaring lion and one of a braying goat. Their long tail ends in a spiked tine, venom drools from their elongated fangs while bat-like wings extend from their shoulders. However, this is only an example of one form that a Chimaera can take  Chimaeras have been found with as many as eight heads, possessing the body of creatures like a rhinoceros with the flexibility of a serpent. The heads all have their own independent brains and they will often fight over food or mates when not in combat  however, they work terribly well together when the time comes to hunt their prey.",
            "Chimaeras are the war beasts of choice for the Chosen of Chaos, chained and starved to be the most ferocious when brought into battle. They also breed them, selling the brood to unscrupulous lords to add to their terrible menageries. For incumbent summoners, their hearts hold the very secret to draw Ætheric Spirits to the Material Realm. If a Chimaera is ever encountered in the wild, they are extremely territorial and will not hesitate to kill anything that threatens them. But every storm has a lining  Chimaeras are surprisingly parental, raising their egg-birthed broods very carefully for nearly a decade before releasing them on their own. If you think a territorial Chimaera is dangerous, imagine one that is protecting its young."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 12
            },
            "agility": {
                "base": 40,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            },
            {
                "value": 16,
                "type": "fly"
            }
        ],
        "damage_threshold": 13,
        "peril_threshold": 9,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 70,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 20,
            "guile": 20,
            "intimidate": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Ragged Claws",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "12",
                "qualities": [
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Spit Fire",
                "chance": 75,
                "distance": "ranged 9 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            },
            {
                "name": "Spiked Tail",
                "chance": 75,
                "distance": "melee engaged or 1 yard",
                "damage": "12",
                "qualities": [
                    "Reach",
                    "Slow",
                    "Powerful"
                ]
            }
        ],
        "traits": [
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "FIERY RETRIBUTION",
                "description": "These creatures can use Spit Fire as an Opportunity Attack without having to Load."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "NATURAL ARMOR (1)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "POISONOUS BITE",
                "description": "When these creatures deal Damage, roll 1D6 Chaos Die. If the result is face '6', they inject Spider Venom into their foe."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "SPIT FIRE",
                "description": "These creatures can use their breath as a ranged weapon. This allows then to strike a single foe within 3+[PB], as the foe suffers 2D10+2 Damage from fire. A foe can attempt to Dodge Spit Fire or Parry it with a shield. Spit Fire can be used while Engaged with foes."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition, they ignore the effects of Hard Terrain."
            },
            {
                "name": "WEAK SPOT (Legs)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Chimaera's heart",
            "Chimaera's hide",
            "Spiked Tail"
        ]
    },
    {
        "type": "COCKATRICE",
        "family": "BEAST",
        "description": [
            "Dragons are a rarity thankfully, but that doesn't stop the populace from declaring every vaguely reptilian beast to be one of those serpentine demons. One of the dragon-like legion is the laughable, yet still dangerous, Cockatrice.",
            "Cockatrices are often the familiars of witches, strange hybrid creatures that are as affectionate and loyal as any hound  albeit far more diabolical. Cockatrices are created by having a hen incubate a Wyvern's egg, strangely causing the embryo within the egg to naturally mutate. A Cockatrice eventually emerges, it's body resembling a scaly Wyvern, but its head being that of a rooster. They don't grow much larger than a chicken, but Cockatrices are savage and prone to rapacious violence. Their vision is also extremely specialized, so much so that their gaze causes their victim to be frozen in a statuelike rigidity. This is not for nutrients, as Cockatrices often feed on vermin and insects  instead, it makes a convenient way for their master to capture a person without all the screaming and general hassle of abducting an animated target. The Cockatrice's only desired reward is raw rats and a coo of affection.",
            "To tell the story of the lap dog chicken-snake to Lowborn dirt farmers may illicit a laugh, but the adventuring-minded know just how dangerous these creatures can be. Their beaks and talons are razor sharp and if they are found in the wild, they hunt in cooperative packs that can easily swarm even the most veteran of fighters. They are fast and surprisingly cunning, able to swoop from above or snap at tendons below. The only positive is that they are quite dumb aside from that, easily suffering from sensory overload if they are ever attacked from multiple sides. Cockatrices are frighteningly quite common and are often pets of nascent warlocks or wizards who have only just begun to dabble in the black arts of Magick."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 35,
                "bonus": 5
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            },
            {
                "value": 14,
                "type": "fly"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 45,
            "qualities": []
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "guile": 10,
            "intimidate": 10,
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Sharp Beak",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "EYES WIDE SHUT",
                "description": "When this creature's eyes are successfully struck with a Called Shot (otherwise treated as their head), they lose their ability to use Petrifying Gaze."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "HERP DERP",
                "description": "These creatures are easily distracted. When their Turn starts, they must succeed a Resolve Test or else lose 1 AP."
            },
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to succeed at Skill Tests."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "PETRIFYING GAZE",
                "description": "When foes are Engaged with this creature (and the creature can see them), they must Resist with a successful Awareness Test or be Petrified. While Petrified, they are left Helpless and unconscious. Petrification lasts until the heart of the creature is pulverized into a bloody concoction and poured over the victim with a successful"
            }
        ],
        "trappings": [
            "Cockatrice hide",
            "Cockatrice heart"
        ]
    },
    {
        "type": "DRAGON TURTLE",
        "family": "BEAST",
        "description": [
            "Dragon Turtles have thought to be very distantly related to dragons, hence the namesake. When spied from below the surf, they are as large as a cottage; squat yet very powerful. Their legs are more like flippers than actual claws and their back is a spiney shell that juts out of the water as they swim. Their head is smooth and beaked, with a terrible crushing power that they use with a crocodilian death-roll to rip their prey asunder. Dragon Turtles spend most of their time deep in the ocean, feeding on phosphorescent fish, squid and the occasional shark. Though they cannot breathe underwater, they can hold their breath up to an hour with little effort. Dragon Turtles only breach the surface to walk upon the shore in order to lay their clutch of eggs. If left alone during this time, they are harmless and will return to the sea when their brood hatches. Unfortunately for them, Dragon Turtle eggs are considered a delicacy in many parts of the world, so the Dragon Turtle fatality count is higher than you would think.",
            "Dragon Turtles are vicious when provoked. They can exhale scalding steam from their throats and their natural strength and armor are obviously helpful. However, they are also quite intelligent and able to telepathically communicate  not in any language, but rather in impressions and feelings. Storms, however, seem to whip something up in their minds  during bad typhoons, they will sunder ships and devour their crews for no reason. Some Elven scholars believe the Dragon Turtles are what kept the Siabra oath-breakers away from their home for so long."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 12
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 9
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 16,
        "peril_threshold": 12,
        "parry": null,
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 20,
            "guile": 20,
            "intimidate": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Snapping Jaw",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "12",
                "qualities": [
                    "Punishing",
                    "Slow",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "CHOMP",
                "description": "When foes Parry this creature's melee attack, it can make an Opportunity Attack at this same foe."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "MONSTROUS BELLOW",
                "description": "When these creatures successfully use a Litany of Hatred, they inflict 2D10+[BB] physical Peril."
            },
            {
                "name": "NATURAL ARMOR (4)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "SWALLOW WHOLE",
                "description": "When these creatures make a successful Chokehold, they deal Damage from a barehanded weapon and force a foe to Resist with a Toughness Test or be Swallowed. While Swallowed, the foe cannot use any Actions, but can attempt to Resist again at the beginning of their Turn to escape. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNNATURAL VISCERA",
                "description": "These creatures are immune to attacks made with ranged weapons. However, they cannot use any Movement Action that requires 3 APs."
            },
            {
                "name": "WEAK SPOT (Legs)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Dragon turtle egg (3)",
            "Dragon turtle hide"
        ]
    },
    {
        "type": "FEN WYRM",
        "family": "BEAST",
        "description": [
            "Bog Behemoths, brigands and disease are more than enough to worry any man who treks through fetid swamps. But something even worse dwells in the muck  the terrible Fen Wyrm that scholars are not entirely convinced isn't just a dragon without its wings.",
            "Fen Wyrms are draconic creatures, roughly the same size and shape as a young dragon. Their heads are massive and broad, so heavy that their necks sweep downward rather than upward. They do not possess wings, but their scales are tough and naturally colored for camouflage in their murky environments. Their hide is severely ridged and slung low to the ground, giving them a low center of gravity that makes them all but unmovable. They are naturally suited to their swamps and bogs, burrowing into a muck-filled habitation. Luckily, Fen Wyrms are eternally stupid and only act on instinct; further, their massive steps and braying betray their coming from yards away. Fen Wyrms favor eating large mammals and can consume multiple tons of food a day without growing lethargic. Their metabolisms are extremely high, being able to eat nearly anything they can wrap their jaws around.",
            "Fen Wyrms usually dwell with a mate, so where there is one, there is often another. They are fiercely protective of their families and will kill any other Fen Wyrm or creature if it looks appetizing enough or wanders too close. Fen Wyrms have a tendency to hoard the shiny objects that drop out of their prey's pockets, using them often as a nest for their young neonates. Many have tried to attack Fen Wyrms for their hoards, mostly on the rumor that they are not as powerful as aged dragons, but that has resulted in many bone-strewn mires and will-o-wisps fluttering near their fetid cave entrances."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 6
            },
            "brawn": {
                "base": 45,
                "bonus": 9
            },
            "agility": {
                "base": 40,
                "bonus": 7
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 6,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 13,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "stealth": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Powerful Bite",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "AUTOTOMY",
                "description": "When these creatures would be Slain!, they immediately break off their tail, remaining Grievously Wounded and can use any Movement Action for 0 APs. Should they suffer Damage again, they are permanently Slain!."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "DEATH ROLL",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a Chokehold."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "NATURAL ARMOR (4)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "SWALLOW WHOLE",
                "description": "When these creatures make a successful Chokehold, they deal Damage from a barehanded weapon and force a foe to Resist with a Toughness Test or be Swallowed. While Swallowed, the foe cannot use any Actions, but can attempt to Resist again at the beginning of their Turn to escape. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNNATURAL VISCERA",
                "description": "These creatures are immune to attacks made with ranged weapons. However, they cannot"
            }
        ],
        "trappings": [
            "Poison sack (as a dose of Animalbane) (3)",
            "Poison sack (as a dose of Beastbane) (3)",
            "Poison sack (as a dose of Folkbane) (3)"
        ]
    },
    {
        "type": "FODDERLING",
        "family": "BEAST",
        "description": [
            "One of the most bizarre beasts of the known world, Fodderlings nearly seem like mobile, bouncing balls of rubber from a distance. However, when they get close, pray you are able to kill them before these vicious beasts fall upon you.",
            "Fodderlings are bipedal, subterranean creatures that appear to be a mole rat or a leech, but not quite either. They are vicious and fast, living in large broods in the deepest part of caverns. There is evidence that Fodderlings may be fungoid creatures, but the dead are often dragged away by Orx parties before they can be examined. Fodderling breeds differ much like dogs, identified by how many rows of teeth, color of their leathery skin and prominence of their horns. Due to their general small stature and quicksilver speed, they are the chosen mount of roving Goblin gangs, as well as attack beasts in the Orx WAAAR! Hordes. Fodderlings are carrion eaters, devouring the corpses of those slain by their riders. The more they consume, the larger they grow in size; a whelp can be no bigger than a small but vicious dog, while a full grown adult could easily support the weight of a Hobgoblin. When not used as mounts, Fodderlings are a convenient food source and potential catapult ammunition for Orx WAAAR! Hordes.",
            "Fodderlings are unintelligent, even less so than an ordinary herd of cattle and nigh untrainable. They exist only to eat or be eaten. Their Goblin masters assemble them into massive herds and then send them to attack, both actions requiring violence, not subtle coaxing. Riders of Fodderlings will often have to dangle food on a stick in front of the creature's face, just to prevent it from bucking the rider and devouring them. Their breeding habits are odd, apparently only breeding in the darkest parts of caves under absolute darkness. Newborn Fodderlings are still just as vicious as their parents, able to devour an Ogre's hand in one bite. These creatures are true pests, as their massive bulk tramples farmlands and their savage attacks killing cattle. Many nations have bounties specifically on Fodderlings, both in hopes to reduce the damage they cause and to waylay Goblins. It also helps that Fodderlings do taste rather good, often compared to fine pork; despite that, many taverns that serve it would never mention that their pig roast is actually Fodderling bunghole (as it is the only meat upon a Fodderling worth eating, the remainder tasting of bile)."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 35,
                "bonus": 3
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "intimidate": 10,
            "simple_melee": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Vicious Bite",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Slow",
                    "Vicious",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage,"
            }
        ],
        "trappings": [
            "Fodderling hide",
            "Fodderling meat (3)",
            "Fodderling teeth (9)"
        ]
    },
    {
        "type": "GRYPHON",
        "family": "BEAST",
        "description": [
            "Gryphons are legendary beasts in their own right, their majesty and power used as a symbol on many a military banner. Some armies are said to have even tamed one as a mounts, but their ferocity could maybe prove otherwise.",
            "Gryphons are avian creatures, roughly twice the size of the largest destrier. Their head and front legs are of those of a majestic eagle, complete with plumage, razor sharp talons and a crooked beak. Their body and hindquarters are either those of a lion or of an equine  those with a horse body are often called 'hippogryphs', though the two types often interbreed. From their shoulders sprout a massive pair of eagle-like wings, easily spanning twenty-five feet from tip to tip and casting a huge shadow wherever they take to the air. Gryphons are solitary creatures who live on the highest mountain peaks, where they dwell with a mate and tend to their hatchlings. They hunt by swooping down on their prey, grappling them into their talons and returning to the nests to feed them live to their young. Their favored quarry are horses, cattle and other domesticated animals. Being noble creatures, they rarely  if ever  eat humanoids. But, they will defend their nests to the death.",
            "Gryphons have a natural enemy in the Harpies, who intrude upon their territory and try to steal their hatchlings. Likened to pests, their mountainsides are often littered with the broken bodies of Harpies and the remains of Gryphon chicks, sometimes called 'sky graveyards'. Gryphons have an upper hand because they are doubly smarter than Harpies, with almost human-like cleverness and sensibilities. These qualities have sent many royal Animal Tamers to wrangle a mount for their lord and some have been successful, passed down in song such as Greywind the Swift, Falkor the Luckdragon and Balthazar, Scourge of the Plains. But this is not the norm  Gryphons are free-spirited and do not want to be caged, escaping with every opportunity if they are not provided with the freshest of game and choicest of mates."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 7
            },
            "agility": {
                "base": 40,
                "bonus": 12
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 18,
                "type": "normal"
            },
            {
                "value": 21,
                "type": "fly"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 9,
        "parry": null,
        "dodge": {
            "chance": 80,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 20,
            "guile": 20,
            "intimidate": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Talons",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "12",
                "qualities": [
                    "Finesse",
                    "Slow",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "CHAMPION'S CALL",
                "description": "One foe is left Defenseless to all this creature's attacks, until the foe is defeated. The creature may select a new foe once the current one is defeated."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DEATH ROLL",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a Chokehold."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "NATURAL ARMOR (1)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "SNIKT! SNIKT!",
                "description": "These creatures may spend 3 APs to attack twice with melee weapons."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition, they ignore the effects of Hard Terrain."
            },
            {
                "name": "STRAFING TALONS",
                "description": "When these creatures execute a successful attack while flying, they also deal 1D10+[AB] physical Peril."
            },
            {
                "name": "WEAK SPOT (Wings)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Gryphon egg (3)",
            "Gryphon hide"
        ]
    },
    {
        "type": "HARPY",
        "family": "BEAST",
        "description": [
            "The only thing worse than a thief is one you cannot catch. The only thing worse than that is the Harpy, a wicked creature who cackles and schemes for its awful treasures.",
            "Harpies are horrific beasts, with bodies that look like the combination of an old woman and a corvid, such as a crow, jackdaw or raven. Their flesh is emaciated and drawn out, their arms being underlined with oily black wings that allow them surprisingly quick flight. Their noses are crooked and their eyes beady, perfectly attuned to spot and feast on small prey such as rats or snakes, only ever hunting at night. What makes them most dangerous is their propensity for theft and murder  they will often swoop on travelling caravans at night to steal up any shiny object that catches their eyes. The more heavily guarded treasures are dealt with more deceptively. They'll sing ribald songs  though they only imitate and do not know their meaning  to attract travelers, eventually ripping at their throats and sorting through their purses for the most intriguing treasures. Appropriately enough, Harpies are thought to be distant relatives of the Siren, perhaps from a single ancestor that split off millennia ago.",
            "Although Harpies are the bane of wanderers, they also have a mortal enemy in the Gryphons. The two beasts often make nests on adjacent mountains, staring at each other in malice. Often, Harpies are too busy infighting with their nest mates to do anything, but will sometimes band together to steal Gryphon chicks or baubles dropped by a Gryphon's victims. These aerial battles are both breathtaking and terrifying to behold, as Harpies must swarm a singular Gryphon if they wish for any chance of victory. Gryphons will generally have the upper hand, but some ravaged nests of those beasts prove that sometimes even the Harpies win."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 5
            },
            "agility": {
                "base": 45,
                "bonus": 8
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 14,
                "type": "normal"
            },
            {
                "value": 17,
                "type": "fly"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 7,
        "parry": {
            "chance": 65,
            "qualities": []
        },
        "dodge": {
            "chance": 75,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "charm": 20,
            "coordination": 20,
            "eavesdrop": 10,
            "resolve": 10,
            "simple_melee": 20,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Ragged Talons",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Finesse",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "CAPTIVATING CRY",
                "description": "When foes can hear this creature, they must Resist with a successful Resolve Test or be drawn towards the sound. Should they enter a dangerous area to find the sound, they can attempt another Resolve Test. Once they are able to visually see the creature, the Captivating Cry's effects end."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "STRAFING TALONS",
                "description": "When these creatures execute a successful attack while flying, they also deal 1D10+[AB]"
            }
        ],
        "trappings": [
            "Harpy's treasures (9)"
        ]
    },
    {
        "type": "HYDRA",
        "family": "BEAST",
        "description": [
            "Dwelling in the coves along the ocean shores, the Hydra emerges rarely, often to dive into the waters to hunt for their meals of Sirens and lonesome sailors. When it materializes, all along the shore flee in terror at the creature's undulating form.",
            "Hydras are thick, serpentine creatures, who rearing up can easily match the height of a Nephilim. Sprouting from their torsos are multiple, thick necks, each one ended in a writhing and spitting reptilian head. Their skulls look much like any other serpent, but their snouts are tipped with beaks instead of soft scales. Each head of the Hydra is independent and able to attack on its own and one can be decapitated without causing the beast to fall. Supposedly, Hydras can also grow back any heads they lose, though the process takes multiple years and the head is worthless while growing. In addition to their bulk and sharp beaks, Hydras also have the capacity to spew fire from their mouths, cooking their meal before gorging themselves upon it. They are fierce predators, becoming more dangerous as they grow injured, risking every undulating limb to simply destroy whatever intends them harm.",
            "Hydras are insular by themselves, but have been known to be tamed and harnessed by the Chosen of Chaos to use as living siege weaponry. They are terrifying and effective foes, seemingly endless in the number of heads they have and tireless in battle. Hydras are very long lived by themselves and clever, some even attack trading vessels to get at their stores of fish in their holds. Other times, less educated communities take up the Hydra as a squamous god, one they shower with gifts and sacrifices. These worshipped Hydras are often very old and powerful and rather admire their position of luxury and bounty. Not surprisingly, Hydras of this age also have the ability to speak the common  albeit forked  tongue."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 10
            },
            "agility": {
                "base": 50,
                "bonus": 9
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 7
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            }
        ],
        "damage_threshold": 15,
        "peril_threshold": 10,
        "parry": null,
        "dodge": {
            "chance": 80,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 20,
            "guile": 30,
            "intimidate": 20,
            "scrutinize": 30,
            "simple_melee": 30,
            "stealth": 20,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Terrible Teeth",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Vicious"
                ]
            },
            {
                "name": "Spit Fire",
                "chance": 75,
                "distance": "ranged 9 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            }
        ],
        "traits": [
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Bloody Flux)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "IN THE FACE",
                "description": "These creatures can only be harmed by melee and ranged weapons by using a Called Shot to the head."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "NATURAL ARMOR (5)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "SNIKT! SNIKT!",
                "description": "These creatures may spend 3 APs to attack twice with melee weapons."
            },
            {
                "name": "SPIT FIRE",
                "description": "These creatures can use their breath as a ranged weapon. This allows then to strike a single foe within 3+[PB], as the foe suffers 2D10+2 Damage from fire. A foe can attempt to Dodge Spit Fire or Parry it with a shield. Spit Fire can be used while Engaged with foes."
            },
            {
                "name": "UNBRIDLED RAGE",
                "description": "When these creatures are Seriously or Grievously Wounded, add an additional 1D6 Fury Die to"
            }
        ],
        "trappings": [
            "Hydra hide"
        ]
    },
    {
        "type": "JABBERWOCKY",
        "family": "BEAST",
        "description": [
            "Burbling through the tulgey wood wiffles the manxome Jabberwocky, a beast so absurd that is must either be the product of random chance or a cruel trick by the gods to put people in a kerfuffle.",
            "The only word to describe a Jabberwocky is obtuse. Their body looks like that of an old, hairless sloth, from which sprouts their neck, four gangly limbs, a kangaroo-like tail and two stunted wings. The neck is long and serpentine, crowned with a fish-like head possessing long antennae, donkey-teeth and lidless eyes. Its tail is long, furred and curls humorously, though it is prehensile and can be used to tangle up foes. Its hands end in long claws, but they are dull as horse hooves and only used for grabbing and scooping  save one digit that is incredibly cumbersome yet sharp and vicious. Their bipedal legs are much like those of a lizard and in fact their arms and legs are both comically scaled. They are far too heavy to fly, but their wings are expressive and flap when the Jabberwocky is excited or threatened. They prey on all manner of woodland animals, but their awkward shapes and movements leave them with extended periods where they border on starvation.",
            "Jabberwocky are intelligent and capable of speech, though they speak in extended nonsense soliloquies. They'll scratch cartoonish drawing inside their cavernous lairs using their 'Vorpal Claw', depicting people and animals engaged in all manner of unseeming pleasures. They favor games and riddles  both of which they are very good at  but they are quite dull in a debate or any other mental pursuit. Jabberwockies are hard to kill, their skin almost rubber-like in quality while their limbs able to regenerate even if sundered. There are a fortunate few of them catalogued in the world and while there is no evidence that they can breed, Jabberwocky will say they've lived upteenths of turmings, whatever that means. Jabberwockies are not inherently hostile, but they will lash out at anyone who refuses to entertain them with a game or riddle. Some have gone mad through their periods of starvation, making their bizarre forms even more alien. The only reason to actively hunt a docile Jabberwocky is its singular claw  legend says they can be sharpened into a 'vorpal sword', a blue-grey blade that can snicker-snack any bandersnatch or jubjub bird. Other tales of the vorpal sword seem to prefer the tried and true tradition of it being used to snicker-snack a head off another, until it goes plip-plop onto the ground."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 45,
                "bonus": 10
            },
            "agility": {
                "base": 35,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 10
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 13,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 12,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 35,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "eavesdrop": 30,
            "gamble": 10,
            "guile": 30,
            "intimidate": 30,
            "scrutinize": 20,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Dull Hoofs",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Pummeling"
                ]
            },
            {
                "name": "Vorpal Claw",
                "chance": 75,
                "distance": "melee engaged or 1 yard",
                "damage": "10",
                "qualities": [
                    "Punishing",
                    "Reach",
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Lashing Tail",
                "chance": 75,
                "distance": "melee engaged or 6 yards",
                "damage": "10",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            },
            {
                "name": "Wall Crawler Grasp",
                "chance": 45,
                "distance": "ranged 13 yards",
                "load": "1 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "DISEASE-RIDDEN (Bloody Flux)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FECKLESS RUNT",
                "description": "When this creature's Turn begins, roll 1D6 Chaos Die. If the result is face '6', they elect to attack that Turn with a senseless object that does no Damage."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "KILL IT WITH FIRE",
                "description": "Only after these creature's remains are set On Fire are they forever Slain!."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "NATURAL ARMOR (2)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "POISONOUS BITE",
                "description": "When these creatures deal Damage, roll 1D6 Chaos Die. If the result is face '6', they inject Spider Venom into their foe."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition, they ignore the effects of Hard Terrain."
            },
            {
                "name": "SWALLOW WHOLE",
                "description": "When these creatures make a successful Chokehold, they deal Damage from a barehanded weapon and force a foe to Resist with a Toughness Test or be Swallowed. While Swallowed, the foe cannot use any Actions, but can attempt to Resist again at the beginning of their Turn to escape. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNGAINLY",
                "description": "When these creatures are Slain! all those Engaged with it must succeed at a Coordination Test or be knocked Prone beneath of it, suffering 3D10+3 Damage from falling."
            },
            {
                "name": "WALL CRAWLER",
                "description": "These creatures can crawl upon both vertical and horizontal surfaces with ease. In addition, they can initiate a ranged Chokehold at a Distance of 3+[PB],"
            }
        ],
        "trappings": [
            "Vorpal Claw (Zweihnder with Castle-forged & Vicious Qualities)"
        ]
    },
    {
        "type": "SIREN",
        "family": "BEAST",
        "description": [
            "You only need to hear the tales of drunken sailors, commiserating over stories of fellow seamen diving into the ocean to never return, to at least lend credence to the myth of Sirens.",
            "Sirens are foul aquatic beings, from the waist up appearing at a distance as comely maidens or men but in fact are horrendous. These beasts play at pretending to be stranded on rocks, tangled in fishing nets or caught under kelp. If a sailor ever approaches to help a Siren, their true form is revealed. The Siren maintains the bulk of its body under the water to belie its bestial origins. Below the water, it has a long, serpentine tale, with manta-like wings attached to the side that aid in swimming and short bursts of flight. Once lured in, the Siren wraps their prey within these fleshy wings, using a proboscis in their abdomen to penetrate the victim's spine and extract their brain fluid for nutrition. The corpse of their victims wash up days later, appearing as if they have drowned and been dashed against rocks upon cursory glance  leaving others none the wiser of the Siren's existence. Dwelling in underwater shoals together, they protect their clutches of overgrown roe. Siren are borderline intelligent, able to charm and woo with their cries, but are actually of animal intelligence, akin to sharks.",
            "Sirens have long haunted the waters, singing their clarion calls that bring oarsmen and fishermen to their lustful dooms. Most are found near the shore, but pods of them have been found in the most chthonic of depths. Sirens and Harpies are undoubtedly kin, and sometimes Siren are seen leaving prey on rocky outcroppings for their brethren to pick at. Sirens are also collectors of baubles like their sisters and bring the most choice of treasures into their underwater cloisters. Many an adventuring party has tried different erudite methods to reach these caches below, but most have washed up weeks later with a bloated goat stomach deflated of air and a punctured spine."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 8
            },
            "agility": {
                "base": 45,
                "bonus": 5
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            },
            {
                "value": 14,
                "type": "fly"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 7,
        "parry": {
            "chance": 65,
            "qualities": []
        },
        "dodge": {
            "chance": 75,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "athletics": 20,
            "awareness": 10,
            "charm": 20,
            "coordination": 20,
            "eavesdrop": 10,
            "guile": 10,
            "resolve": 10,
            "simple_melee": 20,
            "survival": 20
        },
        "attack_profile": [
            {
                "name": "Razor Fin",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Pummeling",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "CAPTIVATING CRY",
                "description": "When foes can hear this creature, they must Resist with a successful Resolve Test or be drawn towards the sound. Should they enter a dangerous area to find the sound, they can attempt another Resolve Test. Once they are able to visually see the creature, the Captivating Cry's effects end."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "STINGING TENTACLE",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a"
            }
        ],
        "trappings": [
            "Siren's treasures (9)"
        ]
    },
    {
        "type": "WYVERN",
        "family": "BEAST",
        "description": [
            "The wretchedness of great Pit Dragons may have originally grown their pattern from their stunted cousins in Wyverns, beasts so close in shape as to be almost indistinguishable to the more common Pit Dragon.",
            "Wyverns have a few noticeable differences from their Abyssal counterparts. The first most obvious is that Wyverns only have two limbs, their arms instead being replaced by jointed, leathery wings. Wyverns are also smaller than Pit Dragons, most the size of a large horse or panther. They are also much bulkier than their kin, as their necks and torsos are firm muscle rather than obsidian scales. While they are unable to exhale flames, Wyverns have barbed tails that drips incessantly with venom. That does not make them any less dangerous  Wyverns are adroit hunters that share several qualities with aerial raptors. They can solve basic problems, have excellent perception and prefer to dive bomb their meals or drop heavy stones upon them. Some Wyverns have a taste for humanoids, but most feast on the easy and meaty livestock found on any farm. When confronted, most Wyverns will try to escape  they will only fight if they have no other means of escape, using intimidation first before striking.",
            "Much like Gryphons, Wyverns have been tamed as mounts, though often more by Mutants than the Chosen of Chaos. A few famous Orx Bigbosses have ridden Wyverns into battle, armored with scrap-heap barding and a mish-mash of buckles and leather straps. They provide a great aerial advantage, as well as fooling humans into thinking the Mutant is borne upon an actual Pit Dragon  a tactic that has won more than a few battles. Wyverns, however, are generally short lived  they lay their first clutch at about a year and die naturally within ten years. They also take wounds terribly, as their wings are weak spots prone to great damage."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 7
            },
            "agility": {
                "base": 50,
                "bonus": 9
            },
            "perception": {
                "base": 45,
                "bonus": 8
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 7
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 11,
        "movement": [
            {
                "value": 15,
                "type": "normal"
            },
            {
                "value": 18,
                "type": "fly"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 10,
        "parry": null,
        "dodge": {
            "chance": 80,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 20,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Claws & Teeth",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "7",
                "qualities": [
                    "Pummeling",
                    "Vicious"
                ]
            },
            {
                "name": "Lashing Tail",
                "chance": 75,
                "distance": "melee engaged or 3 yards",
                "damage": "9",
                "qualities": [
                    "Fast",
                    "Finesse"
                ]
            }
        ],
        "traits": [
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "LASHING TAIL",
                "description": "These creatures can attempt an Opportunity Attack with their tail at the end of their Turn."
            },
            {
                "name": "LICK YOUR WOUNDS",
                "description": "These creatures may spend 1 Misfortune Point to move three steps up the Damage Condition Track positively."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "NATURAL ARMOR (2)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "POISONOUS BITE",
                "description": "When these creatures deal Damage, roll 1D6 Chaos Die. If the result is face '6', they inject Spider Venom into their foe."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "WEAK SPOT (Wings)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Poison sack (as a dose of Animalbane) (3)",
            "Poison sack (as a dose of Beastbane) (3)",
            "Poison sack (as a dose of Folkbane) (3)"
        ]
    },
    {
        "type": "DREAD COUNT",
        "family": "CHOSEN OF CHAOS",
        "description": [
            "There are only a handful of mortal agents that the Abyssal Princes hold dear. They have a myriad of names: Brood-ThatRules, The Chosen and Black Sepulchre, among others. But no matter what they call themselves, these Dread Counts are some of the most terrible and powerful creatures that have ever existed, their potential just short of that of a Higher Demon.",
            "Dread Counts are a terrible hybrid of two Chosen of Chaos: a Fell Knight and a Havoc Conjuror. When an Abyssal Prince has decided to show favor to the chosen, the two beings undergo a twisted and painful ceremony. Lasting days, involving self-mutilation, ingesting hallucinogens and inhaling Wytchstone Essence, the two lieutenants finally fuse together into a massive mountain of demon-born humanoid flesh. Their dark, infernal armor becomes permanently and grotesquely fused to their skin and upon their forehead is emblazoned the 'Mark of Chaos' of their favored Prince. Beyond that, their forms are horribly mutated and their knowledge of both warfare and Magick unparalleled. The new Dread Count speaks in a layered voice, its two halves both separate and as one. Dread Counts are few in number and in fact they only appear every handful of decades. But those that do have the ability to unite the warring hordes to potentially wipe the land clean of all that is right and human. If not, they often lead a horde of mutated beings all their own and are treated as unto demigods  proof of their Abyssal Prince's endless love.",
            "The Dread Count only aspires to higher and higher positions of power. If they do right by their Princes and slaughter to their satisfaction, the Prince will wrap them in their fetid embrace and transmute them into the blessed form of a Higher Demon. However, should the Dread Count curry disfavor with the Princes, they will smite them at the wave of a hand  throwing them down into the lowest layers as a Shimmering Mimic, cursed to spend the rest of their days as a wretched protoplasm or endure the eternal climb to demonhood at the bottom of the pit once again. Thus all Dread Counts will exalt the honor and actions of all the Princes  making them dangerous foes indeed.",
            "Rumor also states that at times, the Princes will invest a large majority of their power into a single Dread Count. This being, often called 'The Usher', is fabled to unite the hordes of Corruption and lead them to victory over the Material Realm. Rumor also states that at times, the Princes will invest a large majority of their power into a single Dread Count. This being, often called 'The Usher', is fabled to unite the hordes of Corruption and lead them to victory over the Material Realm. Rumor also states that at times, the Princes will invest a large majority of their power into a single Dread Count. This being, often called 'The Usher', is fabled to unite the hordes of Corruption and lead them to victory over the Material Realm."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 55,
                "bonus": 10
            },
            "brawn": {
                "base": 50,
                "bonus": 6
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 50,
                "bonus": 6
            },
            "intelligence": {
                "base": 50,
                "bonus": 11
            },
            "willpower": {
                "base": 50,
                "bonus": 8
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 11,
        "parry": {
            "chance": 95,
            "qualities": [
                "Defensive",
                "Protective"
            ]
        },
        "dodge": null,
        "notch": "Elite",
        "risk_factor": "Unique",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "folklore": 20,
            "handle_animal": 10,
            "incantation": 30,
            "interrogation": 10,
            "intimidate": 30,
            "leadership": 20,
            "martial_melee": 30,
            "navigation": 10,
            "ride": 30,
            "scrutinize": 20,
            "simple_melee": 10,
            "toughness": 30,
            "warfare": 20
        },
        "attack_profile": [
            {
                "name": "Morgenstern",
                "chance": 85,
                "distance": "melee engaged",
                "damage": "11",
                "qualities": [
                    "Adaptable",
                    "Powerful",
                    "Vicious"
                ]
            },
            {
                "name": "Steel Staff",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "11",
                "qualities": [
                    "Adaptable",
                    "Pummeling"
                ]
            },
            {
                "name": "Military Lance",
                "chance": 85,
                "distance": "melee engaged or 1 yard",
                "damage": "11",
                "qualities": [
                    "Reach",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "EVER-CHOSEN",
                "description": "When these creatures are encountered, it is clear which of the Abyssal Princes they worship from the marks they bear and the particular gift of darkness they have been given). Those who worship the Abyssal Prince of Decay can inflict a random Disease upon a foe when they generate a Fury Die. Those who worship the Abyssal Prince of Change can Critically Succeed at casting one Petty, Lesser and Greater Magick without having to make an Incantation Test. Those who worship the Abyssal Prince of Pleasure can use their Possession ability to control a foe for hours, instead of minutes. Those who worship the Abyssal Prince of Violence add a 1D6 Fury Die to all Damage they do, regardless of whether it is from weapons or Magick."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "NATURAL ARMOR (5)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "OCCULT MASTERY",
                "description": "When this creature Channels Power and succeed at their Skill Test, they Critically Succeed instead."
            },
            {
                "name": "POSSESSION",
                "description": "These creatures can use Skin Guest from the Arcana of Sorcery without having to make an Incantation Test. In addition, they do not have to maintain Concentration."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these creatures provoke one of the three brands of"
            },
            {
                "name": "Madness",
                "description": "if they are of the Basic Risk Factor, they provoke Stress If they are of the Intermediate Risk Factor, they provoke Fear. Finally, if they are of the Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "WINDS OF CHAOS",
                "description": "When casting Generalist Magick  at their option  these creatures can automatically succeed at the Incantation Test, but must drop one step down the Peril Condition Track negatively. In addition, they must always add 1"
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Metal shield",
            "Military lance",
            "Morgenstern",
            "Reagents for all Magick spells (9)",
            "Steel staff",
            "Infernal holy symbol",
            "Wilderness cloak",
            "Wytchstone Essence (9)"
        ]
    },
    {
        "type": "DVERGAR",
        "family": "CHOSEN OF CHAOS",
        "description": [
            "There were once a clan of Dwarves, who upon digging through their mountains found a huge cache of Wytchstone shards. They immediately struck it out of the ground to harvest and the damage was done: the energies of the arcane seeped into their brains, making them obsessed with the substance. They dug harder and deeper, passing stone and clay in search of more. They eventually dug so far they emerged in the volcanic belly of the earth, but by that time they were too far gone. The whole clan's minds were eaten by the Corruption, their bodies warped by the Wytchstone they crave so dearly. They became xenophobic and militant, eventually becoming so paranoid of their hoards of Wytchstone that they ignited a war with the 'surface' Dwarves that killed thousands. The clan was then struck from every known history book and they became forever known as the Dvergar  a Dwarven word that means 'underdweller'.",
            "Dvergar have been tinged by the Wytchstone and their magma-filled fortresses  their beards are black and ashen, their skin dusky with the texture of basalt. Their eyes are a piercing red and tusks grow from their jaws. Dvergar specialize in gunpowder, as they need it to blast through the hard rock they mine in. When Dvergar do take to the surface, they function as bombardiers for the armies of Corruption  bringing their terrible magma-driven war machines to the front line to launch volleys of lava. Some Dvergar become Havoc Conjurors instead, their exposure to heat being a natural boon. Otherwise, the Dvergar stay within their obsidian ziggurats deep underground, contemplating."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 5
            },
            "agility": {
                "base": 40,
                "bonus": 5
            },
            "perception": {
                "base": 35,
                "bonus": 6
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 7,
        "parry": {
            "chance": 45,
            "qualities": []
        },
        "dodge": null,
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "awareness": 10,
            "bargain": 10,
            "drive": 20,
            "guile": 10,
            "resolve": 10,
            "simple_ranged": 20,
            "stealth": 10,
            "survival": 10,
            "tradecraft": 20,
            "warfare": 20
        },
        "attack_profile": [
            {
                "name": "Knuckleduster",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Pummeling"
                ]
            },
            {
                "name": "Blunderbus",
                "chance": 65,
                "distance": "ranged 9 yards",
                "load": "4 AP",
                "damage": "6",
                "qualities": [
                    "Gunpowder",
                    "Shrapnel",
                    "Volatile",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GUTTER RUNNER",
                "description": "When these creatures attempt to hide in urban and underground environments, they flip the results to succeed at Stealth Tests."
            },
            {
                "name": "HATRED (Goblin)",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "SHOTGUN BANG!",
                "description": "When these creatures successfully strike with a ranged weapon possessing the Gunpowder Quality, they automatically Load their weapon for 0 APs."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth"
            }
        ],
        "trappings": [
            "Blunderbus",
            "Gunpowder & shot (9)",
            "Knuckleduster",
            "Mail armor",
            "Vile holy symbol",
            "Wytchstone Essence (3)"
        ]
    },
    {
        "type": "FELL KNIGHT",
        "family": "CHOSEN OF CHAOS",
        "description": [
            "At times, a Howling Marauder will rise above his peers  unmatched in violence and hatred, having laid waste to hundreds of the sane minded  all to exalt the Abyssal Princes.",
            "The Marauders recognize the terrible power of their fellows and in a terrible ceremony that involves bathing the chosen in the blood of their enemies, they knight them in the terrible energies of the dark lords  turning them into Fell Knights. Fell Knights become lieutenants among the Chosen of Chaos, bedecking themselves in spiked steel armor and carrying axes that could cleave a man in twain just by touch. They are paladins of the Abyssal Princes, devoting themselves first to them and the Dread Count they serve under second. The Princes look with favor upon these dark templars, imbuing them with dark gifts of mutation and corruption. Fell Knights have all the discipline and martial prowess of any knight of the realm, but almost an opposite set of virtues that revolve around destruction, murder and forced conversion. To uphold these unholy tenants is to be closer to the Princes.",
            "Fell Knights may be some of the most powerful of mortal Chosen of Chaos, but they aspire to an even greater status  one of a Dread Count, the Abyssal Princes' chosen children: the brood that rules. Fell Knights will often go on what is called 'The Black Crusade': excursions in the world in order to recover relics, people or locations that could curry favor with their Abyssal masters. They lead large bands of Howling Marauders out of the mountains and down into the occupied valleys, slaying all for terrible glory. Fell Knights are convinced that the more they maim and slaughter, the more the Abyssal Princes will rain blessings upon them and possibly grant them a seat in the endless pit of the Abyss. And given the histories of most known Dread Counts, this seems like the most likely way to achieve their dark goal."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 8
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 6,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 9,
        "parry": {
            "chance": 80,
            "qualities": [
                "Defensive",
                "Protective"
            ]
        },
        "dodge": null,
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "folklore": 10,
            "handle_animal": 10,
            "interrogation": 10,
            "intimidate": 20,
            "leadership": 20,
            "martial_melee": 20,
            "navigation": 10,
            "ride": 20,
            "simple_melee": 10,
            "survival": 10,
            "toughness": 20,
            "warfare": 20
        },
        "attack_profile": [
            {
                "name": "Morgenstern",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Adaptable",
                    "Powerful",
                    "Vicious"
                ]
            },
            {
                "name": "Military Lance",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "9",
                "qualities": [
                    "Reach",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "CHAMPION'S CALL",
                "description": "One foe is left Defenseless to all this creature's attacks, until the foe is defeated. The creature may select a new foe once the current one is defeated."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "LICK YOUR WOUNDS",
                "description": "These creatures may spend 1 Misfortune Point to move three steps up the Damage Condition Track positively."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at"
            }
        ],
        "trappings": [
            "Metal shield",
            "Military lance",
            "Morgenstern",
            "Munitions plate armor",
            "Vile holy symbol"
        ]
    },
    {
        "type": "HAVOC CONJUROR",
        "family": "CHOSEN OF CHAOS",
        "description": [
            "The Abyssal Princes not only grant terrible corruption and madness, but also the gift of fell sorceries. The Abyssal Prince of Change is even believed in some circles to be the source of all Magicks, so it's unsurprising they give powers to their favored thralls.",
            "Thus is birthed the Havoc Conjuror, an arcanist of unrestrained Corruption who has been seen fit to be granted powers from beyond this mortal coil. Havoc Conjurors are always cloaked in an aura of mystery  while Fell Knights may remove their helm to show their true terror, Havoc Conjurors are both covered in heavy plate mail and dark robes  sometimes their eyes are not even visible beneath all the fabric and metal, but the ones that are there whisper with arcane energy. Havoc Conjurors are found worshipping all the Abyssal Princes, save for the Prince of Violence  to Him, Magick is a blasphemy and a weakness. Havoc Conjurors were believed to come about through corruption due to incomplete summoning of demons  resulting in their terrible mutations and slavish worship of them. Havoc Conjurors are rarely alone, as they are often served by an impish familiar and a retinue of Howling Marauders  many even serve side-by-side with Fell Knights as lieutenants beneath a Dread Count.",
            "The arcane might of the Havoc Conjuror is notably different from a mortal practitioner  they draw on the dark powers of the Abyss, learning the Arcana of Pyromancy and Sorcery. But these are not normal Magicks of those types  their Pyromancy is ash and flowing magma, their dark Sorcery is mad and full of chaotic whisperings. Havoc Conjurors are a force to be reckoned with, but they often serve in a more supplementary role on the battlefield. They also serve as ferrymen for the Chosen of Chaos, driving terribly massive battle-wagons that lead the insane folk into the jaws of war. Much like Fell Knights, Havoc Conjurors also aspire to become Dread Counts, though their game is a longer and slower one of building up stores of power rather than carving a path of the dead."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 5
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 50,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 11,
        "parry": {
            "chance": 65,
            "qualities": []
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "bargain": 20,
            "coordination": 10,
            "drive": 20,
            "education": 20,
            "folklore": 20,
            "handle_animal": 20,
            "incantation": 20,
            "intimidate": 20,
            "ride": 10,
            "scrutinize": 10,
            "simple_melee": 20,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Steel Staff",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Adaptable",
                    "Pummeling"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FIREPROOF",
                "description": "These creatures and their possessions are entirely immune to Damage from fire."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GIFT OF DEVILS",
                "description": "These creatures ignore the Heavy Quality of armor to cast Magick."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "WINDS OF CHAOS",
                "description": "When casting Generalist Magick at their option  these creatures can automatically succeed at the Incantation Test, but must drop one step down the Peril Condition Track negatively. In addition, they must always"
            }
        ],
        "trappings": [
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 6 Petty Magick spells",
            "Munitions plate armor",
            "Reagents for all Magick spells (9)",
            "Steel staff",
            "Vile holy symbol",
            "Wilderness cloak",
            "Wytchstone Essence (6)"
        ]
    },
    {
        "type": "HOWLING MARAUDER",
        "family": "CHOSEN OF CHAOS",
        "description": [
            "Living far to the north of civilization, where both the Abyss and the frozen gales bite at your very soul, the Howling Marauders make their savage homes.",
            "Born on horseback, Marauders are barbarians: women and men who have grown in their frost-rimmed villages, taught to hold the Abyssal Princes above all other gods. They are a hardy and powerful people, towering over most other men and being nearly twice as strong. Composed of countless tribes with individual traditions, their one consistency is their devotion to the Corruption made manifest. They raid defenseless northern villages often, storming walls while immersed in hallucinogenic-fueled rages with only a weapon and their armor to defend them against the bite of both cold and steel. They return to their homes with riches and slaves, both to be sacrificed to myriad Abyssal abominations. This is all in hopes that their peers will look favorably upon them and invest them to rise as Fell Knight: the only thing they desire more than glory on the battlefield.",
            "At first sight, Howling Marauders seem to have no culture aside from outright barbarism, but that is not the case. Each tribe has their own nuanced traditions, gods and ideas that set them apart from their clansmen across the valley. Infighting is extremely common, so much so that brother often runs brother through in the heat of battle. Howling Marauders are also slightly open to those who have not tasted Corruption  they don't see them as enemies, but rather mewling babes who have yet to taste the sweet nectar of life. If someone can storm into a Marauder camp, drink their weight in mead, devour a whole boar and bed the first person they see, the raiders will take you in as one of their own without a second word."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 5
            },
            "perception": {
                "base": 35,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 7,
        "parry": {
            "chance": 75,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "athletics": 10,
            "handle_animal": 10,
            "guile": 10,
            "navigation": 10,
            "resolve": 10,
            "ride": 20,
            "simple_melee": 20,
            "simple_ranged": 20,
            "survival": 10,
            "toughness": 10,
            "warfare": 10
        },
        "attack_profile": [
            {
                "name": "Threshing Flail",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "6 or 7 with shield",
                "qualities": [
                    "Adaptable",
                    "Weak"
                ]
            },
            {
                "name": "Francisca",
                "chance": 65,
                "distance": "ranged 9 yards",
                "load": "1 AP",
                "damage": "6",
                "qualities": [
                    "Light",
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "BLITZ",
                "description": "When these creatures Charge, they may flip the results to succeed at their next Attack Action or Perilous Stunt on the same Turn."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "I GOT AXE FOR YOU",
                "description": "After this creature makes an Attack Action with a melee weapon, they can immediately make an Opportunity Attack with any one-handed ranged weapon with the Throwing Quality on the same Turn."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "UNBRIDLED RAGE",
                "description": "When these creatures are Seriously or Grievously Wounded, add an additional 1D6 Fury Die to"
            }
        ],
        "trappings": [
            "Francisca (3)",
            "Hide armor",
            "Red cap mushrooms (3)",
            "Threshing flail",
            "Vile holy symbol",
            "Wooden shield"
        ]
    },
    {
        "type": "NEPHILIM",
        "family": "CHOSEN OF CHAOS",
        "description": [
            "A relic of kingdoms long forgotten, Nephilim are members of an Ancestry in irrevocable decline. Frighteningly tall and massive humanoids that often stand taller than eighteen feet, Nephilim are now a shadow of their once-former glory.",
            "These giants and giantesses have existed for centuries, building up their aged cities in the clouds long before other humanoids knew what fire was. They prospered for many years amidst the mountain homes, believed to be favored by the gods, until they too, were struck by the same tragedy that caused the Ogre's empire to fall and sent them on a vicious warpath. Besieging the Nephilim's sky castles, starving and vicious Ogres ascended to the highest of mountains to camp en masse on their doorstep, demanding food and ale from the insular Nephilim. When the Nephilim refused, the Ogres battered down their walls and slayed the Nephilim indiscriminately, tearing down their castles when the bloodshed was over. The Nephilim's once-formidable sky castles fell to the ground like falling stars. What remained of the towering race spread to the four corners of the lands, a disenfranchised people who live with the guilt of losing everything their ancestors built for them.",
            "In the centuries since, Nephilim as a race have wholeheartedly sunk into their cups, smelling terribly of sour wine, yesterday's meal and piss. They have long memories and they wish to erase the failure of their kingdom by drinking themselves into oblivion. Most are wanderers, wearing little clothing and carrying nothing more than their ever-present sack that contains assorted bits of food, treasure and interesting dead things. Though not inherently stupid, many Nephilim tend to be on a par with Ogres or Orx when it comes to brains. Unfortunately, their fall from the heavens has sent their race into a spiral of depression and addiction to booze, a trait which has been carried from generation to generation. Despite being in a near-constant drunken stupor, Nephilim are surprisingly perceptive and fairly quick, though their massive strides may explain the latter. They are also obviously extremely strong and vigorous  displayed by their favorite weaponry of tree trunks and boulders  but also tend towards bouts of terrible anger if they are ever harmed. They also naturally have an intense hatred of Ogres, as those creatures are the ones that sent the Nephilim so low."
        ],
        "size": [
            "Large",
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 14
            },
            "agility": {
                "base": 35,
                "bonus": 7
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 15,
        "peril_threshold": 9,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 35,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 30,
            "bargain": 30,
            "eavesdrop": 30,
            "gamble": 30,
            "intimidate": 30,
            "rumor": 30,
            "simple_melee": 30,
            "simple_ranged": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Bare-handed",
                "chance": 75,
                "distance": "melee engaged or 1 yard",
                "damage": "14",
                "qualities": [
                    "Pummeling",
                    "Reach",
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Tree Trunk",
                "chance": 75,
                "distance": "melee engaged or 1 yard",
                "damage": "14",
                "qualities": [
                    "Light",
                    "Powerful",
                    "Reach",
                    "Vicious"
                ]
            },
            {
                "name": "Hurled Rock",
                "chance": 75,
                "distance": "ranged 5 yards",
                "load": "1 AP",
                "damage": "14",
                "qualities": [
                    "Pummeling",
                    "Punishing",
                    "Slow",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DIONYSIAN DELIGHTS",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1-5', they are Intoxicated. If the result is face '6', they are not Intoxicated."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "MONSTROUS BELLOW",
                "description": "When these creatures successfully use a Litany of Hatred, they inflict 2D10+[BB] physical Peril."
            },
            {
                "name": "NEVER SURRENDER",
                "description": "Foes do not gain an advantage when they flank or outnumber this creature in combat."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition, they ignore the effects of Hard Terrain."
            },
            {
                "name": "SWALLOW WHOLE",
                "description": "When these creatures make a successful Chokehold, they deal Damage from a barehanded weapon and force a foe to Resist with a Toughness Test or be Swallowed. While Swallowed, the foe cannot use any Actions, but can attempt to Resist again at the beginning of their Turn to escape. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNGAINLY",
                "description": "When these creatures are Slain! all those Engaged with it must succeed at a Coordination Test or be knocked Prone beneath of it, suffering 3D10+3 Damage from falling."
            },
            {
                "name": "UNRULY",
                "description": "When these creatures drop down the Damage Condition Track, they move an equal number of steps down"
            }
        ],
        "trappings": [
            "Bracelet of Elven ears (3)",
            "Hide armor",
            "Heavy hides (9)",
            "Hogshead half full of either cheap wine",
            "Dwarven brew or rotgut",
            "Large sack full of rotten meat",
            "Necklace of skulls",
            "Rock (6)",
            "Tree trunk",
            "as cudgel"
        ]
    },
    {
        "type": "SIABRA",
        "family": "CHOSEN OF CHAOS",
        "description": [
            "Centuries ago, a cadre of Elven explorers looked beyond their homelands over the sea, longing for knowledge of what lies beyond. They built powerful ships out of old growth wood and set out, never to be heard from again... until recently. They returned different, as the land across the sea changed them  no longer Elven sailors, but instead corsairs of the high seas.",
            "Siabra, which is an ancient Elven word meaning 'the exiled', are Elves who gave into the allure of Corruption. It is said in the wild lands beyond the sea, they found the 'true god'  otherwise known as the Witch-Queen. Normally, the Elves would have turned and fled from this corruption, but a terrible plague had fallen over the sea that rose massive leviathans, sundering any ship that sailed beyond its shore. Isolated and desperate, they followed the new god into the darkness. The Witch-Queen whispered of cruelty and hatred and the Elves molded their culture around the calls for slavery and subjugation. They abandoned their name and deemed themselves the Siabra. Over generations, the Siabran culture revolved around worshipping the ones among them who had captured the most thralls and gathered the mightiest of riches, oftentimes allying themselves closely with the Fomorian. The dark weather of their new home bleached their skin to a pale white and the only animals around to hunt were giant lizard creatures. The Siabra do not dress, but rather bind themselves in the tanned leathers and scales of these lizards and sometimes they even ride them into battle.",
            "Eventually, the Siabra learned to not only avoid the monsters in the sea, but control them  yoking them painfully to their jet-black ships to navigate the dangerous waters. Though the Siabra lived for centuries in their new home, it was lacking in many resources. Now, the Siabra have sailed back to their homes to launch coastal raids on their old allies, stealing food, women, livestock and weaponry. Other Elves have disavowed the Siabra, but these new dark creatures claim that they are the true future of the Elves. Not many Siabra corsairs live on this side of the ocean, but those who do answer the call of vile needs journey north to join the ranks of chaos's chosen or embroil themselves in the ranks of the Unseelie Fey."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 5
            },
            "agility": {
                "base": 50,
                "bonus": 8
            },
            "perception": {
                "base": 40,
                "bonus": 6
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 14,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 60,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 10,
            "coordination": 10,
            "interrogation": 20,
            "intimidate": 10,
            "martial_melee": 20,
            "martial_ranged": 20,
            "navigation": 20,
            "pilot": 20,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Siabran Executioner",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Adaptable",
                    "Finesse",
                    "Vicious"
                ]
            },
            {
                "name": "Siabran Crossbow",
                "chance": 65,
                "distance": "ranged 9 yards",
                "load": "2 AP",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Punishing",
                    "Repeating"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HATRED (Elf )",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SERPENTINE CLOAK",
                "description": "When these creatures are encountered, they wear a cloak made from an undersea creature. It provides a Damage Threshold Modifier of 1 when worn."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "UNBRIDLED RAGE",
                "description": "When these creatures are Seriously or Grievously Wounded, add an additional 1D6 Fury Die to"
            }
        ],
        "trappings": [
            "Crossbow bolt (9)",
            "Leather armor",
            "Serpentine cloak",
            "Siabran crossbow",
            "Siabran executioner",
            "Vile holy symbol"
        ]
    },
    {
        "type": "BRIGAND",
        "family": "CORRUPTED SOUL",
        "description": [
            "Roads between cities and towns may be safer than paths through the wilderness, but that does not mean they are devoid of danger.",
            "Creatures may keep their distance from the roads, but Brigands and highwaymen stalk these paths constantly, looking for rich marks to liberate of their shilling and give to the poor, unwashed masses. The dark forests and fetid bogs can get to even these rangers, their band of merry men being picked off by both imperial soldiers and beasts alike. Driven to starvation and poverty, these once-champions of the people will broaden their horizons, targeting any poor traveler wandering through their territory at that moment. These Brigands ambush travelers, fleecing them of coin and provender, leaving them victim to the wilds around them. Though these attacks are reluctant at first, many Brigands start to take a liking to the whole ordeal; they find themselves much richer than before and their infamy is just as good  if not better  than their previous do-gooder reputation. They become true terrors: bandits with no remorse who revel in their despicable duties.",
            "Brigandage seems to be a very Human profession, but more than a few Elves have also resorted to raiding supply lines and trade routes. Most of these Elves are backwards isolationists who don't produce much of their own, so cutting free caravans is their only way of survival. In fact, many cart drivers will hesitate to go through an old growth forest before they enter a wide open field. Halflings are also notorious Brigands at times, though they are often laughed at when they point a flintlock at the knee of a driver rather than their head."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 9,
        "parry": {
            "chance": 65,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 50,
            "qualities": []
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "coordination": 10,
            "folklore": 10,
            "intimidate": 10,
            "martial_melee": 10,
            "martial_ranged": 10,
            "resolve": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Sabre",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Defensive"
                ]
            },
            {
                "name": "Flintlock pistol",
                "chance": 55,
                "distance": "ranged 8 yards",
                "load": "3 AP",
                "damage": "6",
                "qualities": [
                    "Gunpowder",
                    "Volatile"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLITZ",
                "description": "When these creatures Charge, they may flip the results to succeed at their next Attack Action or Perilous Stunt on the same Turn."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "SHOTGUN BANG!",
                "description": "When these creatures successfully strike with a ranged weapon possessing the Gunpowder Quality, they automatically Load their weapon for 0 APs."
            },
            {
                "name": "UNBRIDLED RAGE",
                "description": "When these creatures are Seriously or Grievously Wounded, add an additional 1D6 Fury Die to"
            }
        ],
        "trappings": [
            "Balaclava",
            "Brigandine armor",
            "Flintlock pistol",
            "Gunpowder & shot (6)",
            "Sabre",
            "Wanted letter"
        ]
    },
    {
        "type": "DERANGED ZEALOT",
        "family": "CORRUPTED SOUL",
        "description": [
            "The gods are distant and rarely answer prayers or benedictions, but the few priests who are granted Magickal powers reinforces that they are present, no matter how disinterested they may be.",
            "There are those whose faith becomes shaken, their doubt of the divine gnaws constantly at the back of their brain. These unstable priests and followers start to go to immense lengths to get glimpses of the divine, even if they are false. Selfmutilation, ritualistic torture of heretics and hallucinogenic spirit journeys bring them just to the edge, but never pushes them over completely. The formerly tame worshippers become Deranged Zealots, frustrated and angered that the gods evade them despite their devotion. They believe the solution to this is to swell their numbers and make their devotions even more extreme. These Zealots militantly recruit others into their faith on pain of death or torture, though usually one or both of those follow if they do join the order. No religion is safe; even the throngs of the benevolent Martyr have dissenters who believe the only way to be blessed by Her healing is to inflict monstrous pain on themselves and others.",
            "The worst Zealots are those who worship the Daemons, those outer beings that could either be entities of pure chaos or actual nascent demigods who strive for power. When most folks speak of 'cults', these are the godheads they are referring to. Worship of Daemons is usually outlawed and sometimes even punished by death on the pyre. That is not to say that the 'good' gods do not incite violence  both the GodEmperor and the Winter King seem to have their fair share of fanatics and in fact both churches sometimes welcome these maniacs with open arms as crusaders, inquisitors or righteous paragons."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 7,
        "parry": {
            "chance": 60,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "folklore": 10,
            "interrogation": 10,
            "intimidate": 10,
            "resolve": 10,
            "simple_melee": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Threshing Flail",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "5 or 6 two handed",
                "qualities": [
                    "Adaptable",
                    "Weak"
                ]
            },
            {
                "name": "Rocks",
                "chance": 60,
                "distance": "ranged 6 yards",
                "load": "1 AP",
                "damage": "5",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "HALLUCINOGENIC FRENZY",
                "description": "After ingesting a dose of red cap mushrooms, these creatures add 1D6 Fury Die to melee weapon Damage."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "UNRULY",
                "description": "When these creatures drop down the Damage Condition Track, they move an equal number of steps down"
            }
        ],
        "trappings": [
            "Hammer & nails",
            "Red cap mushrooms (3)",
            "Religious scriptures",
            "Rocks (6)",
            "Tattered rags",
            "Threshing flail",
            "Torture device"
        ]
    },
    {
        "type": "DESERTER SOLDIER",
        "family": "CORRUPTED SOUL",
        "description": [
            "Since the dawn of mankind, tribes have sent capable folk to be spitted upon one another's spears in exchange for small parcels of land, religious freedom or resolution of a personal vendetta reaching back centuries.",
            "The average soldier is not a veteran or decorated general; they are children, barely out of the flower of their youth who are told to take this pistol and this pike and kill someone who looks different from them. They watch as their friends die around them, trusted officials reveal themselves as gluttons and rapists and they witness the light drain out of the eyes of a child that reminds them of their brother back home  all while their sword is plunged hilt-deep through another poor boy's chest. It is no wonder soldiers break, routing and heading off to the woods; unable to return due to fear of court-martial and unable to go home so as not to impose shame on their families. These Deserter Soldiers are desperate people, knowing only killing and war and having to do just that to survive.",
            "While it is true some Deserter Soldiers are bloodthirsty bounty hunters or killers in search of coin, many are desperate, confused and angry, only wishing to melt down the medal they received for their 'service', and live a life in solitude. But the world will not avail them that option and they must ply the only trade they know that of war. Some resort to combat pits in the far south or to mercenary work in order to pay what bills they scrape up. They show little remorse in their killings, but the dead still haunt them in their sleep. Recurring nightmares inspires a world-weariness and nihilism in the common Deserter Soldier, caring aught for even their own lives as a result."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            },
            {
                "value": 10,
                "type": "on horse"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 7,
        "parry": {
            "chance": 60,
            "qualities": []
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "guile": 10,
            "martial_melee": 10,
            "resolve": 10,
            "ride": 10,
            "simple_ranged": 10,
            "toughness": 10,
            "warfare": 10
        },
        "attack_profile": [
            {
                "name": "Pike",
                "chance": 60,
                "distance": "melee engaged or 1 yard",
                "damage": "5",
                "qualities": [
                    "Reach"
                ]
            },
            {
                "name": "Misericorde",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Finesse"
                ]
            }
        ],
        "traits": [
            {
                "name": "LICK YOUR WOUNDS",
                "description": "These creatures may spend 1 Misfortune Point to move three steps up the Damage Condition Track positively."
            },
            {
                "name": "MAN O'WAR",
                "description": "When these creatures are Injured, always refer to the Moderate Injuries table to determine their severity. This Trait overrides any potential for increase in Injuries due to Qualities, Traits or other Talents."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred,"
            }
        ],
        "trappings": [
            "Brigandine armor",
            "Misericorde",
            "Pike",
            "War medal"
        ]
    },
    {
        "type": "OCCULTIST",
        "family": "CORRUPTED SOUL",
        "description": [
            "Often times the foulest of souls can hide in plain sight, going through dark dealings behind closed doors and whispered halls. It is these mysterious places that the Occultist holds sway.",
            "An Occultist is rather a bit of an eccentric; a foreign mystic, a well-travelled explorer or a scholar with radical new ideas. They wish nothing more than to spread their corruption through the upper echelons of society, harboring beneath their robes a diabolical devotion to Abyssal Princes. These intriguing strangers are sometimes invited into the home and hearth of decadent nobles whose preoccupation with boredom welcomes such people of questionable repute. Once ensconced in their company, the Occultist begins their work. An Occultist whispers blasphemies into noble ears, swaying them towards masked orgies, bloody banquets and distasteful religions mankind was not meant to practice. When Occultist are done, they leaves the famed manors in decadent Corruption, feeling the town seethe with chaos as they head down the highway, towards the next lordly manse.",
            "Though Magick is becoming more and more understood, with that knowledge also comes ignorance. The Occultist is a victim of this, believing they are too powerful or too smart to fall under the sway of Corruption, but their massive cloaks covering mutations and diseases brought about from their tampering proves otherwise. Sorcery is often the easiest path for an Occultist, but others explore the more 'traditional' Magicks, if only for research purposes. Some Occultists also fall into worship of Demons and Restless Spirits, as their odd traditions and fell methods are even more appealing."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 35,
                "bonus": 3
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 40,
                "bonus": 5
            },
            "intelligence": {
                "base": 45,
                "bonus": 6
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 50,
                "bonus": 6
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 3,
        "peril_threshold": 9,
        "parry": {
            "chance": 40,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "bargain": 10,
            "charm": 10,
            "eavesdrop": 10,
            "education": 10,
            "folklore": 10,
            "incantation": 10,
            "intimidate": 10,
            "resolve": 10,
            "ride": 10,
            "rumor": 10
        },
        "attack_profile": [
            {
                "name": "Ceremonial Knife",
                "chance": 40,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Vicious",
                    "Weak"
                ]
            },
            {
                "name": "Walking Cane",
                "chance": 40,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Light",
                    "Powerful",
                    "Slow",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "WINDS OF CHAOS",
                "description": "When casting Generalist Magick at their option  these creatures may automatically succeed at the Incantation Test but drop one step down the Peril Condition Track negatively. In addition, they must always"
            }
        ],
        "trappings": [
            "Ceremonial knife",
            "Diabolical holy symbol",
            "Fine clothes",
            "Loose robes",
            "Prayer book with 6 Petty Magick spells",
            "Reagents appropriate for Magicks (9)",
            "Walking cane"
        ]
    },
    {
        "type": "OGRE MERCENARY",
        "family": "CORRUPTED SOUL",
        "description": [
            "Boisterous and gluttonous creatures, Ogres are an often feared and demonized peoples for good reason. Their only driving goal is to feed, as an Ogre's bottomless tummy requires nearly endless amounts of food to keep them going.",
            "Their native lands have long ago been rendered a wasteland by a terrible disaster, so most Ogres now wander as mercenaries in search of coin and new meals. Ogres are gruff and selfcentered, caring little for the other humanoid Ancestries; in all honesty they would make a feast of them if it wasn't so frowned upon in society. Couple that with their terrible smell from eating carrion, a lumbering body covered with coarse hair and their tendency to wear skulls of their kills on their belts, it's no surprise that many will lock their doors and bolt their shutters if an Ogre stomps into town in search of more gold and more grub. Ogres are becoming more and more common in civilized lands, usually dwelling with Halflings in their hovels. They are not fully civilized yet, but most tend to behave themselves.",
            "Ogres give off an air of stupidity, but it is all a ruse. They are both intelligent and natural tacticians, their homeland once being a military state that rivaled many empires in power. It is spoken that Ogres of old tore down the sky castles of the Nephilim; a victory which they still boast of today. Ogres loves revealing their mental fortitude right before they devour a skinflint general who refuses to pay them. They are also a highly spiritual people; their culture is centered around their sizable gut, which holds almost all of their other organs and is protected by a thick slab of muscle and fat. The more prodigious the Ogre's gut, the more they are able to consume and the more capable they are to lead and breed. They firmly believe this is both where their heart and martial spirit resides. Ogre violence is brutal but efficient, as breaking arms or eating a few fingers to get answers is a common tactic. Outright killing their enemies, except when they can potentially extort them for more than simply gold, is desired above all."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 10
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 8
            },
            "fellowship": {
                "base": 40,
                "bonus": 5
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 13,
        "peril_threshold": 11,
        "parry": {
            "chance": 80,
            "qualities": [
                "Defensive",
                "Protective"
            ]
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 10,
            "bargain": 20,
            "gamble": 10,
            "interrogation": 20,
            "intimidate": 20,
            "martial_melee": 20,
            "resolve": 20,
            "simple_melee": 20,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Morgenstern",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "11",
                "qualities": [
                    "Adaptable",
                    "Powerful",
                    "Vicious"
                ]
            },
            {
                "name": "Knuckleduster",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Fast",
                    "Pummeling"
                ]
            }
        ],
        "traits": [
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "BROKEN GUT-PLATE",
                "description": "After these creatures are Seriously Wounded or made subject to a Called Shot to the body and suffer at least 9 Damage, they suffer a penalty of -6 to their Damage Threshold"
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "MAN O'WAR",
                "description": "When these creatures are Injured, always refer to the Moderate Injuries table to determine their severity. This Trait overrides any potential for increase in Injuries due to Qualities, Traits or other Talents."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at"
            }
        ],
        "trappings": [
            "Animal furs (3)",
            "Brigandine armor",
            "Gut-plate",
            "Knuckleduster",
            "Metal shield",
            "Morgenstern",
            "Pot of war paint (3)"
        ]
    },
    {
        "type": "PETTY THUG",
        "family": "CORRUPTED SOUL",
        "description": [
            "Cities are large, hive-like structures that have been built haphazardly as the local lord expanded their lands. Buildings pile atop one another, as blight and twisted alleyways swallow up the poor and destitute. Children born in this setting only know a life of violence, one that requires you to be tougher than your neighborhood and as tough as your friends.",
            "Couple this with an influence of bawdy songs exalting the deeds of a life of crime and what were once local bullies turn into full Petty Thugs. These criminals have their hands in every enterprise imaginable, from extortion rackets and selling illegal opium to back-den gambling and the sale of prostitutes. They form up en masse, recruiting other poor youths to their ranks to engage in a war over territory while threatening local city folk with violence and abuse. Bearing specifically-colored apparel denoting their 'set', or neighborhood gang they owe allegiance to, they often convalesce in busy thoroughfares or in seedy dens to spend their hard-earned brass. Each member of a gang carries ostentatious marks of their prowess as criminals, whether it be tattoos for each major crime they commit or wear dangling jewelry from every lesser noble they've assaulted. These thugs are also said to be foot soldiers of the real wheeler dealers in a city, with greater connections throughout the nation, making them a force to be reckoned with.",
            "Petty Thugs are often pitiful, because many are victims of circumstance. They are born into a social caste they cannot escape, with the only path of progression being the one of crime. Even if they try to make a better life for themselves, many of their fellow gangers see this as a betrayal to their family, their neighborhood or their village. The rate of death among their numbers is extremely high, many dying in lonely alleys with a knife in the gut or on the street under the hail of gunfire  invariably at the hands of their own gang or clan. The saying, there ain't no such thing as an old 'ganger appears to have a ring of truth to it  most Petty Thugs do not see their twentieth year before being swept into the cold embrace of the Custodian."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 5
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 50,
                "bonus": 5
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 7,
        "parry": {
            "chance": 55,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": []
        },
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "brawn": 10,
            "eavesdrop": 10,
            "simple_melee": 10,
            "stealth": 10
        },
        "attack_profile": [
            {
                "name": "Cudgel",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Light",
                    "Powerful",
                    "Weak"
                ]
            },
            {
                "name": "Garrote",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Fast",
                    "Ineffective"
                ]
            },
            {
                "name": "Flintlock Pistol",
                "chance": 45,
                "distance": "ranged 7 yards",
                "load": "3 AP",
                "damage": "5",
                "qualities": [
                    "Gunpowder",
                    "Volatile"
                ]
            }
        ],
        "traits": [
            {
                "name": "MURDEROUS ATTACKS",
                "description": "When these creatures make an attack with a blackjack, garrote or bullwhip, their foe cannot Dodge, Parry or Resist this attack."
            },
            {
                "name": "POINT BLANK",
                "description": "When these creatures make an Attack Action with a weapon possessing the Gunpowder Quality within Short Distance, they add an additional 1D6 Fury Die"
            }
        ],
        "trappings": [
            "Cheap necklace (2)",
            "Cheap pinky ring",
            "Cudgel",
            "Flintlock pistol",
            "Garrote",
            "Gunpowder & shot (3)",
            "Snuff box",
            "Quilted armor",
            "Wanted poster"
        ]
    },
    {
        "type": "REBELLIOUS PEASANT",
        "family": "CORRUPTED SOUL",
        "description": [
            "The life of a peasant is hard-scrabble and often times bare. They work in the fields, raise families, pay taxes and have meager funerals.",
            "This mundanity gets to many; combined with near totalitarian rule from their landowners, peasants strain under a constant yoke of oppression and control. When they rise up, they rise up en masse and nothing terrifies a landowning noble more than a mutinous populace. These Rebellious Peasants have gone far beyond reason; grabbing pitchfork and torch, they have decided to take matters into their own violent, work-worn hands. Normally overthrowing a wicked dictator would be celebrated, but mob mentality rules these Peasants. They will stomp out or assault anyone they see as standing in their way and they will even continue their rampages long after their burgomeister lies in a ditch. Unless the authorities act quickly to put down such an insurrection, a mob of Rebellious Peasants can turn into an army  a rabble army at that, but still an army  and it requires proper soldiery to take to the field and their discipline invariably ends such rebellions in a massacre.",
            "In war time, many peasants will take up their pitchfork or an ancestral sword in order to form militias  groups of citizen soldiers who are set to defend their home from any army. Like their normal uprising, the only advantage a peasant has is their sheer numbers. Every once in a while, though, a leader will stand out amongst them and if they cannot exactly be trained in the ways of war, such a leader can instill a sense of discipline in the peasantry. Perhaps this is a heretic who wishes to overthrow the local government or a rebel who has had enough of his people being exploited for all they are worth. These leaders may be little more than another farmer with a bit more charisma, but some of history's greatest upsets have occurred at the hands of these demagogues."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 35,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 10,
        "parry": {
            "chance": 65,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 50,
            "qualities": []
        },
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Pitchfork",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "4",
                "qualities": [
                    "Defensive",
                    "Reach",
                    "Slow",
                    "Weak"
                ]
            },
            {
                "name": "Torch",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "Special",
                "qualities": [
                    "Immolate",
                    "Ineffective",
                    "Slow"
                ]
            },
            {
                "name": "Bottle Bomb",
                "chance": 45,
                "distance": "ranged 5 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": [
                    "Fiery",
                    "Ineffective",
                    "Volatile"
                ]
            }
        ],
        "traits": [
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to succeed at Skill Tests."
            },
            {
                "name": "SALT OF THE EARTH",
                "description": "These creatures have factored in +3 to their Peril Threshold."
            },
            {
                "name": "SNIVELING WHELP",
                "description": "These creatures can only use 2 AP"
            }
        ],
        "trappings": [
            "Bottle bomb",
            "Pitchfork",
            "Tattered rags",
            "Torch (3)"
        ]
    },
    {
        "type": "TEMPLAR FANATIC",
        "family": "CORRUPTED SOUL",
        "description": [
            "Inquisitors are both a boon and a bane: they weed out witches and devolved Mutants, but seeing one in your village is a sure sign a friend or neighbor has been hiding a secret from both god and country. Thus comes the Templar Fanatic.",
            "It is an unfortunate fact that Templars fall quite easily from grace: having burnt many mutated children or witnessing entire villages sacrificed in a false bid to appease an Abyssal Prince will leave scars that never fade away. These deranged inquisitors become Templar Fanatics, holy warriors who devote themselves to deadly crusades. Fanatics who see any small sign of treachery as a sure signal of heresy. Their interrogations of commoners go from sly to brutal and violent and they believe themselves to be the divine authority of their god, only answerable to them. Soon, it is not only the Occultists who are ritually slaying villagers, but the Templar Fanatic purging an innocent town in holy fire.",
            "The crusade is a curious thing upon first sight  a holy war between two religions for a god neither is entirely sure exists. These forays into holy lands leave thousands dead, bleeding or defiled, while priceless holy relics are either stolen or broken for their blasphemy. It's a sad fact that many conflicts have religion driving them from the background, but that is often the way the world works. Though many consider the ten gods part of a singular 'pantheon', there are still those out there that believe one is superior enough to kill over. The God-Emperor tends to breed Fanatics just as much as they do zealots and the priests grant them grand titles and large tracks of land for the sole purpose of spreading the faith. Though it may spread, it is not through love and compassion, but through violence and hatred."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 7
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 7
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 10,
        "parry": {
            "chance": 80,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": null,
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "folklore": 20,
            "interrogation": 20,
            "intimidate": 20,
            "martial_melee": 20,
            "navigation": 10,
            "resolve": 20,
            "ride": 10,
            "simple_melee": 20,
            "simple_ranged": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Flanged Mace",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "7 or 8 with wooden shield",
                "qualities": [
                    "Powerful"
                ]
            },
            {
                "name": "Light Crossbow",
                "chance": 70,
                "distance": "ranged 9 yards",
                "load": "2 AP",
                "damage": "7",
                "qualities": [
                    "Fast",
                    "Punishing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "CHAMPION'S CALL",
                "description": "One foe is left Defenseless to all this creature's attacks, until the foe is defeated. The creature may select a new foe once the current one is defeated."
            },
            {
                "name": "GROSSLY PARANOID",
                "description": "These creatures have factored in +3 to their Initiative."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "NEVER SAY DIE",
                "description": "When these creatures are Grievously Wounded, add 3 to their Damage Threshold."
            },
            {
                "name": "PUNISHING ATTACK",
                "description": "When these creatures Take Aim and then make a successful Melee Attack, add 3 Damage."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Antivenom (3)",
            "Bandages (3)",
            "Crossbow bolts (9)",
            "Flanged mace",
            "Holy symbol",
            "Laudanum",
            "Light crossbow",
            "Munitions plate armor",
            "Priest's mitre",
            "Torture device",
            "Veteran's guilded nose",
            "Wooden shield"
        ]
    },
    {
        "type": "WITCH",
        "family": "CORRUPTED SOUL",
        "description": [
            "Dark Magicks dwell deep in the primordial woods and bogs, where the energy of creation still pulses with power. There, nascent wizards whose Magick is too perverse and wrong make their study, away from the prying eyes of kings and inquisitors.",
            "Called Witches  though they are not always female  they have supplicated to Abyssal gods in return for dark gifts and abilities; they worship both the mysterious elemental deities as well as the blasphemous Abyssal Princes. Driven mad by the arcane energies that swirl around them, Witches pose as hedge-crafting 'white wizards' who only wish to aid the populace in out-of-the-way places with tinctures and ointments. These remedies are foul, though; they inflict disease and poison on imbibers, which pleases the Witches' dark masters greatly. Witches form covens with others, creating secret societies, but their regular allies are Mutants. Witches will commonly brew a Magickal stew known as 'Mother Knows Best'; bringing Mutants under their sway, most often a High Orx and a smattering of Boogan underlings.",
            "The Fey often have a special relationship with the Witch. Though most Fey despise humanoids of all types, Witches pay a great deal of respect to the Black Lodge  the home and the god of the Fey. Fey will usually willingly carry out tasks or services for a Witch  for a small fee of course  and the Witch in turn will protect the woods and dark lands the Fey stalk. Sometimes a Witch will even take on plant-like qualities like their Fey allies, finding odd stray twigs in their hair or toadstools growing under their arms. Some Witches end up looking more like a compost heap than a person and these are the ones that must be watched out for."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 45,
                "bonus": 6
            },
            "willpower": {
                "base": 50,
                "bonus": 8
            },
            "fellowship": {
                "base": 40,
                "bonus": 5
            }
        },
        "initiative": 11,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 11,
        "parry": {
            "chance": 45,
            "qualities": []
        },
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "Medium",
        "skills": {
            "alchemy": 20,
            "awareness": 10,
            "coordination": 10,
            "disguise": 10,
            "education": 10,
            "folklore": 10,
            "guile": 10,
            "incantation": 20,
            "resolve": 20,
            "simple_melee": 10,
            "survival": 10,
            "tradecraft": 20,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Sacrificial Knife",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "3",
                "qualities": [
                    "Fast",
                    "Vicious",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "GROSSLY PARANOID",
                "description": "These creatures have factored in +3 to their Initiative."
            },
            {
                "name": "WINDS OF CHAOS",
                "description": "When casting Generalist Magick at their option  these creatures can automatically succeed at the Incantation Test, but must drop one step down the Peril Condition Track negatively. In addition, they must always"
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Loose robes",
            "Rune stones",
            "Reagents appropriate for Magicks (9)",
            "Sacrificial knife"
        ]
    },
    {
        "type": "ZOATAR",
        "family": "CORRUPTED SOUL",
        "description": [
            "The world is wildly old and with that comes creatures that have lived and gone. Ruins lay bare, odd skeletons are discovered in peat bogs and corpses are found in ancient glaciers. But there is one creature that is commonly thought to be myth, but in reality still lives to this day  the mysterious, telepathic Zoatar.",
            "Zoatar used to be the dominant life form in the Material Realm, but for some reason even they can't remember, most of them died out and their numbers drastically reduced to the current few hundred. Zoatar are centauroid hominid creatures, their bodies broad and furred, with a primate head atop their shoulders. Though many Zoatar take on the aspect of gorillas, others have been seen that look like bonobos, chimpanzees and orangutans. Their two five-fingered hands are often wrapped around an intricate ceremonial staff, while their massive four prehensile feet enable them to plod through the moors and jungles they haunt. Zoatar are quick-witted creatures, though they are most well versed in the ways of nature and the world. Zoatar maintain that they are the guardians of the Material Realm, sworn to protect it from those who would wish it harm. In that way they are like the Fey, but they are a generally peaceable people unless attacked or the lands they frequent are threatened. They are isolationists and hermits, with some Zoatar going their whole century-long lives without encountering any of their species outside of their family group.",
            "Zoatar assert they were once a powerful and peaceful civilization, until the Aztlan saw fit to enslave them. After some millennia they escaped, but their numbers weakened and their culture was lost to the mists of time. What remains of their culture revolves around the importance of spirits, communion with nature, rejection of technology of all sorts and veneration of ancestors. In fact, their ceremonial staff is inscribed with the names of their forbearers, written in the Zoatar's mysterious and old tongue. They are also naturally blessed with gifts of the Demiurge, treated as favored children. The Zoatar have little motive outside of protecting the lands, as it is all they have left in the world. But let it be known that they will fight to their dying breath against any foe if it means they can save even the weakest of sproutlings."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 45,
                "bonus": 5
            },
            "perception": {
                "base": 45,
                "bonus": 7
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 50,
                "bonus": 10
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 13,
        "parry": null,
        "dodge": {
            "chance": 55,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Advanced",
        "skills": {
            "awareness": 30,
            "bargain": 20,
            "education": 20,
            "folklore": 20,
            "incantation": 30,
            "intimidate": 20,
            "navigation": 30,
            "scrutinize": 20,
            "simple_melee": 20,
            "survival": 30,
            "tradecraft": 30
        },
        "attack_profile": [
            {
                "name": "Ceremonial Staff",
                "chance": 60,
                "distance": "melee engaged or 1 yard",
                "damage": "4 or 5",
                "qualities": [
                    "Adaptable",
                    "Pummeling",
                    "Reach"
                ]
            },
            {
                "name": "Sickle",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Finesse",
                    "Vicious",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "CEREMONIAL RUNES",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1', '2' or '3', they have three Apprentice Runes inscribed upon their staff. If the result is face '4' or '5', they have two Journeyman Runes inscribed upon their staff. If the result is face '6', they have one Master Rune inscribed upon their staff."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FIREPROOF",
                "description": "These creatures and their possessions are entirely immune to Damage from fire."
            },
            {
                "name": "HATRED (Aztlan, Goblin)",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "SWEEPING STRIKE",
                "description": "When these creatures make a successful attack with a two-handed melee weapon, they"
            }
        ],
        "trappings": [
            "Ceremonial staff",
            "Prayer book with 9 Petty Magick spells",
            "Prayer book with 6 Lesser Magick spells",
            "Prayer book with 3 Greater Magick spells",
            "Reagents for all Magick spells (9)",
            "Sickle",
            "Sacred scroll with Inscribe Magick Rune (3)"
        ]
    },
    {
        "type": "LARGE ANIMAL",
        "family": "CRITTER",
        "description": [
            "Large Animals are akin to Small Animals, but significantly bigger in stature and size. Most are herd animals, domesticated or otherwise, that graze forests and pastures. If not that, they are often scavengers who feed off corpses of already-dead animals (or travelers who died from exposure). Elk, horses, cattle, medium cats such as the lynx, grey wolves, war dogs, condors and vultures are all considered Large Animals.",
            "The attributes below represent a sampling of animals and all the qualities may not apply to all creatures. For example, horses are generally not hunter-predators and would most likely would not gain advantage of the Trait of Ripping Teeth."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            },
            {
                "value": 16,
                "type": "fly"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "coordination": 10,
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Bite or Claws",
                "chance": 50,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Slow",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage,"
            }
        ],
        "trappings": [
            "Animal hide",
            "Animal meat (6)"
        ]
    },
    {
        "type": "LARGE INSECT",
        "family": "CRITTER",
        "description": [
            "Insects are a common occurrence in a grim & perilous world. They are mainly pests, rarely ever bigger than a person's palm, but there is no doubt that they outnumber all other living creatures. However, some insects seem to grow much larger and deadlier than others. Whether this is the result of Abyssal influence, mutations brought about by insane Magick wielders or just natural growth is up for debate. Regardless, these Large Insects are hardy and terribly dangerous, many able to grow to the size of a horse. They are extremely resilient creatures, surviving harsh climates and emerging from disastrous cataclysms unscathed. They come in a myriad of shapes and sizes, from spiders to cockroaches to ants to scorpions to dragonflies... and that barely scratches the surface.",
            "Large Insects are luckily not as frequently encountered as their smaller brethren; if this was reversed, the awful beasts would have long ago swarmed the earth with gifts of their natural reproductive abilities and sheer numbers."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 6
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 15,
                "type": "normal"
            },
            {
                "value": 18,
                "type": "fly"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "resolve": 10,
            "simple_melee": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Teeth or Pincers",
                "chance": 50,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Entangling",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "NATURAL ARMOR (2)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Insect hide",
            "Insect meat (6)"
        ]
    },
    {
        "type": "MAN-EATER",
        "family": "CRITTER",
        "description": [
            "Man-eaters are a very real threat in the wilds and it is why the solitary hunter carries a bow or arquebus at their side when ranging the wilds. Creatures of this size are often predators, stalking the night and killing to feed themselves and their offspring. They do not always tower over men, but their animalistic strength and natural weaponry is unmatched against an unarmed traveler (or quite often an armed one). If given a chance, a bear will have no second thoughts about ripping your throat out as you attempt a blood-choked scream or an anaconda drag under the water to twist and drown you. These beasts cannot be faulted, though; they are simply doing what they must to survive and they hold no inherent malice. Bears, large cats like lions or jaguars, bears, sharks, great apes, pythons, komodo drakes and more are all considered Man-eaters.",
            "The attributes below represent a sampling of animals and all the qualities may not apply to all creatures. For example, flying jaguars are still thankfully only the providence of the worst night terrors."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 45,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            },
            {
                "value": 15,
                "type": "fly"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "guile": 10,
            "intimidate": 10,
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Terrible Bite or Claws",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Finesse",
                    "Punishing",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "NATURAL ARMOR (1)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage, they can force a foe to Resist a Takedown."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Animal hide",
            "Animal meat (9)"
        ]
    },
    {
        "type": "SMALL ANIMAL",
        "family": "CRITTER",
        "description": [
            "Creatures of all sorts roam the bogs, forests, dunes and taigas of the world and their variety is nearly as endless as the night's sky. The smallest animals are generally self-sufficient and territorial, but not a very large threat in small numbers. Geese, foxes, dogs, rabbits, monkeys from the far south, jackdaws, snakes and more are all considered Small Animals, generally coming no taller than the waist of a human. Most of these creatures will only attack when provoked; if left alone, they are as harmless as a sleeping babe.",
            "The attributes below represent a sampling of animals and all the qualities may not apply to all creatures. For example, unless a pig is ensorcelled in some way, it is doubtful it possesses the ability to fly."
        ],
        "size": [
            "Small"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 40,
                "bonus": 3
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 45,
                "bonus": 5
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            },
            {
                "value": 15,
                "type": "fly"
            }
        ],
        "damage_threshold": 3,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "coordination": 10,
            "stealth": 10,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Bite or Claws",
                "chance": 35,
                "distance": "melee engaged",
                "damage": "3",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated"
            }
        ],
        "trappings": [
            "Animal hide",
            "Animal meat (3)"
        ]
    },
    {
        "type": "SWARM",
        "family": "CRITTER",
        "description": [
            "Every traveler has heard tales of a rain of toads and fish falling from the sky at the behest of a witch. Every fisherman can spin a yarn or two of massive flocks of razorbills devouring sacks of grain. Every Raconteur can speak of the time the bedeviled Pied Piper came to town, to drive out a host of rodents (and presumably children as well). Every socialite has heard the tale of hundreds of murderous crows attacking both children and adults alike in the sleepy village of Hitchcock.",
            "Swarms of plague-bearing, natural creatures exist throughout the known world, but is largely unknown what drives them together. Nearly incalculable in number, these Swarms gather where civilization prospers. Destroying crops, blight and bringing with them disease and plague, they do great damage to those unable to take shelter."
        ],
        "size": [
            "Small, Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 5
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            },
            {
                "value": 8,
                "type": "fly"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Swarm",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Powerful",
                    "Pummeling"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "CREEPY CRAWLIES",
                "description": "These creature can attack all foes in a Burst Template area of effect and are able to occupy the same space that its foes stand within. It also never provokes Opportunity Attacks."
            },
            {
                "name": "DENSE ANATOMY",
                "description": "These creatures only suffer Damage dealt by fire."
            },
            {
                "name": "DISEASE-RIDDEN (Filth Fever)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Swarm meat (3)"
        ]
    },
    {
        "type": "BOG THING",
        "family": "DEADLY FLORA",
        "description": [
            "The Bog Thing is a story told to children for centuries, a warning by their parents to stay out of the fetid and dangerous swamps beyond the boundaries of their tilled fields. While this is wise advice, what many do not know is that the Bog Thing is very much real, yet an enigma wrapped in swamp grass.",
            "Bog Things, more commonly called 'grey men', are thought to be people who wandered into swamps only to be swallowed by the peat and morass. They are tall and Human-like in shape, though their bark-like flesh sprouts with vines, moss and twigs. Their massive paws end in elongated claws, while their mouthless heads are surmounted by a rack of broken antlers. Aside from these obvious features, the Bog Thing also possesses one odd attribute  its massive maw of a mouth is in the middle of its stomach and appears more like a bramble patch than a gullet. In order to feed, the Bog Thing rakes their prey into ribbons with enormous claws and then shovels the torn entrails into their thorny maw. They mostly feed on natural animals of the swamp.",
            "Curiously enough and in spite of common folktales, Bog Things will never hurt children  in fact, it is rumored they even aid and befriend children who become lost in the swamps, leading them back to safety.",
            "Bog Things are solitary creatures, blending into the murk of the swamp during the day and roaming the cypress groves at night in search of food. Bog Things have also reportedly been seen following the trails of will o'wisps, though the reason why is a mystery. If left alone, Bog Things are less than harmless. However, Bog Things are extremely protective of the moors and fens they haunt  if anyone tries to use the land for peat, water or farming, they will savage the settlers and sink the buildings into the very mud that they were built on."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 9,
        "parry": {
            "chance": 50,
            "qualities": []
        },
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "eavesdrop": 10,
            "intimidate": 10,
            "navigation": 10,
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Bog Claws",
                "chance": 50,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Vicious",
                    "Weak"
                ]
            },
            {
                "name": "Stomach Maw",
                "chance": 50,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Vicious",
                    "Punishing"
                ]
            }
        ],
        "traits": [
            {
                "name": "BOG STENCH",
                "description": "These creatures emit a horrific stench, causing 1D10+1 physical Peril to those who stand Engaged with it."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "WANTON HUNGER",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', their hunger has been sated. If the result is face '6', their hunger has not yet been sated and they are in a state of frenzy. When sated, they add +1 to both Damage and Peril Condition Tracks. When in a state of frenzy, they add an"
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "DEATH HEDGE",
        "family": "DEADLY FLORA",
        "description": [
            "The Death Hedge on first glance appears to be a beautiful rose bush, budding with crimson-red roses. In fact, it once was, until the corruptive winds of the Æther slowly mutated it into its current form.",
            "On closer study, the ground around a Death Hedge is exceedingly fertile and moving the branches will reveal a plethora of bones of all sizes lying by its roots. The Death Hedge is an exsanguinating killer, lying in wait for an animal to attempt to pluck its roses. It will then lash out with thorny vines, piercing and entangling their victim while it slowly drains it of its bloody humors. Though the Death Hedge will often only attack small animals, it will attack a humanoid if it has not fed for several days. When all the blood is absorbed, the Death Hedge drags the corpse under itself to decay in order to fertilize the ground they are rooted in. The Death Hedge also emits a sweet aroma, similar to that of ripe strawberries, allowing them to mask the scent of the decaying bodies underneath their foliage.",
            "Death Hedges hold a strange place in the minds of the young and those feeling ennui, for it is seen as a place of fatalistic beauty. Young lovers will journey to a Death Hedge to join together near it, despite all the inherent danger. Curiously, the Death Hedge leaves these lovers alone so long as one of the lovers plucks one of the roses to give to their partner as a favor. If the two have not heard of this crucial step, they will most likely die screaming in each other's arms as the Death Hedge's thorns pierce their flesh. This exception does not extend to creatures of the animal kingdom, which is the Death Hedge's normal diet. Perhaps the plant has an archaic intellect or it's just odd biology that makes it at least semisympathetic to humanoid plights."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 35,
                "bonus": 3
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 40,
                "bonus": 6
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 9,
        "movement": [],
        "damage_threshold": 7,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "eavesdrop": 10,
            "simple_melee": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Flailing Branch",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Entangling",
                    "Pummeling",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "SWALLOW WHOLE",
                "description": "When these creatures make a successful Chokehold, they deal Damage from a barehanded weapon and force a foe to Resist with a Toughness Test or be Swallowed. While Swallowed, the foe cannot use any Actions, but can attempt to Resist again at the beginning of their Turn to escape. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Blood rose (3)"
        ]
    },
    {
        "type": "ORX-SPORE",
        "family": "DEADLY FLORA",
        "description": [
            "The Orx are a scourge across the land, porcine-faced Mutants that seem never ending in their numbers. Low Orx are traditionally revived corpses, who has been given a new life by a disease called Orx-molt, which spreads from an Orx to a slain victim by way of fungal spores.",
            "This fungus, known as Orx-spore, is rarely be found in the wilds, but almost always found in large concentrations of Orx WAAAR! Horde encampments, growing from Orx droppings. The fungus appears as a pale yellow-green toadstool that reaches about knee height and glows with fluorescence under the cover of night. Orx-spore only lash out if provoked or tread upon, but every attack carries the deadly spores that spreads Orx-molt. An encounter with a ring of Orx-spore will either leave its victims coughing or painfully mutating into one of the barbaric Orx. Luckily, Orx-spore can be destroyed with little risk of exposure by being set alight, either by natural or Magickal means. Otherwise, they continually grow back and cannot be harmed by any method. Woe betide the Hedgewise who mistakes a ring of toadstools for a fairy ring, for Orx-spore appears almost the same!",
            "A question that has long plagued naturalists and druids is where exactly did the Orx-Spore originate from? Obviously it is an aberrant form of plant life, but it has no antecedent or precedent. A popular theory is that the Orx-Spore is not of this earth, but instead a terrible blight that descended from the Vault of Night. Thought to be an extraterrestrial fungus that landed thousands of years ago, it is a feasible if not outlandish answer to the question."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 6,
        "movement": [],
        "damage_threshold": 6,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "eavesdrop": 10,
            "simple_melee": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Fluorophore",
                "chance": 55,
                "distance": "ranged 4 yards",
                "damage": "None",
                "qualities": [
                    "Entangling"
                ]
            }
        ],
        "traits": [
            {
                "name": "CREEPY CRAWLIES",
                "description": "These creature can attack all foes in a Burst Template area of effect and are able to occupy the same space that its foes stand within. It also never provokes Opportunity Attacks."
            },
            {
                "name": "DENSE ANATOMY",
                "description": "These creatures only suffer Damage dealt by fire."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "SPORE-SPLOSION",
                "description": "When these creatures attack, it cannot be Dodged or Parried  it may only be Resisted with a successful Toughness Test. If a foe fails this Toughness Test, they are exposed to Orx-molt."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "SHIMMERING MIMIC",
        "family": "DEADLY FLORA",
        "description": [
            "Some say we have all come from the primordial slime and it is there where we will all find our eventual fate. The flora known as the Shimmering Mimic may perhaps be that slime or at least a piece of that ancient goop.",
            "Shimmering Mimics are mobile slimes with animal intelligence, often covering a ten-foot square area. Often dwelling in sewers, caves or on the surface of water, their main adaptation is their translucent jelly that grants them near-perfect camouflage and perfect mimicry of non-living objects such as chests, crates and other sundries. The Mimic will lie in wait until prey touches or comes near it; once disturbed, it will lash out with slimy pseudopods and engulf their victim. Their body then secretes digestive juices, slowly eating away at the flesh and bone of their meal. Mimics have no discernible organs save its pulsating brain and cannot be reasoned with or tamed; they are beings of pure instinct, only multiplying through a grotesque budding. Not much can harm these creatures, but they shirk away from fire like an unholy light.",
            "Shimmering Mimics are the only flora that can be directly traced back to the Abyss. If an Abyssal servant ever displeases one of their Princes, many are cast down back into this primordial jelly and pushed into the lowest layers of the Abyss. Whether the Abyss spawned these creatures or if the Princes instead delight in the irony and grotesquery of the mimic, is still up for debate. What is known is that Shimmering Mimics are often found near portals to the Abyss or near high concentrations of Corruption. Attempts have been made to capture a Shimmering Mimic to study them, though they eat quickly through any container they are placed in. They are of particular interest to the Choleric Skrzzak, who see much mutagenic potential in these osmoid beings."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 40,
                "bonus": 8
            },
            "perception": {
                "base": 35,
                "bonus": 5
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "awareness": 20,
            "coordination": 20,
            "eavesdrop": 20,
            "simple_melee": 20,
            "simple_ranged": 20,
            "stealth": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Pseudopod Tentacle",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Entangling",
                    "Fast",
                    "Pummeling"
                ]
            },
            {
                "name": "Pseudopod Glob",
                "chance": 65,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "8",
                "qualities": [
                    "Slow",
                    "Pummeling",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "DENSE ANATOMY",
                "description": "These creatures only suffer Damage dealt by fire."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "GASTRIC ACIDITY",
                "description": "When these creatures deal Damage, a foe must Resist with Coordination. If they fail, the foe's armor, weapon or shield is Ruined!."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these creatures."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Remnant bodies of the dead (3)",
            "Shimmering mimic's brain"
        ]
    },
    {
        "type": "DULLAHAN",
        "family": "FEY",
        "description": [
            "Death rides upon a pale horse; one that comes for man and woman, rich and poor alike. The Custodian is not a chaotic being of any sort; She simply knows that a due is owed, for the soul the Demiurge gave must be returned to the Custodian. In order to accomplish this, the Custodian has cursed a certain brand of Fey to be Her eternal ferrymen  the Dullahan.",
            "Fey are Magickal and immortal, so they very rarely die outside of violence. When their spirits die in the Material Realm, the Custodian transforms them into the brooding and terrible Dullahan  a sort of petty revenge for the Fey's rebellion. Dullahan appear as burly, cloaked coachmen, their riding leathers dark and adorned with bits of Human bone. They are not headless so much as they carry it decapitated either beneath a crook of the arm or mounted on their saddle. It can see, smell and hear normally, but it rarely speaks outside of condemning the mortal they have come to collect. Dullahan ride atop Night Mares, which look like skeletal horses wreathed in flame; otherwise, they drive grim, gothic carriages or even pilot river-going skiffs that rattle and quake as skulls of the dead bounce around. Nothing stops the Dullahan  they are immortal, no door is an obstacle to them and they are only visible to a select few people. When they reach their mark designated by the Custodian, they plunge their vertebrae sword through the target's very soul, extracting it and either tossing it in their carriage or thrown over their saddle. As the heat of the body fades, the Dullahan gallops off to deposit the newly harvested spirit into the Well of Souls.",
            "Dullahan are not undead, but they aren't living either  they dwell within that awkward place between that all Fey do. Nor do they possess true will anymore, for sure  they are servants of the Custodian and they will not stop until their designated souls have been collected. That is what makes them most dangerous  they are tenacious and powerful and though a Dullahan can be potentially slain to ward it off, soon another Dullahan will return in its place, equally full of ire and bent on destruction. They also share an odd connection with the Black Lodge that little other Fey do  they serve the destruction and entropy of the Lodge and wherever they go they bring a stench of rotting wood and dried bones."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 8
            },
            "brawn": {
                "base": 45,
                "bonus": 5
            },
            "agility": {
                "base": 40,
                "bonus": 8
            },
            "perception": {
                "base": 45,
                "bonus": 7
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 8,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "drive": 30,
            "eavesdrop": 20,
            "folklore": 20,
            "interrogation": 20,
            "intimidate": 30,
            "martial_melee": 30,
            "navigation": 20,
            "pilot": 10,
            "ride": 30,
            "simple_ranged": 20,
            "stealth": 10
        },
        "attack_profile": [
            {
                "name": "Vertebrae Sword",
                "chance": 80,
                "distance": "melee engaged or 1 yard",
                "damage": "8",
                "qualities": [
                    "Punishing",
                    "Reach",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden and Æthereal foes, and can see in the dark."
            },
            {
                "name": "GIVE ME YOUR EYES",
                "description": "When these creatures inflict a Moderate Injury, it is always a Black Eye. When they inflict a Serious Injury, it is always Head Trauma. Finally, when they inflict a Grievous Injury, it is always a Vitreous Hemorrhage."
            },
            {
                "name": "GOLDEN DEATH",
                "description": "These creatures cannot be permanently Slain! unless their eyes are pierced with a golden pin or 1 gold crown (gc) is placed beneath their tongue. During this time, they remain in a hibernating state, unless the pins or gold crown (gc) are removed, therefore giving life to the creature again. Only by then burning the head in a funeral pyre are they forever Slain!."
            },
            {
                "name": "IN THE FACE",
                "description": "These creatures can only be harmed by melee and ranged weapons by using a Called Shot to the head."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            },
            {
                "name": "UNLATCH DOORS",
                "description": "These creatures can automatically unfasten any lock they can see or touch, Magickal or otherwise."
            },
            {
                "name": "WORD OF DEATH",
                "description": "When these creatures encounter those who are suffering from a Disorder, Disease or Grievous Injury, the foe must Resist with a successful Resolve Test or else be Slain!. However, the foe must be able to see and hear them for Word of Death to work. If a foe succeeds at this"
            }
        ],
        "trappings": [
            "Dullahan's head (storm lantern which uses blood for fuel)",
            "Vertebrae sword",
            "Witchhunter's coat & mantle",
            "Witchhunter's hat"
        ]
    },
    {
        "type": "FACEDANCER",
        "family": "FEY",
        "description": [
            "Myths tell of a curse placed on a clan of people long ago for displeasing long-forgotten gods, a curse that would leave the people without identity, but always longing for one. The world is not kind or loving and a sad yet morbid tale is to be told of the Facedancer's dilemma.",
            "Their unchanged faces are utterly featureless, aside from a wide-gaped mouth with razor sharp teeth filling it. Perhaps surprisingly enough, Facedancers are not demons or any kind of monster: they have normal thoughts and motivations (though normality leaning towards viciousness). Facedancers have an endless desire to be normal again, to have features and a reflection in the mirror and they can accomplish this feat through a sanguine ritual. Upon skinning a victim's face, the Facedancer devours the newly-cut flesh. The Facedancer then undergoes a grotesque transformation, becoming a perfect copy of their meal: a true doppelganger. Everything from physical appearance, memories, affectations and even Magickal powers can be used by the ordinarily-frail Facedancer. This change is not permanent nor is it perfect; the transformation only lasts until the following new moon, while the Facedancer's eyes are of two different colors; a mark of surefire deception  or as Inquisitors put it, the mark of a heretic! Facedancers will often travel with several 'faces' in a water-proof bag at their side; a grotesquery, but one that can help maintain their illusions.",
            "It is unknown if the Facedancers are native Fey or are like the Dullahan, having simply been adopted into their ranks. Many like to say their mutable forms were the first to take form within the Black Lodge, but other Fey laugh at this idea. Facedancers feel spiritual pain while not transformed; the memories from all their assumed identities clash, driving them to moments of dj vu: events which never transpired. Assuming new identifies ebbs away at the despair they feel, but there is no cure for a Facedancer's anguish aside from continuous, ritual murder."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 35,
                "bonus": 3
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 50,
                "bonus": 7
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 3,
        "peril_threshold": 6,
        "parry": {
            "chance": 40,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "bargain": 10,
            "charm": 10,
            "disguise": 10,
            "folklore": 10,
            "guile": 10,
            "interrogation": 10,
            "resolve": 10,
            "rumor": 10,
            "scrutinize": 10
        },
        "attack_profile": [
            {
                "name": "Ritual Dagger",
                "chance": 40,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Light",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "FACES OF DEATH",
                "description": "After these creatures consume the face of someone they've killed, they assume their memories and mannerisms with near perfection. This includes use of Magick, if applicable. Only with a Critically Succeeded Scrutinize Test would someone else tell otherwise. This Trait's effects lasts until the new moon."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden"
            }
        ],
        "trappings": [
            "Ritual dagger",
            "Slingbag with faces inside (3)"
        ]
    },
    {
        "type": "REDCAP",
        "family": "FEY",
        "description": [
            "At times, even the most capricious of creatures are not of the Abyss at all. Skulking among the brush and loam of the forest, the mischievous Redcaps are malicious sprites, given countless names from dunter and powrie to knocker and pixie.",
            "When a man is entwined by the charms of a Woodland Nymph, the conniving figures of the Redcap are the rotten fruit born from such couplings. They are born fully formed and intelligent, emerging from the nymph's sourest of fruits in the downturn of spring. Redcaps are born with the countenance of old men, their backs crooked and their skin wrinkled and tanned. After birth, they then disperse, donning local toadstools as primitive head coverings on their maddening crusade to cleanse the wood.",
            "Redcaps are undoubtedly Fey spirits of fury and vengeance, beings who have inherited the worst traits of both their parents. After killing their enemies, they soak the worn toadstool cap in their victim's blood, using it as a sort of odd sacrifice to ancient devilish spirits of the forest. When not killing intruders, their favorite activities include rolling boulders onto travelers, setting fire to settlements and maiming livestock  they have no interests other than general mayhem. Luckily, Redcaps are vanishingly rare, congregating in small circles among ruined settlements. They are also cautious to the powers of the gods, reticent to attack priests. Redcaps are also of interest to wizards and alchemists, as the red cap mushrooms they grow are a potent inducer of bloodlust and their fangs can be ground into smelling salts."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "eavesdrop": 10,
            "intimidate": 10,
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10
        },
        "attack_profile": [
            {
                "name": "Pike",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "4",
                "qualities": [
                    "Reach"
                ]
            },
            {
                "name": "Redcap Fangs",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "5",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden"
            }
        ],
        "trappings": [
            "Blood-soaked toadstool hat",
            "Iron-shod heavy boots",
            "Pike",
            "Red cap mushroom (3)",
            "Redcap fang (2)"
        ]
    },
    {
        "type": "SIDHE LORD",
        "family": "FEY",
        "description": [
            "In the darkest reaches of the primordial, spectral forests of betwixt here and the Black Lodge, the capricious Sidhe Lords hold court over Fey of all kinds. Little is known about these beings, aside from the fact that they are aspirational, capable and quite mad.",
            "Sidhe Lords were thought to be the original servants of the long dead gods of the dawn age  perhaps champions who were looked upon with favor or maybe semi-divine beings. Either way, they possess both immense power and the prolonged life amongst the fecund lands of the Black Lodge. However, the old gods slowly started to die and with their passing the Black Lodge shrunk and was consumed by the Well of Souls. The Sidhe Lords used what power they could to enforce their borders, but the ravages of the Well's dark energies ate away at their very minds. Soon enough, the Sidhe Lords saw themselves as the only protectors of the Fey  the new gods who were destined to rise up and usurp reality as many know it. Their lack of numbers and their being riven by narcissism and hate not only drove the Sidhe Lords asunder into two 'Coterie' which still exist to this day, but also continue to thwart their ambitions. Each Coterie is full of loyal Fey, fettered mortal prisoners and the occasional Abyssal ally.",
            "Each Sidhe Lord may as well be a god to their chosen people  their power is immense, as they can sing spectral songs to nature itself to allow it to bend to their will. In appearance, Sidhe Lords often appear as one of mankind, though with supernatural, naturalistic aspects  a wispy maid with cascading water for hair, a strong Siabran warrior with stag horns growing from their head or a burly barbarian made completely of stone and ice. They tend towards whimsical or mystical names as well, such as Jack Frost, Morgause, Oberon, Titania, Una or the White Witchling. Many Sidhe Lords also actively welcome mortals into their grand palaces, but do not be fooled. Sidhe Lords are capricious and scheming to a fault, mad manipulators whose benedictions and opinions shift as the seasons do. They will stop at nothing to gain power to intrude upon the Material Realm and become the 'new gods of the old way', maneuvering their Coterie to tear down all idols of false gods and erect a new tomorrow."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 50,
                "bonus": 6
            },
            "agility": {
                "base": 50,
                "bonus": 13
            },
            "perception": {
                "base": 55,
                "bonus": 9
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 55,
                "bonus": 7
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 12,
        "movement": [
            {
                "value": 16,
                "type": "normal"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": 13,
        "parry": null,
        "dodge": {
            "chance": 90,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "athletics": 10,
            "charm": 30,
            "coordination": 30,
            "disguise": 30,
            "eavesdrop": 20,
            "folklore": 30,
            "guile": 30,
            "interrogation": 30,
            "martial_melee": 30,
            "martial_ranged": 30,
            "survival": 30
        },
        "attack_profile": [
            {
                "name": "Ironwood Bow",
                "chance": 80,
                "distance": "ranged 21 yards",
                "load": "2 AP",
                "damage": "13",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Punishing"
                ]
            },
            {
                "name": "Harlequin Sword",
                "chance": 80,
                "distance": "melee engaged",
                "damage": "13",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CAPTIVATING CRY",
                "description": "When foes can hear this creature, they must Resist with a successful Resolve Test or be drawn towards the sound. Should they enter a dangerous area to find the sound, they can attempt another Resolve Test. Once they are able to visually see the creature, the Captivating Cry's effects end."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden and Æthereal foes, and can see in the dark."
            },
            {
                "name": "FORESIGHT",
                "description": "When these creatures fail any Skill Test, they may re-roll to generate a better result but must accept the outcome."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "POSSESSION",
                "description": "These creatures can use Skin Guest from the Arcana of Sorcery without having to make an Incantation Test. In addition, they do not have to maintain Concentration."
            },
            {
                "name": "SHAPESHIFTER",
                "description": "These creatures may take any form they please, providing it is of a Small or Large Animal or Humanoid (including player Ancestries)."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            },
            {
                "name": "TRUE NAME",
                "description": "Unless foes invoke this creature's True Name before casting Magick, it fails to affect the creature."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Harlequin sword",
            "Ironwood bow",
            "Sidhe armor"
        ]
    },
    {
        "type": "TALKING TREE",
        "family": "FEY",
        "description": [
            "Though Fey are terrifying and violent spiritual creatures, some actually are somewhat placid beings. The most common of these are the Talking Trees  they are Fey, true, but they were born and molded in the Material Realm, so their mind and ways are far more stable than their other spectral brethren.",
            "Talking Trees are created when virginal demihumans commit suicide in or among the branches of ancient trees. Their soul takes residence inside the tree, to live forever in a somber and taciturn nature, watching over the relative peace of their deathbeds. Talking Trees generally stay in one place, anchored by their roots, but they are able to walk about slowly when necessary. Their normal movements are ponderous and their minds are slow, but insightful, thinking for hours upon perceived problems and riddles. Many creatures of the forest see the Talking Trees as wise sages worthy of respect  in fact, Elven maidens known as 'dryads' care for and tend to the trees as a sign of devotion. They are also thought to be blessed by the Demiurge, for Talking Trees speak as men during the day and as women at night. Talking Trees stand as tall as a Nephilim and are terribly powerful, yet their branches can be as delicate as a moth's wing. They can appear as many types of trees, but they generally suit their personality  the sad and despondent are weeping willows; the strong and mighty are towering pine trees; and those given to gravidity are petrified.",
            "Though Talking Trees rarely resort to violence, some do rot into corruption and darkness. Many Unseelie Talking Trees are known as foulroots, their bark turned black and their arboreal form given the terrible power of Sorcery. They often lie in wait for passersby, still as a tree until they grasp at their prey and rip them apart. Seelie Talking Trees are instead gifted with prophecy and divine insight, knowing the secrets of the movements of the stars. They also actively enforce the growth of the forest, maintaining hovels deep below the earth where they keep their workshops and raise saplings. Seelie Talking Trees see foulroots as little more than dead wood, things that should be cut down and burnt in order to clear space and undergrowth necessary to grow newer, healthier trees and forests."
        ],
        "size": [
            "Large",
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 13
            },
            "agility": {
                "base": 35,
                "bonus": 3
            },
            "perception": {
                "base": 40,
                "bonus": 7
            },
            "intelligence": {
                "base": 40,
                "bonus": 6
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 6,
                "type": "normal"
            }
        ],
        "damage_threshold": 18,
        "peril_threshold": 9,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 45,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "alchemy": 30,
            "athletics": 20,
            "folklore": 30,
            "handle_animal": 30,
            "incantation": 30,
            "navigation": 20,
            "resolve": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "survival": 30,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Bare-handed",
                "chance": 75,
                "distance": "melee engaged or 1 yard",
                "damage": "13",
                "qualities": [
                    "Reach",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden and Æthereal foes, and can see in the dark."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "NATURAL ARMOR (5)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            },
            {
                "name": "UNGAINLY",
                "description": "When these creatures are Slain! all those Engaged with it must succeed at a Coordination Test or be knocked Prone beneath of it, suffering 3D10+3 Damage from falling."
            },
            {
                "name": "UNNATURAL VISCERA",
                "description": "These creatures are immune to attacks made with ranged weapons. However, they cannot use any Movement Action that requires 3 APs."
            },
            {
                "name": "WINDS OF CHAOS",
                "description": "When casting Generalist Magick at their option  these creatures can automatically succeed at the Incantation Test, but must drop one step down the Peril Condition Track negatively. In addition, they must always"
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Reagents for all Magick spells (9)"
        ]
    },
    {
        "type": "WOODLAND NYMPH",
        "family": "FEY",
        "description": [
            "Probably the most well-known of all the Fey are the lustful and dangerous Woodland Nymphs. They sit among the branches and glades, calling for both the seed and blood of men.",
            "Woodlands Nymphs are comely and otherworldly women, slight of figure but large of hip and breast. Their skin color ranges from greens to browns and their hair appears to be made of vines, leaves or mosses. They are always seen around a specific tree, one that each Nymph protects like a mewling child. They keep the roots of the tree free of weeds, maintaining fairy rings of toadstools and tend the wildflower gardens around the aged giants. The Nymphs also produce a syrupy breast milk; one they squeeze into clay jars to nourish the tree's roots with. Normally the Nymph is very shy and modest; if they are sighted by an outsider, they will turn Æthereal and enter the tree they tend, only emerging when the interloper is gone. But do not threaten their tree or gardens in any way, as this can only lead to death. If the plant is at risk, the Nymph will emerge from their hiding place as the terrible creature called L'wraithian (a word in Elvish meaings 'haunt'). In this violent form of nature's wrath, their skin hardens to bark, their fingers elongating into claws and they take on the aspects of the seasons while maintaining a hauntingly terrible and wrathful beauty.",
            "Woodland Nymphs care for animals of the forest as they do for their precious tree. They have a notorious weakness for musicians and singers  inviting them to sexual congress in more than one way if they can spin a beautiful lyric. This is often used to obtain a sampling of the Nymph's breast milk, which has intrinsic healing properties and is used in druidic spells. In exchange, the Nymph will often coyly ask for the seducer's seed. However, these matings breed terrible fruit in the form of malicious Redcaps. These slight Fey grow like apples from the Nymph's limbs, until they eventually fall off and burst into new life. The Red Cap is then rumored to eventually return and kill its father, which is where the crux of the deal lies."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 5
            },
            "agility": {
                "base": 50,
                "bonus": 8
            },
            "perception": {
                "base": 45,
                "bonus": 8
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 7
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 11,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 10,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Advanced",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "coordination": 10,
            "charm": 30,
            "eavesdrop": 20,
            "folklore": 30,
            "guile": 20,
            "navigation": 10,
            "resolve": 10,
            "rumor": 20,
            "simple_melee": 30,
            "stealth": 10,
            "survival": 30
        },
        "attack_profile": [
            {
                "name": "Branch-like Claws",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "8",
                "qualities": [
                    "Entangling",
                    "Pummeling",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden and Æthereal foes, and can see in the dark."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "HATRED (Human)",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these creatures."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "SEASONS TO PASS",
                "description": "When these creatures enter combat, they take upon a different aspect every Turn which passes. In the first Turn, they take on the aspect of spring, gaining +3 to Damage Threshold. In the next Turn, they lose the +3 to Damage Threshold, but take on the aspect of summer, gaining total immunity to fire. In the following Turn, they lose their total immunity to fire and take on the aspect of autumn, able to use two Melee Attacks on that Turn for 2 AP. In the final Turn, they lose their ability to use two Melee Attacks on and take on the aspect of winter, able to Dodge and Parry for 0 APs. This creature may only take upon one aspect at a time, and continues to cycle through the four seasons as their Turns pass."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Jug of breast milk",
            "Red cap mushrooms (3)"
        ]
    },
    {
        "type": "FOMORI CRONE",
        "family": "FOMORIAN",
        "description": [
            "Crones are the direct descendants of Ogres who ate the largest portion of the mutating kelp offered by the Witch-Queen. This caused their bodies and bloodlines to mutate beyond that of other Ogres and into the massive Crones. Absurdly tall, their entire lower bodies are replaced by massive, suckercovered tentacles, their jaws and heads extending out eellike, their bodies lined with bioluminescent lights similar to an anglerfish. Crones dwell inside massive chthonic pits or off the deepest ends of coastal shelves, from where they rarely leave or indeed venture above water. The only occasions when they do leave are to support the war bands of Thralls and Huscarls they send out to attack the races on land. The Huscarls usually lead these bands, but as leaders of covens  the general organization of the Fomori  Crones will go to any lengths to achieve victory. Crones only communicate with them via psychic communication and indeed, some say that Crones are just the sea serpents and krakens of ancient legend, given the power of telepathy and control over the Fomorian host.",
            "From their deep pits and waters, Crones direct their covens with almost maniacal instinct. Perhaps they are wise, or maybe their thought processes are so alien as to be seemingly mindless. They also possess an instinctual understanding of Magick and in addition to any lore they know, they can summon a hazy mist that can envelop a countryside in minutes. This mist is a boon to other Fomori, as it allows them to stalk during the day without fear of injury from the sun.",
            "The origin and purpose of the Crone remains a mystery, even to their Fomorian followers. What is certain is that Crones are highly dangerous and destructive, consumed with a rage and frustration that leaves armies drowned, ships ravaged and the Fomori with bellies full of meat."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 55,
                "bonus": 7
            },
            "brawn": {
                "base": 50,
                "bonus": 6
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 50,
                "bonus": 7
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 50,
                "bonus": 10
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 13,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": 13,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "alchemy": 30,
            "disguise": 30,
            "eavesdrop": 20,
            "folklore": 30,
            "guile": 30,
            "handle_animal": 10,
            "incantation": 30,
            "simple_melee": 30,
            "survival": 30,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Acidic Spittle",
                "chance": 55,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            },
            {
                "name": "Sacrificial Knife",
                "chance": 85,
                "distance": "melee engaged or 1 yard",
                "damage": "7",
                "qualities": [
                    "Fast",
                    "Reach",
                    "Vicious"
                ]
            },
            {
                "name": "Lashing Tail",
                "chance": 85,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Pummeling",
                    "Punishing",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ACIDIC SPITTLE",
                "description": "These creatures can use their saliva as a ranged weapon. This allows them to strike a single foe within 1+[PB], causing the foe to suffer 1D10+1 Damage from acid. However, Acidic Spittle ignores a foe's Damage Threshold Modifier from armor. A foe can attempt to Dodge Acidic Spittle or Parry it with a shield. Acidic Spittle can be used while Engaged with foes."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "ENVELOPING FOG",
                "description": "These creatures can summon fog anywhere in sight. Those caught in an Explosion Template area of effect must flip the results to fail the following tests,"
            },
            {
                "name": "until they escape",
                "description": "Leadership, Martial Ranged, Resolve and Simple Ranged. Fomorian are immune to its effects."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden and Æthereal foes, and can see in the dark."
            },
            {
                "name": "GROSSLY PARANOID",
                "description": "These creatures have factored in +3 to their Initiative."
            },
            {
                "name": "NATURAL ARMOR (4)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "NEVER SAY DIE",
                "description": "When these creatures are Grievously Wounded, add 3 to their Damage Threshold."
            },
            {
                "name": "OCCULT MASTERY",
                "description": "When this creature Channels Power and succeeds at their Skill Test, they Critically"
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Loose robes",
            "Rune stones",
            "Reagents appropriate for Magicks (9)",
            "Sacrificial knife"
        ]
    },
    {
        "type": "FOMORI HUSCARL",
        "family": "FOMORIAN",
        "description": [
            "The cruel Huscarls are more crustacean-like than the lesser Thralls. Their skin is rigid and plated, with barnacles and starfish clinging to their bodies; their arms end in massive, briny claws; their legs consist of six spindly, yet powerful segmented limbs; and their jaws are a distended mass of mandibles and mouthparts. Huscarls serve as military commanders and nobles, both advising the Crones and leading small squadrons of other Fomori, usually no larger than thirty in number. They carry the flame of the old Ogre kingdoms, naming their squads after lost tribes such as 'Irongut' and 'Blackteeth', but essentially this is a tradition without meaning whatsoever. Huscarls are great strategists and warriors, but terrible diplomats  their frequent bloody rages leave any pact making with landlubbers an impossibility. Their pride is also immense and there is constant infighting among Huscarls to prove who is the coven's worthiest Huscarl.",
            "Huscarls are powerful and intimidating, but they do have a weakness. Roughly once every six years, a Huscarl outgrows their carapace shell and must shed it. During this time, they are extremely vulnerable since their new armor has not fully hardened and the actual process is long and tiring. This is when rival Huscarls strike, so a weakened Huscarl will place the Thralls under his command on 'molt duty' in order to guard against assassination. When the Huscarls emerge however, their armor is tougher and their claws even more massive. Since Fomorian are extremely long-lived, Huscarls have to go through this process an average of a hundred times before their natural death."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 7
            },
            "brawn": {
                "base": 45,
                "bonus": 7
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 12,
        "peril_threshold": 8,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "intimidate": 20,
            "interrogation": 10,
            "martial_melee": 20,
            "martial_ranged": 20,
            "resolve": 20,
            "ride": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "survival": 10,
            "toughness": 10,
            "warfare": 20
        },
        "attack_profile": [
            {
                "name": "Zweihnder",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "7",
                "qualities": [
                    "Punishing",
                    "Reach",
                    "Slow"
                ]
            },
            {
                "name": "Lashing Tail",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "7",
                "qualities": [
                    "Pummeling",
                    "Punishing",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden and Æthereal foes, and can see in the dark."
            },
            {
                "name": "NATURAL ARMOR (5)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries"
            }
        ],
        "trappings": [
            "Metal shield",
            "War trophies (3)",
            "Zweihnder"
        ]
    },
    {
        "type": "FOMORI THRALL",
        "family": "FOMORIAN",
        "description": [
            "Thralls know nothing but toil  their work is varied and horrid, from serving as butchers and carving the Fomorian's native caves to fighting constant wars with the land-dwelling races and being the object of cruel games by their Huscarl masters. Their putrid green skin is rough and scaly and their heads have a definite batrachian cast to them. By far the smallest of the Fomorian, they still tower over most landborn humanoids. They cannot survive long away from the water, but they often venture ashore, moving in more of a floundering flop than walking, to make terrible pacts with coastal villages and to collect tribute. Unlike other Fomori, Thralls are not sterile, but they cannot mate with each other. Instead, they must mate with humanoid females, any children being born at first appearing as normal. At some point during their lives, these children will answer the call of the sea and mutate into a Fomori Thrall. This foul breeding is often included as part of the tribute for the pact.",
            "When not laboring, Thralls spend their time basking at their glory of their Crone or fighting battles against the many despised enemies of Fomori, armed with slings and salt-eaten nets. Fomori have a strange animosity for the Fey  the Fey care little for them, but the Fomori will focus their ire on the Fey if they can. Thralls are neither smart nor great strategists  both are left for the Huscarls to do  they just launch themselves forward like tidal waves, which is usually more than enough to rattle foundations and leave hundreds dead."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 8,
        "parry": {
            "chance": 55,
            "qualities": []
        },
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "intimidate": 10,
            "resolve": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "survival": 10,
            "toughness": 10,
            "tradecraft": 10
        },
        "attack_profile": [
            {
                "name": "Shiv",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Weak"
                ]
            },
            {
                "name": "Shepherd's Sling",
                "chance": 55,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Throwing",
                    "Weak"
                ]
            },
            {
                "name": "Lashing Tail",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Pummeling",
                    "Punishing",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "FEY SIGHT",
                "description": "These creatures automatically spot hidden and Æthereal foes, and can see in the dark."
            },
            {
                "name": "NATURAL ARMOR (2)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these"
            }
        ],
        "trappings": [
            "Dirk",
            "Shepherd's sling",
            "Sling stones (9)"
        ]
    },
    {
        "type": "HOBGOBLIN",
        "family": "GOBLIN",
        "description": [
            "Each of the numerous Goblin gangs is led by a Hobgoblin, which with their skin leathery from lashing winds and their wiry frames, tower over their Goblin kin. Hobgoblins rise through the gang in a series of violent initiation rituals, terrible crimes and acts of perverse frontier justice that feed the demon inside of them, causing them to grow taller and broader than other Goblins. They are efficient and brutal leaders, but hesitate to kill their own as their main strength is in their numbers. That does not mean they are above violent intimidation, however. Hobgoblins often wear sobriquet or elaborate garb over their face, if only to appear more menacing to outsiders. It is said that all Hobgoblins follow a leader called a 'Khan', upjumped from the roughest of their crew. Most scholars agree the Khan is actually an Orx Bigboss, but Hobgoblins scoff at the notion while nervously glancing over their shoulder.",
            "Unlike Goblins, they are unable to reproduce asexually. Instead, they mate with each other, producing Hobgoblin offspring. This gives them a sense of permanence and family that Goblins do not quite have and is one more reason why Hobgoblins are both leaders and teachers among their kind. Hobgoblins are, however, notorious liars and untrustworthy  allying with Orx one day, only to turn their backs and join the Dvergar the next. Their reputation is so bad that even other Hobgoblins distrust them, despite a shared heritage."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 7,
        "parry": {
            "chance": 65,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 50,
            "qualities": []
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "alchemy": 10,
            "athletics": 10,
            "awareness": 10,
            "eavesdrop": 10,
            "martial_melee": 10,
            "ride": 10,
            "simple_ranged": 10,
            "stealth": 10,
            "survival": 10,
            "warfare": 10
        },
        "attack_profile": [
            {
                "name": "Sabre",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Defensive"
                ]
            },
            {
                "name": "Hunting Bow",
                "chance": 55,
                "distance": "ranged 15 yards",
                "load": "1 AP",
                "damage": "7",
                "qualities": [
                    "Finesse",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HATRED (Dwarf, Gnome)",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "SADDLE TACTICS",
                "description": "When fighting on horseback or a vehicle, they flip the results to succeed at Attack Actions and Perilous Stunts."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth"
            }
        ],
        "trappings": [
            "Arrows (6)",
            "Folkbane (3)",
            "Hunting bow",
            "Leather armor",
            "Sabre"
        ]
    },
    {
        "type": "KOBOLD",
        "family": "GOBLIN",
        "description": [
            "When there is smoke, there is fire  and the acrid smoke of the Orx WAAAR! Horde is often led in the vanguard by a massive wave of gibbering Kobolds. Meaning 'swarm ' in their tongue, they make up the bulk of any Goblin gang. These bands consist of the hedonistic and lackadaisical Kobolds, who like nothing more than wandering between backwater villages to raid, drink and kill. Among themselves, Kobolds possess a strange sense of honor and camaraderie, seeing each other more as blood brothers than simply allies. That does not mean they will not turn on each other, as respect through force essentially sums up their brutal philosophy. Enough disrespect leaves the Kobold ousted from their band and liable to be enslaved by Orx WAAAR! Hordes.",
            "Kobolds are often mischievous, cruel and even stupid. They love to torture their live meals before devouring them with their sharp, filed teeth, as they feel 'wriggly' meat is the best. Many Kobold bands will run into towns, slaughtering livestock, destroying property and crops and stealing candy from babies  all for the fun of it. If a Kobold is ever found alone, it is absurdly weak and little more than a bother to kill, but they often tend to get too big for their britches  they will curse, tease and talk a big game even as they are being pulled apart piece by piece. Perhaps that is what sets Kobolds apart from their Orx masters  they have seen and been through so much, that they have come to see anarchy and nihilism as the best way to cope."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 7,
        "parry": {
            "chance": 65,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 60,
            "qualities": []
        },
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "ride": 10,
            "simple_melee": 10,
            "stealth": 10
        },
        "attack_profile": [
            {
                "name": "Fire-hardened Spear",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "4",
                "qualities": [
                    "Reach",
                    "Weak"
                ]
            },
            {
                "name": "Javelin",
                "chance": 45,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HATRED (Dwarf, Gnome)",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "SQUABBLE AMONG THEMSELVES",
                "description": "When these creatures suffer from Fear or Terror, they must succeed at a Resolve Test as their Turn starts or else attack their own"
            }
        ],
        "trappings": [
            "Buckler",
            "Fire-hardened spear",
            "Javelin (3)",
            "Quilted armor"
        ]
    },
    {
        "type": "KOBOLD FANATIC",
        "family": "GOBLIN",
        "description": [
            "Stalking out of the shadows, wearing ratty cloaks that conceal their pale flesh from the sun, slink the Kobold Fanatics. Trailing behind a rabid mob of hungry Fodderlings, Fanatics direct them where they can sow the most chaos.",
            "Kobold Fanatics are the Goblins who breed and care for Fodderlings, the trademark mount of the Goblin gangs. Fodderlings are born and live in deep caves and as they care for them, the chthonic darkness changes the Kobold Fanatics. They become more deranged, hunched over with pale skin and pupiless eyes. When they do climb to the surface and crouch under the sun, they must wear heavy cloaks or garments to avoid its burning light. Apart from their duties caring for Fodderlings, Fanatics are all but useless to a Goblin gang during the daytime. As dusk falls, however, they remove their cloaks and are sent on dangerous espionage and assassination missions against the gang's enemies.",
            "Goblin lands are notoriously spider-infested, but these arachnids are often the size of large hounds and must be placated lest they devour whole tribes. Kobold Fanatics are sometimes tasked with warding off or even caring for these massive spiders, often breeding them alongside Fodderlings. Some even fall under the thrall of the monstrous Attercap Spider  poisoned with her hallucinatory venom, they do as the Attercap demands, bringing food and expanding their territory. The Orx do not mind this all that much, as Kobold Fanatics are easy to find and Attercap Spiders make powerful allies in their endless war machine. As beneficial as this is, both Orx and Goblins are understandably wary of letting the Attercap take control over the entire situation."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 7,
        "parry": {
            "chance": 55,
            "qualities": []
        },
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "coordination": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "stealth": 10
        },
        "attack_profile": [
            {
                "name": "Threshing Flail",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "4 or 5 two-handed",
                "qualities": [
                    "Adaptable",
                    "Reach",
                    "Weak"
                ]
            },
            {
                "name": "Net",
                "chance": 55,
                "distance": "ranged 5 yards",
                "load": "1 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "AVERSION TO LIGHT",
                "description": "When these creatures are exposed to any sort of light (such as from a torch), they suffer a penalty of -3 to their Damage Threshold."
            },
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HATRED (Dwarf, Gnome)",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "SQUABBLE AMONG THEMSELVES",
                "description": "When these creatures suffer from Fear or Terror, they must succeed at a Resolve Test as their Turn starts or else attack their own"
            }
        ],
        "trappings": [
            "Net",
            "Red cap mushroom (3)",
            "Religious token",
            "Tattered clothing",
            "Threshing flail"
        ]
    },
    {
        "type": "BROO",
        "family": "GRENDEL",
        "description": [
            "Grendel are not fully formed when they emerge from their mother's womb. Amongst the squalling and the screaming is a rather humanoid-looking babe, but with a few radical differences.",
            "These adolescents are called Broo (so named for the braying they make as they fight and eat) and they appear as Human males covered in thick courses of hair. In addition to being quite hirsute, Broo possess other bestial traits, though none too extreme. Depending on what sort of beast they take after, their teeth will often either be quite sharp or be squat and flattened. Their nails grow to incredible lengths, with eyes of an animalistic mien, while some will even display other traits such as small goat-like horns or hairy snouts. Broo are eager to prove themselves, to 'earn their horns', spending their days learning the ways of the Wild Hunt by wrestling, hunting and being mentored by the Wose. Though few Grendel take personal possession of their children, they will often communally raise the Broo in order to raise them to be strong and capable. The weak and sickly are abandoned, if not outright killed. There is also a special breed of Broo, those fathered by Taurus. They are born with animalistic legs, but a humanoid upper body. They tend to be tricksters and ne'erdo-wells, the pride of the debauched Grendel packs.",
            "Broo age quickly and as they grow, their bestial traits become more exaggerated and explicit. By the age of two, they appear as a Human in their late teens, ready to find their first mate. The Broo will go about an adulthood ceremony at this time called the 'first rut', which occurs on the first full moon of that year. Broo at this age are also deemed warready and take part in smaller pack raids, though they are still deemed too inexperienced to join the Wild Hunt. By the age of three, a Broo is fully grown, becoming a Hircus and a full member of the Wild Hunt. This rite of passage, like most things in Grendel life, is a cause for celebration  the main aspect of the event being a sparring bout between the Broo and its father, in order to show their assertion of dominance and their rejection of control. Nothing makes a Grendel father prouder than to be supplanted by their son."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 7,
        "parry": {
            "chance": 65,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Basic",
        "risk_factor": "Low",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "intimidate": 10,
            "simple_melee": 10
        },
        "attack_profile": [
            {
                "name": "Fire-hardened Spear",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "4",
                "qualities": [
                    "Reach",
                    "Weak"
                ]
            },
            {
                "name": "Mutated Hooves",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Pummeling",
                    "Powerful",
                    "Slow"
                ]
            },
            {
                "name": "Javelin",
                "chance": 45,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "DIONYSIAN DELIGHTS",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', they are Intoxicated. If the result is face '6', they are not Intoxicated."
            },
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to"
            }
        ],
        "trappings": [
            "Buckler",
            "Fire-hardened spear",
            "Hide armor",
            "Javelin (3)",
            "Jug of Dionysian Delights"
        ]
    },
    {
        "type": "EQUIGORN",
        "family": "GRENDEL",
        "description": [
            "Grendel follow a myriad of countless Daemons in history who supposedly laid with women, men and animals. Along with the countless women and men in Grendel mating pens, a true animal or two  such as a cow or mare  is often seen.",
            "Grendel mate, too with these creatures and the resulting birth are the maligned Equigorn. These creatures have their legs replaced with the entire body of their maternal animal, while their upper torso sits upon where the head and neck of the beast would normally be. These six-limbed creatures age much as their other Broo brothers, growing more bestial with age, but they are never fully accepted by the Wild Hunt. There is an unspoken taboo of Grendel siring offspring with a beast, as it is believed it pollutes the blood. Though never exiled or banished, Equigorn are often excluded from group matings, larger raids and are given burdensome tasks that no Hircus would dare undertake. It is no surprise that Equigorn take heavily to alcohol more than other Grendel.",
            "When Equigorn are taken on raids, they are front line fighters who use their fury to strike down all in their path, wielding giant axes and trampling enemies with their enormously powerful legs. Their bodies also lend themselves to speed, making Equigorn ideal messengers and scouts. Within the Wild Hunt, they are used as little more than beasts of burden. There is rarely more than a dozen or so Equigorn in a Wild Hunt and even they avoid each other. Equigorn are brooding beasts and even the sex slaves rarely want to mate with them due to their size. This is not the fate of all these tortured souls, though; there have been reports of Wild Hunts composed of only these Grendel and their alphas are truly terrifying creatures."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 8
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 8
            },
            "fellowship": {
                "base": 40,
                "bonus": 5
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 11,
        "parry": {
            "chance": 70,
            "qualities": []
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "guile": 10,
            "interrogation": 10,
            "intimidate": 20,
            "martial_melee": 20,
            "resolve": 20,
            "simple_melee": 20,
            "stealth": 20,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Pike",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "5",
                "qualities": [
                    "Reach"
                ]
            },
            {
                "name": "Mutated Antlers",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            },
            {
                "name": "Mutated Hooves",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Pummeling",
                    "Powerful",
                    "Slow"
                ]
            },
            {
                "name": "Francisca",
                "chance": 50,
                "distance": "ranged 5 yards",
                "load": "1 AP",
                "damage": "5",
                "qualities": [
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLITZ",
                "description": "When these creatures Charge, they may flip the results to succeed at their next Attack Action or Perilous Stunt on the same Turn."
            },
            {
                "name": "DIONYSIAN DELIGHTS",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', they are Intoxicated. If the result is face '6', they are not Intoxicated."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to succeed at Skill Tests."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            },
            {
                "name": "WEAK SPOT (Legs)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Francisca (3)",
            "Hide armor",
            "Jug of Dionysian Delights (3)",
            "Pike"
        ]
    },
    {
        "type": "HIRCUS",
        "family": "GRENDEL",
        "description": [
            "Most Broo grow up to become a Hircus, by far the most common and well-known of the Grendel. As they age, their body hair becomes thicker and covers their whole form, their heads become those of animals, they sprout tails and their legs take on a bestial shape with an inverted knee. These are the main force of the Wild Hunt and they are generally considered the betas of the pack. The Hircus is also the first Grendel that truly displays their bestial heritage  while Broo are too young to feel the call and Wose have purposefully been castrated to control their urges, the Hircus are a different story. They are wild and bloodthirsty, resorting to ritualistic fights, called 'tussles', in order to quell the many conflicts within a Wild Hunt. Hircus also mate and cavort often, spending their free time in either the throes of passion or the depths of drink. The Hircus life is one filled with jealousy, one bent on proving your worth to the alpha and usurping your brothers. If a Hircus is ever deemed weak, they are ousted from the Wild Hunt and marked with a brand as a sign to others not to trust the wretch.",
            "Hircus are wild fighters, using their hooves and horns just as often as their blood-encrusted weaponry. They are intimidating as well, making bestial howls and screeches to herald their arrival, then jittering from the dark just out of sight. When not fighting, mating or drinking, Hircus undertake various menial chores for the Wild Hunt  slaughtering food, standing guard and scouting for new bases of operations. Wild Hunts are semi-nomadic, settling in the choked wilderness near a town and picking it free of mates, food and weapons  killing any remains before packing up and moving to the next settlement. Humanoids always fear for the random raids of a Wild Hunt and it is true that they are random  they are performed on a whim by the Hunt's Taurus."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 5
            },
            "brawn": {
                "base": 50,
                "bonus": 9
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": 7,
        "parry": {
            "chance": 75,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "charm": 10,
            "eavesdrop": 10,
            "gamble": 10,
            "intimidate": 10,
            "resolve": 10,
            "simple_melee": 20,
            "simple_ranged": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Fire-hardened Spear",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Adaptable",
                    "Slow",
                    "Weak"
                ]
            },
            {
                "name": "Mutated Antlers",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            },
            {
                "name": "Mutated Hooves",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Pummeling",
                    "Powerful",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLITZ",
                "description": "When these creatures Charge, they may flip the results to succeed at their next Attack Action or Perilous Stunt on the same Turn."
            },
            {
                "name": "DIONYSIAN DELIGHTS",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', they are Intoxicated. If the result is face '6', they are not Intoxicated."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to succeed at Skill Tests."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth"
            }
        ],
        "trappings": [
            "Fire-hardened spear",
            "Hide armor",
            "Jug of Dionysian Delights",
            "Wooden shield"
        ]
    },
    {
        "type": "TAURUS",
        "family": "GRENDEL",
        "description": [
            "Grendel lives are often short-lived; between accidental overdoses of hallucinogens, exhaustion from sexual conquest or simply dying by the sword during a raid, few Hircus see beyond their fourth year. But there are those that survive and conquer adversity, falling to the call of their bestial blood in order to ascend past mere status as a beta.",
            "These aged and powerful Grendel are known as Taurus and they tower above their Wild Hunts in both stature and power. The exact moment a Hircus becomes a Taurus is yet to be pinpointed, but the youngest Taurus ever slain is said to have been at least twenty years old  others in their second centuries have been found still leading their Wild Hunts. Taurus are tall and broadly muscled, reaching nearly the same heights as an Ogre: crisscrossed with scars, bestial hoofs, polycerate horns and tails grown massive and imposing. Few have retained the ability to speak, only communicating in growls, howls and brays that are interpreted by Wose. Taurus lay upon thrones of their dead enemies, surrounded by supplicants and slaves while fed a steady diet of food, alcohol and drugs. They rule above all other Grendel, their worth proved through combat and the strength of their seed.",
            "Though a Wild Hunt may be home to several Taurus, only one of them is favored the aforementioned luxuries of an alpha. Any Taurus who believes they can serve the pack better can issue a formal challenge to the current alpha, one that culminates in tests of combat, sexual prowess and imbibing. If the alpha is usurped, there is little shame in it  he was proven not as capable and thus submits and joins the throngs of the Wild Hunt once again. Motivations of the alphas are inscrutable and varied, but there is one certainty: that every Taurus desires to show humanoids the true ways of bestiality  though these true ways involve nothing more than violent acts of perversion. The alphas dream of a world of pleasure populated solely by Grendel. Some will even look towards fell gods for power, bringing their entire Wild Hunt in line with the terrible wishes of the Abyssal Princes, specifically The Outsider."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 10
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 9,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 30,
            "awareness": 20,
            "charm": 30,
            "interrogation": 10,
            "intimidate": 20,
            "martial_melee": 30,
            "resolve": 20,
            "simple_melee": 20,
            "stealth": 20,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Battle Axe",
                "chance": 80,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Adaptable",
                    "Slow"
                ]
            },
            {
                "name": "Mutated Horns",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Pummeling",
                    "Powerful",
                    "Punishing",
                    "Slow"
                ]
            },
            {
                "name": "Mutated Hooves",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Pummeling",
                    "Powerful",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DIONYSIAN DELIGHTS",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', they are Intoxicated. If the result is face '6', they are not Intoxicated."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to succeed at Skill Tests."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these creatures provoke one of the three brands of Madness: if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            }
        ],
        "trappings": [
            "Battle axe",
            "Hide armor",
            "Jug of Dionysian Delights"
        ]
    },
    {
        "type": "WOSE",
        "family": "GRENDEL",
        "description": [
            "When some Broo show exceptional learning and aptitude for the Grendel's runic language, they are removed from the main bulk of the Wild Hunt to commune with the Wose. There, they are ritually castrated and their masculinity burned upon a flame. This turns the simple Broo into the Wose, the advisors and scholars of the Grendel.",
            "Trained in all manners of knowledge from strategy to simple arithmetic to the proper prayers to say to the gods, Wose are deemed as wise sages who have been removed of their 'second mind' to fully focus on their first one. A side-effect of the castration seems to be that the Wose's bestial features are mitigated  their body hair will fall out and their claws will shorten, but their head shape shifts radically. Eventually, a Wose will appear as a normal man with the head of a beast and this is considered a divine blessing. Wose spend much of their day either consulting with the alpha, caring for the wives in the mating pens or teaching Broo all the intricacies of beastdom. In common lore, it is the Wose who often tempt mortals into drinking from decanters of Dionysian Delights. This blessed wine reputedly turns mortals into a fledgling Grendel, and these rumors are, in fact, true to some degree. Those who drink of the jug not only gain a Horrid Mutation they eventually devolve into a Wose too.",
            "Wose carry and wear little, often possessing only a flowing toga and a walking staff. Few ever accompany raiding parties on expeditions  those that do usually serve as morale boosters shouting benedictions to their kin. That does not mean Wose are defenseless, as they have undergone the first rut and the ritual fights all Grendel must perform. Indeed, Wose have less to lose, as they are incapable of mating and can never aspire to become the alpha. Rumors abound that some Wose have even learned to beseech the woods and their spirits for their aid and such whisperings pose a mighty threat if they hold any merit. Above all, the Wose speak to The Outsider  the abandoned fifth Abyssal Prince who has become the patron of the Grendel and undivided chaos. It is He that allows the Wose to speak to the trees and it is He who wishes their libations to never end."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 35,
                "bonus": 3
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 40,
                "bonus": 5
            },
            "intelligence": {
                "base": 45,
                "bonus": 6
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 50,
                "bonus": 6
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 9,
        "parry": {
            "chance": 40,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": []
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "bargain": 10,
            "coordination": 10,
            "eavesdrop": 10,
            "folklore": 10,
            "incantation": 10,
            "intimidate": 10,
            "leadership": 10,
            "resolve": 10,
            "rumor": 10,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Staff",
                "chance": 40,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Adaptable",
                    "Pummeling",
                    "Weak"
                ]
            },
            {
                "name": "Mutated Antlers",
                "chance": 40,
                "distance": "melee engaged",
                "damage": "3",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "DIONYSIAN DELIGHTS",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', they are Intoxicated. If the result is face '6', they are not Intoxicated."
            },
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to succeed at Skill Tests."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            },
            {
                "name": "WINDS OF CHAOS",
                "description": "When casting Generalist Magick at their option  these creatures can automatically succeed at the Incantation Test, but must drop one step down the Peril Condition Track negatively. In addition, they must always"
            }
        ],
        "trappings": [
            "Diabolical holy symbol",
            "Hide armor",
            "Jug of Dionysian Delights",
            "Loose robes",
            "Prayer book with 6 Petty Magick spells",
            "Reagents appropriate for Magicks (9)",
            "Staff"
        ]
    },
    {
        "type": "BARGHEST",
        "family": "HELLBEAST",
        "description": [
            "'Cry havoc and let slip the dogs of war' is a popular phrase among those generals who use war hounds in their armies. But it more often slips from the lips of Mutants and Demon legions, who send their vicious Barghests to chase and rend their enemies.",
            "Barghests are demonic hounds, created through feeding a dog many dosages of Wytchstone Essence. The hound grows massive and hunched over, their visages cut back into a permanent, fanged Cheshire smile. Their hair becomes coarse and grey, as spikes grow from their backs and aggression takes them over. Like Night Mares, this is only the general, common form of a Barghest. Some are skeletal hounds, others are deadly iron-covered juggernauts and still others are even odder chimeras. Barghests thirst for nothing more than blood and viscera, but they are actually quite loyal and intelligent. Demons and those tainted by Corruption rarely keep close allies, but it turns out that dog remains man's best friend even if both parties have a few extra limbs.",
            "Barghests can also be created accidentally, especially if a hapless pooch stumbles upon a Wytchstone cache and decides to sleep near it. The mutation is often instant and quite painful, as the powers of Corruption seem to have their terrible ways with the poor forms of mindless beasts. Even then, the Barghest retains its loyalty and despite being a mutated demon will still follow its owner as a trusted companion. That being said, few townsfolk look favorably upon the local child walking around with a hellhound from the depths of the Abyss, so the Barghest is often put down violently with little ceremony."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 0
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "coordination": 10,
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Hellbite",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Entangling",
                    "Slow",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "FEEL THE HEAT",
                "description": "When foes are Engaged with this creature, they must Resist with a successful Coordination Test or be exposed to Mildly Dangerous flames."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage,"
            }
        ],
        "trappings": [
            "Animal hide",
            "Animal meat (6)"
        ]
    },
    {
        "type": "LEMURIAN HOST",
        "family": "HELLBEAST",
        "description": [
            "All Abyssal life starts somewhere and the beginning of the terrible hell that is demoniac existence starts in the wretched form of the Lemurian Host.",
            "The lowest level of the Abyss constantly spits out motes of concentrated chaos, many of which will rise through the layers on hopes of reaching the status of becoming a Lower Demon. Alas, not all of these motes of chaos are destined for even the lowest of ranks in the Abyssal hierarchy  in fact, a large majority of them will swirl endlessly around the Abyss like dust in the sunlight. These bits of chaos often appear as flaming skulls, screaming and exuding a stench of sulphur or even tiny, discombobulated creatures of malice. Occasionally, they will band together  more or less  for survival purposes, forming a Lemurian Host. Lemurian Hosts are basically massive swarms of these motes, who when banded together can combine their essence to make new forms and objects. Lemurian Hosts are wholly mindless  they just seem to go where violence happens and commit it themselves. Hosts have different names throughout society, such as will o' wisps, sprites or cacodemons, but they are all mostly mischievous and dangerous.",
            "Lemurian Hosts cannot be summoned traditionally, though some may creep through an Æthereal breach when a Greater Demon is summoned. Once there, they are uncontrollable yet cannot exist outside of the Abyss' nurturing embrace for long. Lemurian Hosts can, however, reproduce rapidly and have been said to be able grow their numbers enough that their bulk covers a whole village. Swarming like firebugs, they ignite houses, devour souls and leave nothing in their wake  perfect unsuspecting shock troops for the hordes of demons that follow behind these screaming skulls."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 7
            },
            "perception": {
                "base": 35,
                "bonus": 6
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "awareness": 30,
            "athletics": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "simple_ranged": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Fleshy Whips",
                "chance": 75,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Fast",
                    "Reach"
                ]
            },
            {
                "name": "Wall Crawler Reach",
                "chance": 75,
                "distance": "ranged 9 yards",
                "load": "1 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "CREEPY CRAWLIES",
                "description": "These creature can attack all foes in a Burst Template area of effect and are able to occupy the same space that its foes stand within. It also never provokes Opportunity Attacks."
            },
            {
                "name": "DEATH ROLL",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a Chokehold."
            },
            {
                "name": "DENSE ANATOMY",
                "description": "These creatures only suffer Damage dealt by fire."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "SWALLOW WHOLE",
                "description": "When these creatures make a successful Chokehold, they deal Damage from a barehanded weapon and force a foe to Resist with a Toughness Test or be Swallowed. While Swallowed, the foe cannot use any Actions, but can attempt to Resist again at the beginning of their Turn to escape. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            },
            {
                "name": "WALL CRAWLER",
                "description": "These creatures can crawl upon both vertical and horizontal surfaces with ease. In addition, they can initiate a ranged Chokehold at a Distance of 3+[PB],"
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "NIGHT MARE",
        "family": "HELLBEAST",
        "description": [
            "The Abyss can field a mighty army and even the greatest armies in the land are nothing without powerful and swift mounts. In this case, the Abyss is no exception, though their favored beasts are known as Night Mares.",
            "Night Mares are not born, but rather created: spawned out of terrible factories and breeding pools, they are built on upon the whims of their creator and soul-bound to but one rider by a phylactery. From creation, they take upon the countenance most favorable to their master. The most common form a Night Mare takes is of a demonic destrier  their coat dark, their mane billowing like heatless flame and their eyes ablaze. However, that's not the only shape they can take  some Night Mares summoned by Necromancers appear as half-decayed palfreys, whereas the mount of a Filth Demon may instead be a massive slug-like creature wreathed in flies, another as quivering, manta ray-like floating membrane or a saw-toothed flying disk of flesh. Night Mares are versatile but actually quite hard to create  they require time and effort, while the random hordes of the Abyss split like cancers. More often than not, an Abyssal Prince will bless a particularly faithful servant with a Night Mare, while keeping a large majority of them in a personal stable for larger conflicts.",
            "Night Mares are wholly mindless, but refuse to obey anyone but the master who holds their phylactery. Trying to mount a Night Mare usually results in being bucked, trampled and possibly even eaten. That being said, Night Mares have been broke by particularly crafty wranglers  even without a phylactery. Night Mares make capable and strong steeds, save that you can ignore the decaying effect they have on your sanity, as well as the propensity for other natural creatures to want to attack the Night Mare, given their hellish nature."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 5
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 45,
                "bonus": 9
            },
            "perception": {
                "base": 35,
                "bonus": 5
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 15,
                "type": "normal"
            },
            {
                "value": 18,
                "type": "fly"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": null,
        "parry": null,
        "dodge": {
            "chance": 75,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "awareness": 30,
            "athletics": 30,
            "coordination": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Fire Hooves",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Immolate",
                    "Pummeling"
                ]
            }
        ],
        "traits": [
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SOUL JAR",
                "description": "These creatures cannot be permanently Slain! unless the fetish or phylactery which houses their soul is destroyed. However, they can be made to remain in hibernation, left unconscious if a jar of salt is poured into"
            }
        ],
        "trappings": [
            "Saddle",
            "Soul-bound phylactery"
        ]
    },
    {
        "type": "PIT DRAGON",
        "family": "HELLBEAST",
        "description": [
            "No creature has entered the mythos of different cultures more than the legendary dragon. They are a symbol of might, intelligence and worship or at least they once were.",
            "Dragons used to rule the land, sentient and highly intelligent creatures who would ravage farmlands and destroy entire cities when their wrath was piqued. They were massive reptilian creatures, born aloft on leathery wings and being lined with scales as hard as steel. In the early ages, they were considered to be the de facto rulers of all they surveyed, worshipped by frightful mortals who sacrificed virginal maidens at the mouths of their great lairs. However, legends tell of a powerful warrior-princess who banished the mightiest of dragons, slaying its brood and casting the rest into the deep darkness of the Abyss. Within the endless pit, over the ages the dragons slowly mutated and lost their intelligence, turning into little more than pure killing machines. Most of these new Pit Dragons are thankfully stuck within the confines of the Abyss, but an unlucky Æthereal breach can cause one to return to the Material Realm once again.",
            "Pit Dragons are terribly vile and lustful brutes  their main weapon either being their rending claws or their trademark breath of flames and bile. Note that not all Pit Dragons are the same  some may be born aloft on feathered wings or Magickal energies, while others may breathe toxic miasma or even biting frost. Many myths abound about Pit Dragons, the most common being their tendency to hoard treasure in their lair. This is true to an extent  Pit Dragon nests tend to be full of tarnished metal, broken coins and shattered jewels, but that is only because the Pit Dragon cannot digest these items as they once did, meaning that most of them were probably vomited back up.",
            "Though the energies of the Abyss have made them feral, every once in a while a gleam of their former intellect shines through. Some Pit Dragons have been said to have moments of complete lucidity before descending back into snarling beastdom."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 55,
                "bonus": 13
            },
            "agility": {
                "base": 50,
                "bonus": 9
            },
            "perception": {
                "base": 50,
                "bonus": 8
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 50,
                "bonus": 8
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 11,
        "movement": [
            {
                "value": 15,
                "type": "normal"
            },
            {
                "value": 18,
                "type": "fly"
            }
        ],
        "damage_threshold": 19,
        "peril_threshold": 11,
        "parry": null,
        "dodge": null,
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "awareness": 30,
            "athletics": 30,
            "bargain": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "simple_ranged": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Claws & Teeth",
                "chance": 80,
                "distance": "melee engaged or 1 yard",
                "damage": "13",
                "qualities": [
                    "Immolate",
                    "Punishing",
                    "Reach",
                    "Vicious"
                ]
            },
            {
                "name": "Lasting Tail",
                "chance": 80,
                "distance": "melee engaged or 1 yards",
                "damage": "13",
                "qualities": [
                    "Fast",
                    "Powerful",
                    "Reach",
                    "Vicious"
                ]
            },
            {
                "name": "Spit Fire",
                "chance": 80,
                "distance": "ranged 11 yards",
                "load": "0 AP",
                "damage": "Special",
                "qualities": [
                    "Shrapnel"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CORRUPTIVE BREATH",
                "description": "When these creatures cause a foe to suffer Damage from Spit Fire, they also inflict 3 Corruption as a result. In addition, their Spit Fire ability gains the Shrapnel Quality."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FIERY RETRIBUTION",
                "description": "These creatures can use Spit Fire as an Opportunity Attack without having to Load."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "LASHING TAIL",
                "description": "These creatures can attempt an Opportunity Attack with their tail at the end of their Turn."
            },
            {
                "name": "NATURAL ARMOR (6)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "POTENT BLOWS",
                "description": "When these creatures succeed any Attack Action to strike, their foe must flip the results to fail when Dodging or Parrying the attack."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SPIT FIRE",
                "description": "These creatures can use their breath as a ranged weapon. This allows them to strike a single foe within 3+[PB], as the foe suffers 2D10+2 Damage from fire. A foe can attempt to Dodge Spit Fire or Parry it with a shield. Spit Fire can be used while Engaged with foes."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "SWALLOW WHOLE",
                "description": "When these creatures make a successful Chokehold, they deal Damage from a barehanded weapon and force a foe to Resist with a Toughness Test or be Swallowed. While Swallowed, the foe cannot use any Actions, but can attempt to Resist again at the beginning of their Turn to escape. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "WEAK SPOT (Wings)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Dragon hide"
        ]
    },
    {
        "type": "PROTOPLASMIC DOLOR",
        "family": "HELLBEAST",
        "description": [
            "It may seem at first glance that the Abyss is only a realm where demons dwell and nothing more. Quite the contrary, as the Abyss is less of a location and more of a living, writhing dimension of energy and emotion  one that is a horrid reflection of the Material Realm. One of the results of this reflection is the Protoplasmic Dolor.",
            "The Protoplasmic Dolor is not a demon, but rather an Abyssal entity that actually feeds on demons. In the Material Realm in locations of high stress, emotion or violence, the Abyss folds and reacts  splitting off like a cancer or bud. This bud is a writhing ball of violence and sadness given physical form  the aforementioned Protoplasmic Dolor. These beings look like mobile mounds of putrescent and undulating flesh, shifting in color much like an oil slick. Mouths, eyes, ropy tentacles and extraneous organs bubble to its surface, only to sink back again into the mass of meat. Dolors weep, gibber and scream from their countless mouths, a terrible cacophony to any man's ears. Their only purpose is to eat. The gluttonous Dolor survives by absorbing the spiritual energy of anyone they encounter, although their preferred meals are the terrible energies of demons. As they feed, they become larger, more violent and crazed  they are mindless and exist only to kill and languish in their sorrow.",
            "In addition to weighing usually a ton or more and being able to manifest all matter of natural weaponry, the Dolor's skin is naturally acidic and eats away at flesh and steel alike. They can survive in nearly any environment and are also surprisingly capable swimmers despite their lack of bones and muscle. Their only repulsion is generally to bright sources of light  if found on the Material Realm, they will languish in deep caves, only coming out at night to hunt. They are easy to track however  their trail of acidic slime eats through any vegetation and scours any rock they pass over. The only difficulty is actually killing the damned thing."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 6
            },
            "brawn": {
                "base": 45,
                "bonus": 10
            },
            "agility": {
                "base": 40,
                "bonus": 9
            },
            "perception": {
                "base": 35,
                "bonus": 5
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "awareness": 30,
            "athletics": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Undulating Tentacles",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "10",
                "qualities": [
                    "Reach",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CANCEROUS ABSORPTION",
                "description": "When they render an enemy Slain!, they gain +3 to [BB] and move three steps up the Damage Condition Track positively."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GASTRIC ACIDITY",
                "description": "When these creatures deal Damage, a foe must Resist with Coordination. If they fail, the foe's armor, weapon or shield is Ruined!."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            },
            {
                "name": "UNNATURAL VISCERA",
                "description": "These creatures are immune to attacks made with ranged weapons. However, they cannot"
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "ARCH CENOBITE",
        "family": "HIGHER DEMON",
        "description": [
            "The Abyssal Prince of Pleasure is not to be trusted. His words may drip with honey and He makes grand promises, but He is by far the most selfish and narcissistic of the Princes. His Greater Demon embodies all these same values, providing so much pleasure it is painful and so much pain it is pleasurable.",
            "Classified by demonologists as Chaigidel, which means 'confusion of the powers of the gods', Arch Cenobites ultimately blur the senses and cause witnesses to question what is reality and what is not. Arch Cenobites are mental manipulators, able to seduce and slow senses with just a glance. They move almost like water, felling all in their graceful and blasted wake. Arch Cenobites take great pleasure in the slaughter, but quickly become bored and distracted if new developments do not interest them. Arch Cenobites are obscenely beautiful, crossing over into 'uncanny valley' where their glory is so high they appear grossly unnatural. They are also pierced with all manner of weapons and stakes, their skin artfully flayed to provide pleasure and pain. An Arch Cenobite's body is divided in two, one side male and the other female. Four arms end either in wicked claws or boney, saw-toothed blades while their hoofed legs support sensual bodies bristling with muscle and breasts. Their heads are demonically angular, cresting in multiple horns and a terminating in a jagged maw with a barbed, serpentine tongue.",
            "When an Arch Cenobite is drawn into the Material Realm, the area around it seems to warp and struggle with potential. Artists go mad, animals breed and maul their partners and even inanimate objects vibrate with near-explosive, Ætheric energy. Arch Cenobites are notoriously elusive in their mannerisms  they will try to seduce just as quickly as they try to kill. Emitting a soporific musk, their bodies undulate with a grotesque sensuality. These demons exude a presence that is almost like a drug  one that shows their victims things and feel sensations they could never feel outside the creature's presence. Their serpentine tongues are barbed like a cat-o-nine-tails, easily able to lash mortals and demons alike with Ætheric power."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 50,
                "bonus": 8
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 50,
                "bonus": 8
            },
            "fellowship": {
                "base": 55,
                "bonus": 10
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 13,
        "peril_threshold": 11,
        "parry": {
            "chance": 70,
            "qualities": []
        },
        "dodge": {
            "chance": 70,
            "qualities": []
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "awareness": 30,
            "charm": 30,
            "coordination": 20,
            "education": 30,
            "guile": 30,
            "incantation": 20,
            "interrogation": 30,
            "intimidate": 20,
            "leadership": 30,
            "scrutinize": 30,
            "simple_melee": 20,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Penetrating Tongue",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "8",
                "qualities": [
                    "Entangling",
                    "Fast",
                    "Finesse",
                    "Reach",
                    "Vicious"
                ]
            },
            {
                "name": "Deplorable Claws",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Powerful",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 onehanded melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of their next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "GIFT OF DEVILS",
                "description": "These creatures ignore the Heavy Quality of armor to cast Magick."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "HYPNOTIC MUSK",
                "description": "Foes who stand within 9 yards of this creature cannot add their Apprentice Skill Rank to Combat and Willpower-based Skill Tests. In addition, foes who are Engaged with these creatures must successfully Resist using Toughness or be unable to add any Skill Ranks to Combat and Willpower-based Skill Tests for a day."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "IMPLACABLE DEFENSE",
                "description": "When this creature's Turn begins, they gain 1 additional AP. However, they can only be used to Dodge and Parry."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "POSSESSION",
                "description": "These creatures can use Skin Guest from the Arcana of Sorcery without having to make an Incantation Test. In addition, they do not have to maintain Concentration."
            },
            {
                "name": "POTENT BLOWS",
                "description": "When these creatures succeed any Attack Action to strike, their foe must flip the results to fail when Dodging or Parrying the attack."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SNIKT! SNIKT!",
                "description": "These creatures may spend 3 APs to attack twice with melee weapons."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "TRUE NAME",
                "description": "Unless foes invoke this creature's True"
            }
        ],
        "trappings": [
            "Chaos armor (as full plate armor with Castle-forged Quality)",
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Infernal holy symbol",
            "Reagents for all Magick spells (9)"
        ]
    },
    {
        "type": "BRASS PRIMARCH",
        "family": "HIGHER DEMON",
        "description": [
            "When the Abyssal Prince of Violence wages war, He ensures that His closest generals are pure engines of destruction  bloody creatures who only desire to destroy all in their path and also have the capability of doing it.",
            "Called Brass Primarchs, their order is classified by demonologists as Golachab, a word that means 'one adorned with fire'. Massive beasts of war and death, they radiate an unearthly heat, one that both ignites bodies and minds. Just being near a Brass Primarch incites violence, anger and hatred and this aura of menace is the only thing that can keep the normally-murderous Legion Demons functioning under any kind of leadership. Brass Primarchs have endless forms, but they are almost always bathed in oozing blood and carrying enormous and frightening weapons. They stink of burning flesh and sulfur, the air around them shimmering from their mere presence and the call of wild beasts echoing their every footstep. Brass Primarchs look the most like the classical idea of a demon: massive, black of skin, a bull-headed and horned visage, as well as possessing massive bat-like wings which sprout from iron-hard scaled shoulders.",
            "Brass Primarchs bring violence and storms in their wake when they are summoned, causing brother to turn against brother and the sky to literally rain blood and ash. They are terrible to behold, leaving splashes of crimson mist and fields of dismembered bodies wherever they tread, with massive axes and whips dripping with gore. Brass Primarchs cannot bear to work with the demons of other Princes, as they find that they are no match for their infernal might. May the heavens forbid a Brass Primarch and a Light-Eater should ever encounter each other  a fight of such vitriol and power could easily level an entire city. It was once said the GodEmperor entered personal combat with a Brass Primarch and the fight lasted for ten days and nights before the demon was struck down, the God-Emperor supposedly left in a coma for five months after the battle."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 55,
                "bonus": 10
            },
            "brawn": {
                "base": 50,
                "bonus": 10
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 50,
                "bonus": 9
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            },
            {
                "value": 15,
                "type": "fly"
            }
        ],
        "damage_threshold": 16,
        "peril_threshold": 12,
        "parry": {
            "chance": 85,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": []
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "awareness": 30,
            "athletics": 30,
            "guile": 30,
            "incantation": 10,
            "interrogation": 30,
            "intimidate": 30,
            "leadership": 10,
            "martial_melee": 30,
            "scrutinize": 20,
            "simple_melee": 30,
            "toughness": 30,
            "warfare": 20
        },
        "attack_profile": [
            {
                "name": "Demonic Axe",
                "chance": 85,
                "distance": "melee engaged",
                "damage": "11",
                "qualities": [
                    "Adaptable",
                    "Punishing",
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Whip of the Fallen",
                "chance": 85,
                "distance": "melee engaged or 1 yard",
                "damage": "Special",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Powerful",
                    "Reach"
                ]
            },
            {
                "name": "Demonic Claws",
                "chance": 85,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Pummeling",
                    "Vicious",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of their next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DEMONIC AXE",
                "description": "When these creatures are encountered, they carry a battle axe infused with the souls of other demons. When they inflict an Injury, the foe is Slain! instead. This can only be used in the hands of these creatures, being otherwise useless relics to others. If picked up by PCs whose Order Ranks are higher than their Chaos Ranks, they are instantly disintegrated into ash, Slain!."
            },
            {
                "name": "DEMONIC FRENZY",
                "description": "When this creature's Turn starts, roll 6D6 Chaos Dice. If at least three dice land on face '6', they enter a demonic Frenzy. While Frenzied, they can make up to three Melee Attacks during their Turn, costing 1 AP for each Melee Attack. In addition, they can make an Opportunity Attack at the end of a Charge. However, they cannot Counterspell, Dodge or Parry while in a frenzied state. Finally, their Frenzied state makes them more susceptible to harm, as foes add an additional 1D6 Fury Die to Damage when striking the creature."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "HOWL OF THE ABYSS",
                "description": "When these creatures successfully use a Litany of Hatred, affected foes must Resist with a Resolve Test or be made subject to Confusion. While Confused, they must roll 1D6 Chaos Die as their Turn starts. If the result is face '1', they lose 1 AP on this Turn. If the result is face '2', they lose 2 APs on this Turn. If the result is face '3', they elect to run in a random direction. If the result is face '4', they elect to attack the nearest ally. If the result is face '5', they do nothing. However, if the result is face '6', they are left Helpless. Once subject to one of these effects, they can attempt to Resist at the beginning of their Turn to shake it off. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "IMPLACABLE DEFENSE",
                "description": "When this creature's Turn begins, they gain 1 additional AP. However, it can only be used to Dodge and Parry."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement use 6+[AB] or 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "POTENT BLOWS",
                "description": "When these creatures succeed any Attack Action to strike, their foe must flip the results to fail when Dodging or Parrying the attack."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "TRUE NAME",
                "description": "Unless foes invoke this creature's True Name before casting Magick, it fails to affect the creature."
            },
            {
                "name": "UNGAINLY",
                "description": "When these creatures are Slain! all those Engaged with it must succeed at a Coordination Test or be knocked Prone beneath of it, suffering 3D10+3 Damage from falling."
            },
            {
                "name": "WHIP OF THE FALLEN",
                "description": "When these creatures are encountered, they carry a bullwhip woven from the skins of Dread Counts. When they strike with the bullwhip, the foe suffers 2D10+[BB] physical Peril. This can only be used in the"
            }
        ],
        "trappings": [
            "Chaos armor (as full plate armor with Castle-forged Quality)",
            "Demonic axe",
            "Infernal holy symbol",
            "Whip of the fallen"
        ]
    },
    {
        "type": "GREAT DEVOURER",
        "family": "HIGHER DEMON",
        "description": [
            "The Abyssal Prince of Decay is oddly gregarious, a loving paternal figure who only wants to see His brood propagate and flourish. Though His Filth Demons do this well, his Higher Demons are truly spawns of disease and hunger.",
            "Known as Great Devourers, their order is classified by demonologists as Gamchicoth, a word simply meaning 'sineater'. Fetid, disgusting and bloated, they take jubilant pleasure in how every one of their movements spreads illness and disease. In their wake they leave a sickening trail of slime, out of which Abyssal-born Goblins rise. Not to mention its aura of disease and bile, the Great Devourer spreads sickness to all surrounding it. Great Devourers are gross and rent, often over-spilling with humors and ruptured organs and followed by a dark cloud of carrion, their wet entrails hang openly from their stomach and undulate grotesquely, while their skin turns like worms amid soil, sloughing off and crawling back over their body. They smell of sweet decay and the landscape around them withers and grows foul as they step. Most appear as fat, bloated and decaying monsters with elk-like horns atop a gaunt, skeletal face.",
            "When a Great Devourer appears, water and food festers in front of your eyes as maggots and flies seem to breed out of the woodwork or crawl out of living flesh. The Great Devourer forms are disturbing, but what is more grotesque is the great pleasure they take in their work. As they rend and destroy, they laugh and joke with their minions, preach delicious ironies and are extremely relaxed in their works. It uses its build-up of acids and diseases in combat, vomiting and defecating on both enemies and allies  if you manage to survive its blows, the infections it spreads could very well number your days. While few other demons of the Prince of Decay relish in fighting, the Great Devourer does  it knows the people it kills will soon be hosts for new and beloved poxes, diseases and maladies."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 6
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 50,
                "bonus": 6
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 55,
                "bonus": 13
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 16,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": {
            "chance": 90,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "athletics": 10,
            "coordination": 30,
            "education": 30,
            "guile": 20,
            "incantation": 30,
            "interrogation": 20,
            "intimidate": 30,
            "leadership": 30,
            "scrutinize": 10,
            "simple_melee": 30,
            "toughness": 30,
            "warfare": 30
        },
        "attack_profile": [
            {
                "name": "Acidic Spittle",
                "chance": 80,
                "distance": "ranged 7 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            },
            {
                "name": "Demonic Flail",
                "chance": 80,
                "distance": "melee engaged or 1 yard",
                "damage": "7",
                "qualities": [
                    "Adaptable",
                    "Entangling",
                    "Reach",
                    "Weak"
                ]
            },
            {
                "name": "Demonic Claws",
                "chance": 80,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Pummeling",
                    "Vicious",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACIDIC SPITTLE",
                "description": "These creatures can use their saliva as a ranged weapon. This allows them to strike a single foe within 1+[PB], causing the foe to suffer 1D10+1 Damage from acid. However, Acidic Spittle ignores a foe's Damage Threshold Modifier from armor. A foe can attempt to Dodge Acidic Spittle or Parry it with a shield. Acidic Spittle can be used while Engaged with foes."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "CAUSTIC FLESH",
                "description": "Whenever a foe strikes these creatures, their weapon gains the Ruined! Quality (unless the weapon has the Castle-forged Quality)."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Chaotic Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GASTRIC ACIDITY",
                "description": "When these creatures deal Damage, a foe must Resist with Coordination. If they fail, the foe's armor, weapon or shield is Ruined!."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "MIASMA OF SICKNESS",
                "description": "When foes stand within nine yards of these creatures, they must Resist with a successful Toughness Test or succumb to a disease. Roll 1D6 Chaos Die. If it lands on face '1', they contract Bloody Flux. If it lands on face '2', they contract Filth Fever. If it lands on face '3', they contract Grey Plague. If it lands on face '4', they contract Tomb Rot. If it lands on face '5', they contract Venereal Disease. And finally, if it lands on face '6', they contract all the aforementioned Diseases. Whether they Resisted or not, they cannot be infected with Miasma of Sickness again, until at least a day has passed."
            },
            {
                "name": "NATURAL ARMOR (5)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "POTENT BLOWS",
                "description": "When these creatures succeed any Attack Action to strike, their foe must flip the results to fail when Dodging or Parrying the attack."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SLIME-BORN HOST",
                "description": "When this creature's Turn ends, roll 1D6 Chaos Die. Whatever the result is, a number of Kobold Fanatics sprout forth beside this creature. In addition, foes who begin their Turn Engaged with this creature must Resist with a successful Coordination Test or be unable to use Movement Actions, as they are clawed at by tiny hands along the creature's body."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "TRUE NAME",
                "description": "Unless foes invoke this creature's True Name before casting Magick, it fails to affect the creature."
            },
            {
                "name": "UNGAINLY",
                "description": "When these creatures are Slain! all those Engaged with it must succeed at a Coordination Test or be knocked Prone beneath of it, suffering 3D10+3 Damage"
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spells",
            "Demonic flail",
            "Infernal holy symbol",
            "Reagents for all Magick spells (9)"
        ]
    },
    {
        "type": "LIGHT-EATER",
        "family": "HIGHER DEMON",
        "description": [
            "The Abyssal Prince of Change trusts no one, be it demon or man. He surrounds himself with gibbering demons so as to not spoil his deep and intricate plans for the fall of all reality. Besides his mortal champions, the only ones He trusts are His Higher Demons, so-named the Light-Eaters.",
            "Their order is classified by demonologists as Sathariel, a word meaning 'concealment of the gods'. Light-Eaters purposefully deceive and scheme to lead mortals astray. They are also majestic sorcerers, some of the most powerful arcanists that one could ever fear to encounter. Not only that, but their very being is anathema to other spells cast against them. It exudes a terrible aura of chaos, mutating and driving mad anyone who stands near these beasts. A Light-Eater does not often draw weapons  it would rather use trickery and false words than violence. In appearance, a Light-Eater is a massive humanoid vulture, their plumage a deep purplishred and their beaks long and saw-toothed. They seem to all but crackle with Ætheric potential as their wicked scythes thirst for souls.",
            "Light-Eaters are the most easily summoned of the Higher Demons  in fact, they desire to be summoned in order to propose Faustian pacts to their binders. Chaos precedes a summoning of a Light-Eater  weather shifts uncontrollably, animals jitter and morph and mortals run amok with madness. Other extreme changes include shifting colors or perhaps words being jumbled as others speak. Light-Eaters should not be underestimated in combat, for though they tend to be smaller than many other Higher Demons, their Magickal prowess is blasphemous to behold. Light-Eaters will still try to avoid combat  they believe a well-placed word and a promise of power causes much more discord on the Material Realm than a simple act of violence. And that is what has the potential to make them the most dangerous of the Higher Demons."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 50,
                "bonus": 10
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 50,
                "bonus": 7
            },
            "willpower": {
                "base": 55,
                "bonus": 11
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 16,
                "type": "normal"
            },
            {
                "value": 19,
                "type": "fly"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 14,
        "parry": {
            "chance": 70,
            "qualities": []
        },
        "dodge": {
            "chance": 90,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "bargain": 30,
            "coordination": 30,
            "education": 30,
            "guile": 30,
            "incantation": 20,
            "interrogation": 30,
            "intimidate": 20,
            "leadership": 10,
            "martial_melee": 20,
            "scrutinize": 30,
            "toughness": 10,
            "warfare": 30
        },
        "attack_profile": [
            {
                "name": "Weeping Scythe",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "10",
                "qualities": [
                    "Reach",
                    "Powerful",
                    "Vicious"
                ]
            },
            {
                "name": "Demonic Beak",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Pummeling",
                    "Punishing",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ÆTHERIC DOMINATION",
                "description": "When these creatures would potentially invoke a Chaos Manifestation, they must have two or more face '6' on Chaos Dice to invoke it."
            },
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 onehanded melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "ENTRANCING",
                "description": "When this creature uses a Transfixed Gaze, they can use other Actions while they maintain its Mesmerizing effect. In addition, foes suffer 1 Corruption and 2D10+2 mental Peril on each of their Turns while they remain Mesmerized."
            },
            {
                "name": "FORESIGHT",
                "description": "When these creatures fail any Skill Test, they may re-roll to generate a better result but must accept the outcome."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "NATURAL ARMOR (2)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "POTENT BLOWS",
                "description": "When these creatures succeed any Attack Action to strike, their foe must flip the results to fail when Dodging or Parrying the attack."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "STRAFING TALONS",
                "description": "When these creatures execute a successful attack while flying, they also deal 1D10+[AB] physical Peril."
            },
            {
                "name": "THE CHANGER OF WAYS",
                "description": "When these creatures are made subject to Chaos Manifestations, they can roll twice and choose the most desirous result. Finally, they can Counterspell for 0 APs and can use Dispel Magick without having to make an Incantation Test."
            },
            {
                "name": "TRANSFIXED GAZE",
                "description": "When these creatures can see a foe (who can also see them), they force a foe to Resist with a Resolve Test or be Mesmerized. Mesmerized foes cannot use any Actions In Combat, but may attempt to Resist again at the beginning of their Turn to escape. The creature cannot use any other Actions while they maintain a Transfixed Gaze. However, they can release the gaze at any time. If the creature suffers Damage while a foe is Mesmerized, they immediately relinquish the hold. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "TRUE NAME",
                "description": "Unless foes invoke this creature's True Name before casting Magick, it fails to affect the creature."
            },
            {
                "name": "WEEPING SCYTHE",
                "description": "When these creatures are encountered, they carry a scythe infused with the blood of mortals. When they deal Damage, the foe also begins to Bleed. This can only be used in the hands of these creatures, being otherwise useless relics to others. If picked up by PCs whose Order Ranks are higher than their Chaos Ranks, they"
            }
        ],
        "trappings": [
            "Weeping scythe",
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Infernal holy symbol",
            "Reagents for all Magick spells (9)"
        ]
    },
    {
        "type": "NUMINA",
        "family": "HIGHER DEMON",
        "description": [
            "With all the darkness and chaos inherent in the world, one wonders if there is even a mote of potential 'goodness' within the endless pit of the Abyss. And there is in a sense  it takes the form of the Numina, agents of chaos fighting against other agents of chaos.",
            "Long ago when the world was young and humanity still suckled at the teats of existence, the Abyssal Princes were five in number. The fifth Prince, whose name has long been lost and is now known only as The Outsider, represented all of the forces of chaos combined: He represented anarchy in its purest form and thus bucked against the reins of the other four Princes. After several wars that nearly wracked the Abyss to the point of destruction, the other four Princes banded together and banished The Outsider to the darkest realms of reality. Not only that, but they ripped the souls of His worshippers from their bodies and imprisoned them with their lord. There, The Outsider brooded and planned for centuries on his revenge. Eventually, He formed the idea of recasting his worshippers into divine heralds, ones that could breach the Æthereal Veil and enter the Material Realm to strike at the forces of Corruption. Many souls volunteered for the process, but only the strongest were not obliterated instantly. Those that lived were given both beautiful and terrifying forms. Their new bodies turned into dizzying patterns of lines, points and angles of divine proto-math. These forms coalesced into strange patterns, likened to the abstract designs of a fanciful tattoo or a mad painter. Their beautifully Human visages are only apparent in the remnants of appendages  feminine hands supporting multiple angular faces, legs born atop swords and bodies composed of pulsating metal, twisted viscera and non-Euclidean shapes  all beating with a strange Æthereal heartbeat, as they became less real and more inexplicably geometric. These new beings called Numina promised to serve The Outsider for all eternity  serving as his fury and his hate.",
            "The Numina lost their ability to reason, only following strict orders with unflinching, automata-like devotion. They also lost their ability to speak, their intentions and words instead manifesting above them as holographic pictographs using 22 divine glyphs. Now, with mighty divine powers as well as the holy vengeance instilled in them, they wait on the glacial commands of The Outsider. Their primary objective: devour the land and destroy chaos from within. Numina are rarely seen and little known about, but when they manifest or awaken from their slumber, cities are leveled, pestilence comes and people are turned into pillars of salt."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 50,
                "bonus": 14
            },
            "perception": {
                "base": 55,
                "bonus": 9
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 12,
        "movement": [
            {
                "value": 20,
                "type": "normal"
            },
            {
                "value": 23,
                "type": "fly"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": null,
        "parry": null,
        "dodge": {
            "chance": 90,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "awareness": 30,
            "athletics": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "simple_ranged": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Euclidean Strike",
                "chance": 80,
                "distance": "melee engaged",
                "damage": "14",
                "qualities": [
                    "Finesse",
                    "Weak"
                ]
            },
            {
                "name": "Hyperbolic Fluctuation",
                "chance": 80,
                "distance": "ranged 13 yards",
                "load": "1 AP",
                "damage": "14",
                "qualities": [
                    "Finesse",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "CEREMONIAL RUNES",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1', '2' or '3', they have three Apprentice Runes inscribed upon their staff. If the result is face '4' or '5', they have two Journeyman Runes inscribed upon their staff. If the result is face '6', they have one Master Rune inscribed upon their staff."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "TRANSFIXED GAZE",
                "description": "When these creatures can see a foe (who can also see them), they force a foe to Resist with a Resolve Test or be Mesmerized. Mesmerized foes cannot use any Actions In Combat, but may attempt to Resist again at the beginning of their Turn to escape. The creature cannot use any other Actions while they maintain a Transfixed Gaze. However, they can release the gaze at any time. If the creature suffers Damage while a foe is Mesmerized, they immediately relinquish the hold. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "ADVERSARY DEMON",
        "family": "LOWER DEMON",
        "description": [
            "Sitting upon His hidden throne, the Abyssal Prince of Change schemes for the end times. A conspirator and hoarder of knowledge and magic, He plays the long con when it comes to the oblivion. Unlike the other three Abyssal Princes, the Prince of Change will further exacerbate the mutating form of the Sorrow Demon when it is sponsored, changed into an Adversary Demon.",
            "This new demon of nearly pure chaos steps up to join in the Adversary Demon order. Demonologists calls this order of demons Thamiel, translating to 'duality in the gods', as the Adversary Demon represents the inherent disharmony of their forms. While almost every other order of demons has at least a Human level of intellect, the Adversary Demon's minds have become so soured and warped that they are little more than gibbering fiends lashing out at anything that comes their way. Their minds and bodies are at the whims of madness, their physical forms being able to shift before a person's very eyes. All for the better, as simple minded aides allow the Abyssal Prince of Change to plot unhindered. Aside from being insane foes, the Adversary Demon's main purpose is that their bodies are literally brimming with Magickal energies. This ability only amplifies as more Adversary Demons gather together, so they are often summoned in small hordes rather than by themselves. Adversary Demons shift their physical structure at a moment's notice, but their most 'stable' form is often an impish, pink humanoid with a wide maw, wild eyes and four long, spindly limbs.",
            "Adversary Demons are most often brought to the Material Realm to serve as Magickal conduits or in hopes of revealing arcane secrets a sorcerer cannot find elsewhere. If the Adversary Demon can manage a moment of clarity in their manic minds, then sometimes they will reveal such secrets  the Abyssal Prince of Sacrifice is often very generous with His powers, as He knows how easily they corrupt. More often than not the Adversary Demon will fight, as their particular order can barely stand existing outside of the Abyss. These creatures are more powerful than they appear, many mages falsely associating madness with weakness. Mangled corpses and the lingering sign of powerful Magicks around summoning circles attest to the fact that myth is untrue."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 7
            },
            "perception": {
                "base": 40,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 50,
                "bonus": 8
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 11,
        "parry": {
            "chance": 65,
            "qualities": []
        },
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "awareness": 20,
            "bargain": 20,
            "coordination": 20,
            "education": 10,
            "folklore": 20,
            "guile": 20,
            "incantation": 20,
            "interrogation": 20,
            "intimidate": 20,
            "resolve": 10,
            "simple_melee": 20
        },
        "attack_profile": [
            {
                "name": "Demonic Claws",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Fast",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "ARCANA OF HORROR",
                "description": "These creatures can use Bolt of Flame from the Arcana of Pyromancy. If at least three of these creatures are standing within sight of one another, they can use Withering Touch from the Arcana of Sorcery. If at least nine of these creatures are standing within sight of one another, they can use Death's Embrace from the Arcana of Morticism. Should a foe be Slain! by use of any of these Magicks, they are immediately turned into a Lemurian Host. Reagents are unnecessary to cast this Magick."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DIVIDE & CONQUER",
                "description": "When these creatures are Slain!, roll 1D6 Chaos Die. If the result is face '1-3', they instantaneously split into two minor Adversary Demons (albeit unable to cast Magick, use the Underling rules later in this chapter and can now be permanently Slain!). If the result is face '4-6', it bursts into a cloud of mutating energy. Those caught in a Burst Template area of effect must succeed at a Resolve Test or gain a Taint of Chaos."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "LIVING CHAOS",
                "description": "When these creatures use Magick, they must roll 3D6 Chaos Dice. If all land on face '6', they are instantaneously Slain!."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Infernal holy symbol"
        ]
    },
    {
        "type": "BUTLER DEMON",
        "family": "LOWER DEMON",
        "description": [
            "When a spark of chaos breaks off through the cauldron of the bottom of the Abyss, they float off to start their hellish existence in the layers above. There, with a terrible birthing cry and the clap of Ætheric lightning, the Butler Demon is birthed.",
            "Their order is classified by the term Samael by demonologists, meaning 'the left hand', demonologists accurately describe the role of Butler Demons as minor servants to other demons and arcanists alike. They are small and weak creatures  rarely ever larger than a small child  who resort more on wiles and trickery to survive than any kind of martial prowess. Their forms are multitudinous: some appear as attractive sprites or fairies, others as disgusting balls of bile and others as impish hellhounds. All their forms emit a horrendous stench from their mouths. Though they are weak creatures, that does not mean they are slouches  most of them possess useless wings of some sort, as well as razor sharp claws and teeth perfect for rending.",
            "The nature of the Abyss is an odd place  we speak of layers and ascension, but the Abyss is not a physical place that can be navigated. It's a realm of spirit and energy and the layers are as overlapping as they are separate. It is a relentlessly vile and violent place. One tactic the Butler Demon employs to gain power is to actually spread their True Name through dark whispers through the Veil into the Material Realm. They do this in hopes of being bound as a familiar, where they can feed off and corrupt the energies of their summoner. Once powerful enough, they will usually try to kill their summoner to release their binding and return to the Abyss in order to fight their way into the ranks of the Sorrow Demons."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 45,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 40,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 5
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 8,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "athletics": 10,
            "charm": 20,
            "coordination": 20,
            "education": 10,
            "folklore": 20,
            "guile": 20,
            "interrogation": 20,
            "intimidate": 10,
            "resolve": 10,
            "rumor": 20,
            "scrutinize": 20,
            "simple_melee": 10
        },
        "attack_profile": [
            {
                "name": "Halitosis Bite",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "7",
                "qualities": [
                    "Fast",
                    "Finesse"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "IMPISH DELIGHTS",
                "description": "When these creatures are encountered, roll 1D6 Chaos die to determine what special power they can grant to one ally, once a day. If the result is a face '1', '2' or '3', they can cause any one ally standing within 3 yards of them to gain the benefits of Demonic Frenzy (as a Legion Demon). If the result is face '4' to '5', they allow any one ally standing within 3 yards of them to flip the results to succeed at a single Skill Test. If the result is face '6', anyone standing within 3 yards of this creature must succeed at a Resolve Test or temporarily lower their Fellowship by 9% for a day."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at"
            }
        ],
        "trappings": [
            "Bonds of servitude",
            "Grimoire of chaotic deeds",
            "Infernal holy symbol"
        ]
    },
    {
        "type": "CARNAL DEMON",
        "family": "LOWER DEMON",
        "description": [
            "Being a dualistic being, the Abyssal Prince of Pleasure is an entity of wild extremes. To them, pleasure and pain are just two sides of the same coin, one leading to another in a permanent cycle.",
            "When a Sorrow Demon is favored by the Abyssal Prince of Pleasure, it is welcomed into the horrible, velvety folds of Carnal Demons. Demonologists classify this order as Nehemoth, a phrase meaning 'dark whisperer', as these demons susurrate vivid desires and thoughts into the waiting ears of mortals. Carnal Demons are both beautiful and savage  they are comely versions of the form they choose, but always have a hidden edge that can spill blood like water. They usually appear as disturbingly bewitching hermaphrodites, with skin taking upon a bluish tinge as their arms terminate into razor sharp, crustacean-like claws. Instead of hair, hardened ridges of bone sweep backwards over their shoulders.",
            "A Carnal Demon's main responsibility is to secretly spread the word of their Prince through their demonic whispers, as the cult of the Abyssal Prince of Pleasure is by far the most insidious of the Material Realm. They promise fame, fortune and prowess to those who pay homage, filling their victim's heads with Cronenbergian nightmares. Carnal Demons are often summoned to serve as concubines in bizarre sex cults or are used to woo and corrupt officials and priests. Carnal Demons are true deceivers, so they care very little if their pacts are ever broken  either way, they are going to have their due. This is the aspect to most be wary of with Carnal Demons  they are nothing more than liars and serpents. No matter what they promise or how fetching they appear, a Carnal Demon will 'finish' their victims with perverse delight  penetrating them with a barbed phallus: the coup de grce in their twisted tryst."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 45,
                "bonus": 7
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 45,
                "bonus": 6
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 9,
        "parry": {
            "chance": 70,
            "qualities": []
        },
        "dodge": {
            "chance": 75,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "charm": 20,
            "coordination": 20,
            "folklore": 20,
            "guile": 20,
            "interrogation": 20,
            "rumor": 20,
            "scrutinize": 20,
            "simple_melee": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Eviscerating Pincer",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Dire Penetration",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "7",
                "qualities": [
                    "Entangling",
                    "Fast",
                    "Finesse",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "CAPTIVATING CRY",
                "description": "When foes can hear this creature, Facundo Garcia (Order #13879293)477"
            },
            {
                "name": "CHAPTER 12",
                "description": "BESTIARY they must Resist with a successful Resolve Test or be drawn towards the sound. Should they enter a dangerous area to find the sound, they can attempt another Resolve Test. Once they are able to visually see the creature, the Captivating Cry's effects end."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "HYPNOTIC MUSK",
                "description": "Foes who stand within 9 yards of this creature cannot add their Apprentice Skill Rank to Combat and Willpower-based Skill Tests. In addition, foes who are Engaged with these creatures must successfully Resist using Toughness or be unable to add any Skill Ranks to Combat and Willpower-based Skill Tests for a day."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SNIKT! SNIKT!",
                "description": "These creatures may spend 3 APs to attack twice with melee weapons."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at"
            }
        ],
        "trappings": [
            "Dread mementos from former lovers (9)",
            "Infernal holy symbol."
        ]
    },
    {
        "type": "FILTH DEMON",
        "family": "LOWER DEMON",
        "description": [
            "The Abyssal Prince of Decay is a being of hunger and disease, one that coos at its demons and encourages them to embrace sickness and bile in order to feel His fatherly love.",
            "When the Abyssal Prince of Decay sponsors a Sorrow Demon, it stabilizes its mutable form and it becomes part of the Filth Demon order. Their order is classified by demonologists as Gamaliel, a word meaning 'polluted of the gods', and just smelling the fetid stench of one of these beings is enough to warrant the title. Filth Demons are ravaged by disease and sickness  open, weeping sores sprout from every surface, flesh sloughs off the bone only to regenerate and flies follow the sickly sweet stench of decay. Filth Demons are human-sized creatures: their guts are rent and spilt, their skin is rotten and sweet smelling and their ambling form is always propped upon a rotting crutch of a 'fetid stick'.",
            "Besides the spread of any number of poxes and plagues, it is also the duty of the Filth Demon to chronicle and tally sickness. Many of them have tally marks carved in their flesh for each disease they carry and it is a point of pride  and violence  among the teeming hordes. When a Filth Demon is summoned onto the Material Realm, their summoner beseeches them to spread disease or to poison enemies  an activity the demons are more than happy to do. The price for meddling with a Filth Demon is often the same, inflicting wasting diseases or gangrene on any pact-breaker who refuses to hold up their end of the deal. Filth Demons are nevertheless fairly magnanimous for demons  they are gracious and complimenting, honey dripping from their tongues just as much as bile. They only wish to spread the divine love and joy the Abyssal Prince of Decay fills them with and all it takes is an open heart and an open wound."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 45,
                "bonus": 6
            },
            "perception": {
                "base": 40,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 50,
                "bonus": 9
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 12,
        "parry": {
            "chance": 65,
            "qualities": []
        },
        "dodge": {
            "chance": 75,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "bargain": 20,
            "charm": 20,
            "coordination": 20,
            "folklore": 20,
            "guile": 20,
            "interrogation": 20,
            "scrutinize": 20,
            "simple_melee": 20
        },
        "attack_profile": [
            {
                "name": "Fetid Stick",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Adaptable",
                    "Slow"
                ]
            },
            {
                "name": "Ragged Claws",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Pummeling"
                ]
            },
            {
                "name": "Corrosive Bile",
                "chance": 45,
                "distance": "ranged 6 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "CORROSIVE BILE",
                "description": "When creatures use this attack, it ignores their foe's Damage Threshold Modifier from armor."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Filth Fever)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FETID STICK",
                "description": "When these creatures are encountered, they carry a staff which buzzes with flies. When they deal Damage, the foe's wounds are also Infected. It can only be used in the hands of these creatures, being otherwise useless relics to others. If picked up by PCs whose Order Ranks are higher than their Chaos Ranks, they are instantly infected with Chaotic Rot."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GASTRIC ACIDITY",
                "description": "When these creatures deal Damage, a foe must Resist with Coordination. If they fail, the foe's armor, weapon or shield is Ruined!."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "WANTON HUNGER",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', their hunger has been sated. If the result is face '6', their hunger has not yet been sated and they are in a state of frenzy. When sated, they add +1 to both Damage and Peril Condition Tracks. When in a state of frenzy, they add an"
            }
        ],
        "trappings": [
            "Fetid stick",
            "Infernal holy symbol"
        ]
    },
    {
        "type": "LEGION DEMON",
        "family": "LOWER DEMON",
        "description": [
            "The Abyssal Prince of Violence knows nothing but war and bloodshed and only despises Magick more than He despises the Material Realm. He is constantly thirsty for death and destruction, a craving that can never be satisfied.",
            "When He chooses to sponsor a Sorrow Demon, its rage billows and envelops them to become a Legion Demon. Demonologists classify this order with the term Hareb Serapel, a phrase meaning 'ravenous flock'. Legion Demons flock like a murder of crows, burning and destroying everything in their path. Indeed, the old saying, 'We are Legion', truly applies to this particular batch of Lower Demons. Legion demons are almost alien in their appearance  their heads are long and horned, their crimson bodies rippling with herculean muscle and yellow protective scales. They brim with heat from indelible anger and fury, raising their hellblades in unholy righteousness while painting their bodies with the gore of their defeated enemies.",
            "Legion Demons have no purpose other than destruction  that is what they were sponsored to do and it what they strive to do. Their battlefields end up as bloody mires with the amount of viscera spilt and their terrible rallying cries strike fear even in the hardiest generals. Legion Demons absolutely abhor being summoned  they share their Princes' loathing of Magicks of all types, as they view it as a tool for weaklings. If they do not instantly kill the summoner, a Legion Demon will often be told to join some kind of retinue or to slaughter an entire township. Though Legion Demons will sometimes make deals with Mutants, their hatred for mankind will prevent them from forming an alliance they are not forced into. They will try their damnedest to break a summoning circle, as their loosened bonds could allow them to carve a swathe of destruction with little consequence."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 8
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 45,
                "bonus": 4
            },
            "perception": {
                "base": 40,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 9,
        "parry": {
            "chance": 80,
            "qualities": [
                "Defensive",
                "Protective"
            ]
        },
        "dodge": {
            "chance": 75,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "coordination": 20,
            "interrogation": 20,
            "intimidate": 20,
            "leadership": 20,
            "martial_melee": 20,
            "scrutinize": 20,
            "toughness": 20,
            "warfare": 20
        },
        "attack_profile": [
            {
                "name": "Sword of the Pit",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "8",
                "qualities": [
                    "Punishing",
                    "Reach",
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Stunted Claws",
                "chance": 50,
                "distance": "melee engaged or 1 yard",
                "damage": "6",
                "qualities": [
                    "Pummeling",
                    "Reach",
                    "Vicious"
                ]
            },
            {
                "name": "Bull-like Horns",
                "chance": 50,
                "distance": "melee engaged or 1 yard",
                "damage": "6",
                "qualities": [
                    "Pummeling",
                    "Reach",
                    "Slow",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BLITZ",
                "description": "When these creatures Charge, they may flip the results to succeed at their next Attack Action or Perilous Stunt on the same Turn."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DEMONIC FRENZY",
                "description": "When this creature's Turn starts, roll 6D6 Chaos Dice. If at least three dice land on face '6', they enter a demonic Frenzy. While Frenzied, they can make up to three Melee Attacks during their Turn, costing 1 AP for each Melee Attack. In addition, they can make an Opportunity Attack at the end of a Charge. However, they cannot Counterspell, Dodge or Parry while in a frenzied state. Finally, their Frenzied state makes them more susceptible to harm, as foes add an additional 1D6 Fury Die to Damage when striking the creature."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "RESISTANCE TO CHAOS",
                "description": "This creature may flip the results to succeed at when Resisting the effects of Magick."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "UNBRIDLED RAGE",
                "description": "When these creatures are Seriously or Grievously Wounded, add an additional 1D6 Fury Die to"
            }
        ],
        "trappings": [
            "Infernal holy symbol",
            "Metal Shield",
            "Sword of the Pit"
        ]
    },
    {
        "type": "SORROW DEMON",
        "family": "LOWER DEMON",
        "description": [
            "Dwelling in the miasma of the lower layers of the Abyss, the Sorrow Demons are the true hordes of the demonic. Their order is classified by demonologists as Thagirion, meaning 'those who bellow anger and tears', and all throughout the Abyss their deathly wailing and anger can be heard.",
            "Sorrow Demons are formed in one of two ways: the first is if a Butler Demon manages to gain enough power to kill another Sorrow Demon. Upon the death of the Sorrow Demon, the Butler Demon will mutate and twist into their shape and join the Sorrow Demon Order. The second way is through the corruption and mutation of humanoids. If a person is 'blessed by the gifts' of the Abyss and found to be too vulnerable or weak, their bodies are wracked with pain and grow leathery as they are turned into a Sorrow Demon. Most of these transmuted beings do not dwell within the actual Abyss, but instead roam the countryside.",
            "Sorrow Demon forms are grotesque  while Butler Demon are proto-beings fresh from creation and the other orders have achieved sponsorship, Sorrow Demons are inundated with energies that cause their forms to swell into fierce creatures whose appearance are the first thing that Smallfolk think of upon hearing the word, 'demon'! Traditionally hoofed and red of skin, Sorrow Demons carry barbed black iron weapons while leaving fiery hoof prints in their wake. Their draconic heads perch atop muscle-knotted bodies and two sets of terrible leathery wings sprout from their shoulders. Sorrow Demons, being the most populous of Lower Demons, tend to serve as foot soldiers during raids and are almost always under the terrible whip-and-chain of a Greater Demon. When not enlisted in a demoniac army  such as when having been transmuted from a Human form  Sorrow Demons are wild and unpredictable, trying to make sense of their new abilities, knowledge and mental state."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 8
            },
            "agility": {
                "base": 45,
                "bonus": 4
            },
            "perception": {
                "base": 40,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            },
            {
                "value": 13,
                "type": "fly"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 9,
        "parry": {
            "chance": 65,
            "qualities": []
        },
        "dodge": {
            "chance": 75,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "coordination": 20,
            "guile": 20,
            "intimidate": 20,
            "interrogation": 20,
            "leadership": 20,
            "martial_melee": 20,
            "scrutinize": 10,
            "toughness": 20,
            "warfare": 10
        },
        "attack_profile": [
            {
                "name": "Devil's Pitchfork",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Adaptable",
                    "Weak"
                ]
            },
            {
                "name": "Ragged Claws",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CALL OF THE ABYSS",
                "description": "When this creature suffers Damage and is unable to deal Damage by the end of its next Turn, roll 3D6. If all three dice show face '6', the creature is banished back to the Abyss, until summoned once again."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "HORROR OF THE PIT",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, PCs whose Order Ranks exceed their Chaos Ranks reduce their Damage by -3 when striking these creatures."
            },
            {
                "name": "NATURAL ARMOR (1)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "WEAK SPOT (Wings)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Infernal holy symbol",
            "Devil's Pitchfork"
        ]
    },
    {
        "type": "CRYPT SKELETON",
        "family": "MINDLESS UNDEAD",
        "description": [
            "Bones litter the countless vaults and mausoleums of the world, collecting dust as the memory of the dead fades far from existence. However, some can give blasphemous life to these inert stacks of dried collagen and marrow, creating Crypt Skeletons.",
            "Most are constructed from ragtag bones of artisans and servants employed by twisted Necromancers. Ritualistically slain  as to hide the inner workings of their laboratories and secret towers  the Necromancer then takes great pains to cleanse the bones in acid or let nature take its course, letting them rot in a fetid pit. Once assembled into a horrendous amalgamation of remnant bones, stitches and tack, they are driven by pure Magick, with no muscle or rotted viscera to motor the joints. They do not hunger, they do no sleep and they do not think; in short, they are perfect automatons who are able to continually revive if their bones are near to each other. Skeletons have no consciousness, but can follow complex commands to a fault. They do, however, all possess an unremitting hatred of the living. Crypt Skeleton's victims are often maimed beyond belief, with remnants taken back to their necromantic lord to be recycled into new skeletons.",
            "Most are constructed from ragtag bones of artisans and servants employed by twisted Necromancers. Ritualistically slain  as to hide the inner workings of their laboratories and secret towers  the Necromancer then takes great pains to cleanse the bones in acid or let nature take its course, letting them rot in a fetid pit. Once assembled into a horrendous amalgamation of remnant bones, stitches and tack, they are driven by pure Magick, with no muscle or rotted viscera to motor the joints. They do not hunger, they do no sleep and they do not think; in short, they are perfect automatons who are able to continually revive if their bones are near to each other. Skeletons have no consciousness, but can follow complex commands to a fault. They do, however, all possess an unremitting hatred of the living. Crypt Skeleton's victims are often maimed beyond belief, with remnants taken back to their necromantic lord to be recycled into new skeletons."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 35,
                "bonus": 4
            },
            "perception": {
                "base": 40,
                "bonus": 5
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": null,
        "parry": {
            "chance": 65,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 80,
            "qualities": [
                "Defensive",
                "Protective"
            ]
        },
        "notch": "Basic",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "intimidate": 10,
            "martial_melee": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "stealth": 10
        },
        "attack_profile": [
            {
                "name": "Rapier",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Weak"
                ]
            },
            {
                "name": "Ruined Hunting Bow",
                "chance": 55,
                "distance": "ranged 14 yards",
                "load": "1 AP",
                "damage": "1",
                "qualities": [
                    "Finesse",
                    "Ruined",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SKELETAL REMAINS",
                "description": "Only after these creature's bones are spread around for 1 AP are they forever Slain!."
            },
            {
                "name": "UNNATURAL VISCERA",
                "description": "These creatures are immune to attacks made with ranged weapons. However, they cannot"
            }
        ],
        "trappings": [
            "Arrows (6)",
            "Buckler",
            "Hunting bow (Ruined Quality)",
            "Mail armor",
            "Rapier"
        ]
    },
    {
        "type": "DRAUGR",
        "family": "MINDLESS UNDEAD",
        "description": [
            "War is a sad reality of a grim & perilous world. Nations claw at each other's throats for a few tracts of land, sending young boys to fight wars between old men  rarely even knowing the lord's name of whom they fight for. Many will march out of their villages for a lord they do not know and never return to their loved ones.",
            "Middling swordsmen end up in ditches, but renowned and revered soldiers are buried in hilltop crypts ringed with plinths of hand-etched rock and towering menhir, meant to memorialize their victories as much as the sacrifices of these fallen warriors under their command. It is here  both in the ditches and the crypts  that Necromancers find fresh corpses to defile. Draugr are the reanimated flesh of these poor souls, knowing nothing but a lifetime of war. They look akin to emaciated corpses, devoid of most of its flesh, dressed in full battle garb. Having been buried with their armor and their sword, these reanimated fighters stand once again to fight battles for lords they never knew. These souls are confused, believing they are still in a war that seems to last for an eternity. Of all the Mindless Undead, they are the most formidable foes, with particularly strong Draugr often serving as a Necromancer's right-hand confidante, recognized by exorcists as 'wights of the barrow'. The only way to fell one of these wretched souls is destruction of the head; otherwise, they will continually rise again and again and again. War war never changes. And the Draugr know aught else, damned to an eternity of battle until the end of days.",
            "If one is ever unlucky enough to encounter a force of Draugr, you can see the pattern of war across history as if someone cut through a solid layer of rock. Some may wear kabuto armor of a far eastern land, others wear tattered kilts and intricate knots of a civilization long lost and some the ring mail of battles a year ago. Their war wounds are just as diverse, ranging from spear wounds inflicted with flint tips to massive cannon shots that tore through their guts. When Draugr do speak, they speak in a variety of tongues, yet they all seem to understand each other. They bring together thousands of years of experience, strategy and weaponry to create both a terrifying and effective force."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 8
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 5
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 6,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": null,
        "parry": {
            "chance": 75,
            "qualities": [
                "Defensive",
                "Protective"
            ]
        },
        "dodge": null,
        "notch": "Intermediate",
        "risk_factor": "Medium",
        "skills": {
            "awareness": 20,
            "eavesdrop": 10,
            "intimidate": 20,
            "martial_melee": 20,
            "martial_ranged": 20,
            "ride": 20,
            "simple_melee": 20,
            "simple_ranged": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Draugr Blade",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, these creatures suffer 2D10+2 Damage from fire."
            },
            {
                "name": "IMPLACABLE DEFENSE",
                "description": "When this creature's Turn begins, they gain 1 additional AP. However, it can only be used to Dodge and Parry."
            },
            {
                "name": "IN THE FACE",
                "description": "These creatures can only be harmed by melee and ranged weapons by using a Called Shot to the head."
            },
            {
                "name": "KILL IT WITH FIRE",
                "description": "Only after these creature's remains are set On Fire are they forever Slain!."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "NEVER SURRENDER",
                "description": "Foes do not gain an advantage when they flank or outnumber this creature in combat."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these creatures"
            },
            {
                "name": "provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke"
            }
        ],
        "trappings": [
            "Draugr blade",
            "Mail armor",
            "Memento from their former life",
            "Metal shield",
            "Tattered burial shroud"
        ]
    },
    {
        "type": "REANIMATED CORPSE",
        "family": "MINDLESS UNDEAD",
        "description": [
            "The Reanimated Corpse is both the most savage and pitiable Mindless Undead that necromancy can forge.",
            "The Reanimated Corpse is a shambling monstrosity, a rotting humanoid whose viscera is visible, limbs are missing and ragged jaws hang agape. These creatures are created from those dead who died in unnatural ways, such as by murder or poisoning. The Necromancer summons the deceased's soul from the Well of Souls, binding it into a fetish or phylactery and placing it around their neck. They then animate the corpse, which rises and follows the commands of those who hold the object containing their soul. As the Corpses do not possess a soul in their physical bodies, they are reduced to little more than beasts: they hunger for Human flesh, but their undead form means they do not require it. They attack savagely with teeth and nail, biting off chunks of flesh with a ragged jawbone bereft of teeth and scooping out eyeballs of their victims with broken, bony digits. Unless the object housing the soul is destroyed, Reanimated Corpses will continually revive no matter how often they are slain. Aside from this object, the Reanimated Corpses also have a supernatural aversion to rock salt; they become inert if a jar of it is placed in their mouths.",
            "Reanimated Corpses are quite easy to raise en masse and together they are both powerful and terrifying. Across the known world, you can hear countless stories of small towns being swept away under a wave of these shambling monsters. The worst stories to hear from raconteurs are those of the weary travelers  lost in the woods, forced to hold up in a ramshackle cabin as the undead and their own paranoia claws away at them until they are nothing more than worm food. Some also say that the world has become so foul and tainted that Reanimated Corpses are starting to rise on their own volition, leaving graveyards empty and their villages soaked in blood and entrails."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 35,
                "bonus": 5
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": null,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "intimidate": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Tooth & Nail",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, these creatures suffer 2D10+2 Damage from fire."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage, they can force a foe to Resist a Takedown."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SOUL JAR",
                "description": "These creatures cannot be permanently Slain! unless the fetish or phylactery which houses their soul is destroyed. However, they can be made to remain in hibernation, left unconscious if a jar of salt is poured into"
            }
        ],
        "trappings": [
            "Fetish",
            "Memento from their former life",
            "Tattered burial shroud"
        ]
    },
    {
        "type": "TENEBRAE",
        "family": "MINDLESS UNDEAD",
        "description": [
            "Tenebrae are shadow-people possessed by chaos: puppets which exist only to serve Corruption made manifest. They have no free will, essentially mindless though compelled to snuff out the 'blinding light' of other living creatures.",
            "Often haunting places where ritual murders or excessive violence has occurred (particularly villages that have fallen beneath the blade of Vampiric Templars), Tenebrae exist only to kill. They sit upon the threshold of the Material Realm and the Well of Souls, able to detect when living beings come near the remains of their corpses. And as the living are sensed, Tenebrae bodies shamble and animate once more, surrounded by a strange warping of light and darkness. A shimmering shadow emanates from their body, appearing as if made from smoke, protecting them wholly from physical harm unless exposed to light. They then pick up the implements from their former profession: axes, scythes, hammers and the like and take to the killing.",
            "Strangely, Tenebrae will recite bits and pieces of the language they spoke in life. This always sounds like nonsense and out of place, but it is always intimidating, suggestive of a horrid intelligence or hope that they might not be dead at all. This strange behavior has been recorded by many an exorcist, who inexplicably state that it must have been the living soul fighting against the chaos, beseeching the living to put them out of their misery. It is often intimidating and quite terrifying to see the wispy form of a Tenebrae lash out with jagged weaponry, only to hear it weep openly and beg for its life to end. Tenebrae straddle the line between flesh and spirit and it seems even the creatures themselves often do not know which realm they inhabit."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 45,
                "bonus": 6
            },
            "perception": {
                "base": 40,
                "bonus": 6
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 20,
        "peril_threshold": null,
        "parry": null,
        "dodge": {
            "chance": 65,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 20,
            "eavesdrop": 10,
            "intimidate": 20,
            "simple_melee": 20,
            "simple_ranged": 10,
            "stealth": 10,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Improvised Hand Weapon",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Pummeling",
                    "Weak"
                ]
            },
            {
                "name": "Improvised Thrown Weapon",
                "chance": 45,
                "distance": "ranged 6 yards",
                "damage": "5",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "LIGHT SENSITIVE",
                "description": "When a foe uses Take Aim for 2 APs to shine a source of light upon this creature before making their attack, reduce its Damage Threshold by a -6 penalty (to a minimum of 1)."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Improvised hand weapon",
            "Improvised thrown weapon",
            "Memento from their former life",
            "Tattered burial shroud"
        ]
    },
    {
        "type": "DRAGON-BORN OGRE",
        "family": "MUTANT",
        "description": [
            "The great dragons are a long-feared beast, primordial monsters who dwell deep in the Abyssal pit; ancient and powerful beyond measure. Similar things have been said about the Ogre; though they are far more common, they are too stupid to live, but too strong to die. Perhaps that is why one of the greatest living nightmares in existence is the improbable mating of the two: the imposing Dragon-born Ogre.",
            "Dragons and Ogres do not cross paths very often, as dragons are exceedingly (and thankfully) rare and Ogres maintain congress with aught other than a Halfling or two. However, during terrible storms, Ogres often retreat to caves for shelter. If they are unlucky  or perhaps lucky enough  they will stumble into a Pit Dragon's lair, with the resident in presence. What happens next is undoubtedly violent, confusing and smelly, but there are no real accounts of their copulation outside of tall tales. The most likely reason for this is that dragons have a tendency to devour their mates after breeding, as so few Ogres have lived to tell of their 'conquest'. After many months, emerging from an enormous, slimy egg is the new whelp born of their interbreeding  a Dragon-born Ogre. These horrid children appear as a terrible centauroid amalgamation of the two parents, born the size of a hippopotamus. Their lower bodies are massive and scaled, with its feet terminating into terrible claws with ridges running along their back to their spined tails. Their upper body is of a bristled Ogre torso, with enormous jaws, reptilian eyes and jagged horns emanating from their skulls. Erupting from their backs are stunted and worn wings, too weak to carry the monsters but intimidating enough to prove useful. Fortunate then, that only one is ever birthed at a time.",
            "Dragon-born Ogres naturally heed the call of the Abyss and their monstrous frames and abilities are more than a small boon to armies devoted to the spread of chaos. They are elemental creatures of storm, calling lightning with their braying as tempests gather within their presence. When a Dragon-born Ogre heeds the call of demonic hell-trumpets, they rumble down from their mountain lair at maddening speeds to thunder into battle. They wield wicked axes and their rage is both sung of and feared by all. Dragon-born Ogres are luckily solitary creatures, unable to breed. When not at war, they remain in hibernation among war trophies and the bones of the dead. They seem to live for long ages uncounted and almost never stop growing. Some of the most ancient of them tower over townhomes and towers, throwing them aside like a child topples blocks. These living elementals are worshipped as gods upon earth, as tribute is given in golden sacrifices and prayers poised before bloody exsanguinations."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 10
            },
            "brawn": {
                "base": 45,
                "bonus": 8
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 12,
        "peril_threshold": 9,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "coordination": 30,
            "education": 10,
            "folklore": 10,
            "intimidate": 30,
            "leadership": 30,
            "martial_melee": 30,
            "resolve": 30,
            "survival": 20,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Battle Axe",
                "chance": 80,
                "distance": "melee engaged or 1 yard",
                "damage": "11",
                "qualities": [
                    "Adaptable",
                    "Reach",
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Thundering Claws",
                "chance": 50,
                "distance": "melee engaged or 1 yard",
                "damage": "8",
                "qualities": [
                    "Pummeling",
                    "Powerful",
                    "Reach"
                ]
            },
            {
                "name": "Tail Vertebrae",
                "chance": 50,
                "distance": "melee engaged or 1 yard",
                "damage": "8",
                "qualities": [
                    "Entangling",
                    "Pummeling",
                    "Reach"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HATRED (Elf )",
                "description": "When facing the Ancestries indicated in parentheses, these creatures add an additional 1D6 Fury Die to Damage and automatically succeed at Resolve Tests."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "NATURAL ARMOR (4)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition, they ignore the effects of Hard Terrain."
            },
            {
                "name": "STORM RAGE",
                "description": "These creatures are immune to all attacks from electricity and lightning. Furthermore, when they are struck by such attacks, it empowers them, adding a one-time bonus of +3 to Damage Threshold while enabling them to add a 1D6 Fury Die to weapon Damage for an hour."
            },
            {
                "name": "WEAK SPOT (Wings)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Battle axe",
            "Dragon-born Ogre hide",
            "Tail vertebrae"
        ]
    },
    {
        "type": "LYCANTHROPE",
        "family": "MUTANT",
        "description": [
            "In even the most primal of memories, there lurk terrible humanoid-beast hybrids who stalked our primordial ancestors across frozen tundra and through old growth forests  beasts that were once thought to be friends and loved ones. These terrible and tragic beasts exist to this day as the cursed Lycanthropes.",
            "Lycanthropes have directly suffered the bane of a god's wrath, usually the Demiurge. Those who were cursed to become Lycanthropes have a long history of hunting for pleasure, cruelty to animals and bestiality. Their hubris takes them over in a strange way  while they can appear normal on most occasions, during the turn of a moon they undergo a horrid change. Their flesh splits and grows, course hair sprouts from their bodies, their faces elongate and they bay in pain as their body mutates uncontrollably. Once they have fully transformed, they resemble a monstrous fusion between a man and a beast: an upright wolf with Human eyes, a sleek puma with humanoid hands, shaggy bears with muscled arms  or worse. Lycanthropes are at their most dangerous when in the sway of the cosmos  their long, black-barbed claws drip with Tomb Rot, as their needle-like rows of teeth gnash and slobber with a Cheshire Cat smile, possessed by an unstoppable hunger. When the moon sets, the Lycanthrope will revert back to it humanoid form; this is not a guarantee though, as some Lycanthropes have become so terribly mutated that they stay in their beast form forever.",
            "Lycanthropes can also spread their curse to others  if the victim ever obtains a Disorder while suffering from a Lycanthrope's Tomb Rot, they contract a Mutation called Moon Fever and join the legions of the skin-changers. Those that do often lead double lives, quarantining themselves while under the control of the full moon's glow. They also tend to live together in packs, understanding each other's suffering and hunting together as a group. It is rumored that there exist whole villages that are home to nothing but these vile creatures. Their forms are also endless  though most Lycanthropes take on the shape of bears, wolves and bats, other forms such as crocodilians, felines and porcine are also reported. Luckily, they all suffer the same aversion to wolfsbane, despite its namesake."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 8
            },
            "agility": {
                "base": 45,
                "bonus": 6
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 9,
        "parry": null,
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "coordination": 20,
            "eavesdrop": 20,
            "guile": 20,
            "handle_animal": 10,
            "intimidate": 20,
            "resolve": 20,
            "simple_melee": 20,
            "stealth": 20,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Eviscerating Claws",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Vicious"
                ]
            },
            {
                "name": "Ragged Teeth",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Entangling",
                    "Pummeling",
                    "Slow"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "BANE OF LYCANTHROPES",
                "description": "When creatures are struck with weapons coated with a sprig of wolfsbane, they suffer an additional 1D6 Fury Die in Damage."
            },
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "LICK YOUR WOUNDS",
                "description": "These creatures can spend 1 Misfortune Point to move three steps up the Damage Condition Track positively."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            },
            {
                "name": "SNIKT! SNIKT!",
                "description": "These creatures may spend 3 APs to attack twice with melee weapons."
            },
            {
                "name": "WANTON HUNGER",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1-5', their hunger has been sated. If the result is face '6', their hunger has not yet been sated and they are in a state of frenzy. When sated, they add +1 to both Damage and Peril Condition Tracks. When in a state of frenzy, they add an"
            }
        ],
        "trappings": [
            "Lycanthrope hide"
        ]
    },
    {
        "type": "MEDUSA",
        "family": "MUTANT",
        "description": [
            "Basilisks and Cockatrices are imposing beings, not only because of their monstrous forms, but also because of their petrifying gaze. But what happens to their terrible statues as they suffer the wear of ages and the call of Corruption? Some of those statues turn into the hideous womanizers and seductresses called Medusa.",
            "The Medusa were once folk petrified by the gaze of a Basilisk or Cockatrice, only to be broken free of such shackles by an outside source such as dark Magicks and stink of the Abyss. When revived, their body transforms and turns towards serpentine features: their eyes become almond shaped, smooth scales grow upon their lower torso and they begin to speak in a sibilant  yet alluring  hiss. Their hair undulates upon its own accord, each lock twisting and curling as if underwater. Some Medusa return to civilization in an attempt to reintegrate back into the populace, yet keep their lair out in the wilderness. They are intelligent creatures, but Medusa generally feel two emotions the strongest  lust and hunger. Medusa can come off as hauntingly handsome or beautiful, luring numerous lovers into their dens with the promise of a night of passion. But once the love is over, the Medusa feasts upon their favorite meal  the freshly plucked eyes of their lovers. Those that attempt to escape are petrified on the spot and the Medusa's lair is filled with countless statues of men and women in flight  as well as warriors who attempted to free the imprisoned.",
            "Medusa use their lightning reflexes and sharp vision to benefit their already formidable archery skills. Some Medusa have become so horribly mutated that their entire lower body is replaced by a muscular and squamous snake tail. Their blood is also highly acidic like Snake Venom  their heart drips a murky green and the fluid is the only antidote to undo a victim's petrification. An odd aspect of the Medusa is that their petrifying gaze seems to be a reflex rather than a willed ability  Medusa heads have been detached and have maintained their petrifying ability  administered through the strategic pull of a few nerves and tendons. This pulls the chaotic Ætheric winds towards the user however  becoming more and more Corrupted with each yank of the entrails."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 45,
                "bonus": 11
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 7
            },
            "fellowship": {
                "base": 40,
                "bonus": 6
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 14,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 10,
        "parry": null,
        "dodge": {
            "chance": 75,
            "qualities": []
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "charm": 30,
            "coordination": 30,
            "disguise": 30,
            "folklore": 10,
            "guile": 20,
            "interrogation": 20,
            "intimidate": 30,
            "martial_ranged": 30,
            "resolve": 20,
            "scrutinize": 10,
            "simple_melee": 20
        },
        "attack_profile": [
            {
                "name": "Rapier",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "11",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Weak"
                ]
            },
            {
                "name": "Composite Bow",
                "chance": 75,
                "distance": "ranged 16 yards",
                "load": "2 AP",
                "damage": "11",
                "qualities": [
                    "Fast",
                    "Finesse"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "CAPTIVATING CRY",
                "description": "When foes can hear this creature, they must Resist with a successful Resolve Test or be drawn towards the sound. Should they enter a dangerous area to find the sound, they can attempt another Resolve Test. Once they are able to visually see the creature, the Captivating Cry's effects end."
            },
            {
                "name": "CHTHONIAN DWELLER",
                "description": "These creatures do not need to breathe and are immune to Chokehold. In addition, they can burrow or swim at the same rate of Movement as they can on foot. Finally, they may flip the results to succeed at Resolve Tests."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "EYES WIDE SHUT",
                "description": "When this creature's eyes are successfully struck with a Called Shot (otherwise treated as head), they lose their ability to use Petrifying Gaze."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GIVE ME YOUR EYES",
                "description": "When these creatures inflict a Moderate Injury, it is always a Black Eye. When they inflict a Serious Injury, it is always Head Trauma. Finally, when they inflict a Grievous Injury, it is always a Vitreous Hemorrhage."
            },
            {
                "name": "I GOT AXE FOR YOU",
                "description": "After this creature makes an Attack Action with a melee weapon, they can immediately make an Opportunity Attack with any one-handed ranged weapon with the Throwing Quality on the same Turn."
            },
            {
                "name": "PETRIFYING GAZE",
                "description": "When foes are Engaged with this creature (and the creature can see them), they must Resist with a successful Awareness Test or be Petrified. While Petrified, they are left Helpless and unconscious. Petrification lasts until the heart of the creature that caused the Petrification is pulverized into a bloody concoction and poured over the victim with a successful Alchemy Test."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "W hen first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Arrows (9)",
            "Composite bow",
            "Mask of golden coins",
            "Medusa's head",
            "Medusa's heart",
            "Quilted armor",
            "Rapier"
        ]
    },
    {
        "type": "RAVENOUS GHOUL",
        "family": "MUTANT",
        "description": [
            "There are those in this world that are slave to their own vices. Ones given to gluttony can fall prey to chaos' touch during their weakest moments of overindulgence. Once upon this dark path, it cannot be abandoned as the path to ghouldom begins.",
            "At first, their hunger is satiated only by rich foods; thick blood sausage links, lemon cakes by the baker's dozen and eel pies. Over time, the hunger grows more severe. They turn to darker appetites to satisfy their hunger; gruesome buffets of honey-cured Human bacon, Elven ears braised with leafy vegetables and Dwarven fingerling fingers boiled with potatoes. As their body begins to grow obese from overeating, in turn their spirit twists within. As their humanity slips away bite by bite, their corpulence grows as the moon moves towards its final phase. Upon the new moon's zenith, the darkness has fully possessed the poor soul. The flesh sloughs off, revealing a mutated, emaciated shape beneath. Their transformation is complete, forever damned to their gluttonous ways as a Ravenous Ghoul.",
            "Oftentimes mistaken for supernatural creatures, ghouls are Mutants given to less-than-savory predilections. These foul creatures live on the fringes of society, consumed by their insatiable lust for consumption of flesh. Traveling in packs, Ravenous Ghouls picaroon the countryside to sate their hungers with knife and claw and fork. They sometimes follow in the wake of armies, feeding upon both the living and dead. Hunter ghouls may bide their time amidst the rag pickers in the baggage train, donning disguises to hide their grotesquery. Those given to wanton destruction simply lash out with abandon, possessed by their lust for flesh. Feeding this hunger is paramount, for if a Ravenous Ghoul goes without, they wither and die a terrible, painful death. It is not unheard of for a pack of ghouls to frenzy upon one another when times are lean, leaving only the strongest to survive."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 7
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 9,
        "parry": null,
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Intermediate",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 20,
            "coordination": 10,
            "disguise": 20,
            "intimidate": 20,
            "resolve": 20,
            "simple_melee": 20,
            "stealth": 20,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Fork & Knife",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "WANTON HUNGER",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', their hunger has been sated. If the result is face '6', their hunger has not yet been sated and they are in a state of frenzy. When sated, they add +1 to both Damage and Peril Condition Tracks. When in a state of frenzy, they add an"
            }
        ],
        "trappings": [
            "Bent silver fork",
            "Fillet knife",
            "Grotesque trophies (3)",
            "Stained bib and napkin",
            "Tattered clothing"
        ]
    },
    {
        "type": "TROLL",
        "family": "MUTANT",
        "description": [
            "Trolls are monstrous and terrifying creatures that stalk forest paths and mountain passes, as well as hiding under bridges in the wilderness. Large and strong of body, they are (thankfully) cut short by their utter lack of concrete intelligence.",
            "Where the first Troll came from is a bit of a mystery. It is oft believed that they were once humanoids who fell to the lurid powers of the Abyss, namely the Prince of Decay. As they became more engrossed in the Prince's thrall, their skin turned ruddy, gelatinous and sloughed off in great heaps, only to be regenerated once again. Trolls like to bandy about that they were the 'first big guy' in the Material Realm, but the evidence really points to creatures like the Nephilim being the first inhabitants of the Material Realm. Trolls generally grow to about ten feet in height and although stooped, they are broad of shoulder with a sharp, angular face filled with razor sharp teeth. It's hard to tell if they are even humanoid creatures anymore  their skin is sticky and slime-like with bones so malleable that they can compress their heads like a half-deflated pig's stomach. Their malodorous skin attracts all manner of debris to it  rocks, sticks, foliage and trash. This is a great advantage to the Troll, as it gives them natural camouflage and armor without them having to think too hard about it. Trolls are cruel and greedy and the only thing they enjoy more than feasting on the entrails of travelers is picking through their precious stores of lucre.",
            "Thus, comes the trap that many Trolls find themselves in, but are often unaware of. Bailiffs and minor officials can easily recruit trolls as guardsmen with the promise of cash  the Troll only has to occupy dangerous passage against travelers and take a tithe as they pass. Their infamous ditty 'pay the troll toll to get in' is often sung mockingly to the Troll's customers, as they dance about. Even paying the toll is no guarantee the creature will let you pass, but they can be distracted long enough to be evaded. There are a number of myths circulating of Trolls and their arcane nature  one false one is that sunlight petrifies them and that they will not abandon their posts. There are others which perhaps have a greater ring of truth to them  such as the petrified penis of a Troll being useful in druidic Magicks, their preference for skinning their meals alive and their uncanny ability to regenerate from deadly wounds"
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 10
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 7
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": 10,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": {
            "chance": 50,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 30,
            "awareness": 20,
            "bargain": 20,
            "eavesdrop": 20,
            "interrogation": 10,
            "intimidate": 30,
            "resolve": 10,
            "simple_melee": 30,
            "simple_ranged": 30,
            "stealth": 30,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Petrified Club",
                "chance": 80,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Light",
                    "Powerful",
                    "Weak"
                ]
            },
            {
                "name": "Terrible Claws",
                "chance": 80,
                "distance": "melee engaged",
                "damage": "10",
                "qualities": [
                    "Pummeling",
                    "Slow"
                ]
            },
            {
                "name": "Acidic Spittle",
                "chance": 80,
                "distance": "ranged 5 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            }
        ],
        "traits": [
            {
                "name": "ACIDIC SPITTLE",
                "description": "These creatures can use their saliva as a ranged weapon. This allows them to strike a single foe within 1+[PB], causing the foe to suffer 1D10+1 Damage from acid. However, Acidic Spittle ignores a foe's Damage Threshold Modifier from armor. A foe can attempt to Dodge Acidic Spittle or Parry it with a shield. Acidic Spittle can be used while Engaged with foes."
            },
            {
                "name": "BIG GRIM",
                "description": "These creatures can use two-handed weapons in one hand and take advantage of the Adaptable Quality."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "FLAMMABLE",
                "description": "When exposed to flames, these creatures suffer an additional 1D10+1 Damage from fire."
            },
            {
                "name": "FOUL MUTATION",
                "description": "When these creatures are encountered, roll 1D6 Chaos Dice if of Basic Risk Factor; 2D6 Chaos Dice if of Intermediate Risk Factor; 3D6 if of Advanced Risk Factor; 4D6 if of Elite Risk Factor. For every face '6', add one Taint of Chaos to the creature."
            },
            {
                "name": "GASTRIC ACIDITY",
                "description": "When these creatures deal Damage, a foe must Resist with Coordination. If they fail, the foe's armor, weapon or shield is Ruined!."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "HERP DERP",
                "description": "These creatures are easily distracted. When their Turn starts, they must succeed a Resolve Test or else lose 1 AP."
            },
            {
                "name": "KILL IT WITH FIRE",
                "description": "Only after these creature's remains are set On Fire are they forever Slain!."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "PERFECT CAMOUFLAGE",
                "description": "Foes must flip the results to fail attacks made with ranged weapons to strike these creatures."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SILENT STALKER",
                "description": "When these creatures use the Stealth or Survival Skill, they flip the results to succeed at their Test."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition,"
            }
        ],
        "trappings": [
            "Bailiff 's lockbox",
            "Petrified club",
            "Petrified troll penis",
            "Tattered rags",
            "Troll hide"
        ]
    },
    {
        "type": "BOOGAN",
        "family": "ORX",
        "description": [
            "Boogan are the offspring of Orx, but that would be a generous term for them. Impregnated into the humanoid victims of savage WAAAR! Hordes, Boogans are born of sexual violence.",
            "They gestate within mere weeks; unfortunately, mothers rarely survive postpartum, but those who do are immediately prepared by the Orx host for their next child. Orx in miniature, they are near countless in number among a WAAAR! Horde. These 'half Orx' are not beloved children to the Orx; they are often viewed as pests, cannon fodder and even food, being likened to excitable puppies at best. Boogans are barely sentient and are quick to lick their Orx master's boots. It is known that Boogans do not mature like other mammals; they remain stunted and small yet able to survive on their own. If a Boogan manages to live for a full year without being eaten by other Orx, they sometimes undergo a transformation not unlike that which gives birth to the Low Orx. This transformation is also ritualized by the hand of a High Orx.",
            "Boogans often travel in small 'swarms', usually consisting of roughly two dozen members that constantly fall over each other, curse and in-fight. Boogans have suicidal tendencies  not due to any mental issue, but rather due to stupidity and an inability to comprehend the consequences. To them, jumping off of a cliff means nothing, because they have no concept that jumping from a large height onto jagged rocks will kill them. They laugh when other Boogans die, feeling no sense of remorse or sorrow and even treating it as a game. Not only do Orx hate Boogans, but so do humans  they infest camps during conflicts with a WAAAR! Horde and always prove to be a nuisance. In fact, in a strange sense of camaraderie, an Orx will even apologize to the enemy for such Boogan infestations  making these whelps the lowest of the low."
        ],
        "size": [
            "Small"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 45,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 7,
        "parry": {
            "chance": 60,
            "qualities": []
        },
        "dodge": {
            "chance": 75,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Low",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Shiv",
                "chance": 50,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Weak"
                ]
            },
            {
                "name": "Rocks",
                "chance": 50,
                "distance": "ranged 5 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Throwing",
                    "Weak"
                ]
            },
            {
                "name": "Red Cap Mushroom",
                "chance": 50,
                "distance": "melee engaged or ranged 5 yards",
                "damage": "None",
                "qualities": []
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FECKLESS RUNT",
                "description": "When this creature's Turn begins, roll 1D6 Chaos Die. If the result is face '6', they elect to attack that Turn with a senseless object that does no Damage."
            },
            {
                "name": "SNIVELING WHELP",
                "description": "These creatures can only use 2 AP"
            }
        ],
        "trappings": [
            "Red cap mushrooms (3)",
            "Rocks (3)",
            "Shiv",
            "Tattered clothing"
        ]
    },
    {
        "type": "HIGH ORX",
        "family": "ORX",
        "description": [
            "The Tusked One demands powerful sacrifices from the Orx folk and those that dare take up the challenge may ascend to become a High Orx.",
            "After a Low Orx makes an appropriate number of blood sacrifices to the Bigboss, they are sometimes granted an advancement in rank among the WAAAR! Horde. Should the Bigboss approve, they will ritually slay a Low Orx, laying them among the shallow ditches where dead humanoid mothers previously used to birth Boogan are interred. While their body decomposes in this grotesque 'rebirthing chamber', a Low Orx undergoes a hibernation period until the new moon rises. Then their horrid transformation begins. Their shoulders broaden to the width of the span between a man's hands, as their skin calcifies yellow and grey. Their arms take upon the appearance of corded rope, sinewy and powerful. Their head swells  if only to accommodate a row of vicious teeth and gelatinous mass to further protect their peanutsized brain  as a massive cleft chin bristles with hair. And when the blinding pain fades, they arise, having undergone a rebirth from the vast sickness, as a High Orx.",
            "Long ago, High Orx were enslaved by the Dvergar, used as a force of cheap labor and sometimes even a source of food. They soon rebelled, as High Orx knew they were capable of much more than their masters wanted of them. Intelligent and brutal, High Orx serve as both front-line fighters and lieutenants within the WAAAR! Horde. While they still hold to the three cultural values of the Low Orx  birth, violence and death  they also seek to exemplify the legacy of their clan. High Orx are visionaries and big thinkers, commanding slaves to erect towers of iron and stone, build statues in testament to the greatness of Bigbosses and aid in crafting the numerous war machines of the Orx WAAAR! Horde."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 10,
        "peril_threshold": 8,
        "parry": {
            "chance": 55,
            "qualities": []
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "intimidate": 10,
            "resolve": 10,
            "scrutinize": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "survival": 10,
            "toughness": 10,
            "tradecraft": 10,
            "warfare": 10
        },
        "attack_profile": [
            {
                "name": "Rusted Chains",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "7",
                "qualities": [
                    "Adaptable",
                    "Pummeling",
                    "Reach"
                ]
            },
            {
                "name": "Flanged Mace",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Powerful"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "ONE-TWO PUNCH",
                "description": "When these creatures Take Aim and then make a successful Melee Attack, they force a foe to Resist a Stunning Blow."
            },
            {
                "name": "SHOOTFIGHTING",
                "description": "These creatures ignore the Pummeling and Weak Qualities when fighting bare-handed, with a blackjack or using knuckledusters. In addition, they can refer to [BB] or [CB] when inflicting Damage with these same weapons."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at"
            }
        ],
        "trappings": [
            "Brigandine armor",
            "Flanged mace",
            "Rusted chains"
        ]
    },
    {
        "type": "LOW ORX",
        "family": "ORX",
        "description": [
            "Low Orx make up the largest portion of an Orx host, aside from the countless Boogans.",
            "Crass, violent and prone to squabbling, Low Orx prefer nothing else than wanton murder and destruction and they fear very little. Some scholars even hypothesize that part of the Orx mutation makes them forget the concept of fear. Low Orx constantly look up in jealousy at their High Orx peers and Bigboss leaders, waiting for the day when they are strong enough to take over and command the WAAAR! Horde in a bloody coup. This is unlikely however, given that Low Orx lack the natural military mind that Bigbosses have. They hold very little dear, being more akin to rabid beasts who understand nothing but fighting and 'ploughing' young maidens. Low Orx have little in the way of scope for cultural growth; to them, there are only three values they can attest to: birth, violence and death.",
            "The Orx WAAAR! Horde is often the only thing a Low Orx knows. No horde is exactly the same. They have names ranging from Thousand Spears to Black Teeth and every right-thinking Orx takes an almost barbaric familial pride in the Orx WAAAR! Horde they belong to. That is not to say there is any love between brothers, but the concept of the 'clan'  maintaining banners of it, attesting your clan is the best and so on  is a common enough trait. Perhaps this is inherited from the Orx long wars against Dwarfs, but the small folk scoff at the idea. Maybe that is why every Low Orx aspires to become the Bigboss, in order to be the leader of the only thing they can say they truly care about."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 7,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 7,
        "parry": {
            "chance": 60,
            "qualities": []
        },
        "dodge": {
            "chance": 40,
            "qualities": []
        },
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "interrogation": 10,
            "intimidate": 10,
            "martial_melee": 10,
            "resolve": 10,
            "ride": 10,
            "simple_ranged": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "War Chopper",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Adaptable",
                    "Slow",
                    "Vicious"
                ]
            },
            {
                "name": "Javelin",
                "chance": 60,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "5",
                "qualities": [
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "RINGEN",
                "description": "These creatures penalize their foe's ability to Resist a Chokehold and Dirty Tricks by a -10 Base Chance to their Skill Test. In addition, when they use a Chokehold, add an additional 1D10 to determine how much physical Peril they inflict."
            },
            {
                "name": "SQUABBLE AMONG THEMSELVES",
                "description": "When these creatures suffer from Fear or Terror, they must succeed at a Resolve Test as their Turn starts or else attack their own"
            }
        ],
        "trappings": [
            "Javelin (3)",
            "Leather armor",
            "War Chopper"
        ]
    },
    {
        "type": "RUDEBOY",
        "family": "ORX",
        "description": [
            "Rudeboys are the demented priests of Orx clans, cannibalistic creatures that worship at the feet of the Tusked One.",
            "Though both Low and High Orx consume the bodies of their fallen foes, they know consuming Orx flesh will drive them into madness. This is what Rudeboys do, however; subsequently, their bodies burst forth with a mutative fungus that ravages their bodies and rots them from the inside out. Most Orx cannibals are slain by this process, torn asunder from within as a punishment by the Tusked One, but those who do survive live onwards as Rudeboys. Blood-red toadstools grow all over their bodies which the Rudeboy consumes regularly to gain spirit visions with the Tusked One and to enter a psychedelic battle frenzy.",
            "The Rudeboy's worship of the Tusked One is different than the rest of the Orx WAAAR! Horde, for their murder is wanton and crazed, resulting in shouted litanies of descriptions of the brutal deaths they've inflicted. High Orx begrudgingly accept the Rudeboy's place in the clans, while Low Orx literally worship at their feet. Which means that Bigbosses must tolerate Rudeboys, lest they lose control of the WAAAR! Horde. One odd aspect of Orx biology is that they cannot regulate their body temperatures well and thus they tend to overheat quite easily. Toadstools are known to reduce heat regulation, so Rudeboys literally cook their brains in their frenzies. It's not hard to see why a majority of Rudeboys border on the unintelligent."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 7,
        "parry": {
            "chance": 60,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Basic",
        "risk_factor": "Medium",
        "skills": {
            "athletics": 10,
            "coordination": 10,
            "resolve": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Cleaver",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Slow",
                    "Vicious",
                    "Weak"
                ]
            },
            {
                "name": "Shiv",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Weak"
                ]
            },
            {
                "name": "Hunting Bow",
                "chance": 60,
                "distance": "ranged 14 yards",
                "load": "1 AP",
                "damage": "6",
                "qualities": [
                    "Finesse",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HALLUCINOGENIC FRENZY",
                "description": "After ingesting a dose of red cap mushrooms, these creatures add 1D6 Fury Die to melee weapon Damage."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "SQUABBLE AMONG THEMSELVES",
                "description": "When these creatures suffer from Fear or Terror, they must succeed at a Resolve Test as their Turn starts or else attack their own"
            }
        ],
        "trappings": [
            "Animal pelts",
            "as clothing",
            "Arrows (9)",
            "Cleaver",
            "Hunting bow",
            "Pot of war paint",
            "Red cap mushrooms (3)",
            "Shiv"
        ]
    },
    {
        "type": "ATTERCAP SPIDER",
        "family": "PRIMEVAL",
        "description": [
            "In the deepest thickets of woods, where not even elder creatures of the forest dare to venture, the sky grows dark with foliage as the ground becomes sticky with Ætheric webs of shadow-stuff. This retreat is the home of the Attercap Spider.",
            "A massive Primeval creature who has lived in the dark copses since time immemorial, Attercaps embody the cardinal sin of envy made manifest. Attercaps come in as many varieties as normal spiders, but they often reach heights of ten to fifteen feet when fully extending their eight monstrous legs. Their eyes gleam with a terrible intellect and they speak the common tongue in a clicky, garbled mess. Most Attercaps are female and spend a majority of time protecting their massive clutches of eggs, sending swarms of her servant spiders against any who disturb her Ætheric webs. Attercaps are extremely envious of other forest natives, as they inhabit the land the Attercap believes is rightfully theirs by birthright and savagery. However, they rarely leave their cobwebs, as they have an intense aversion to light that forces them to stay in gloom.",
            "Few have made allies or even parlayed with an Attercap; the most notable example are Goblins, who have managed uneasy alliances with these conniving spiders. Make no mistake, the Attercap controls them under threat of painful, nutrientextracting death. The creature's motives for dealing with the Goblins is questionable, but it's thought to be as a means for the Attercap to easily expand her lair throughout the forest realm. Kobold Fanatics, specifically, are known to be quite enthralled by the Attercap  some of them worshiping the select few as gods, perhaps a manifestation of The Maw or even the Black Lodge."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 5
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 8
            },
            "perception": {
                "base": 45,
                "bonus": 7
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "High",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "coordination": 20,
            "eavesdrop": 10,
            "guile": 20,
            "simple_melee": 20,
            "stealth": 20,
            "survival": 20
        },
        "attack_profile": [
            {
                "name": "Mandibles",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Vicious"
                ]
            },
            {
                "name": "Wall Crawler Spinneret",
                "chance": 45,
                "distance": "ranged 10 yards",
                "load": "1 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "AVERSION TO LIGHT",
                "description": "When these creatures are exposed to any sort of light (as from a torch), they suffer a penalty of -3 to their Damage Threshold."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "NATURAL ARMOR (3)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "POISONOUS BITE",
                "description": "When these creatures deal Damage, roll 1D6 Chaos Die. If the result is face '6', they inject Spider Venom into their foe."
            },
            {
                "name": "WALL CRAWLER",
                "description": "These creatures can crawl upon both vertical and horizontal surfaces with ease. In addition, they can initiate a ranged Chokehold at a Distance of 3+[PB],"
            }
        ],
        "trappings": [
            "Spider hide",
            "Spider meat (9)"
        ]
    },
    {
        "type": "BARREL-BELLIED ORC",
        "family": "PRIMEVAL",
        "description": [
            "Boars and other domesticated swine are both a common sight and source of food, but the Barrel-Bellied Orc is a different creature altogether.",
            "A Primeval that embodies the cardinal sin of wrath, they are massive and ridge-backed, their tusks reaching a length of six feet and as sharp as any pike. The Orx, those terrible ravaging Mutants, take their name from this massive swine and the Barrel-bellied Orc does not seem to mind much. Of all Primeval species, the Barrel-bellied Orc is the least intelligent, though not through lack of adaptation. Instead, they have a predilection for red cap mushrooms. Not only does it induce visions, but it slowly eats away at their intellect and ability to plan. When not in that red mist, Barrel-bellied Orcs only wish to root around, scrounge and mate, doing very little planning for further conquest. They are surly and wrathful, charging at the slightest insult and maiming anyone who jeers at them. They do not brook fools gladly nor play games.",
            "Barrel-Bellied Orcs are often tamed as steeds for the Orx, usually under the promise of more of the delicious red caps. Facing down a fully enraged Barrel-bellied Orc is a terrible idea; their tusks are used solely for mauling and can run a grown man completely through. There is an odd bonus of slaying a Barrel-Bellied Orc  apparently, their meat is quite savory and full of nutrients and a single Orc could feed a starving village for a week. Orx sometimes kill old BarrelBellied Orcs for this purpose and they even use every piece of the animal  more than a few Orx villages, weapons and artifacts are made from shattered Orc bones, stretched hide and remnants of organs."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 35,
                "bonus": 5
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": []
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "guile": 10,
            "intimidate": 10,
            "resolve": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Tusks",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Powerful",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "BATTLE FRENZY",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If it lands on a face '1', '2' or '3', they are under the effect of that many doses of red cap mushrooms."
            },
            {
                "name": "CHOMP",
                "description": "When foes Parry this creature's melee attack, it can make an Opportunity Attack at this same foe."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage,"
            }
        ],
        "trappings": [
            "Orc hide",
            "orc meat (9)",
            "orc tusk (2)"
        ]
    },
    {
        "type": "DEATH BAT",
        "family": "PRIMEVAL",
        "description": [
            "Death Bats are perhaps the most mysterious creature of all Primeval creatures. Personifying the cardinal sin of lust, they dwell in the deepest caves with their broods.",
            "They are monstrous bats the height of a small hut with a wingspan of around twenty feet. They say little and venture out rarely, preferring to spend their time hanging from cavern ceilings within the colony to mate. When they go out to hunt at night, their shadows can cover entire villages, often mistaken as Pit Dragons from below. They snatch up their prey, taking it back to their cave where they claw open the victim's chest and devour the heart: their favorite food. This meal makes them extremely virile, allowing them to mate for hours: their second favorite activity. Death Bat colonies are enormous for this reason, filling entire cavern systems with chattering and toxic droppings. Death Bats say little when they do speak and their active worshippers are few and far between due to their predilection for killing them.",
            "Those that do win the Death Bat's trust are permitted to ride them, a night on the wing that few ever forget. Death Bats will also willingly join the forces of Corruption for the pure promise of warm hearts, often allying with necromongers or Vampires. Death Bats are often quite intelligent, but care little for company other than their colonies. Many would just prefer to be left alone. Like the Pit Dragons they are often mistaken for, Death Bats also have a predilection for shiny objects. The bottom of their cave floors are often littered with baubles and coins, but the only hindrance to getting to them is having to sift through tons of toxic guano, venomous carrion insects and vicious plant life. Many have left Death Bat caves with a small fortune in treasure, only to succumb to illness a few days later."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            },
            {
                "value": 16,
                "type": "fly"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "coordination": 10,
            "guile": 10,
            "eavesdrop": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Vampiric Fangs",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "3",
                "qualities": [
                    "Fast",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "WEAK SPOT (Wings)",
                "description": "When a creature's body part indicated in parentheses is successfully struck by a Called"
            }
        ],
        "trappings": [
            "Bat hide",
            "Bat meat (6)"
        ]
    },
    {
        "type": "DIRE RAT",
        "family": "PRIMEVAL",
        "description": [
            "Dire Rats are the smallest of the Primevals, but that does not make them any less bijou or foul. They are also the most common, for it seems like every major city has at least a single Dire Rat king that lords over the many other, small rodents.",
            "About the size of a large mongrel, these rodents of unusual size often dwell in the sewers beneath a city where they search for baubles to fulfill their endless sin of greed. Dire Rats are riddled with disease and their teeth are long and gnarled. They are treated as pets of the Skrzzak, specifically bred like dogs (and exhibiting some the same qualities such as loyalty and ferocity). In that capacity, they are viewed as little more than beasts trained to hunt, rather than intelligent, elder creatures. It is said they are attracted to the smell of embalming fluid, possibly a remnant of the Skrzzak vats where they are sometimes bred (and further mutated to become Warren Runners). They are notorious hoarders; using their primitive thumbs, they collect any 'shinies' they find interesting and take it back to their junk-strewn nests.",
            "Though not very strong, Dire Rats are both cunning and sneaky, surviving less on might and more on wits. When their stash of shinies is threatened, however, they will stop at nothing to protect their horde. Dire Rats also have an odd talent of being able to whistle, running air through their jagged teeth to produce a high-pitched melodic squeal. Legends tell that children are often drawn to this sound, eventually squeezing their way through sewer heads or storm drains to find the source. Dire Rats love to capture these children and will treat them about as well as a halfsentient rodent could; as playthings. Their real reason behind the kidnapping is to extort their family for jewels and coins, returning the child as soon as they get payment."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Basic",
        "skills": {
            "awareness": 10,
            "coordination": 10,
            "eavesdrop": 10,
            "simple_melee": 10,
            "stealth": 10,
            "survival": 10,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Yellow Teeth",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "3",
                "qualities": [
                    "Fast",
                    "Vicious",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Bloody Flux)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "PACK MENTALITY",
                "description": "When three or more of these creatures are alive during combat, they may flip the results to"
            }
        ],
        "trappings": [
            "Rat hide",
            "Rat meat (9)"
        ]
    },
    {
        "type": "HOWLBEAR",
        "family": "PRIMEVAL",
        "description": [
            "While other Primevals have similar appearances and mannerisms to other animals, the elusive and prideful Howlbear are unlike any other critter that wanders the wood.",
            "They are roughly the size of  and have similar features  to a brown bear, but between their torso and their arms they have a membrane of feathers that grant limited flight; their eyes are also much more raptor-like than mammalian; and their roars sound more like avian screeches. Howlbears typically dwell in taigas or mountains, living alone. All Howlbears are asexual females who, upon reaching breeding season, will seek out a local barn and kill all the livestock within. It then lays its eggs, ingesting the dead meat to regurgitate later in their babes' mouths. Howlbears are extremely proud and protective of their brood; there is no greater danger than attempting to fight a Howlbear while she is roosting during the autumn. Once they hatch, the cubs are adorable but quite vicious, learning to hoot and speak only days after leaving their shells. One in three of these babes will survive the long cold winter, as they are born blind and must rely fully on their mother, but those that do live reach adulthood in only three years.",
            "Howlbears hunt during storms, knowing how hard it is for their prey to see them. The most fearsome fact about Howlbears is their ability to fly  nothing instills more terror in an average person than a fully-grown brown bear swooping down upon them like a bird of prey. Frightened peasants will hire Slayers to drive out the Howlbears from their farms, especially since their eggs are considered a delicacy. In addition, Howlbear blood is regenerative and a single pint can reputedly replace opium in most alchemical processes."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 11
            },
            "agility": {
                "base": 40,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            },
            {
                "value": 13,
                "type": "fly"
            }
        ],
        "damage_threshold": 11,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "athletics": 20,
            "bargain": 20,
            "eavesdrop": 20,
            "intimidate": 20,
            "resolve": 20,
            "simple_melee": 20,
            "stealth": 10,
            "survival": 20,
            "toughness": 20
        },
        "attack_profile": [
            {
                "name": "Talons & Beak",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Powerful"
                ]
            }
        ],
        "traits": [
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "LICK YOUR WOUNDS",
                "description": "These creatures may spend 1 Misfortune Point to move three steps up the Damage Condition Track positively."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "PRIMAL SCREAM",
                "description": "When these creatures successfully use a Litany of Hatred, those affected must Resist with a Resolve Test or suffer from Fear."
            },
            {
                "name": "STRAFING TALONS",
                "description": "When these creatures execute a successful attack while flying, they also deal 1D10+[AB]"
            }
        ],
        "trappings": [
            "Howlbear pints of blood (18)",
            "Howlbear hide",
            "Howlbear meat (12)",
            "Howlbear eggs (3)"
        ]
    },
    {
        "type": "SILVERBACK WARG",
        "family": "PRIMEVAL",
        "description": [
            "The Silverback Warg is almost never seen, as this horrid creature operates from the shadows and deep in the midst of their packs. Displaying the cardinal sin of sloth, the Warg is a massive black wolf with a shock of silver hair running to it tail, though its face is nearly hairless. Its enormous jaws are peeled back in a near-permanent Cheshire Cat-like grin and it has a maniacal cackle that heralds its arrival. Silverback Wargs spend most of their time in their den, sending their hordes of alphas out to hunt for them and bring back tribute. They often like to devour their prey while they are still alive, lounging on comfortable rocks while surrounded by females in heat. They also love games and taunting, natural tricksters that will let their prey go only to pounce on them when they turn their backs. Japes and trickery are one of the Silverback's favorite pastimes. When the Warg gets up enough energy to fight on its own merits, its teeth and claws can rend any armor or flesh asunder in just a few swipes. They will then lick the scraps and wander back to their perches, falling asleep until their droves of supplicant wolves bring them another meal.",
            "Silverback Wargs are often seen as holy creatures of the Winter King faithful, beings that should be respected perhaps not as gods, but something greater than a normal animal. Scripture says that the Winter King once journeyed for many months with a Silverback Warg as a companion and despite the Warg's nature it proved a valuable ally. In their arrogance, Silverback Wargs are more than aware of this situation and many will dwell near villages of the Winter King's supplicants in order to exploit the inhabitants for free meals and a modicum of undeserved protection from the White Wolves. This isn't entirely one sided, as the Warg will often ward off greater predators that threaten their village in a kind of codependent survival relationship."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 5
            },
            "agility": {
                "base": 45,
                "bonus": 7
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 7,
        "parry": null,
        "dodge": {
            "chance": 55,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "awareness": 20,
            "athletics": 10,
            "coordination": 10,
            "eavesdrop": 10,
            "guile": 20,
            "simple_melee": 20,
            "stealth": 20,
            "survival": 20,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Rending Bite",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Fast",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "LICK YOUR WOUNDS",
                "description": "These creatures may spend 1 Misfortune Point to move three steps up the Damage Condition Track positively."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage,"
            }
        ],
        "trappings": [
            "Silverback warg hide",
            "Silverback warg meat (9)"
        ]
    },
    {
        "type": "WATER PANTHER",
        "family": "PRIMEVAL",
        "description": [
            "Of all the Primevals, the Water Panther is perhaps the most dangerous (and oddly named).",
            "Water Panthers are massive bull alligators whose heads are the size of a cart and scales possess the resilience of steel. Water Panthers were created thousands of seasons ago by sorcerers in order to guard their island lairs. Now, they lurk in swamps or glide through the fetid sewage that flows under every city. Water Panthers have two moods: they have either fed recently and thus are calm and communicative or they have not eaten and are in the throes of a feeding frenzy. They will snap up any morsels that float near them, rolling around in a frenzy to rip their prey limb from limb, swallowing each chunk with a hearty gulp. They are driven by hunger in everything they do, embodying the sin of gluttony, feeding when they don't even have a desire to eat. Rumors abound that a Water Panther can easily devour a division of footmen in under a minute; upon hearing this, a Water Panther will often chuckle and say they've eaten things 'far more grandiose'.",
            "The Water Panther has long held an odd relationship with the reptilian Aztlan. They have long dwelt on the native lands of the Aztlan and perhaps were even there before those invaders arrived from the Vault of Night. Water Panthers often willingly offer themselves up to the Aztlan as war machines or moat protectors, wanting nothing in return. Perhaps this is a form of reptilian camaraderie that a Human mind could not understand or the Water Panther has even more obtuse plans than the Aztlan. Either way, the Aztlan often treat Water Panthers as trusted allies and mounts, though more than one Aztlan has become a midday meal for the crocodilians. The Tlaloc at times like to claim that they brought up the Water Panthers out of the primordial ooze, but the Panthers know this to be just another of a Tlaloc's many lies."
        ],
        "size": [
            "Normal",
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 7
            },
            "brawn": {
                "base": 50,
                "bonus": 8
            },
            "agility": {
                "base": 45,
                "bonus": 4
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 4
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 7,
        "parry": null,
        "dodge": null,
        "notch": "Intermediate",
        "risk_factor": "Low",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "guile": 20,
            "intimidate": 20,
            "simple_melee": 20,
            "stealth": 20,
            "survival": 20,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Horrendous Bite",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "7",
                "qualities": [
                    "Fast",
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "DEATH ROLL",
                "description": "When these creatures deal Damage, at their option, they can force a foe to Resist a Chokehold."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "WANTON HUNGER",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', their hunger has been sated. If the result is face '6', their hunger has not yet been sated and they are in a state of frenzy. When sated, they add +1 to both Damage and Peril Condition Tracks. When in a state of frenzy, they add an"
            }
        ],
        "trappings": [
            "Water panther hide",
            "Water panther meat (9)"
        ]
    },
    {
        "type": "APPARITION",
        "family": "RESTLESS SPIRIT",
        "description": [
            "A good majority of Restless Spirits are victims of circumstance, disembodied spirits who were unwillingly bound to the Material Realm and now unable to move past it. Others, too have been trapped here due to past misdeeds, possessed by unmitigated rage. These Apparitions are their violent echoes.",
            "Apparitions are entities that are bound to the Material Realm because of a promise they broke or a curse they invoked on themselves. Their grief has driven them completely mad and it is difficult to determine if they were even mortal before. Apparitions have no set form, being made of pure ectoplasm  instead, they appear as a dead loved one or friend to whoever sees them. They always carry a spectral lantern, using it to guide their way. They tempt and call to the viewer, acting wayward and lost, until their rescuers come near enough to be touched and drained of their life energy. Apparitions will lie in wait for years to torment certain people and they are incredibly malevolent and violent when their mark is within reach. They can also send forth spectral hands to grasp and claw at foes from a distance, a fearful and dangerous ability.",
            "Apparitions are hateful beings, wanting nothing more than to spread discord and pain. Those they strike down will arise automatically as Phantoms and this breeds a vicious cycle of spiritual pain."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 8
            },
            "agility": {
                "base": 40,
                "bonus": 7
            },
            "perception": {
                "base": 35,
                "bonus": 6
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 50,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 5
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 8,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "awareness": 20,
            "coordination": 20,
            "eavesdrop": 20,
            "folklore": 20,
            "guile": 30,
            "intimidate": 30,
            "rumor": 20,
            "scrutinize": 20,
            "simple_melee": 30,
            "stealth": 30
        },
        "attack_profile": [
            {
                "name": "Ghastly Touch",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Entangling",
                    "Pummeling",
                    "Slow",
                    "Weak"
                ]
            },
            {
                "name": "Phantom Limb",
                "chance": 70,
                "distance": "ranged 10 yards",
                "load": "1 AP",
                "damage": "8",
                "qualities": [
                    "Entangling",
                    "Throwing"
                ]
            },
            {
                "name": "Wall Crawler's Grasp",
                "chance": 40,
                "distance": "ranged 9 yards",
                "load": "1 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BONDS OF DEATH",
                "description": "These creatures can manipulate physical objects in Æthereal Form."
            },
            {
                "name": "COLD HANDS",
                "description": "These creatures ignore the Damage Threshold Modifier a foe's armor may confer when they inflict Damage."
            },
            {
                "name": "COME INTO THE LIGHT",
                "description": "These creatures cannot be permanently Slain! unless their lantern is destroyed. If the lantern is snuffed out, they remain in hibernation until it is relit."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "PARALYZING TOUCH",
                "description": "When these creatures make a successful attack bare-handed, they deal both Damage and force a foe to Resist with a Toughness Test or be Paralyzed. While Paralyzed, foes are unable to use Movement Actions until they successfully Resist."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            },
            {
                "name": "WALL CRAWLER",
                "description": "These creatures can crawl upon both vertical and horizontal surfaces with ease. In addition, they can initiate a ranged Chokehold at a Distance of 3+[PB],"
            }
        ],
        "trappings": [
            "Ghostly lantern"
        ]
    },
    {
        "type": "HOODED ONE",
        "family": "RESTLESS SPIRIT",
        "description": [
            "Immortality has been a desire since people first knew that time for them was finite. Over the centuries as Magickal research has evolved, several breakthroughs have been made in terms of cheating death, though all of them are blasphemous  there is no greater sin in the Custodian's eyes than to refuse Him your rightful soul. Those that succeed may become Defilers, but those that fail in their labors are cursed forever as Hooded Ones.",
            "Hooded Ones are the unfortunates who failed to correctly grant themselves immortality through profane Magicks. Now, they are destined to live forever, stalking the world in a pained existence. Also called wraiths or reapers, Hooded Ones wear dark and tattered cloaks that hide their wispy forms. Underneath their hood is a skeletal form, out of which crawl grave worms as dust emanates from their mouths when they speak. Hooded Ones always carry a light with them, one they use to search for the path to the Well of Souls and a wicked sickle for reaping souls. Hooded Ones are morbid and wrathful, enjoying nothing more than terrorizing the living. They can make mirror imagelike copies of themselves to confuse and haunt, as well as supernaturally manipulate the world around them. They also can drain life force, feeding upon the living.",
            "Hooded Ones are grim harbingers of death and destruction, though they often carve out a territory which they reap their ghoulish harvests from. Those killed by a Hooded One will arise as one of the Restless Spirits and sometimes the more ambitious Hooded Ones have raised armies of the dead in attempts to overthrow the living. Most, however, are condemned to pilot skiffs down the River of Oblivion, where all dead are shepherded. Hooded Ones can be stopped  they are repelled by holy symbols, as they cannot stand the concept of the divine power that binds them to the Material Realm. They also all have a True Name, which they keep hidden: one that if it is revealed, weakens the Hooded One and makes them more vulnerable."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 40,
                "bonus": 9
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 50,
                "bonus": 9
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 6,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            }
        ],
        "damage_threshold": 8,
        "peril_threshold": 12,
        "parry": {
            "chance": 70,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Medium",
        "skills": {
            "awareness": 20,
            "coordination": 20,
            "eavesdrop": 20,
            "education": 30,
            "folklore": 30,
            "guile": 20,
            "incantation": 20,
            "interrogation": 20,
            "intimidate": 30,
            "martial_melee": 20,
            "scrutinize": 20,
            "stealth": 20
        },
        "attack_profile": [
            {
                "name": "Sickle",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Defensive",
                    "Fast",
                    "Finesse",
                    "Vicious"
                ]
            },
            {
                "name": "Cold Touch",
                "chance": 40,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "COLD HANDS",
                "description": "These creatures ignore the Damage Threshold Modifier a foe's armor may confer when they inflict Damage."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "MANY FACES OF DEATH",
                "description": "By spending 1 Misfortune Point, these creatures create 1D6 illusions of itself that mimics its precise actions. Once a mirrored image is successfully struck with any weapon or Magick, it immediately dematerializes. Determining which is the creature and the illusion requires a Secret Test using Scrutinize."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "TRUE NAME",
                "description": "Unless foes invoke this creature's True Name before casting Magick, it fails to affect the creature."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Book of the dead",
            "Tattered robes",
            "Sickle"
        ]
    },
    {
        "type": "PHANTOM",
        "family": "RESTLESS SPIRIT",
        "description": [
            "The mortal soul is highly complicated and sometimes the ferrymen aren't able to recover it properly. These aimless spirits manifest into a Phantom; ghostly beings that cannot journey to the Well of Souls upon their own nor animate the body they once lived within.",
            "Phantoms spend their 'unlife' haunting the places they used to live. They all carry some sort of unfinished business, a symptom of their fragmented soul that broods on a specific loss, deeds undone or enemies left unchecked. This dismay and anger causes the Phantom to lash out, tossing aside furniture, rattling chains and frightening residents so the spirits can chase out the living to dwell within their own sorrow. Life is an anathema to the Phantom and their despair cannot be forgotten if other living beings reside in the place of their death. Because of this, they rarely take physical shape. If the living attempt to banish them or drive them away, a Phantom will manifest physically and attack for all they're worth. Phantoms will fight if they have to and their ability to become Æthereal and immunity to normal weapons makes them dangerous to all but the most prepared foes.",
            "If anyone should ever be struck down by a Phantom, their spirit will arise as an Apparition  set to continue the endless cycle of death and terror. Though like most Restless Spirits, Phantoms do have vulnerabilities  mostly in that they are repelled by all things divine, particularly by symbols of the Custodian. Phantoms are not chaotic creatures to any extent  rather they are tortured, confused and worried. A Phantom's only want is to join their loved ones and friends in the Well of Souls, but they lack the means to do it. If an outside force would come to the Phantom's aid, resolving the unchecked deeds which keeps it bound to the Material Realm, they can finally find the rest they seek."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 35,
                "bonus": 7
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 50,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 5
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 8,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "awareness": 20,
            "coordination": 20,
            "eavesdrop": 20,
            "folklore": 20,
            "intimidate": 20,
            "rumor": 20,
            "scrutinize": 20,
            "simple_melee": 20,
            "stealth": 20
        },
        "attack_profile": [
            {
                "name": "Ghostly Touch",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "None",
                "qualities": [
                    "Fast"
                ]
            },
            {
                "name": "Improvised Thrown Weapon",
                "chance": 60,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BONDS OF DEATH",
                "description": "These creatures can manipulate physical objects in Æthereal Form."
            },
            {
                "name": "BRUSH WITH DEATH",
                "description": "When these creatures make a successful attack bare-handed, they provoke Fear, but do no Damage. They also force a foe to Resist with a Resolve Test or be aged by one year. For every year aged, the foe permanently reduces Brawn by 1%."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            },
            {
                "name": "WITCHBOARD",
                "description": "These creatures cannot be permanently Slain! unless their remains are placed beneath a spirit board. Once placed beneath a spirit board, they enter hibernation and will remain so unless the board or remains are removed, therefore giving life to the creature again as it is restored to Unharmed. Only by successfully casting Last Rites over the"
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "RED REVENANT",
        "family": "RESTLESS SPIRIT",
        "description": [
            "The threat of heretics, Mutants and witchcraft is a serious thing all over the countryside, but that doesn't mean there aren't innocent victims who die by an inquisitor's flame due to rumor or falsehood. Sometimes, those killed for such false reasons come back as a terrible Red Revenant.",
            "A Red Revenant is a spirit of one who was unfairly purged by an inquisitor or zealous township. Perhaps a girl undergoing her quickening is accused by her insane mother of cavorting with demons or a foreigner from a far-off land is accused of eating strange meats and speaking in tongues. When these poor souls are tied to the pyre and the flames have consumed their bodies, they occasionally do not find proper rest and come back to haunt those that killed them. Red Revenants look like smoldering corpses, but they only ever appear to those they are about to kill. Their hands bound forever with a rope, a holy symbol transfixed between their fingers, they seem to appear from doorways, windows or other portals. The smell of sulphur and tang of blood is ever-present when a Red Revenant makes their appearance.",
            "Red Revenants are vengeful spirits, but their wrath is almost always aimed at those that killed them. They also take pleasure in fomenting discord, causing as much terror as possible before handing out their punishment to their victims. Their claw swipes leave scorched flesh and their footprints leave the ground black and sooty. The only way for a Red Revenant to find rest is to have their ashes gathered into a consecrated relic ensorcelled by the Blessed Sacrament Ritual. Even then, they can find a way to be resurrected, for the families of Red Revenants oftentimes conspire to have their relatives continue exacting revenge upon the guilty. Unfortunately, these same families are often consumed by the Red Revenant's wrath as well, in the end."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 9
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 50,
                "bonus": 9
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 6,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 12,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Advanced",
        "skills": {
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "education": 30,
            "folklore": 30,
            "guile": 30,
            "interrogation": 30,
            "intimidate": 30,
            "scrutinize": 30,
            "simple_ranged": 30,
            "stealth": 30
        },
        "attack_profile": [
            {
                "name": "Spit Fire",
                "chance": 65,
                "distance": "ranged 9 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "ASHES TO ASHES",
                "description": "These creatures cannot be permanently Slain! unless their remains are placed into a decanter consecrated with the Blessed Sacrament Ritual. Within, they remain in hibernation, unless the remains are removed therefore giving life to the creature again. Only by placing the decanter into a font of holy water are they forever Slain!."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BONDS OF DEATH",
                "description": "These creatures can manipulate physical objects in Æthereal Form."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FEEL THE HEAT",
                "description": "When foes are Engaged with this creature, they must Resist with a successful Coordination Test or be exposed to Mildly Dangerous flames."
            },
            {
                "name": "FIREPROOF",
                "description": "These creatures and their possessions are entirely immune to Damage from fire."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SPIT FIRE",
                "description": "These creatures can use their breath as a ranged weapon. This allows then to strike a single foe within 3+[PB], as the foe suffers 2D10+2 Damage from fire. A foe can attempt to Dodge Spit Fire or Parry it with a shield. Spit Fire can be used while Engaged with foes."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Holy symbol"
        ]
    },
    {
        "type": "WAILING WOMAN",
        "family": "RESTLESS SPIRIT",
        "description": [
            "The myth of the mourning widow in the abandoned castle or the dark wraith who prowls the moors at night, is found among all cultures. It is both a tale of the tenacity of women, as well as a slight against their frailty and dependency. But these are not just tricks of the light or tales to deter children, but tragic spirits known as a Wailing Woman.",
            "Wailing Women are cursed maidens who were killed on their wedding night or worse violated and left for the clattering call of a corpse collector. They take on the appearance of a spectral washerwoman, bride, maiden or crone  or even more rarely a weasel or hooded crow. During the day, Wailing Women remain partially Æthereal, dwelling in the homes and hills they were killed in. They do not interact with others, only mimicking mundane tasks they performed while alive  such as drying linens or picking vegetables. If approached, they disappear into the hills just out of sight, not to be encountered during the light hours. At night, they take upon physical form and haunt those very hills and dales. They wander in anguish, letting out a terribly chilling wail that eats at a person's very soul. Their physical form is much like their Æthereal one, though it is far more monstrous and has a permanently distended jaw and face. While Wailing Women are intelligent and can be negotiated with, their despair is often too overwhelming for them to be reasoned with.",
            "Wailing Women are more like forces of nature than malevolent spirits  they will not go out of their way to harm anybody, but they will destroy anyone who treads too close to where they wallow in misery. They also tend to dwell around odd weather phenomenon  many dwell within mists near no moor or stand in the middle of storms when the sky is clear  whether this is an odd coincidence or an extension of their powers remains to be seen. Those they kill arise as Phantoms, expanding the abundance of Restless Spirits. Wailing Women will not, however, harm any woman that is in distress  they do not want the poor wretch to share their fate, so they will forgo attacking them in favor of a less pitiable target."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 45,
                "bonus": 4
            },
            "agility": {
                "base": 40,
                "bonus": 5
            },
            "perception": {
                "base": 40,
                "bonus": 7
            },
            "intelligence": {
                "base": 45,
                "bonus": 4
            },
            "willpower": {
                "base": 50,
                "bonus": 9
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 4,
        "peril_threshold": 12,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "awareness": 20,
            "charm": 10,
            "coordination": 20,
            "disguise": 20,
            "eavesdrop": 20,
            "folklore": 20,
            "guile": 10,
            "interrogation": 20,
            "intimidate": 20,
            "simple_melee": 20,
            "stealth": 20
        },
        "attack_profile": [
            {
                "name": "Dirk",
                "chance": 55,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Light",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "BONDS OF DEATH",
                "description": "These creatures can manipulate physical objects in Æthereal Form."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "I THEE WED",
                "description": "These creatures cannot be permanently Slain! unless their wedding ring is placed into a decanter consecrated with the Ritual of Blessed Sacrament. As long as it remains in the decanter, they also remain in hibernation, unless the ring is removed therefore giving life to the creature again. Only by then submerging the decanter into a large body of water are they forever Slain!."
            },
            {
                "name": "KEENING WAIL",
                "description": "These creatures can spend 2 APs to make an Interrogation Test. If successful, foes caught in a Cone Template who fail to Resist with a Resolve Test immediately suffers from Fear and cannot Counterspell, Dodge or Parry until their next Turn. A foe can be made victim to Keening Wail every Turn."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "PHYSICAL INSTABILITY",
                "description": "Every minute these creatures remain in the Material Realm in physical form, roll 3D6 Chaos Dice at the end of their Turn. If all three dice show face '6', the creature is banished from the Material Realm, until the new moon."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Dirk",
            "Expensive ring",
            "Tattered wedding veil and dress"
        ]
    },
    {
        "type": "BILIOUS SKRZZAK",
        "family": "SKRZZAK",
        "description": [
            "Though all Skrzzak will fight for their Broodmother and her interests, they will very rarely purposefully die for them. The Skrzzak's natural sense of self-preservation is too strong for them to simply fall upon their own poisoned knives; they can easily tell when they are outgunned and outnumbered and then it is time for them to run!",
            "The Broodmother needs a devoted force, one that is both extremely effective and fanatically devoted. Altering her black bile through injections of Wytchstone Essence, she gives birth to the terrible Bilious Skrzzak. Their journey does not end there, as the new pups are shipped to the most dangerous and vile places of the Great Warren, left alone to survive and adapt. They are conditioned through rigorous torture and administration of Wytchstone fumes by the Choleric doktors to obey their Broodmother above all else. Few survive this harrowing ordeal, but those that do are merciless, fanatically devoted and cruel beyond measure. They emerge a number of years later, permanently masked by strange breathing apparatuses and mutated into frighteningly fanatical monsters.",
            "Born in the Great Warren, molded by it; aside from Tyrants, the Bilious are the strongest of all Skrzzak breeds, broad of shoulder and often crisscrossed with battle scars. They are always preoccupied with cruelty and unfairness, often descending into laconic depression when not actively engaged in acts involving either. Though going toe-to-toe in a brawl against a Bilious is dangerous, their real advantage comes from their arsenal of weaponry. Outfitted by their Choleric brethren, their guns and bombs are powered by Wytchstone shards to devastating effect. These weapons deal casualty and injury, mowing through both enemy and Skrzzak alike; the Bilious do not care if they destroy friend or foe, only that they destroy. Mercy is not a quality known to the Bilious, as they take no prisoners and thoroughly eviscerate those that manage to escape their barrages. They also have little sense of self-preservation; their suicidal tendencies mean they are more than willing to wade into hordes that are far stronger than they are simply to prove their devotion."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 8
            },
            "perception": {
                "base": 40,
                "bonus": 6
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 6
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 9,
        "parry": null,
        "dodge": {
            "chance": 60,
            "qualities": []
        },
        "notch": "Intermediate",
        "risk_factor": "Medium",
        "skills": {
            "awareness": 20,
            "coordination": 10,
            "education": 10,
            "folklore": 10,
            "martial_ranged": 10,
            "navigation": 10,
            "resolve": 10,
            "simple_ranged": 20,
            "stealth": 10,
            "survival": 10,
            "toughness": 20,
            "tradecraft": 20
        },
        "attack_profile": [
            {
                "name": "Dirk",
                "chance": 45,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Vicious"
                ]
            },
            {
                "name": "Glass Grenade",
                "chance": 65,
                "distance": "ranged 7 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": [
                    "Ineffective",
                    "Throwing",
                    "Volatile"
                ]
            },
            {
                "name": "Wytchfyre Pistol",
                "chance": 65,
                "distance": "ranged 15 yards",
                "load": "3 AP",
                "damage": "8",
                "qualities": [
                    "Finesse",
                    "Gunpowder",
                    "Immolate",
                    "Vicious",
                    "Volatile"
                ]
            },
            {
                "name": "Wytchfyre Jezzail",
                "chance": 65,
                "distance": "ranged 17 yards",
                "load": "4 AP",
                "damage": "8",
                "qualities": [
                    "Finesse",
                    "Gunpowder",
                    "Immolate"
                ]
            },
            {
                "name": "Wytchfyre Thrower",
                "chance": 65,
                "distance": "ranged 8 yards",
                "load": "4 AP",
                "damage": "4",
                "qualities": [
                    "Gunpowder",
                    "Fiery",
                    "Repeating",
                    "Shrapnel",
                    "Volatile",
                    "Weak"
                ]
            },
            {
                "name": "Tail Vertebrae",
                "chance": 45,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Reach"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLAM! BLAM!",
                "description": "These creatures may spend 3 APs to attack twice with 2 ranged weapons, providing they possess the Gunpowder Quality and are Loaded."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "GRENADIER",
                "description": "When these creatures are encountered, they have a glass grenade. When thrown, it affects multiple foes in a Burst Template. Affected foes must succeed a Toughness Test or contract Tomb Rot."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SHARP-SIGHTED",
                "description": "These creatures do not suffer penalties for ranged weapons at Medium or Long Distances."
            },
            {
                "name": "WYTCH-SCIENCE",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1-4', they carry two Wytchfyre pistols. If the result is face '5', they carry a Wytchfyre jezzail. If the result is face '6', they carry a Wytchfyre thrower. While carrying a Wytchfyre thrower,"
            }
        ],
        "trappings": [
            "Brigandine armor",
            "Dirk",
            "Plague mask",
            "Glass grenade (1)",
            "Wytchfyre (6)",
            "Wytchfyre jezzail -or- Wytchfyre pistol (2) -or- Wytchfyre thrower"
        ]
    },
    {
        "type": "BROODMOTHER",
        "family": "SKRZZAK",
        "description": [
            "The revered prognosticators of the Skrzzak and practitioners of warped Magicks, Broodmothers are the backbone and origin of their society.",
            "Each Broodmother leads her monstrous progeny in one of the hundreds of Kabals that make up the Great Warren. Being the only females of the Skrzzak, they are waited on hand and foot in lightless chambers by their children, treated as the royalty they rightly are. Broodmothers are monstrous creatures, always heavy with child and constantly expelling new litters as she rules from an ossein throne wrought from a Kabal's enemies. Broodmothers always use the royal 'we', feeling that they do not need to do anything when their brood can easily handle it for them. The halls of the Broodmother are lined with supplicants, tribute and the fetid stench of sewage and rot amid darkness encompassing every stretch of her hallowed chambers. When not issuing commands to her children, the Broodmother is found in a Wytchstone-imbued stupor to bring them into communion with The Thirteen, the Skrzzak rat god. Aside from the surface world, the biggest obstacle to the Broodmothers' task are other females  they believe only one queen can rule absolute, yet are unwilling to murder her usurpers. Their royal personage thus resorts to deception, assassination and doublespeak to gain an edge over her 'beloved sisters'.",
            "Broodmothers always carry a distinctive mark  such as a notched ear or a limp tail  that is present in every member of their brood. Their Kabal often uses this trait as their name, such as the 'Cropped Ear' or 'Crooked Tail'. Broodmothers rarely fight, but when they do, their Wytchstone dream-states have provided them with precognition of the immediate future (thus making a nearly implacable foe). Though the Broodmother's relationship with her children is apparent, their association with vivimancers is unusual. Allowed to bear torchlight before the Broodmother, vivimancers are faithful Human servants of the Skrzzak, whose job it is to spread the interests of the Skrzzak into the 'surface courts'. Many vivimancers are influential people in one way or another, but their reasons for aiding the ratkin are mysterious. Perhaps it is a promise of power, wealth or eternal life, but they are not aware that they are just merely pawns of the Broodmothers. True, the Skrzzak may not dwell amongst us, but their agents do."
        ],
        "size": [
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 50,
                "bonus": 5
            },
            "agility": {
                "base": 50,
                "bonus": 5
            },
            "perception": {
                "base": 55,
                "bonus": 9
            },
            "intelligence": {
                "base": 50,
                "bonus": 7
            },
            "willpower": {
                "base": 50,
                "bonus": 9
            },
            "fellowship": {
                "base": 50,
                "bonus": 7
            }
        },
        "initiative": 12,
        "movement": [
            {
                "value": 8,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 12,
        "parry": null,
        "dodge": {
            "chance": 50,
            "qualities": []
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "awareness": 10,
            "bargain": 10,
            "eavesdrop": 20,
            "education": 10,
            "folklore": 20,
            "guile": 30,
            "incantation": 30,
            "interrogation": 20,
            "intimidate": 10,
            "leadership": 30,
            "navigation": 10,
            "resolve": 10,
            "scrutinize": 30,
            "survival": 10,
            "toughness": 10,
            "warfare": 30
        },
        "attack_profile": [
            {
                "name": "Tail Vertebrae",
                "chance": 50,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Reach"
                ]
            }
        ],
        "traits": [
            {
                "name": "AVERSION TO LIGHT",
                "description": "When these creatures are exposed to any sort of light (as from a torch), they suffer a penalty of -3 to their Damage Threshold."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "OCCULT MASTERY",
                "description": "When this creature Channels Power and succeeds at their Skill Test, they Critically Succeed instead."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "STOMP THE YARD",
                "description": "When these creatures deal Damage, they can automatically knock their foe Prone. In addition, they ignore the effects of Hard Terrain."
            },
            {
                "name": "THE VIRIDESCENT PATH",
                "description": "When this creature makes a Skill Test, they roll twice and assume the better of the two results. In addition, they never suffer from Critical Failures, instead treating them as regularly failed Skill Tests instead."
            },
            {
                "name": "UNGAINLY",
                "description": "When these creatures are Slain! all those Engaged with it must succeed at a Coordination Test or be knocked Prone beneath of it, suffering 3D10+3 Damage"
            }
        ],
        "trappings": [
            "Arcane tome with 9 Petty Magick spells",
            "Arcane tome with 6 Lesser Magick spells",
            "Arcane tome with 3 Greater Magick spells",
            "Kabal banner",
            "Gilded throne of bones",
            "Hide armor",
            "Reagents for all Magick spells (9)",
            "Tome of faith & disease",
            "Wytchstone Essence (9)"
        ]
    },
    {
        "type": "CHOLERIC SKRZZAK",
        "family": "SKRZZAK",
        "description": [
            "Broodmothers have foretold the birth of a special pup: one who will be a 'super ratling', a Skrzzak that can go to the regions of prescient knowledge where Broodmothers dare not enter.",
            "Through careful (yet imprecise) manipulation of Kabal bloodlines, the Broodmothers have attempted to create this superior being, whom they call the Skrzzak Cholerach. Wytchstone-infused elixirs are suffused within the belly of the Broodmother, increasing her yellow bile while filling her with exalting passions and religious babblings. The Choleric Skrzzak are her resultant children, groomed from birth to become priests of the Kabal. Although they have failed up to this point, litters born of her gravidity are extraordinarily different than their kindred. The only Skrzzak in the Great Warren that are closer to the Skrzzak god than Broodmothers are the Choleric. They sell themselves entirely over to the divine worship of The Thirteen, appeasing their god with flagellation and the spread of disease.",
            "Choleric Skrzzak appear frail and without meat on their bones, riddled with all manner of boils that wrack their body. However, they rejoice in the divine pain, as they feel blessed by the magnificent plague aspects of their god. They, too consume Wytchstone Essence to enter similar, dream-like fugue states as their matrons. Through these dreams, they are able to foretell the immediate future, much as Truthsayers can from the world above. They hope that visions of countless threads in cause and effect among the Great Warren will lead them to a true, optimum path to dominate the world above. Although they've been unable to see what the distant future holds, they are often the ones to raise the war gong among the Kabals. The Choleric Skrzzak rally armies with blasphemous words and banners of frightening symbols, playing monstrous brass bells and chimes that exude a noxious miasma: a clarion call to the Kabal. United, Choleric Skrzzak wish nothing more than to spread illness upon their enemies, to watch them writhe in divine pain and die at the hand of their Thirteen's chosen people."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 50,
                "bonus": 8
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 9,
        "parry": null,
        "dodge": {
            "chance": 80,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "coordination": 20,
            "education": 20,
            "folklore": 10,
            "martial_melee": 20,
            "navigation": 10,
            "scrutinize": 10,
            "simple_melee": 20,
            "stealth": 10,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Fetid Blade",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Vicious"
                ]
            },
            {
                "name": "Tail Vertebrae",
                "chance": 65,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Reach"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "HARBINGER OF PLAGUES",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1-5', they carry a massive gong. If the result is face '6', they carry a set of chimes. When they play the gong with Litany of Hatred, Characters so affected also suffer 1D10+1 physical Peril. When they play the chime with Litany of Hatred, Characters so affected lower their Damage and Peril Condition Tracks by -3 instead. These trappings can only be used by hands of these creatures who bear them, otherwise useless relics."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage, they can force a foe to Resist a Takedown."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Fetid blade",
            "Kabal banner",
            "Tome of faith & disease",
            "Wytchstone Essence (6)"
        ]
    },
    {
        "type": "PHLEGMATIC SKRZZAK",
        "family": "SKRZZAK",
        "description": [
            "The Broodmother oft needs counsel for her various machinations, but the intelligent vivimancers are few in number and spend much of their time in the world above, unable to serve their mistress's immediate needs. She needs a pup with a mind for strategy, scholarship and the very physical maintenance of the Broodmother's gravidity.",
            "The matron can alter her humors to excel her phlegm to monstrous levels, a predicator for the birth of Phlegmatic Skrzzak. This breed is nearly as intelligent as their Broodmother, but in differing ways; their intellect is much more calculating and objective, while the Broodmother's is often focused on her dream-state machinations. Phlegmatic mutations give them lightning fast deduction, calculating information at alarming rates to hypothesize possible outcomes. Not only are they a Kabal's scholars, but they keep the apocryphal histories and living histories. Phlegmatic Skrzzak are also the primary caregivers and advisers to Broodmothers, often found in their chambers discussing policies and plans while serving as wet nurse or doktor.",
            "Phlegmatic Skrzzak differ from other Skrzzak with their massively elongated heads as well as their almost supernaturally calm demeanors. They are also insatiably curious scientists and experimenters. Using the arcane energies of Wytchstone, they perform diabolical and grotesque experiments on beings of all sorts. Though they prefer to inflict these bloody and visceral mutations upon humanoids, they regularly draw from the prisons of the Great Warren, literally using Warren Runners as their lab rats. Successful results have come about from their transmutative tinkerings, most famously the enormous Tyrant Skrzzak. They treat them more like beloved pets than slaves, typically praising a Tyrant right after the beast has dismembered an intrusive band of adventurers in the Great Warren. Broodmothers keep their eye heavily focused on this breed, as a slip of their tongue could potentially cause the whole Kabal to collapse. Phlegmatic Skrzzak are too willful and confident to be ignored and they know this full well."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 35,
                "bonus": 3
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 40,
                "bonus": 6
            },
            "intelligence": {
                "base": 45,
                "bonus": 5
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 45,
                "bonus": 3
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 9,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": []
        },
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "coordination": 20,
            "education": 20,
            "handle_animal": 20,
            "heal": 20,
            "leadership": 10,
            "ride": 20,
            "simple_melee": 10,
            "simple_ranged": 10,
            "survival": 1
        },
        "attack_profile": [
            {
                "name": "Man-Catcher",
                "chance": 45,
                "distance": "melee engaged or 1 yard",
                "damage": "3",
                "qualities": [
                    "Entangling",
                    "Reach"
                ]
            },
            {
                "name": "Tail Vertebrae",
                "chance": 45,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Reach"
                ]
            },
            {
                "name": "Bolas",
                "chance": 45,
                "distance": "ranged 7 yards",
                "load": "1 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "LICK YOUR WOUNDS",
                "description": "These creatures may spend 1 Misfortune Point to move three steps up the Damage Condition Track positively."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Animalbane",
            "Beastbane",
            "Leather armor",
            "Man-Catcher",
            "Wytchstone Essence (6)"
        ]
    },
    {
        "type": "SANGUINE SKRZZAK",
        "family": "SKRZZAK",
        "description": [
            "Special breeds of Skrzzak are more feared than the teeming masses of the Kabals. Called the Sanguine, they are an independent lot, dwelling on the fringes of the Great Warren.",
            "Like other Broodmother pups, Sanguine are born from a chemically-induced exsanguination of blood in the Broodmother, making this breed very excitable, flighty and highly dangerous. They are lithe and quick, their limbs thin and fur a midnight black color; they leave no trace of their presence. Sanguine Skrzzak are bred and trained to become contract killers, caring little for personal possessions, save their weeping blade. This sacred knife is one of deep tradition and a hallmark of these warriors. They hold near-religious reverence over it, insomuch that they only draw it when ready to kill. To sheath a weeping blade without first drawing blood is considered sacrilegious, an affront to both their own honor and the worship of The Thirteen. Reputedly, this blade is made from their first incisor to fall out as a pup, which is then mutated by exposure to Wytchstone to elongate and grow ever-sharp.",
            "The Sanguine play a role of great importance in the Great Warren and the world above. As highly experienced assassins for hire, they are not only sent to eliminate troublesome infiltrators from the surface, but also to eliminate members of the rival other Kabals. They have countless tools at their disposal for exterminating their marks: throwing knives, phials of poison, climbing ropes and their weeping blade are just a few of their toys. Sanguine Skrzzak see their assassination missions as near divine guidance from The Thirteen and they ingest Wytchstone Essence before undertaking a mission. Sanguine Skrzzak believe it heightens their focus and vision, allowing The Thirteen to see their kills through their eyes."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 5
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 8
            },
            "perception": {
                "base": 45,
                "bonus": 6
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 5
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 8,
        "parry": null,
        "dodge": {
            "chance": 70,
            "qualities": []
        },
        "notch": "Medium",
        "risk_factor": "Intermediate",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "coordination": 20,
            "folklore": 10,
            "navigation": 20,
            "scrutinize": 20,
            "simple_melee": 20,
            "simple_ranged": 20,
            "stealth": 20,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Weeping Blade",
                "chance": 65,
                "distance": "melee engaged",
                "damage": "8",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Light",
                    "Vicious"
                ]
            },
            {
                "name": "Tail Vertebrae",
                "chance": 65,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Reach"
                ]
            },
            {
                "name": "Throwing Star",
                "chance": 65,
                "distance": "ranged 7 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Throwing"
                ]
            },
            {
                "name": "Smoke Bomb",
                "chance": 65,
                "distance": "ranged 7 yards",
                "load": "1 AP",
                "damage": "Special",
                "qualities": []
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "GUTTER RUNNER",
                "description": "When these creatures attempt to hide in urban and underground environments, they flip the results to succeed at Stealth Tests."
            },
            {
                "name": "SMOKE BOMB",
                "description": "When these creatures are encountered, they have a smoke bomb. When thrown, it explodes outwards. Those caught in an Explosion Template area of effect reduces their Movement by a -6 penalty, until they escape. In addition, it provides enough concealment for the creature to use their Stealth Skill."
            },
            {
                "name": "SNIKT! SNIKT!",
                "description": "These creatures may spend 3 APs to attack twice with melee weapons."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Balaclava mask",
            "Dirk",
            "Folkbane (3)",
            "Leather armor",
            "Smoke bomb (3)",
            "Throwing star (6)",
            "Scorpion Venom (3)",
            "Weeping blade",
            "Wytchstone Essence (3)"
        ]
    },
    {
        "type": "TYRANT SKRZZAK",
        "family": "SKRZZAK",
        "description": [
            "The trademark monstrosities of the Phlegmatic Skrzzak's vile experiments, Tyrants are a terrifying sight to behold.",
            "All Tyrants were once Warren Runners, often criminals who volunteered for experimentation in order to reduce their sentences. What these poor wretches did not expect was the sheer torture and grotesquery of these experiments. These whelps were subjected to ampules of liquefied Wytchstone Essence which fused Ogrish blood into their very essence. The Skrzzak then amputated and re-attached multiple limbs, before infecting these creatures with nearly every known disease. These treatments drive the Warren Runner to blinding insanity, making them a willing thrall. They then emerge weeks later from the Phlegmatic's fetid chambers, fully transmuted into a Tyrant Skrzzak.",
            "Tyrants (also called Ogre-kin) are massive and grotesque creatures, towering nearly twelve feet high and bristling with muscle. Their bodies are awful to behold, riddled with tumors, patches of mismatched skin, engorged thew, withered limbs and even having pieces of metal bolted into their very flesh. They are nearly always in blinding pain, so much that they only have two emotions: obedience and violence. Tyrants are terrors on the battlefield, throwing down foes like wretched rag dolls at the command of their Phlegmatic masters. When given reprieve from the cycle of violence, Tyrants are forced to undertake heavy labor, digging the endless tunnels of the Great Warren and other strength-intensive tasks. More importantly, they are the principal miners of Wytchstone; without them, Skrzzak society would collapse. Their exposure to the Ætheric mineral keeps them docile while they work, but as soon as they are separated from it, they re-enter a blinding rage of pain. The only thing that can relieve it is either by ingesting more Wytchstone 'treats' from their Phlegmatic masters or the promise of being drafted back to the mines."
        ],
        "size": [
            "Large"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 45,
                "bonus": 11
            },
            "agility": {
                "base": 40,
                "bonus": 8
            },
            "perception": {
                "base": 45,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 6
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 11,
                "type": "normal"
            }
        ],
        "damage_threshold": 14,
        "peril_threshold": 9,
        "parry": {
            "chance": 80,
            "qualities": []
        },
        "dodge": {
            "chance": 80,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "intimidate": 30,
            "simple_melee": 30,
            "stealth": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Tyrant spike",
                "chance": 80,
                "distance": "melee engaged or 1 yard",
                "damage": "11",
                "qualities": [
                    "Fast",
                    "Reach",
                    "Powerful",
                    "Pummeling",
                    "Vicious"
                ]
            },
            {
                "name": "Tail Vertebrae",
                "chance": 80,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Reach"
                ]
            }
        ],
        "traits": [
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DIM-WITTED",
                "description": "In combat, these creatures only use Attack Actions unless commanded otherwise by their leader with a successful Leadership Test."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage, they can force a foe to Resist a Takedown."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "SNIVELING WHELP",
                "description": "These creatures can only use 2 AP"
            }
        ],
        "trappings": [
            "None"
        ]
    },
    {
        "type": "WARREN RUNNER",
        "family": "SKRZZAK",
        "description": [
            "The Broodmother's birthings are not always exact; not every litter born is fit to be elevated from the horde into a position of prominence. These offshoot pups are collectively called Warren Runners and they are the most common of the Skrzzaks horde.",
            "Viewed as failed experiments in creating stronger breeds, these corrupted and sickly weaklings make up the largest portion of the Great Warren's number. They are mostly factionless, but one day hope to join a noble Kabal. Born into thralldom, no more than a handful of them will amount to anything else. The Kabals see them as nothing more than chattel, primarily using them as either slave labor or cannon fodder to be thrown into battle to soften up their enemies before the 'real attack' begins. Survivors who are still fit to fight as warriors are 'elected' to lead dangerous guerrilla raids into the world above, while the indolent are granted freedom, spending their days in a Wytchstone-induced haze, forgetting the pain of their position (or lack thereof ) in the Great Warren.",
            "Warren Runners vary wildly in size and shape, but most are either piebald or possess only a few tufts of fur, which marks them as undesirables. Though they may be weaker than any of the true members of the Kabal, Runners are still capable and fast on their feet, navigating both cramped passages and winding city streets with ease as they tend to their duties. However, Warren Runners are notoriously lackadaisical, committing to tasks only when demanded at the threat of excision (the unceremonious act of having their tail removed). It is for this reason alone that Warren Runners sit at the bottom of the pecking order in the Great Warren."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 4
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 45,
                "bonus": 5
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 40,
                "bonus": 5
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 5,
        "peril_threshold": 8,
        "parry": null,
        "dodge": {
            "chance": 60,
            "qualities": []
        },
        "notch": "High",
        "risk_factor": "Basic",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "coordination": 10,
            "folklore": 10,
            "navigation": 10,
            "scrutinize": 10,
            "simple_melee": 10,
            "simple_ranged": 10,
            "stealth": 10,
            "survival": 10
        },
        "attack_profile": [
            {
                "name": "Fire-hardened spear",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "5",
                "qualities": [
                    "Adaptable",
                    "Reach",
                    "Weak"
                ]
            },
            {
                "name": "Tail Vertebrae",
                "chance": 55,
                "distance": "melee engaged or 1 yard",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Reach"
                ]
            },
            {
                "name": "Shepherd's Sling",
                "chance": 55,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Throwing",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk"
            }
        ],
        "trappings": [
            "Folkbane",
            "Hide armor",
            "Shepherd's sling",
            "Stiletto",
            "Wytchstone Essence (1)"
        ]
    },
    {
        "type": "DEFILER",
        "family": "SUPERNATURAL",
        "description": [
            "Though anyone who knows the proper rituals can raise undead thralls, the most horrendous tales of undead hordes from the histories lay far to the barren south in a place where kings were worshipped as gods. It was home to a verdant civilization that was plagued by Defilers, terrible sorcerers of unmitigated power. When it was discovered that these dark magi were searching for the secret of immortality  a taboo in their cultures which still continues to this day  the peoples of these lands rose up as one and drove the Defilers out. After being banished, the Defilers delved deeper into the secrets of necromancy and eventually discovered their goal  eternal life. What they did not expect was the gnawing emptiness and void of immortality being the true cost of its power. Enraged at their own hubris, they journeyed back to the once-verdant cities and conjured fell Magick; it swept across the land, killing the residents and re-raising them as Mindless Undead, as well as turning the once verdant jungles into barren dunes. Now the Defilers rule in debauchery, cackling as their former enemies are forced to fight, die and be reborn for all eternity.",
            "The process to become a Defiler requires a terrible and bloody ritual fueled by the darkest of Magicks. Using a ritually-treated bronze knife, the Defiler eviscerates themselves, removing their stomach, lungs, liver and intestines, placing the organs into four large sandstone jars; otherwise called soul jars. This leaves only their still beating heart, the presumed location of the soul and a massive entry into their chest cavity. They then pack the cavity full of salts and ritual spices to extract all their humors, finally wrapping themselves in linen to preserve the flesh. The ritual complete, their withered heart glows with an unearthly black light from the Well of Souls, granting the creature the immortality they craved. Even with Magick serving to keep the Defiler able to do the ritual, few survive to become immortal. Those that do are inexorably insane, as the magnitude of pain causes their mental fortitude to snap. Not only that, but to become immortal is to become inhuman; Defilers lose all light and connection to the Gods when they enchant their ragged hearts. What remains is a black existence, an immortality of immense power but intense spiritual nihilism.",
            "Defilers are insanely powerful Necromancers, bending life and unlife to their every whim. With a wave of their emaciated hand they can re-raise an entire cemetery, send a forest to withering, befouled water and spread disease with their breath. They form massive dynasties around themselves, delusional royalty of the Mindless Undead who only seek to destroy any that have turned against them. Defilers are also notoriously petty, sending their dynastic troops against other Defilers mostly out of sheer boredom and ennui. This and the small number of Defilers mitigates their threat, but it is said the whole of the once-verdant kingdoms fell in a day under the force of these crypt lords. The Defiler's main weakness is their four soul jars. They are always guarded heavily or under the constant eye of the Defiler; should one be separated from the others or destroyed, the Defiler's power wanes immensely. Destroying all of them is the only way to kill a Defiler, their soul so blackened and foul that it is destroyed by the Custodian instead of allowing entrance into the Well of Souls. Some Defilers are so paranoid about their soul jars that they will hold them in their gaping chest cavity so that they are never separated."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 6
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 50,
                "bonus": 7
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 50,
                "bonus": 5
            },
            "willpower": {
                "base": 55,
                "bonus": 11
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 10,
                "type": "normal"
            }
        ],
        "damage_threshold": 12,
        "peril_threshold": 13,
        "parry": {
            "chance": 70,
            "qualities": []
        },
        "dodge": {
            "chance": 70,
            "qualities": []
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "athletics": 10,
            "awareness": 20,
            "coordination": 20,
            "education": 30,
            "folklore": 20,
            "guile": 30,
            "incantation": 30,
            "intimidate": 30,
            "ride": 20,
            "rumor": 20,
            "scrutinize": 30,
            "simple_melee": 20,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Khopesh",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "7",
                "qualities": [
                    "Slow",
                    "Vicious",
                    "Weak"
                ]
            },
            {
                "name": "Crook",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Light",
                    "Powerful",
                    "Weak"
                ]
            },
            {
                "name": "Flail",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Adaptable",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ÆTHERIC DOMINATION",
                "description": "When these creatures would potentially invoke a Chaos Manifestation, they must have two or more face '6' on Chaos Dice to invoke it."
            },
            {
                "name": "AWAKENING",
                "description": "Anytime these creatures attempt to raise and control Supernatural creatures using Magick and Rituals, they Critically Succeed at their Skill Test. In addition, they automatically understand how to use the Ritual of Awaken the Dead, don't need reagents to cast it and treat the casting time as one minute (3 AP), instead of one hour."
            },
            {
                "name": "BLOODLESS",
                "description": "These creatures cannot Bleed."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "ENTRANCING",
                "description": "When this creature uses a Transfixed Gaze, they can use other Actions while they maintain its Mesmerizing effect. In addition, foes suffer 1 Corruption and 2D10+2 mental Peril on each of their Turns while they remain Mesmerized."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, these creatures suffer 2D10+2 Damage from fire."
            },
            {
                "name": "LORD OF THE DAMNED",
                "description": "These creatures can control a number of Mindless Undead equal to three times their [WB]. They need not remain within sight of these creatures to be controlled, as they share a telepathic bond. Finally, they can see through the eyes of all Mindless Dead they control."
            },
            {
                "name": "OCCULT MASTERY",
                "description": "When this creature Channels Power and succeeds at their Skill Test, they Critically Succeed instead."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SOUL JAR",
                "description": "These creatures cannot be permanently Slain! unless the fetish or phylactery which houses their soul is destroyed. However, they can be made to remain in hibernation, left unconscious if a jar of salt is poured into their mouths."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "TRANSFIXED GAZE",
                "description": "When these creatures can see a foe (who can also see them), they force a foe to Resist with a Resolve Test or be Mesmerized. Mesmerized foes cannot use any Actions In Combat, but may attempt to Resist again at the beginning of their Turn to escape. The creature cannot use any other Actions while they maintain a Transfixed Gaze. However, they can release the gaze at any time. If the creature suffers Damage while a foe is Mesmerized, they immediately relinquish the hold. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "UNCERTAIN FORM",
                "description": "These creatures are immune to Injuries and Perilous Stunts."
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Crook",
            "Expensive necklace (3)",
            "Expensive ring (6)",
            "Flail",
            "Golden death mask",
            "Khopesh",
            "Pharaoh's armor (as munitions plate armor with the Castle-forged Quality)",
            "Phylactery (4)",
            "Tattered rags",
            "Reagents for all Magick spells (9)"
        ]
    },
    {
        "type": "GOLEM",
        "family": "SUPERNATURAL",
        "description": [
            "When the dead are buried, stakes are driven through the lid of a coffin to kill any zombified being that may arise. However, what happens if the corpse is exhumed and then vivisected upon for nefarious purposes? The result is the anthropogenic Golem.",
            "Golems are brooding husks, shaped out of mud and dessicated remains. Despite being made of corpses, Golems themselves are not truly 'undead' creatures  rather, their bodies are infused with the mysterious Ætheric Fluid drawn from Wytchstone shards to provide mobility. The parts that make up a Golem are often mismatched and stitched together, taken from sometimes up to a dozen of the strongest bodies. The sum of these parts then bristle with inhuman strength and agility, yet retain a base level of intelligence due to an intact brain required to pilot the body. Golems are also remarkably well-preserved, often dunked in alchemical baths in between their forays into the world. Golems are often created by those of a scientific bent who wish to plunge into the mysteries of Galenic medicine, humorism and necrosis. Given a good dose of stage makeup and a burly coat, a Golem could even walk amongst mankind  though as an admittedly larger-than-average looking being.",
            "Golems are naturally quite confused and prone to fits of anger  their limbs sometimes rebel against them, given to the muscle memory of a body that no longer exists. Golems are also quite lonely and prone to melancholy, given their status as 'monsters', brought back into life with no say in the matter. They long to reconnect to a being they cannot return to, as their existence is divided between a handful of previous lives. Golems harbor a jealousy towards other living beings, and desperately want a connection to something greater than themselves. All they wish to do is reach out to another and become 'human' once more, but their anguish more often than not drives them towards madness and violence."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 50,
                "bonus": 9
            },
            "agility": {
                "base": 40,
                "bonus": 6
            },
            "perception": {
                "base": 40,
                "bonus": 7
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 45,
                "bonus": 5
            },
            "fellowship": {
                "base": 35,
                "bonus": 3
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 9,
                "type": "normal"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 8,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 80,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "athletics": 30,
            "coordination": 30,
            "disguise": 10,
            "guile": 20,
            "intimidate": 30,
            "resolve": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Iron-hard Fists",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Vicious"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "BOTH-HANDEDNESS",
                "description": "When they wield 2 one-handed melee weapons and fail a Combat-based Skill Test, they may re-roll to generate a better result, but must accept the outcome."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "LUMBERING BRUTE",
                "description": "These creatures cannot Charge, Run or use Movement Actions that require 3 AP."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "SHOOTFIGHTING",
                "description": "These creatures ignore the Pummeling and Weak Qualities when fighting bare-handed, with a blackjack or using knuckledusters. In addition, they can refer to [BB] or [CB] when inflicting Damage with"
            }
        ],
        "trappings": [
            "Memento of a former life"
        ]
    },
    {
        "type": "LIVING STATUE",
        "family": "SUPERNATURAL",
        "description": [
            "When Defiler-kings overthrew the southern kingdoms and raised hordes of the undead, they found that the newly deceased had little to offer in terms of muscle power. As a solution, they developed a process that would create a Living Statue  one that would breed monstrous results.",
            "Living Statues are towering relics of an older age, reaching over thirty feet in height  more than a match for any Nephilim. They are composed of natural materials such as basalt, bronze or obsidian, but sandstone is by far the most common medium. Constructed with the likeness of ancient and terrible gods, they are imposing and powerful to behold, crowned with anthropomorphic heads. The Defiler  or any other Necromancer  animates the Living Statue by collecting nine souls in canopic jars. The souls are then transferred into the form of the Living Statue, where it is animated with Ætheric energy to serve indefinitely. The nine souls do not lose their personalities, so these towering golems are at constant odds with themselves, a hive of disparate minds trying to control a singular body. Many become insane, but some become placid enough to be successfully controlled. If their controller is able to cement one personality, usually a powerful spirit like an Ætheric Spirit or a Fey is one of the nine inhabitants  they can generally cow the other eight to obedience.",
            "Living Statues know only toil  they build endless ziggurats, monuments and obelisks for their master, ones both beautiful in craftsmanship and terrible in purpose. When the war drums are beaten, the Living Statue picks up its pike and shield and becomes a terrible engine of war who towers over the battlefield and destroys siege engines with a simple kick. Living Statues are also used to defend tombs and temples from intruders, standing still for millennia and only activating to destroy intruders. They are monsters with no feelings, only instincts to kill and build. Thankfully, as the sands have progressed and many Defiler kingdoms have been overthrown, many Living Statues have been left inert amidst the ever-changing dunes. They wait for their old masters to bring them back to life, lying dormant and impotent, guarding a tomb swallowed by the sands centuries ago."
        ],
        "size": [
            "Normal, Large",
            "Huge"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 7
            },
            "brawn": {
                "base": 45,
                "bonus": 13
            },
            "agility": {
                "base": 40,
                "bonus": 10
            },
            "perception": {
                "base": 35,
                "bonus": 6
            },
            "intelligence": {
                "base": 0,
                "bonus": 0
            },
            "willpower": {
                "base": 0,
                "bonus": 0
            },
            "fellowship": {
                "base": 0,
                "bonus": 0
            }
        },
        "initiative": 9,
        "movement": [
            {
                "value": 16,
                "type": "normal"
            }
        ],
        "damage_threshold": 18,
        "peril_threshold": null,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 80,
            "qualities": [
                "Defensive",
                "Protective"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 30,
            "awareness": 30,
            "coordination": 30,
            "eavesdrop": 30,
            "intimidate": 30,
            "martial_melee": 30,
            "martial_ranged": 30,
            "scrutinize": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Bronze Shotel",
                "chance": 70,
                "distance": "melee engaged or 1 yard",
                "damage": "13",
                "qualities": [
                    "Powerful",
                    "Reach",
                    "Vicious"
                ]
            },
            {
                "name": "Bronze Bow",
                "chance": 70,
                "distance": "ranged 18 yards",
                "load": "2 AP",
                "damage": "10",
                "qualities": [
                    "Fast",
                    "Finesse"
                ]
            }
        ],
        "traits": [
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "FETID WEAPONRY",
                "description": "When these creatures inflict an Injury, their foe's wounds are also Infected."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "HIDEOUS MIGHT",
                "description": "These creatures add the Reach and Vicious Qualities to melee weapons they wield. They also ignore the Weak Quality of melee weapons."
            },
            {
                "name": "LAMB TO THE SLAUGHTER",
                "description": "When these creatures Injure a foe with a melee weapon, they inflict two Injuries instead of one."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "MINDLESS",
                "description": "These creatures do not possess Fellowship, Intelligence or Willpower and cannot be made to Resist effects which affect the mind. They can also see in the dark."
            },
            {
                "name": "NATURAL ARMOR (5)",
                "description": "These creatures have factored in a bonus to their Damage Threshold, equal to the value in parentheses."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SOUL JAR",
                "description": "These creatures cannot be permanently Slain! unless the fetish or phylactery which houses their soul is destroyed. However, they can be made to remain in hibernation, left unconscious if a jar of salt is poured into their mouths."
            },
            {
                "name": "SWEEPING STRIKE",
                "description": "When these creatures make a successful attack with a two-handed melee weapon, they strike up to three foes they're Engaged with."
            },
            {
                "name": "UNNATURAL VISCERA",
                "description": "These creatures are immune to attacks made with ranged weapons. However, they cannot"
            }
        ],
        "trappings": [
            "Arrows (9)",
            "Bronze bow",
            "Bronze death mask",
            "Bronze shotel",
            "Cheap necklace",
            "Cheap ring (3)",
            "Metal shield",
            "Phylactery (9)",
            "Tattered robes"
        ]
    },
    {
        "type": "LAMASHTU",
        "family": "VAMPIRE",
        "description": [
            "In these dark times, even children need to be made aware that they should not place trust in anyone, not even their own family; the reason being they never know who could be a dreaded Lamashtu. The Lamashtu is cowardly, taking flight when it senses danger, but otherwise these Vampires are terrible monsters and mimics, sating their hunger with the blood of babes. Able to polymorph its own shape, they assume the form of an elderly woman and pass into villages unmolested and unnoticed, seeking children to feed upon. They can even mimic voices, but since Lamashtu are more creature than man, it is little more than parroting vocal noises. The Lamashtu, now fully disguised and holding the child's trust, escorts the babe out of the village to a remote killing ground. Once there, the Lamashtu reveals its true form: that of a bristling giantess, with a marsupial-like face, gangling limbs and eyes the size of tea saucers. The Lamashtu is most vulnerable in this form, so they only take it on to feed or when cowed.",
            "The beast lashes its undulating, leech-like tongue down the child's throat and beyond its stomach, where it entwines within their entrails. Over the next nine grueling hours, the tongue sucks out all the fat within a child, taking another innocent life. Once finished, the Lamashtu will strangely return the babe to its crib, allowing it to be interred for reasons unknown. A Lamashtu will feed multiple times from a village, taking no more than nine victims and only one at a time. Afterwards, the Lamashtu will journey to another village, only to repeat this ritual of sheer terror."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 4
            },
            "brawn": {
                "base": 40,
                "bonus": 7
            },
            "agility": {
                "base": 45,
                "bonus": 10
            },
            "perception": {
                "base": 50,
                "bonus": 7
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 35,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 10,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            },
            {
                "value": 19,
                "type": "fly"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 8,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 65,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Low",
        "skills": {
            "awareness": 30,
            "charm": 10,
            "coordination": 10,
            "eavesdrop": 20,
            "guile": 30,
            "intimidate": 20,
            "scrutinize": 30,
            "simple_melee": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Ragged Talons",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "7",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Weak"
                ]
            },
            {
                "name": "Stiletto",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "4",
                "qualities": [
                    "Fast",
                    "Vicious",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "ACCURSED",
                "description": "These creatures cannot be harmed by normal weapons, unless they have been imbued with Magick."
            },
            {
                "name": "ÆTHEREAL FORM",
                "description": "Creatures in Æthereal Form cannot inflict Damage or manipulate physical objects, but can pass through objects effortlessly and hover 1 yard off the ground. They can manifest into physical form instantaneously, but assuming Æthereal Form once more costs 2 APs."
            },
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "BROKEN WINGS",
                "description": "Once this creature is Grievously Wounded, it can no longer fly."
            },
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SHAPESHIFTER",
                "description": "These creatures may take any form they please, providing it is of a Small or Large Animal or Humanoid (including player Ancestries)."
            },
            {
                "name": "STRAFING TALONS",
                "description": "When these creatures execute a successful attack while flying, they also deal 1D10+[AB]"
            }
        ],
        "trappings": [
            "Stiletto"
        ]
    },
    {
        "type": "NOSFERATU",
        "family": "VAMPIRE",
        "description": [
            "Every damned being of the night has an origin and most Vampiric lines can eventually be traced back to the venerable and monstrous Nosferatu.",
            "Nosferatu were once ancient Elves, part of a dark Kindred who had attempted to curry the favor of the Abyssal Princes, but was all for naught. The Princes see little use in Elves, not even fit to be reincarnated as Lower Demons. All that effort, power and pain turns about to be worthless to the Elves, so in retaliation they became demonologists in order to bind their former lords. Worrisome of their growing power, the Abyssal Princes placed a terrible hex on the Kindred  one that forever cursed them to stalk the earth, hurt to sunlight and a never-ending hunger for blood. They became known as the Nosferatu, being hauntingly beautiful and elegant  their skin snow white and their hair raven black. They masquerade as princes and rulers, with Ghouls dressed up to serve as their council and then hold court with finely dressed aristocrats and socially ambitious burghers.. They dress as rulers because they are megalomaniacs, enforcing their will on a local populace, fashioning themselves a lord or count worthy of fealty. This is all an elaborate faade though, for they invite guests not for wine and fine dining, but for being eating  ripping out their vocal cords to mute their dying screams.",
            "Nosferatu are a thankful rarity, but those that exist are extremely powerful and nigh-unkillable. Many are masters of the blackest of sorceries and possess further supernatural abilities such as shapeshifting and a terrifying mien. They are usually tended by a Butler Demon, often their only friend as they despise all other Vampires. This is quite ironic, as Nosferatu are also the only Vampires who can create other Vampires  though different bloodlines can create other creatures, the Nosferatu's deadly kiss can cause any person to 'turn' into any other type of Vampire apart from a Nosferatu. That's because they are a dying breed and it is hypothesized that if all of them should die, Vampires will be wiped from the Material Realm. However, killing one of these powerful monstrosities is often easier said than done  and many failed Slayers have ended up served as supper for the Nosferatu."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 5
            },
            "brawn": {
                "base": 50,
                "bonus": 7
            },
            "agility": {
                "base": 50,
                "bonus": 10
            },
            "perception": {
                "base": 50,
                "bonus": 5
            },
            "intelligence": {
                "base": 50,
                "bonus": 8
            },
            "willpower": {
                "base": 55,
                "bonus": 7
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 8,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            }
        ],
        "damage_threshold": 7,
        "peril_threshold": 10,
        "parry": {
            "chance": 70,
            "qualities": []
        },
        "dodge": {
            "chance": 80,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Unique",
        "risk_factor": "Elite",
        "skills": {
            "athletics": 10,
            "awareness": 10,
            "charm": 20,
            "coordination": 20,
            "education": 30,
            "guile": 30,
            "incantation": 30,
            "intimidate": 30,
            "resolve": 20,
            "ride": 10,
            "rumor": 20,
            "scrutinize": 30,
            "simple_melee": 20,
            "toughness": 10
        },
        "attack_profile": [
            {
                "name": "Sceptre",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "5",
                "qualities": [
                    "Light",
                    "Powerful",
                    "Weak"
                ]
            },
            {
                "name": "Vampiric Claws",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "7",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Weak"
                ]
            },
            {
                "name": "Wall Crawler Grasp",
                "chance": 50,
                "distance": "ranged 8 yards",
                "load": "1 AP",
                "damage": "None",
                "qualities": [
                    "Entangling",
                    "Ineffective",
                    "Throwing"
                ]
            }
        ],
        "traits": [
            {
                "name": "ÆTHERIC DOMINATION",
                "description": "When these creatures would potentially invoke a Chaos Manifestation, they must have two or more face '6' on Chaos Dice to invoke it."
            },
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "INESCAPABLE",
                "description": "When these creatures use a Chokehold, they are able to maintain it for 0 AP and use other Actions In Combat."
            },
            {
                "name": "OCCULT MASTERY",
                "description": "When this creature Channels Power and succeed at their Skill Test, they Critically Succeed instead."
            },
            {
                "name": "POSSESSION",
                "description": "These creatures can use Skin Guest from the Arcana of Sorcery without having to make an Incantation Test. In addition, they do not have to maintain Concentration."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SHAPESHIFTER",
                "description": "These creatures may take any form they please, providing it is of a Small or Large Animal or Humanoid (including player Ancestries)."
            },
            {
                "name": "VAMPIRE'S CURSE",
                "description": "These creatures cannot cross running water, unless by bridge or boat. When exposed to sunlight, they suffer 2D10+2 Damage from fire for every minute they remain exposed. Instead of suffering Injuries in this case, they instead are caught On Fire. When Slain!, they turn into mist and immediately return to their resting place to sleep. For every hour they sleep, they move one step up the Damage Condition Track positively. Once Unharmed, all Injuries are healed as well. However, while asleep, they can be permanently Slain! if an ironwood stake is driven through their heart and the head is removed."
            },
            {
                "name": "WALL CRAWLER",
                "description": "These creatures can crawl upon both vertical and horizontal surfaces with ease. In addition, they can initiate a ranged Chokehold at a Distance of 3+[PB],"
            }
        ],
        "trappings": [
            "Arcane tome with 6 Petty Magick spells",
            "Arcane tome with 3 Lesser Magick spells",
            "Arcane tome with 1 Greater Magick spell",
            "Fancy wig",
            "Fashionable clothing",
            "Fine perfume",
            "Reagents for all Magick spells (9)",
            "Sceptre"
        ]
    },
    {
        "type": "RAKSHASA",
        "family": "VAMPIRE",
        "description": [
            "Not all Vampires relish in their power and use it for deceptive reasons  some have become utterly feral and chaotic, slavering with an endless hunger for blood and flesh. These despicable creatures are known as Rakshasa and they are one of the most feared of the Vampiric brood.",
            "Rakshasa were once men who had a tendency towards illgotten gains and political corruption, though not always succumbing to the sway of the Abyss. For their terrible hubris, they are reborn in their new anthropomorphic body as a form of punishment. Rakshasa are natural shapeshifters, able to blend into any crowd or high-society event, stalking potential targets for their hunger. They dress in fine regalia and often attend masquerades and banquets with little molestation. Their true form is far more monstrous  they are sinewy and part rotted, their muscle visible striated under the skin. Their head is that of a terrible lion or monkey with ragged jaws and their long claws bend backwards into reversed palms. They feed delicately, as their claws are perfect for tearing apart every little morsel of meat from the bone. Ghoul attendants tend to follow in their wake, serving at their beck and call, awaiting the end of the Rakshasa's meal and a chance to pick over their table scraps.",
            "Rakshasa sup delicately, as they are naturally slow and methodical. They like to stretch out their meals, as they believe terror makes the blood richer in piquancy. They only need to feed every few decades or so, and do not marry or sire offspring. Their only true companions are Ghouls; former victims fed the flesh of their loved ones by the Rakshasa, turned to the allure of darkness. Though Rakshasa seem powerful, they do harbor several weaknesses. They cannot enter a home unless invited into it, though they can enter common houses easily and they curse anything holy. It is still notoriously difficult to kill one: they possess a True Name, one that must be uttered upon their death to prevent them from rising again at the next new moon. There are rumors that blessed crossbow bolts can kill one, but the fate of numerous Slayers has put such rumblings to rest. Their real weakness is brass, as it burns their skin upon touch. Ironically, the Rakshasa's mortal weapon of choice is a brass dagger, though it is sheathed until absolutely necessary."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 45,
                "bonus": 6
            },
            "brawn": {
                "base": 40,
                "bonus": 6
            },
            "agility": {
                "base": 45,
                "bonus": 7
            },
            "perception": {
                "base": 50,
                "bonus": 8
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 35,
                "bonus": 5
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 11,
        "movement": [
            {
                "value": 13,
                "type": "normal"
            },
            {
                "value": 16,
                "type": "fly"
            }
        ],
        "damage_threshold": 6,
        "peril_threshold": 8,
        "parry": {
            "chance": 75,
            "qualities": []
        },
        "dodge": {
            "chance": 85,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "awareness": 30,
            "charm": 20,
            "coordination": 30,
            "eavesdrop": 30,
            "guile": 30,
            "interrogation": 20,
            "intimidate": 30,
            "scrutinize": 30,
            "simple_melee": 30,
            "survival": 30,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Teeth & Claws",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Slow",
                    "Vicious",
                    "Weak"
                ]
            },
            {
                "name": "Brass Dagger",
                "chance": 75,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Fast",
                    "Vicious",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "DISEASE-RIDDEN (Tomb Rot)",
                "description": "When these creatures reduce a foe to Seriously Wounded, the foe's wounds are Infected. When they reduce a foe to Grievously Wounded, the foe contracts the Disease indicated in parentheses."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "PAW/HOOF/WING",
                "description": "These creatures' movement uses 6+[AB] on foot and 9+[AB] for flight. Fliers are indicated under Movement."
            },
            {
                "name": "POISONOUS BITE",
                "description": "When these creatures deal Damage, roll 1D6 Chaos Die. If the result is face '6', they inject Spider Venom into their foe."
            },
            {
                "name": "POISON RESISTANCE",
                "description": "These creatures are immune to poisons."
            },
            {
                "name": "REANIMATOR",
                "description": "When this creature's Turn begins, they move one step up the Damage Condition Track positively. Other Traits cover how these creatures are permanently Slain!."
            },
            {
                "name": "SCAR THE FLESH",
                "description": "These creatures add +3 to their Damage Threshold, but wear no armor."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SHAPESHIFTER",
                "description": "These creatures may take any form they please, providing it is of a Small or Large Animal or Humanoid (including player Ancestries)."
            },
            {
                "name": "TRUE NAME",
                "description": "Unless foes invoke this creature's True Name before casting Magick, it fails to affect the creature."
            },
            {
                "name": "WEAKNESS TO BRASS",
                "description": "These creatures cannot be permanently Slain! unless a killing blow is rendered with a"
            }
        ],
        "trappings": [
            "Brass dagger"
        ]
    },
    {
        "type": "SUCCUBUS",
        "family": "VAMPIRE",
        "description": [
            "Vampires are lustful creatures, full of primordial mating urges and hypnotic body movements. This is not true for every breed of Vampire, but is more than adequate in describing the Succubus.",
            "Succubus (or the male incubus) are drawn to victims in dreams  particularly those who are lustful, wanton and lecherous. Their victims will have recurring dreams of a comely man or maiden, one that tempts them and calls their name. Nocturnal emissions are often the result of such dreams. However, in actuality, the damned spirit of the Succubi is feeding on these visions to construct the perfect persona for their target. A Succubus will always latch onto a certain person, taking on the aspects of what that mortal sees as an ideal mate. Eventually, they will emerge from dreams into reality, taking upon a form pleasing to the victim. They then spend months draining them of riches, causing mental abuse while seemingly making up for it with spectacular bouts of intercourse. However, once their victim has outlived their worth, they shift into their true form  a horrifyingly mirror image of the victim, with grotesquely-throbbing pudenda and hemorrhaging teats.",
            "The Succubi will then drain the remaining life-force from their victim, leaving them a withered husk after their hunger has been sated. They will then move on to another victim, haunting dreams once more. If their victims ever do live long enough to give birth, the resulting spawn are known as cambion; beings that eventually devolve into demonic Imps after they grow into maturity. Succubus may be deceiving and tempting, but rely most upon their wiles rather than physical prowess. That is not to say they are not dangerous, just simply less lethal in a fight than any other of the other accursed Vampiric brood."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 40,
                "bonus": 4
            },
            "brawn": {
                "base": 45,
                "bonus": 9
            },
            "agility": {
                "base": 40,
                "bonus": 9
            },
            "perception": {
                "base": 35,
                "bonus": 3
            },
            "intelligence": {
                "base": 40,
                "bonus": 4
            },
            "willpower": {
                "base": 55,
                "bonus": 9
            },
            "fellowship": {
                "base": 50,
                "bonus": 5
            }
        },
        "initiative": 6,
        "movement": [
            {
                "value": 12,
                "type": "normal"
            }
        ],
        "damage_threshold": 9,
        "peril_threshold": 12,
        "parry": null,
        "dodge": {
            "chance": 60,
            "qualities": [
                "Natural"
            ]
        },
        "notch": "Advanced",
        "risk_factor": "Medium",
        "skills": {
            "awareness": 10,
            "charm": 30,
            "coordination": 20,
            "eavesdrop": 20,
            "education": 30,
            "folklore": 30,
            "guile": 30,
            "interrogation": 10,
            "rumor": 30,
            "scrutinize": 30,
            "simple_melee": 20,
            "stealth": 20
        },
        "attack_profile": [
            {
                "name": "Stunted Claws",
                "chance": 60,
                "distance": "melee engaged",
                "damage": "9",
                "qualities": [
                    "Slow",
                    "Vicious",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "AMBUSH TACTICS",
                "description": "These creatures roll 2D10 to determine Initiative."
            },
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "BRUTE STRENGTH",
                "description": "These creatures refer to [BB] for Damage with melee weapons and ones with the Throwing Quality. They also have factored in +3 to their [BB]. Finally, they can inflict Injuries with Pummeling weapons."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "ENTRANCING",
                "description": "When this creature uses a Transfixed Gaze, they can use other Actions while they maintain its Mesmerizing effect. In addition, foes suffer 1 Corruption and 2D10+2 mental Peril on each of their Turns while they remain Mesmerized."
            },
            {
                "name": "FAST ON THEIR FEET",
                "description": "These creatures reduce all Movement Actions by 1 AP (to a minimum of 1 AP). They can also Dodge both melee and ranged weapons."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "RIPPING TEETH",
                "description": "When these creatures deal Damage, they can force a foe to Resist a Takedown."
            },
            {
                "name": "TRANSFIXED GAZE",
                "description": "When these creatures can see a foe (who can also see them), they force a foe to Resist with a Resolve Test or be Mesmerized. Mesmerized foes cannot use any Actions In Combat, but may attempt to Resist again at the beginning of their Turn to escape. The creature cannot use any other Actions while they maintain a Transfixed Gaze. However, they can release the gaze at any time. If the creature suffers Damage while a foe is Mesmerized, they immediately relinquish the hold. Foes who attempt to Resist must flip the results to fail their Skill Test."
            },
            {
                "name": "SHAPESHIFTER",
                "description": "These creatures may take any form they please, providing it is of a Small or Large Animal or Humanoid (including player Ancestries)."
            },
            {
                "name": "UNLATCH DOORS",
                "description": "These creatures can automatically unfasten any lock they can see or touch, Magickal or otherwise."
            },
            {
                "name": "VAMPIRE'S CURSE",
                "description": "These creatures cannot cross running water, unless by bridge or boat. When exposed to sunlight, they suffer 2D10+2 Damage from fire for every minute they remain exposed. Instead of suffering Injuries in this case, they instead are caught On Fire. When Slain!, they turn into mist and immediately return to their resting place to sleep. For every hour they sleep, they move one step up the Damage Condition Track positively. Once Unharmed, all Injuries are healed as well. However, while asleep, they can be permanently Slain! if an ironwood stake is driven through their heart and the head is removed."
            },
            {
                "name": "WANTON HUNGER",
                "description": "When these creatures are encountered, roll 1D6 Chaos Die. If the result is face '1- 5', their hunger has been sated. If the result is face '6', their hunger has not yet been sated and they are in a state of frenzy. When sated, they add +1 to both Damage and Peril Condition Tracks. When in a state of frenzy, they add an"
            }
        ],
        "trappings": [
            "Fashionable clothing"
        ]
    },
    {
        "type": "VAMPIRIC TEMPLAR",
        "family": "VAMPIRE",
        "description": [
            "Most Vampires are individualists, disregarding others of their kind and surviving alone in their cursed existence. One Vampiric bloodline, however, usurps this expectation and those are the Vampiric Templars.",
            "The Templars were once the most feared group of Grail Knights  reputedly the greatest in the land  until they were all struck down in a single night by the powerful Nosferatu they were hunting. They arose from a bloody haze, victims of the Vampires they so hated, instilled with fury. A curse now ran through their veins, altering their minds. Though they still despised all Vampire kind, they became obsessed with becoming the perfect warriors. Wandering the land to master the martial ways, their contempt for humanity drove them deeper into the exsanguinous ways of the Vampire. Driven by hunger, at first, they drank only from victims they spitted on their swords. However, as their dark gifts continued to evolve, they became less Human and more... divine. Their hunger became greater. And as they drank, so grew their martial prowess. Thus, their descent was complete, becoming the very thing they so hated.",
            "Vampiric Templars appear just as they did in their mortal life, often as strong and well-disciplined soldiers. However, beneath the clatter of armor they are monstrously tall, wellmuscled with dark blood coursing through varicose veins with jackal-like countenances. They slurp up the blood they spill from their defeated goes with long serpentine tongues and often maim their victims to sate their hunger. Those who live to tell the tale eventually arise as Tenebrae, angry spectral beings that are just as violent as their sire. There is a terrible pecking order among their kind as well. Vampiric Templars who exhibit the most monstrous of physical attributes are worshipped, as bitter rivalries can erupt between those who evolve faster than others. Surprisingly, despite their nature, Vampiric Templars can be reasoned with. Their word is generally considered unbreakable, as long as one takes proper precautions and distances themselves from the Templar once the deal is done."
        ],
        "size": [
            "Normal"
        ],
        "attributes": {
            "combat": {
                "base": 50,
                "bonus": 7
            },
            "brawn": {
                "base": 45,
                "bonus": 6
            },
            "agility": {
                "base": 45,
                "bonus": 11
            },
            "perception": {
                "base": 40,
                "bonus": 4
            },
            "intelligence": {
                "base": 35,
                "bonus": 3
            },
            "willpower": {
                "base": 40,
                "bonus": 7
            },
            "fellowship": {
                "base": 40,
                "bonus": 4
            }
        },
        "initiative": 7,
        "movement": [
            {
                "value": 14,
                "type": "normal"
            }
        ],
        "damage_threshold": 12,
        "peril_threshold": 10,
        "parry": {
            "chance": 90,
            "qualities": [
                "Defensive"
            ]
        },
        "dodge": null,
        "notch": "Advanced",
        "risk_factor": "High",
        "skills": {
            "athletics": 20,
            "awareness": 20,
            "coordination": 20,
            "education": 20,
            "guile": 30,
            "interrogation": 20,
            "intimidate": 30,
            "martial_melee": 30,
            "ride": 30,
            "rumor": 20,
            "scrutinize": 10,
            "simple_melee": 20,
            "toughness": 30
        },
        "attack_profile": [
            {
                "name": "Heartseeker Sword",
                "chance": 80,
                "distance": "melee engaged",
                "damage": "11",
                "qualities": [
                    "Fast",
                    "Finesse",
                    "Vicious"
                ]
            },
            {
                "name": "Vampiric Claws",
                "chance": 70,
                "distance": "melee engaged",
                "damage": "6",
                "qualities": [
                    "Pummeling",
                    "Slow",
                    "Weak"
                ]
            }
        ],
        "traits": [
            {
                "name": "BLOODLUST",
                "description": "Every time these creatures inflict an Injury, they move one step up the Damage Condition Track positively."
            },
            {
                "name": "CRIPPLING CONSTRICTOR",
                "description": "When these creatures maintain a Chokehold, they deal Damage as if they were using a bare-handed weapon."
            },
            {
                "name": "DARK SENSE",
                "description": "These creatures can see in the dark."
            },
            {
                "name": "HANGIN' TOUGH",
                "description": "These creatures cannot Bleed or suffer Injuries."
            },
            {
                "name": "HARD-NOSED",
                "description": "These creatures are immune to Knockout! and Stunning Blow."
            },
            {
                "name": "HOLY RETRIBUTION",
                "description": "When struck by holy water, they suffer 2D10+2 Damage from fire."
            },
            {
                "name": "MASTERFULLY ADROIT",
                "description": "These creatures refer to their [AB] for all Damage they inflict with weapons. They also have factored in +3 to their [AB]."
            },
            {
                "name": "MENACING",
                "description": "When these creatures use a Litany of Hatred, they inflict 1D10+[BB] mental Peril."
            },
            {
                "name": "SNIKT! SNIKT!",
                "description": "These creatures may spend 3 APs to attack twice with melee weapons."
            },
            {
                "name": "SANITY-BLASTING",
                "description": "When first encountered, these"
            },
            {
                "name": "creatures provoke one of the three brands of Madness",
                "description": "if of Basic Risk Factor, they provoke Stress; if of Intermediate Risk Factor, they provoke Fear; if of Advanced or Elite Risk Factor, they provoke Terror."
            },
            {
                "name": "SHAPESHIFTER",
                "description": "These creatures may take any form they please, providing it is of a Small or Large Animal or Humanoid (including player Ancestries)."
            },
            {
                "name": "STEELY FORTITUDE",
                "description": "These creatures always succeed at Resolve Tests and cannot be Intimidated."
            },
            {
                "name": "VAMPIRE'S CURSE",
                "description": "These creatures cannot cross running water, unless by bridge or boat. When exposed to sunlight, they suffer 2D10+2 Damage from fire for every minute they remain exposed. Instead of suffering Injuries in this case, they instead are caught On Fire. When Slain!, they turn into mist and immediately return to their resting place to sleep. For every hour they sleep, they move one step up the Damage Condition Track positively. Once Unharmed, all Injuries are healed as well. However, while asleep, they can be permanently Slain! if an ironwood stake is driven through"
            }
        ],
        "trappings": [
            "Full plate armor",
            "Heartseeker sword",
            "Metal shield",
            "Wilderness cloak",
            "Witchhunter's coat & mantle",
            "Witchhunter's hat"
        ]
    }
]