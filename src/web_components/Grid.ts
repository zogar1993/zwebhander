import styled from "styled-components"
import {SEPARATION} from "web_components/constants"

const Grid = styled.div<Props>`
  display: grid;
  gap: ${SEPARATION};
  grid-template-columns: repeat(${({columns}) => columns || "auto-fit"}, 1fr);
  ${({width}) => width ? `width: ${width}` : ''}; 
  flex-grow: 1;

  ${({area}) => area ? `grid-area: ${area}` : ""};

  @media (max-width: 768px) {
    grid-template-columns: repeat(${props => props["mobile-columns"] || props.columns || "auto-fit"}, 1fr);
  }
`


export default Grid

interface Props {
	columns?: number
	"mobile-columns"?: number
	area?: string
	width?: string
}
