import styled from "styled-components"

const Section = styled.section<Props>`
    width: 100%;
    height: 100%;
    user-select:${props => props.selectable ? "none" : "auto"};
    cursor:${props => props.selectable ? "pointer" : "default"};
    max-width: ${props => props["max-width"] || "auto"};
`
export default Section

interface Props {
    selectable?: boolean
    "max-width"?: string;
}