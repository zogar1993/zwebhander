import styled from "styled-components"

const StrippedTableData = styled.td`
    padding-left: 8px;
    text-align: right;
    &:last-child {
        padding-right: 8px;
    }
`
export default StrippedTableData
