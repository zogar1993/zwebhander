import React, {ReactNode} from "react"
import styled from "styled-components"

const StrippedTableContent = (props: Props) => {
    return (
        <Table>
            <TableBody>
                {props.children}
            </TableBody>
        </Table>
    )
}
export default StrippedTableContent

type Props = {
    children: ReactNode
}

const Table = styled.table``
const TableBody = styled.tbody``

