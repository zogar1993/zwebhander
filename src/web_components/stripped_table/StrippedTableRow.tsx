import React, {ReactNode} from "react"
import styled from "styled-components"

const StrippedTableRow = (props: Props) => {
    return (
        <Row>
            <Header>
                {props.header}
            </Header>
            {props.children}
        </Row>

    )
}
export default StrippedTableRow

type Props = {
    header: string
    children: ReactNode
}

const Row = styled.tr``
const Header = styled.th``
