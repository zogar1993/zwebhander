import React, {ReactNode} from "react"
import StrippedTableContent from "web_components/stripped_table/StrippedTableContent"
import {Box} from "misevi"

const StrippedTable = ({children, width}: Props) => {
	return (
		<Box width={width}>
			<StrippedTableContent>
				{children}
			</StrippedTableContent>
		</Box>
	)
}
export default StrippedTable

type Props = {
	children: ReactNode
	width?: string
}

