export const SEPARATION = "4px"
export const INPUT_HEIGHT = "45px"
export const BORDER_RADIUS = "6px"

export const PAGE_CONTENT_QUARTER_WIDTH = "255px"
export const PAGE_CONTENT_WIDTH = `calc((${PAGE_CONTENT_QUARTER_WIDTH} * 4) + (${SEPARATION} * 3))`