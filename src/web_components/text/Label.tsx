import styled from "styled-components"

const Label = styled.label<Props>`
    height: ${props => props.height};
    width: ${props => props.width};
    font-family: ${({font}) => font ? `${font}, ` : ""}Times, serif;
    font-weight: ${props => props.bold ? "bold" : "none"};
    font-style: ${props => props.italic ? "italic" : "none"};
    font-size: ${props => size(props)};   
    color: black; 
`
export default Label

interface Props {
    font?: string
    fontSize?: string
    width?: string
    height?: string
    italic?: boolean
    bold?: boolean
    small?: boolean
}

function size(props: Props) {
    if (props.small) return "12px"
    return undefined
}
