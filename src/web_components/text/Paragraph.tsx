import styled from "styled-components"

const Paragraph = styled.p<Props>`
    font-weight: ${props => props.bold ? "bold" : "none"};
    font-style: ${props => props.italic ? "italic" : "none"};
    font-size: ${props => props.small ? "small" : "medium"};
    color: black;
`
export default Paragraph

interface Props {
    font?: string
    handwritten?: boolean
    small?: boolean
    italic?: boolean
    bold?: boolean
}
