import React from "react"
import Paragraph from "web_components/text/Paragraph"

const Paragraphs = (props: Props) => {
	return <> {props.values.map(value => <Paragraph {...props}>{value}</Paragraph>)} </>
}
export default Paragraphs

interface Props {
	values: Array<string>
	handwritten?: boolean
	fontSize?: string
	italic?: boolean
}
