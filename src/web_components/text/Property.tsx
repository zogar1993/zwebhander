import React, {ReactNode} from "react"
import Paragraph from "web_components/text/Paragraph"
import styled from "styled-components"

export default function Property({name, font, small, children}: Props) {
    return (
        <Paragraph small={small} font={font}>
            <PropertyName small={small} font={font}>{name}: </PropertyName>
            {children}
        </Paragraph>
    )
}

type Props = {
    name: string
    font?: string
    children: ReactNode
    small?: boolean
}


const PropertyName = styled.span<SpanProps>`
    font-family: ${({font}) => font ? `${font}, ` : ""}Times, serif;
    font-weight: bold; 
    font-size: ${props => size(props)};  
    color: black; 
`

interface SpanProps {
    font?: string
    small?: boolean
}

function size(props: SpanProps) {
    if (props.small) return "12px"
    return undefined
}