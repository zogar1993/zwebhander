import React from "react"
import ReactDOM from "react-dom"
import "web_components/reset.css"
import App from "zweb/web/app/App"

ReactDOM.render(
    <App/>,
    document.getElementById("root")
)